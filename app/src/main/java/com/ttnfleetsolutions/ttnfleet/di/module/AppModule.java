/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.ttnfleetsolutions.ttnfleet.di.module;

import android.app.Application;
import android.arch.persistence.room.Room;
import android.content.Context;

import com.ttnfleetsolutions.ttnfleet.data.AppDataManager;
import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.data.local.db.AppDatabase;
import com.ttnfleetsolutions.ttnfleet.data.local.db.AppDbHelper;
import com.ttnfleetsolutions.ttnfleet.data.local.db.DbHelper;
import com.ttnfleetsolutions.ttnfleet.data.local.prefs.AppPreferencesHelper;
import com.ttnfleetsolutions.ttnfleet.data.local.prefs.PreferencesHelper;
import com.ttnfleetsolutions.ttnfleet.data.remote.ApiHeader;
import com.ttnfleetsolutions.ttnfleet.data.remote.ApiHelper;
import com.ttnfleetsolutions.ttnfleet.data.remote.AppApiHelper;
import com.ttnfleetsolutions.ttnfleet.di.DatabaseInfo;
import com.ttnfleetsolutions.ttnfleet.di.PreferenceInfo;
import com.ttnfleetsolutions.ttnfleet.utils.AppConstants;
import com.ttnfleetsolutions.ttnfleet.utils.rx.AppSchedulerProvider;
import com.ttnfleetsolutions.ttnfleet.utils.rx.SchedulerProvider;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Sandip Chaulagain on 10/3/2017.
 *
 * @version 1.0
 */

@Module

public class AppModule {

    @Provides
    @Singleton
    Context provideContext(Application application) {
        return application;
    }

    @Provides
    SchedulerProvider provideSchedulerProvider() {
        return new AppSchedulerProvider();
    }

    @Provides
    @PreferenceInfo
    String providePreferenceName() {
        return AppConstants.PREF_NAME;
    }

    @Provides
    @DatabaseInfo
    String provideDatabaseName() {
        return AppConstants.DB_NAME;
    }

    @Provides
    @Singleton
    PreferencesHelper providePreferencesHelper(AppPreferencesHelper appPreferencesHelper) {
        return appPreferencesHelper;
    }

    @Provides
    @Singleton
    DataManager provideDataManager(AppDataManager appDataManager) {
        return appDataManager;
    }

    @Provides
    @Singleton
    AppDatabase provideAppDatabase(@DatabaseInfo String dbName, Context context) {
        return Room.databaseBuilder(context, AppDatabase.class, dbName)
                .build();
    }

   /* static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
        }
    };*/

    @Provides
    @Singleton
    DbHelper provideDbHelper(AppDbHelper appDbHelper) {
        return appDbHelper;
    }

    @Provides
    @Singleton
    ApiHelper provideApiHelper(AppApiHelper appApiHelper) {
        return appApiHelper;
    }

    @Provides
    @Singleton
    ApiHeader provideProtectedApiHeader(PreferencesHelper preferencesHelper) {
        return new ApiHeader(preferencesHelper.getAccessToken());
    }

}
