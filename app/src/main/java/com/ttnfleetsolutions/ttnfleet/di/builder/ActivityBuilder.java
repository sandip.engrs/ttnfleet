/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.ttnfleetsolutions.ttnfleet.di.builder;

import com.ttnfleetsolutions.ttnfleet.ui.fleet.DashboardActivity;
import com.ttnfleetsolutions.ttnfleet.ui.fleet.DashboardActivityModule;
import com.ttnfleetsolutions.ttnfleet.ui.fleet.order.OrderDetailsActivityModule;
import com.ttnfleetsolutions.ttnfleet.ui.fleet.order.OrderDetailsActivityNew;
import com.ttnfleetsolutions.ttnfleet.ui.fleet.order.chat.ChatFragmentProvider;
import com.ttnfleetsolutions.ttnfleet.ui.fleet.order.repairs.RepairsFragmentProvider;
import com.ttnfleetsolutions.ttnfleet.ui.fleet.order.status.StatusFragmentProvider;
import com.ttnfleetsolutions.ttnfleet.ui.fleet.order.technician.TechnicianFragmentProvider;
import com.ttnfleetsolutions.ttnfleet.ui.fleet.order.vehicle.VehicleFragmentProvider;
import com.ttnfleetsolutions.ttnfleet.ui.fleet.provider.ServiceProviderFragmentProvider;
import com.ttnfleetsolutions.ttnfleet.ui.fleet.services.NewServiceFragmentProvider;
import com.ttnfleetsolutions.ttnfleet.ui.fleet.services.note.AddNoteFragmentProvider;
import com.ttnfleetsolutions.ttnfleet.ui.login.LoginActivity;
import com.ttnfleetsolutions.ttnfleet.ui.login.LoginActivityModule;
import com.ttnfleetsolutions.ttnfleet.ui.serviceprovider.ServiceProviderDashboardActivity;
import com.ttnfleetsolutions.ttnfleet.ui.serviceprovider.ServiceProviderDashboardActivityModule;
import com.ttnfleetsolutions.ttnfleet.ui.serviceprovider.jobroster.JobRosterFragmentProvider;
import com.ttnfleetsolutions.ttnfleet.ui.serviceprovider.technician.AllocateTechnicianFragmentProvider;
import com.ttnfleetsolutions.ttnfleet.ui.splash.SplashActivity;
import com.ttnfleetsolutions.ttnfleet.ui.splash.SplashActivityModule;
import com.ttnfleetsolutions.ttnfleet.ui.technician.TechnicianDashboardActivity;
import com.ttnfleetsolutions.ttnfleet.ui.technician.TechnicianDashboardActivityModule;
import com.ttnfleetsolutions.ttnfleet.ui.technician.additional.CreateAdditionalJobFragmentProvider;
import com.ttnfleetsolutions.ttnfleet.ui.technician.inspection.JobInspectionFragmentProvider;
import com.ttnfleetsolutions.ttnfleet.ui.technician.requirements.RequirementsFragmentProvider;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.MainActivityNew;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.MainTruckerActivityModule;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.dashboard.OngoingServicesFragmentProvider;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.history.ServiceHistoryFragmentProvider;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.MainServicesFragmentProvider;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.details.CreateEventFragmentProvider;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.vmrs.system.SelectServiceFragmentProvider;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.notification.NotificationsFragmentProvider;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.servicerequest.ServiceDetailsFragmentProvider;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.vehicle.details.VehicleDetailsFragmentProvider;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Sandip Chaulagain on 10/3/2017.
 *
 * @version 1.0
 */

@Module
public abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = SplashActivityModule.class)
    abstract SplashActivity bindSplashActivity();

    @ContributesAndroidInjector(modules = LoginActivityModule.class)
    abstract LoginActivity bindLoginActivity();

    @ContributesAndroidInjector(modules = {OrderDetailsActivityModule.class,
            RepairsFragmentProvider.class, StatusFragmentProvider.class,
            TechnicianFragmentProvider.class, VehicleFragmentProvider.class,
            ChatFragmentProvider.class})
    abstract OrderDetailsActivityNew bindOrderDetailsActivityNew();

    @ContributesAndroidInjector(modules = {MainTruckerActivityModule.class, OngoingServicesFragmentProvider.class, MainServicesFragmentProvider.class,
            com.ttnfleetsolutions.ttnfleet.ui.trucker.vehicle.VehicleFragmentProvider.class, VehicleDetailsFragmentProvider.class, ServiceDetailsFragmentProvider.class,
            ServiceHistoryFragmentProvider.class, NotificationsFragmentProvider.class, SelectServiceFragmentProvider.class, CreateEventFragmentProvider.class})
    abstract MainActivityNew bindMainActivityNew();

    @ContributesAndroidInjector(modules = {DashboardActivityModule.class, MainServicesFragmentProvider.class,
            SelectServiceFragmentProvider.class, CreateEventFragmentProvider.class, NewServiceFragmentProvider.class,
            ServiceProviderFragmentProvider.class, ServiceDetailsFragmentProvider.class, NotificationsFragmentProvider.class,
            ServiceHistoryFragmentProvider.class, AddNoteFragmentProvider.class})
    abstract DashboardActivity bindDashboardActivity();

    @ContributesAndroidInjector(modules = {ServiceProviderDashboardActivityModule.class, AllocateTechnicianFragmentProvider.class, JobRosterFragmentProvider.class,
            ServiceHistoryFragmentProvider.class, ServiceDetailsFragmentProvider.class})
    abstract ServiceProviderDashboardActivity bindServiceProviderDashboardActivity();

    @ContributesAndroidInjector(modules = {TechnicianDashboardActivityModule.class, JobRosterFragmentProvider.class, ServiceHistoryFragmentProvider.class
            , ServiceDetailsFragmentProvider.class, SelectServiceFragmentProvider.class, JobInspectionFragmentProvider.class, RequirementsFragmentProvider.class
            , CreateAdditionalJobFragmentProvider.class})
    abstract TechnicianDashboardActivity bindTechnicianDashboardActivity();
}
