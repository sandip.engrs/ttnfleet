package com.ttnfleetsolutions.ttnfleet.data.model.api.event.create;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 6/1/2018.
 *
 * @version 1.0
 */
public class AssemblyCode {


    @SerializedName("code2")
    @Expose
    private String code2;
    @SerializedName("componentcode")
    @Expose
    private String componentCode;

    private String assemblyCodeName;

    private List<String> componentCodeName;

    public AssemblyCode(String code2, String componentCode, String assemblyCodeName, List<String> componentCodeName) {
        this.code2 = code2;
        this.componentCode = componentCode;
        this.assemblyCodeName = assemblyCodeName;
        this.componentCodeName = componentCodeName;
    }

    public String getCode2() {
        return code2;
    }

    public void setCode2(String code2) {
        this.code2 = code2;
    }

    public String getComponentCode() {
        return componentCode;
    }

    public void setComponentCode(String componentCode) {
        this.componentCode = componentCode;
    }

    public String getAssemblyCodeName() {
        return assemblyCodeName;
    }

    public void setAssemblyCodeName(String assemblyCodeName) {
        this.assemblyCodeName = assemblyCodeName;
    }

    public List<String> getComponentCodeName() {
        return componentCodeName;
    }

    public void setComponentCodeName(List<String> componentCodeName) {
        this.componentCodeName = componentCodeName;
    }
}
