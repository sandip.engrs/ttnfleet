package com.ttnfleetsolutions.ttnfleet.data.model.api.theme;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandip Chaulagain on 5/23/2018.
 *
 * @version 1.0
 */
public class Theme {

    @SerializedName("themeid")
    @Expose
    private Integer themeId;
    @SerializedName("companyid")
    @Expose
    private Integer companyId;
    @SerializedName("logo")
    @Expose
    private String logo;
    @SerializedName("backgroundimage")
    @Expose
    private String backgroundImage;
    @SerializedName("statusbarcolor")
    @Expose
    private String statusBarColor;
    @SerializedName("titlebarcolor")
    @Expose
    private String titleBarColor;
    @SerializedName("buttoncolor")
    @Expose
    private String buttonColor;
    @SerializedName("textcolor")
    @Expose
    private String textColor;
    @SerializedName("footertextcolor")
    @Expose
    private String footerTextColor;
    @SerializedName("callforlabel")
    @Expose
    private String callForLabel;
    @SerializedName("iconcolor")
    @Expose
    private String iconColor;
    @SerializedName("footertextdisplay")
    @Expose
    private Integer footerTextDisplay;
    @SerializedName("backgroundcolor")
    @Expose
    private String backgroundColor;

    public Integer getThemeId() {
        return themeId;
    }

    public void setThemeId(Integer themeId) {
        this.themeId = themeId;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getBackgroundImage() {
        return backgroundImage;
    }

    public void setBackgroundImage(String backgroundImage) {
        this.backgroundImage = backgroundImage;
    }

    public String getStatusBarColor() {
        return statusBarColor;
    }

    public void setStatusBarColor(String statusBarColor) {
        this.statusBarColor = statusBarColor;
    }

    public String getTitleBarColor() {
        return titleBarColor;
    }

    public void setTitleBarColor(String titleBarColor) {
        this.titleBarColor = titleBarColor;
    }

    public String getButtonColor() {
        return buttonColor;
    }

    public void setButtonColor(String buttonColor) {
        this.buttonColor = buttonColor;
    }

    public String getTextColor() {
        return textColor;
    }

    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }

    public String getFooterTextColor() {
        return footerTextColor;
    }

    public void setFooterTextColor(String footerTextColor) {
        this.footerTextColor = footerTextColor;
    }

    public String getCallForLabel() {
        return callForLabel;
    }

    public void setCallForLabel(String callForLabel) {
        this.callForLabel = callForLabel;
    }

    public String getIconColor() {
        return iconColor;
    }

    public void setIconColor(String iconColor) {
        this.iconColor = iconColor;
    }

    public Integer getFooterTextDisplay() {
        return footerTextDisplay;
    }

    public void setFooterTextDisplay(Integer footerTextDisplay) {
        this.footerTextDisplay = footerTextDisplay;
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    @Override
    public String toString() {
        return "Theme{" +
                "themeId=" + themeId +
                ", companyId=" + companyId +
                ", logo='" + logo + '\'' +
                ", backgroundImage='" + backgroundImage + '\'' +
                ", statusBarColor='" + statusBarColor + '\'' +
                ", titleBarColor='" + titleBarColor + '\'' +
                ", buttonColor='" + buttonColor + '\'' +
                ", textColor='" + textColor + '\'' +
                ", footerTextColor='" + footerTextColor + '\'' +
                ", callForLabel='" + callForLabel + '\'' +
                ", iconColor='" + iconColor + '\'' +
                ", footerTextDisplay=" + footerTextDisplay +
                ", backgroundColor='" + backgroundColor + '\'' +
                '}';
    }
}
