package com.ttnfleetsolutions.ttnfleet.data.model.api.technician;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ttnfleetsolutions.ttnfleet.data.model.api.location.Location;

/**
 * Created by Sandip Chaulagain on 5/28/2018.
 *
 * @version 1.0
 */
public class Technician {

    @SerializedName("technicianid")
    @Expose
    private int technicianid;
    @SerializedName("serviceproviderid")
    @Expose
    private int serviceproviderid;
    @SerializedName("locationid")
    @Expose
    private int locationid;
    @SerializedName("availabilitystatusid")
    @Expose
    private Integer availabilitystatusid;
    @SerializedName("userid")
    @Expose
    private int userid;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("User")
    @Expose
    private Profile user;
    @SerializedName("Location")
    @Expose
    private Location location;
    @SerializedName("Availability")
    @Expose
    private Availability availability;

    public int getTechnicianid() {
        return technicianid;
    }

    public void setTechnicianid(int technicianid) {
        this.technicianid = technicianid;
    }

    public int getServiceproviderid() {
        return serviceproviderid;
    }

    public void setServiceproviderid(int serviceproviderid) {
        this.serviceproviderid = serviceproviderid;
    }

    public int getLocationid() {
        return locationid;
    }

    public void setLocationid(int locationid) {
        this.locationid = locationid;
    }

    public Integer getAvailabilitystatusid() {
        return availabilitystatusid;
    }

    public void setAvailabilitystatusid(Integer availabilitystatusid) {
        this.availabilitystatusid = availabilitystatusid;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Profile getUser() {
        return user;
    }

    public void setUser(Profile user) {
        this.user = user;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Availability getAvailability() {
        return availability;
    }

    public void setAvailability(Availability availability) {
        this.availability = availability;
    }
}
