package com.ttnfleetsolutions.ttnfleet.data.model.api.event.status;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandip Chaulagain on 6/8/2018.
 *
 * @version 1.0
 */
public class AssignAcceptWorkOrderRequest {


    @SerializedName("WorkOredrId")
    @Expose
    private Integer workOredrId;
    @SerializedName("IsAccepted")
    @Expose
    private Boolean isAccepted;
    @SerializedName("TechnicianId")
    @Expose
    private Integer technicianId;

    public AssignAcceptWorkOrderRequest(Integer workOredrId, Boolean isAccepted, Integer technicianId) {
        this.workOredrId = workOredrId;
        this.isAccepted = isAccepted;
        this.technicianId = technicianId;
    }

    public Integer getWorkOredrId() {
        return workOredrId;
    }

    public void setWorkOredrId(Integer workOredrId) {
        this.workOredrId = workOredrId;
    }

    public Boolean getIsAccepted() {
        return isAccepted;
    }

    public void setIsAccepted(Boolean isAccepted) {
        this.isAccepted = isAccepted;
    }

    public Integer getTechnicianId() {
        return technicianId;
    }

    public void setTechnicianId(Integer technicianId) {
        this.technicianId = technicianId;
    }
}
