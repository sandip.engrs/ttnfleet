package com.ttnfleetsolutions.ttnfleet.data.model.api.event.requirements;

import android.net.Uri;

/**
 * Created by Sandip Chaulagain on 6/22/2018.
 *
 * @version 1.0
 */
public class ImageRequirement {

    private int index;

    private String label;

    private Uri filePath;

    public ImageRequirement(int index, String label, Uri filePath) {
        this.index = index;
        this.label = label;
        this.filePath = filePath;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Uri getFilePath() {
        return filePath;
    }

    public void setFilePath(Uri filePath) {
        this.filePath = filePath;
    }
}
