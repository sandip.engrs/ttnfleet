package com.ttnfleetsolutions.ttnfleet.data.model.api.event.status;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandip Chaulagain on 6/11/2018.
 *
 * @version 1.0
 */
public class AcceptRejectJobRequest {

    @SerializedName("JobId")
    @Expose
    private Integer jobId;
    @SerializedName("JobStatusId")
    @Expose
    private Integer jobStatusId;
    @SerializedName("Isrejected")
    @Expose
    private Boolean isrejected;
    @SerializedName("UserId")
    @Expose
    private Integer userId;
    @SerializedName("EventId")
    @Expose
    private Integer eventId;
    @SerializedName("RejectionDescription")
    @Expose
    private String rejectionDescription;
    @SerializedName("RejectionTypeId")
    @Expose
    private Integer rejectionTypeId;

    public AcceptRejectJobRequest(Integer jobId, Integer jobStatusId, Boolean isrejected, Integer userId, Integer eventId, String rejectionDescription, Integer rejectionTypeId) {
        this.jobId = jobId;
        this.jobStatusId = jobStatusId;
        this.isrejected = isrejected;
        this.userId = userId;
        this.eventId = eventId;
        this.rejectionDescription = rejectionDescription;
        this.rejectionTypeId = rejectionTypeId;
    }

    public Integer getJobId() {
        return jobId;
    }

    public void setJobId(Integer jobId) {
        this.jobId = jobId;
    }

    public Integer getJobStatusId() {
        return jobStatusId;
    }

    public void setJobStatusId(Integer jobStatusId) {
        this.jobStatusId = jobStatusId;
    }

    public Boolean getIsrejected() {
        return isrejected;
    }

    public void setIsrejected(Boolean isrejected) {
        this.isrejected = isrejected;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    public String getRejectionDescription() {
        return rejectionDescription;
    }

    public void setRejectionDescription(String rejectionDescription) {
        this.rejectionDescription = rejectionDescription;
    }

    public Integer getRejectionTypeId() {
        return rejectionTypeId;
    }

    public void setRejectionTypeId(Integer rejectionTypeId) {
        this.rejectionTypeId = rejectionTypeId;
    }
}
