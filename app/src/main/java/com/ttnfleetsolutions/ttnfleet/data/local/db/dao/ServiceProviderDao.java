package com.ttnfleetsolutions.ttnfleet.data.local.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.ServiceProvider;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 4/4/2018.
 *
 * @version 1.0
 */
@Dao
public interface ServiceProviderDao {

    @Query("SELECT * FROM ServiceProvider")
    LiveData<List<ServiceProvider>> loadAll();

    @Query("SELECT * FROM ServiceProvider WHERE id LIKE :id LIMIT 1")
    LiveData<ServiceProvider> findById(long id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(ServiceProvider location);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<ServiceProvider> locations);

    @Delete
    void delete(ServiceProvider location);
}
