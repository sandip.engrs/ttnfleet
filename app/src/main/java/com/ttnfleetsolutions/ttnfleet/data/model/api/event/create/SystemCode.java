package com.ttnfleetsolutions.ttnfleet.data.model.api.event.create;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 6/1/2018.
 *
 * @version 1.0
 */
public class SystemCode {

    @SerializedName("code1")
    @Expose
    private String code1;
    @SerializedName("assemblycode")
    @Expose
    private List<AssemblyCode> assemblyCodeList = null;

    private String systemCodeName;

    public SystemCode(String code, String systemCodeName) {
        this.code1 = code;
        this.systemCodeName = systemCodeName;
    }

    public int checkIfAssemblyCodeExists(String assemblyCode) {
        if (assemblyCodeList != null && assemblyCodeList.size() > 0)
            for (int i = 0; i < assemblyCodeList.size(); i++) {
                if (assemblyCode.equalsIgnoreCase(assemblyCodeList.get(i).getCode2()))
                    return i;
            }
        return -1;
    }

    public String getCode1() {
        return code1;
    }

    public void setCode1(String code1) {
        this.code1 = code1;
    }

    public List<AssemblyCode> getAssemblyCodeList() {
        return assemblyCodeList;
    }

    public void setAssemblyCodeList(List<AssemblyCode> assemblyCodeList) {
        this.assemblyCodeList = assemblyCodeList;
    }

    public String getSystemCodeName() {
        return systemCodeName;
    }

    public void setSystemCodeName(String systemCodeName) {
        this.systemCodeName = systemCodeName;
    }
}
