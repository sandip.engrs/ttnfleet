package com.ttnfleetsolutions.ttnfleet.data.local.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.Vehicle;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 11/2/2017.
 *
 * @version 1.0
 */

@Dao
public interface VehicleDao {

    @Query("SELECT * FROM vehicles")
    LiveData<List<Vehicle>> loadAll();

    @Query("SELECT * FROM vehicles WHERE id LIKE :id LIMIT 1")
    LiveData<Vehicle> findById(long id);

    @Query("SELECT * FROM vehicles WHERE id LIKE :id LIMIT 1")
    Vehicle findVehicleById(long id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Vehicle vehicle);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Vehicle> vehicles);

    @Delete
    void delete(Vehicle vehicle);

}

