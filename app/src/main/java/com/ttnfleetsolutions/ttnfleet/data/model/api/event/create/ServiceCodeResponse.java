package com.ttnfleetsolutions.ttnfleet.data.model.api.event.create;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 5/21/2018.
 *
 * @version 1.0
 */
public class ServiceCodeResponse {

    @SerializedName("Result")
    @Expose
    private List<ServiceCode> result = null;

    public List<ServiceCode> getResult() {
        return result;
    }

    public void setResult(List<ServiceCode> result) {
        this.result = result;
    }

}
