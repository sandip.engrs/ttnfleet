package com.ttnfleetsolutions.ttnfleet.data.model.api.event.create;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandip Chaulagain on 7/9/2018.
 *
 * @version 1.0
 */
public class AddNoteResponse {

    @SerializedName("Result")
    @Expose
    private Result result;

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public class Result {

        @SerializedName("eventid")
        @Expose
        private Integer eventid;
        @SerializedName("fleetdispacterid")
        @Expose
        private Integer fleetdispacterid;
        @SerializedName("fdnote")
        @Expose
        private String fdnote;

        public Integer getEventid() {
            return eventid;
        }

        public void setEventid(Integer eventid) {
            this.eventid = eventid;
        }

        public Integer getFleetdispacterid() {
            return fleetdispacterid;
        }

        public void setFleetdispacterid(Integer fleetdispacterid) {
            this.fleetdispacterid = fleetdispacterid;
        }

        public String getFdnote() {
            return fdnote;
        }

        public void setFdnote(String fdnote) {
            this.fdnote = fdnote;
        }

    }
}
