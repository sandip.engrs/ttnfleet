package com.ttnfleetsolutions.ttnfleet.data.local.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by Sandip Chaulagain on 11/2/2017.
 *
 * @version 1.0
 */

@Entity(tableName = "users")
public class User {

    @PrimaryKey
    public Long id;

    public String name;

    @Ignore
    @ColumnInfo(name = "created_at")
    public String createdAt;

    @Ignore
    @ColumnInfo(name = "updated_at")
    public String updatedAt;
}
