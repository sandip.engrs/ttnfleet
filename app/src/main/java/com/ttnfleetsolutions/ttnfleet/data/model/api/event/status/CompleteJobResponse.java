package com.ttnfleetsolutions.ttnfleet.data.model.api.event.status;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandip Chaulagain on 6/16/2018.
 *
 * @version 1.0
 */
public class CompleteJobResponse {

    @SerializedName("Result")
    @Expose
    private Result result;

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public class Result {

        @SerializedName("jobid")
        @Expose
        private Integer jobid;
        @SerializedName("statusid")
        @Expose
        private Integer statusid;
        @SerializedName("id")
        @Expose
        private Integer id;

        public Integer getJobid() {
            return jobid;
        }

        public void setJobid(Integer jobid) {
            this.jobid = jobid;
        }

        public Integer getStatusid() {
            return statusid;
        }

        public void setStatusid(Integer statusid) {
            this.statusid = statusid;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }
    }

}
