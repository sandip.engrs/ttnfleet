package com.ttnfleetsolutions.ttnfleet.data.local.db;


import android.arch.lifecycle.LiveData;

import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.Location;
import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.ServiceProvider;
import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.ServiceRequest;
import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.Vehicle;
import com.ttnfleetsolutions.ttnfleet.data.model.ServiceRequestSummary;
import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.Repair;
import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.User;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

/**
 * Created by Sandip Chaulagain on 11/2/2017.
 *
 * @version 1.0
 */
@Singleton
public class AppDbHelper implements DbHelper {

    private final AppDatabase mAppDatabase;

    @Inject
    public AppDbHelper(AppDatabase appDatabase) {
        this.mAppDatabase = appDatabase;
    }

    @Override
    public Observable<List<User>> getAllUsers() {
        return Observable.fromCallable(() -> mAppDatabase.userDao().loadAll());
    }

    @Override
    public Observable<Boolean> insertUser(final User user) {
        return Observable.fromCallable(() -> {
            mAppDatabase.userDao().insert(user);
            return true;
        });
    }

    @Override
    public Observable<Boolean> insertRepair(Repair repair) {
        return Observable.fromCallable(() -> {
            mAppDatabase.repairDao().insert(repair);
            return true;
        });
    }

    @Override
    public Observable<LiveData<List<Repair>>> getAllRepairs() {
        return Observable.fromCallable(() -> mAppDatabase.repairDao().loadAll());
    }

    @Override
    public Observable<Boolean> insertAllRepairs(List<Repair> repairs) {
        return Observable.fromCallable(() -> {
            mAppDatabase.repairDao().insertAll(repairs);
            return true;
        });
    }

    @Override
    public Observable<Boolean> insertServiceRequest(ServiceRequest serviceRequest) {
        return Observable.fromCallable(() -> {
            mAppDatabase.serviceRequestDao().insert(serviceRequest);
            return true;
        });
    }

    @Override
    public Observable<Boolean> insertAllServiceRequests(List<ServiceRequest> serviceRequests) {
        return Observable.fromCallable(() -> {
            mAppDatabase.serviceRequestDao().insertAll(serviceRequests);
            return true;
        });
    }

    @Override
    public Observable<LiveData<List<ServiceRequestSummary>>> getAllServiceRequests() {
        return Observable.fromCallable(() -> mAppDatabase.serviceRequestDao().loadAll());
    }

    @Override
    public Observable<LiveData<List<ServiceRequestSummary>>> getCompletedServiceRequests() {
        return Observable.fromCallable(() -> mAppDatabase.serviceRequestDao().loadCompleted());
    }

    @Override
    public Observable<ServiceRequestSummary> getServiceRequestById(long _id) {
        return Observable.fromCallable(() -> mAppDatabase.serviceRequestDao().getServiceRequestSummary(_id));
    }

    @Override
    public Observable<Boolean> insertVehicle(Vehicle vehicle) {
        return Observable.fromCallable(() -> {
            mAppDatabase.vehicleDao().insert(vehicle);
            return true;
        });
    }

    @Override
    public Observable<LiveData<List<Vehicle>>> getAllVehicles() {
        return Observable.fromCallable(() -> mAppDatabase.vehicleDao().loadAll());
    }

    @Override
    public Observable<LiveData<Vehicle>> getVehicleById(long id) {
        return Observable.fromCallable(() -> mAppDatabase.vehicleDao().findById(id));
    }

    @Override
    public Observable<Vehicle> getVehicleDetailsById(long _id) {
        return Observable.fromCallable(() -> mAppDatabase.vehicleDao().findVehicleById(_id));
    }

    @Override
    public Observable<Boolean> insertLocation(Location location) {
        return Observable.fromCallable(() -> {
            mAppDatabase.locationDao().insert(location);
            return true;
        });
    }

    @Override
    public Observable<LiveData<List<Location>>> getLocation() {
        return Observable.fromCallable(() -> mAppDatabase.locationDao().loadAll());
    }

    @Override
    public Observable<LiveData<Location>> getLocationById(long id) {
        return Observable.fromCallable(() -> mAppDatabase.locationDao().findById(id));
    }

    @Override
    public Observable<Boolean> insertAllServiceProviders(List<ServiceProvider> serviceProviders) {
        return Observable.fromCallable(() -> {
            mAppDatabase.serviceProviderDao().insertAll(serviceProviders);
            return true;
        });
    }

    @Override
    public Observable<LiveData<List<ServiceProvider>>> getAllServiceProviders() {
        return Observable.fromCallable(() -> mAppDatabase.serviceProviderDao().loadAll());
    }

    @Override
    public Observable<Boolean> insertServiceProviders(ServiceProvider serviceProvider) {
        return Observable.fromCallable(() -> {
            mAppDatabase.serviceProviderDao().insert(serviceProvider);
            return true;
        });
    }
}
