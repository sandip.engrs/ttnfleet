package com.ttnfleetsolutions.ttnfleet.data.model;

/**
 * Created by Sandip Chaulagain on 1/19/2018.
 *
 * @version 1.0
 */

public class ServiceRequestSummary {

    public long id;

    public String serviceType;

    public String name;
    public long vehicleID;
    public long locationID;


    public String serviceIssue;
    public String ETA;
    public String ATA;
    public String ATC;
    public String ETC;

    public String datetime;

    public int status;

    //Drop-off & Pick-up
    public String dropoff;
    public String pickup;

    //Location
    public double latitude;
    public double longitude;

    public String address;

    //Vehicle
    public String vehicle_name;
    public String vin;
    public String licence;
    public String brand;
    public String year;
    public String fuelType;

}
