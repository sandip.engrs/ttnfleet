package com.ttnfleetsolutions.ttnfleet.data.model.api.event.listing;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandip Chaulagain on 5/29/2018.
 *
 * @version 1.0
 */
public class Status {

    @SerializedName("statusid")
    @Expose
    private Integer statusid;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("statustype")
    @Expose
    private Object statustype;

    public Integer getStatusid() {
        return statusid;
    }

    public void setStatusid(Integer statusid) {
        this.statusid = statusid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Object getStatustype() {
        return statustype;
    }

    public void setStatustype(Object statustype) {
        this.statustype = statustype;
    }

}
