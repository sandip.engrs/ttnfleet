package com.ttnfleetsolutions.ttnfleet.data.model.api.event.create;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 5/21/2018.
 *
 * @version 1.0
 */
public class LocationTypeResponse {

    @SerializedName("Result")
    @Expose
    private List<LocationType> result = null;

    public List<LocationType> getResult() {
        return result;
    }

    public void setResult(List<LocationType> result) {
        this.result = result;
    }

}
