package com.ttnfleetsolutions.ttnfleet.data.model.api.event.status;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandip Chaulagain on 6/16/2018.
 *
 * @version 1.0
 */
public class ConfirmBreakdownResponse {

    @SerializedName("Result")
    @Expose
    private Result result;

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public class Result {

        @SerializedName("jobid")
        @Expose
        private Integer jobid;
        @SerializedName("isactual")
        @Expose
        private Integer isactual;
        @SerializedName("id")
        @Expose
        private Integer id;

        public Integer getJobid() {
            return jobid;
        }

        public void setJobid(Integer jobid) {
            this.jobid = jobid;
        }

        public Integer getIsactual() {
            return isactual;
        }

        public void setIsactual(Integer isactual) {
            this.isactual = isactual;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

    }
}
