/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.ttnfleetsolutions.ttnfleet.data.local.prefs;

import com.ttnfleetsolutions.ttnfleet.data.DataManager;

/**
 * Created by Sandip Chaulagain on 10/3/2017.
 *
 * @version 1.0
 */

public interface PreferencesHelper {

    String getGUID();

    void setGUID(String GUID);

    String getAccessToken();

    void setAccessToken(String accessToken);

    int getCurrentUserLoggedInMode();

    void setCurrentUserLoggedInMode(DataManager.LoggedInMode mode);

    String getCurrentUserFullName();

    void setCurrentUserFullName(String fullName);

    String getCurrentUserRoleName();

    void setCurrentUserRoleName(String roleName);

    String getCurrentUserProfileImage();

    void setCurrentUserProfileImage(String imageUrl);

    String getCurrentUserEmail();

    void setCurrentUserEmail(String email);

    String getCurrentUserPassword();

    void setCurrentUserPassword(String password);

    int getCurrentUserID();

    void setCurrentUserID(int userID);

    int getCurrentUserRole();

    void setCurrentUserRole(DataManager.Role role);

    int getDriverCount();

    void setDriverCount(int count);

    boolean isDataAvailable();

    void setIsDataAdded(boolean added);

    /**
     * Theme Details
     */

    int getThemeID();

    void setThemeID(int id);

    String getThemeLogo();

    void setThemeLogo(String logo);

    int getThemeStatusBarColor();

    void setThemeStatusBarColor(int color);

    int getThemeTitleBarColor();

    void setThemeTitleBarColor(int color);

    int getThemeTransparentColor();

    void setThemeTransparentColor(int color);

    String getThemeBgImage();

    void setThemeBgImage(String image);

    int getThemeButtonColor();

    void setThemeButtonColor(int color);

    int getThemeTextColor();

    void setThemeTextColor(int color);

    int getThemeCompanyID();

    void setThemeCompanyID(int id);

    String getCallForHelpLabel();

    void setCallForHelpLabel(String label);

    int getThemeFooterDisplayFlag();

    void setThemeFooterDisplayFlag(int flag);

    int getThemeFooterTextColor();

    void setThemeFooterTextColor(int color);

    /**
     * Event Request
     */
    void setEventRequest(String eventRequest);

    String getEventRequest();

    /**
     * Additional Job Request
     */
    void setAdditionalJobRequest(String additionalJobRequest);

    String getAdditionalJobRequest();
}
