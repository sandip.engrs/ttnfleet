package com.ttnfleetsolutions.ttnfleet.data.model.api.event.create;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 7/2/2018.
 *
 * @version 1.0
 */
public class DriverListResponse {

    @SerializedName("Result")
    @Expose
    private List<Driver> result = null;

    public List<Driver> getResult() {
        return result;
    }

    public void setResult(List<Driver> result) {
        this.result = result;
    }

    public class Driver {


        @SerializedName("userid")
        @Expose
        private Integer userid;
        @SerializedName("companyid")
        @Expose
        private Integer companyid;
        @SerializedName("usertypeid")
        @Expose
        private Integer usertypeid;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("lastname")
        @Expose
        private String lastname;
        @SerializedName("firstname")
        @Expose
        private String firstname;
        @SerializedName("phone")
        @Expose
        private String phone;
        @SerializedName("zip")
        @Expose
        private Object zip;
        @SerializedName("fax")
        @Expose
        private Object fax;
        @SerializedName("image")
        @Expose
        private String image;

        public Integer getUserid() {
            return userid;
        }

        public void setUserid(Integer userid) {
            this.userid = userid;
        }

        public Integer getCompanyid() {
            return companyid;
        }

        public void setCompanyid(Integer companyid) {
            this.companyid = companyid;
        }

        public Integer getUsertypeid() {
            return usertypeid;
        }

        public void setUsertypeid(Integer usertypeid) {
            this.usertypeid = usertypeid;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getLastname() {
            return lastname;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public Object getZip() {
            return zip;
        }

        public void setZip(Object zip) {
            this.zip = zip;
        }

        public Object getFax() {
            return fax;
        }

        public void setFax(Object fax) {
            this.fax = fax;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getDisplaytitle() {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(getFirstname());
            stringBuilder.append(" ");
            stringBuilder.append(getLastname());
            return stringBuilder.toString();
        }
    }

}
