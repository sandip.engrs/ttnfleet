package com.ttnfleetsolutions.ttnfleet.data.model.api.event.survey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 6/18/2018.
 *
 * @version 1.0
 */
public class SurveyRequest {

    @SerializedName("EventId")
    @Expose
    private Integer eventId;
    @SerializedName("Comments")
    @Expose
    private String comments;
    @SerializedName("SurveyQuestion")
    @Expose
    private List<SurveyQuestion> surveyQuestion = null;

    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public List<SurveyQuestion> getSurveyQuestion() {
        return surveyQuestion;
    }

    public void setSurveyQuestion(List<SurveyQuestion> surveyQuestion) {
        this.surveyQuestion = surveyQuestion;
    }

    public class SurveyQuestion {

        @SerializedName("SurveyQuestionId")
        @Expose
        private Integer surveyQuestionId;
        @SerializedName("Rating")
        @Expose
        private float rating;

        public Integer getSurveyQuestionId() {
            return surveyQuestionId;
        }

        public void setSurveyQuestionId(Integer surveyQuestionId) {
            this.surveyQuestionId = surveyQuestionId;
        }

        public float getRating() {
            return rating;
        }

        public void setRating(float rating) {
            this.rating = rating;
        }

    }
}
