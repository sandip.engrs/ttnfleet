package com.ttnfleetsolutions.ttnfleet.data.local.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.Location;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 11/2/2017.
 *
 * @version 1.0
 */

@Dao
public interface LocationDao {

    @Query("SELECT * FROM location")
    LiveData<List<Location>> loadAll();

    @Query("SELECT * FROM location WHERE id LIKE :id LIMIT 1")
    LiveData<Location> findById(long id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Location location);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Location> locations);

    @Delete
    void delete(Location location);

}


