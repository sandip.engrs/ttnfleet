/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.ttnfleetsolutions.ttnfleet.data.remote;

import android.util.Log;

import com.androidnetworking.common.Priority;
import com.ttnfleetsolutions.ttnfleet.data.model.api.asset.AssetsListResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.AddNoteResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.AdditionalJobResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.CreateEventResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.DriverListResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.LocationTypeResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.ServiceCodeResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.TruckLoadResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.listing.EventDetailsResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.listing.EventResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.requirements.ImageRequirement;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.requirements.RequireImagesResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.AcceptWorkOrderResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.CompleteJobResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.ConfirmBreakdownResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.DispatchEventResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.EnrouteResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.RejectionTypesResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.survey.SubmitSurveyResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.survey.SurveyQuestionsResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.login.LoginRequest;
import com.ttnfleetsolutions.ttnfleet.data.model.api.login.LoginResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.login.TokenResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.serviceprovider.ServiceProviderListResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.technician.TechnicianListResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.theme.ThemeResponse;
import com.rx2androidnetworking.Rx2ANRequest;
import com.rx2androidnetworking.Rx2AndroidNetworking;

import org.json.JSONObject;

import java.io.File;
import java.util.List;
import java.util.concurrent.Executors;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

/**
 * Created by Sandip Chaulagain on 5/18/2018.
 * AppApiHelper contains networks calls implementation using RxNetworking lib.
 * @version 1.0
 */

@Singleton
public class AppApiHelper implements ApiHelper {

    private ApiHeader mApiHeader;

    @Inject
    public AppApiHelper(ApiHeader apiHeader) {
        mApiHeader = apiHeader;
    }

    /**
     * @return ApiHeader instance
     */
    @Override
    public ApiHeader getApiHeader() {
        return mApiHeader;
    }
    /**
     * @param request the model contains username and password value.
     * @return Observable<LoginResponse> the user-profile
     */
    @Override
    public Observable<LoginResponse> doServerLoginApiCall(LoginRequest request) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SERVER_LOGIN)
                .addBodyParameter(request)
                .build()
                .getObjectObservable(LoginResponse.class);
    }

    /**
     * @param GUID the authentication needed to enter in the app for specific user-role
     * @return Observable<LoginResponse> the user-profile
     */
    @Override
    public Observable<LoginResponse> doServerAuthenticationApiCall(String GUID) {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_SERVER_LOGIN + GUID)
                .build()
                .getObjectObservable(LoginResponse.class);
    }

    /**
     * @return Observable<TokenResponse> the access-token {@link ApiHeader}
     */
    @Override
    public Observable<TokenResponse> getAccessTokenApiCall() {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_SERVER_GET_TOKEN)
                .build()
                .getObjectObservable(TokenResponse.class);
    }

    /**
     * @param companyId key to get company-based theme
     * @return Observable<ThemeResponse> the theme properties
     */
    @Override
    public Observable<ThemeResponse> getThemeApiCall(String companyId) {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_SERVER_GET_APP_THEME + companyId)
                .addHeaders(mApiHeader)
                .build()
                .getObjectObservable(ThemeResponse.class);
    }

    /* DRIVER MODULE APIs */

    /**
     *
     * @return Observable<ServiceCodeResponse> the list of system codes
     */
    @Override
    public Observable<ServiceCodeResponse> doServerGetSystemListApiCall() {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_SERVER_GET_SYSTEM_LIST_TABLE)
                .addHeaders(mApiHeader)
                .build()
                .getObjectObservable(ServiceCodeResponse.class);
    }

    /**
     * @param systemCode key to get assembly codes for specified systemCode
     * @return Observable<ServiceCodeResponse> the list of assembly codes
     */
    @Override
    public Observable<ServiceCodeResponse> doServerGetAssemblyListApiCall(String systemCode) {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_SERVER_GET_ASSEMBLY_LIST_TABLE + systemCode)
                .addHeaders(mApiHeader)
                .build()
                .getObjectObservable(ServiceCodeResponse.class);
    }

    /**
     * @param systemCode  key to get component codes for specified systemCode
     * @param assemblyCode  key to get component codes for specified assemblyCode
     * @return Observable<ServiceCodeResponse> the list of component codes
     */
    @Override
    public Observable<ServiceCodeResponse> doServerGetComponentListApiCall(String systemCode, String assemblyCode) {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_SERVER_GET_COMPONENT_LIST_TABLE + systemCode + "/" + assemblyCode)
                .addHeaders(mApiHeader)
                .build()
                .getObjectObservable(ServiceCodeResponse.class);
    }

    /**
     * @return Observable<TruckLoadResponse> the list of truck-load plans
     */
    @Override
    public Observable<TruckLoadResponse> doServerGetTruckLLoadTypesApiCall() {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_SERVER_GET_TRUCK_LOAD_TYPES)
                .addHeaders(mApiHeader)
                .build()
                .getObjectObservable(TruckLoadResponse.class);
    }

    /**
     * @param userId  key to get assets owned/driven by user
     * @return Observable<AssetsListResponse> the list of assets
     */
    @Override
    public Observable<AssetsListResponse> doServerGetAssetsApiCall(int userId) {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_SERVER_GET_ASSETS + userId)
                .addHeaders(mApiHeader)
                .build()
                .getObjectObservable(AssetsListResponse.class);
    }

    /**
     * @param eventType key to get locations based on event-type
     * @param userId  key to get locations saved by user
     * @return Observable<LocationTypeResponse> the list of locations
     */
    @Override
    public Observable<LocationTypeResponse> doServerLocationTypeApiCall(String eventType, int userId) {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_SERVER_GET_LOCATION_TYPE + eventType + "/" + userId)
                .addHeaders(mApiHeader)
                .build()
                .getObjectObservable(LocationTypeResponse.class);
    }

    /**
     * @param userId  key to get new/active events created/related/assigned to user
     * @return Observable<EventResponse> the list of new/active events
     */
    @Override
    public Observable<EventResponse> doServerGetEventsApiCall(int userId) {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_SERVER_GET_ACTIVE_EVENTS + userId)
                .addHeaders(mApiHeader)
                .build()
                .getObjectObservable(EventResponse.class);
    }

    /**
     * @param eventRequestData the details of event entered by user
     * @return Observable<CreateEventResponse> status of event-creation
     */
    @Override
    public Observable<CreateEventResponse> doServerCreateEventsApiCall(JSONObject eventRequestData) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SERVER_CREATE_EVENT)
                .addJSONObjectBody(eventRequestData)
                .addHeaders(mApiHeader)
                .build()
                .getObjectObservable(CreateEventResponse.class);
    }

    /**
     * @param userId  key to get event-history created/related/assigned to user
     * @return Observable<EventResponse> the list of completed/closed events
     */
    @Override
    public Observable<EventResponse> doServerGetClosedEventsApiCall(int userId) {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_SERVER_GET_CLOSED_EVENTS + userId)
                .addHeaders(mApiHeader)
                .build()
                .getObjectObservable(EventResponse.class);
    }

    /**
     * @return Observable<SurveyQuestionsResponse> the list of survey questions
     */
    @Override
    public Observable<SurveyQuestionsResponse> doServerGetSurveyQuestionsApiCall() {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_SERVER_GET_SURVEY_QUESTIONS)
                .addHeaders(mApiHeader)
                .build()
                .getObjectObservable(SurveyQuestionsResponse.class);
    }

    /**
     * @param surveyRequestData details of survey questions-answers
     * @return Observable<SubmitSurveyResponse> status of submission
     */
    @Override
    public Observable<SubmitSurveyResponse> doServerSubmitSurveyApiCall(JSONObject surveyRequestData) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SERVER_SUBMIT_SURVEY)
                .addJSONObjectBody(surveyRequestData)
                .addHeaders(mApiHeader)
                .build()
                .getObjectObservable(SubmitSurveyResponse.class);
    }

    /* FLEET DISPATCHER MODULE APIs */

    /**
     * @param userId  key to get new/active/closed/completed events created/related/assigned to user
     * @return Observable<EventResponse> the list of events
     */
    @Override
    public Observable<EventResponse> doServerEventListingFleetDispatcherApiCall(int userId, int eventStatus) {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_SERVER_GET_EVENTS_FD + userId + "/" + eventStatus)
                .addHeaders(mApiHeader)
                .build()
                .getObjectObservable(EventResponse.class);
    }

    /**
     * @return Observable<ServiceProviderListResponse> the list of service-providers
     */
    @Override
    public Observable<ServiceProviderListResponse> doServerServiceProviderListingApiCall() {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_SERVER_GET_SERVICE_PROVIDER)
                .addHeaders(mApiHeader)
                .build()
                .getObjectObservable(ServiceProviderListResponse.class);
    }

    /**
     * @param dispatchRequest details of event-dispatch
     * @return Observable<DispatchEventResponse> the status of event-dispatch
     */
    @Override
    public Observable<DispatchEventResponse> doServerDispatchEventApiCall(JSONObject dispatchRequest) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SERVER_DISPATCH_EVENT)
                .addJSONObjectBody(dispatchRequest)
                .addHeaders(mApiHeader)
                .build()
                .getObjectObservable(DispatchEventResponse.class);
    }

    /**
     * @param companyId key to get list of driver of specified company
     * @return Observable<DriverListResponse> the list of drivers
     */
    @Override
    public Observable<DriverListResponse> doServerGetDriverListingApiCall(int companyId) {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_SERVER_GET_DRIVERS + companyId)
                .addHeaders(mApiHeader)
                .build()
                .getObjectObservable(DriverListResponse.class);
    }

    /**
     * @param addNoteRequest details of notes
     * @return Observable<AddNoteResponse> the status of note-submission
     */
    @Override
    public Observable<AddNoteResponse> doServerAddNoteFDApiCall(JSONObject addNoteRequest) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SERVER_ADD_NOTE_FD)
                .addJSONObjectBody(addNoteRequest)
                .addHeaders(mApiHeader)
                .build()
                .getObjectObservable(AddNoteResponse.class);
    }

    /* SERVICE PROVIDER MODULE APIs */

    /**
     * @param userId  key to get new/active/closed/completed events related/assigned to user
     * @param eventStatus  key to get define status of the event
     * @return Observable<EventResponse> the list of events for specified status
     */
    @Override
    public Observable<EventResponse> doServerEventListingServiceProviderApiCall(int userId, int eventStatus) {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_SERVER_GET_EVENTS_SP + userId + "/" + eventStatus)
                .addHeaders(mApiHeader)
                .build()
                .getObjectObservable(EventResponse.class);
    }

    /**
     * @param serviceProviderId  key to get technician list for specified service-provider
     * @return Observable<TechnicianListResponse> the list of technicians
     */
    @Override
    public Observable<TechnicianListResponse> doServerTechnicianListingApiCall(int serviceProviderId) {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_SERVER_GET_TECHNICIANS + serviceProviderId)
                .addHeaders(mApiHeader)
                .build()
                .getObjectObservable(TechnicianListResponse.class);
    }

    /**
     * @param acceptWorkOrderData details of work-order
     * @return Observable<AcceptWorkOrderResponse> the status of acceptance
     */
    @Override
    public Observable<AcceptWorkOrderResponse> doServerAcceptWorkOrderApiCall(JSONObject acceptWorkOrderData) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SERVER_ACCEPT_WORK_ORDER)
                .addJSONObjectBody(acceptWorkOrderData)
                .addHeaders(mApiHeader)
                .build()
                .getObjectObservable(AcceptWorkOrderResponse.class);
    }

    /**
     * @return Observable<RejectionTypesResponse> the list of rejection reasons
     */
    @Override
    public Observable<RejectionTypesResponse> doServerGetRejectTypesApiCall() {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_SERVER_REJECT_TYPES)
                .addHeaders(mApiHeader)
                .build()
                .getObjectObservable(RejectionTypesResponse.class);
    }

    /**
     * @param rejectWorkOrderData details of work-order
     * @return Observable<AcceptWorkOrderResponse> the status of rejection
     */
    @Override
    public Observable<AcceptWorkOrderResponse> doServerRejectWorkOrderApiCall(JSONObject rejectWorkOrderData) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SERVER_REJECT_WORK_ORDER)
                .addJSONObjectBody(rejectWorkOrderData)
                .addHeaders(mApiHeader)
                .build()
                .getObjectObservable(AcceptWorkOrderResponse.class);
    }

    /**
     * @param acceptAssignWorkOrderData details of work-order
     * @return Observable<AcceptWorkOrderResponse> the status of acceptance and assignment to technician
     */
    @Override
    public Observable<AcceptWorkOrderResponse> doServerAcceptAssignWorkOrderApiCall(JSONObject acceptAssignWorkOrderData) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SERVER_ACCEPT_ASSIGN_WORK_ORDER)
                .addJSONObjectBody(acceptAssignWorkOrderData)
                .addHeaders(mApiHeader)
                .build()
                .getObjectObservable(AcceptWorkOrderResponse.class);
    }

    /* TECHNICIAN MODULE APIs */
    /**
     * @param userId  key to get new/active/closed/completed events related/assigned to user
     * @param eventStatus  key to get define status of the event
     * @return Observable<EventResponse> the list of events
     */
    @Override
    public Observable<EventResponse> doServerEventListingTechnicianApiCall(int userId, int eventStatus) {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_SERVER_GET_EVENTS_TECH + userId + "/" + eventStatus)
                .addHeaders(mApiHeader)
                .build()
                .getObjectObservable(EventResponse.class);
    }

    /**
     * @param acceptRejectJobData details of job
     * @return Observable<AcceptWorkOrderResponse> the status of acceptance or rejection
     */
    @Override
    public Observable<AcceptWorkOrderResponse> doServerAcceptRejectJobApiCall(JSONObject acceptRejectJobData) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SERVER_ACCEPT_REJECT_JOB)
                .addJSONObjectBody(acceptRejectJobData)
                .addHeaders(mApiHeader)
                .build()
                .getObjectObservable(AcceptWorkOrderResponse.class);
    }

    /**
     * @return Observable<RejectionTypesResponse> the list of rejection reasons
     */
    @Override
    public Observable<RejectionTypesResponse> doServerGetRejectTypesTechApiCall() {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_SERVER_REJECT_TYPES_TECH)
                .addHeaders(mApiHeader)
                .build()
                .getObjectObservable(RejectionTypesResponse.class);
    }

    /**
     * @param createAdditionalJobData details of additional job
     * @return Observable<AdditionalJobResponse> the status of job-creation
     */
    @Override
    public Observable<AdditionalJobResponse> doServerCreateAdditionalJobApiCall(JSONObject createAdditionalJobData) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SERVER_CREATE_ADDITIONAL_JOB)
                .addJSONObjectBody(createAdditionalJobData)
                .addHeaders(mApiHeader)
                .build()
                .getObjectObservable(AdditionalJobResponse.class);
    }

    /**
     * @param completeJobRequest details of job
     * @return Observable<CompleteJobResponse> the status of job-completion
     */
    @Override
    public Observable<CompleteJobResponse> doServerCompleteJobApiCall(JSONObject completeJobRequest) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SERVER_COMPLETE_JOB)
                .addJSONObjectBody(completeJobRequest)
                .addHeaders(mApiHeader)
                .build()
                .getObjectObservable(CompleteJobResponse.class);
    }

    /**
     * @param jobId key to confirm job
     * @return Observable<ConfirmBreakdownResponse> the status of confirmation
     */
    @Override
    public Observable<ConfirmBreakdownResponse> doServerConfirmJobApiCall(int jobId) {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_SERVER_CONFIRM_BREAKDOWN + jobId)
                .addHeaders(mApiHeader)
                .build()
                .getObjectObservable(ConfirmBreakdownResponse.class);
    }

    /**
     * @param enrouteRequest details of job
     * @return Observable<EnrouteResponse> the status of enroute
     */
    @Override
    public Observable<EnrouteResponse> doServerEnrouteApiCall(JSONObject enrouteRequest) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SERVER_ENROUTE)
                .addJSONObjectBody(enrouteRequest)
                .addHeaders(mApiHeader)
                .build()
                .getObjectObservable(EnrouteResponse.class);
    }

    /**
     * @param imageRequirementList list of images
     * @param eventId key to upload images for specified event
     * @param jobId key to upload images for specified job
     * @param userId id of logged-in user
     * @param OdometerReading value of Odometer entered by logged-in user
     * @return Observable<String> the status of image-uploading
     */
    @Override
    public Observable<String> doServerUploadRequirementsApiCall(List<ImageRequirement> imageRequirementList, int eventId, int jobId, int userId, String OdometerReading) {
        Rx2ANRequest.MultiPartBuilder multiPartBuilder = Rx2AndroidNetworking.upload(ApiEndPoint.ENDPOINT_UPLOAD_REQUIREMENTS);

        for (int i = 0; i < imageRequirementList.size(); i++) {
            if (imageRequirementList.get(i).getFilePath() != null)
                multiPartBuilder.addMultipartFile(String.valueOf(imageRequirementList.get(i).getIndex() + 1), new File(imageRequirementList.get(i).getFilePath().getPath()));
        }
        return multiPartBuilder.addMultipartParameter("EventId", String.valueOf(eventId))
                .addMultipartParameter("JobId", String.valueOf(jobId))
                .addMultipartParameter("UserId", String.valueOf(userId))
                .addMultipartParameter("OdometerReading", OdometerReading)
                .setTag("uploadTest")
                .setPriority(Priority.HIGH)
                .setExecutor(Executors.newSingleThreadExecutor()) // setting an executor to get response or completion on that executor thread
                .build().setUploadProgressListener((bytesUploaded, totalBytes) -> Log.e("NETWORK", "bytesUploaded: " + bytesUploaded + " totalBytes: " + totalBytes))
                .getObjectObservable(String.class);
    }

    /* COMMON APIs */

    /**
     * @param eventId key to get event details
     * @return Observable<EventDetailsResponse> an event details
     */
    @Override
    public Observable<EventDetailsResponse> doServerGetEventDetailsApiCall(int eventId) {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_SERVER_GET_EVENT_DETAILS + eventId)
                .addHeaders(mApiHeader)
                .build()
                .getObjectObservable(EventDetailsResponse.class);
    }

    /**
     * @param jobId key to get images
     * @return Observable<RequireImagesResponse> the list of images
     */
    @Override
    public Observable<RequireImagesResponse> doServerGetRequirementImagesApiCall(int jobId) {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_SERVER_GET_REQUIREMENTS_PHOTOS + jobId)
                .addHeaders(mApiHeader)
                .build()
                .getObjectObservable(RequireImagesResponse.class);
    }
}
