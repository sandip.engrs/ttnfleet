package com.ttnfleetsolutions.ttnfleet.data.model.api.event.status;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandip Chaulagain on 6/16/2018.
 *
 * @version 1.0
 */
public class EnrouteRequest {

    @SerializedName("JobId")
    @Expose
    private Integer jobId;
    @SerializedName("JobStatusId")
    @Expose
    private Integer jobStatusId;

    public Integer getJobId() {
        return jobId;
    }

    public void setJobId(Integer jobId) {
        this.jobId = jobId;
    }

    public Integer getJobStatusId() {
        return jobStatusId;
    }

    public void setJobStatusId(Integer jobStatusId) {
        this.jobStatusId = jobStatusId;
    }
}
