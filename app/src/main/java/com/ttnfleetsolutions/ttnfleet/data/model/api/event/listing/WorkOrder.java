package com.ttnfleetsolutions.ttnfleet.data.model.api.event.listing;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.ServiceCode;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 5/29/2018.
 *
 * @version 1.0
 */
public class WorkOrder {

    @SerializedName("workorderid")
    @Expose
    private Integer workorderid;
    @SerializedName("serviceproviderid")
    @Expose
    private Integer serviceproviderid;
    @SerializedName("eventid")
    @Expose
    private Integer eventid;
    @SerializedName("eta")
    @Expose
    private Object eta;
    @SerializedName("etc")
    @Expose
    private Object etc;
    @SerializedName("createdtime")
    @Expose
    private String createdtime;
    @SerializedName("updatedby")
    @Expose
    private Object updatedby;
    @SerializedName("updatedtime")
    @Expose
    private String updatedtime;
    @SerializedName("statusid")
    @Expose
    private Integer statusid;
    @SerializedName("servicecode")
    @Expose
    private String servicecode;
    @SerializedName("ServiceCode")
    @Expose
    private ServiceCode serviceCode;
    @SerializedName("Jobs")
    @Expose
    private List<Job> jobs = null;
    @SerializedName("Status")
    @Expose
    private Status status;
    @SerializedName("ServiceProvider")
    @Expose
    private ServiceProvider serviceProvider;

    public Integer getWorkorderid() {
        return workorderid;
    }

    public void setWorkorderid(Integer workorderid) {
        this.workorderid = workorderid;
    }

    public Integer getServiceproviderid() {
        return serviceproviderid;
    }

    public void setServiceproviderid(Integer serviceproviderid) {
        this.serviceproviderid = serviceproviderid;
    }

    public Integer getEventid() {
        return eventid;
    }

    public void setEventid(Integer eventid) {
        this.eventid = eventid;
    }

    public Object getEta() {
        return eta;
    }

    public void setEta(Object eta) {
        this.eta = eta;
    }

    public Object getEtc() {
        return etc;
    }

    public void setEtc(Object etc) {
        this.etc = etc;
    }

    public String getCreatedtime() {
        return createdtime;
    }

    public void setCreatedtime(String createdtime) {
        this.createdtime = createdtime;
    }

    public Object getUpdatedby() {
        return updatedby;
    }

    public void setUpdatedby(Object updatedby) {
        this.updatedby = updatedby;
    }

    public String getUpdatedtime() {
        return updatedtime;
    }

    public void setUpdatedtime(String updatedtime) {
        this.updatedtime = updatedtime;
    }

    public Integer getStatusid() {
        return statusid;
    }

    public void setStatusid(Integer statusid) {
        this.statusid = statusid;
    }

    public String getServicecode() {
        return servicecode;
    }

    public void setServicecode(String servicecode) {
        this.servicecode = servicecode;
    }

    public ServiceCode getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(ServiceCode serviceCode) {
        this.serviceCode = serviceCode;
    }

    public List<Job> getJobs() {
        return jobs;
    }

    public void setJobs(List<Job> jobs) {
        this.jobs = jobs;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public ServiceProvider getServiceProvider() {
        return serviceProvider;
    }

    public void setServiceProvider(ServiceProvider serviceProvider) {
        this.serviceProvider = serviceProvider;
    }
}
