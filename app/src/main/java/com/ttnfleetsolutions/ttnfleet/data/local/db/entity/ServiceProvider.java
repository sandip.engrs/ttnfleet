package com.ttnfleetsolutions.ttnfleet.data.local.db.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by Sandip Chaulagain on 4/4/2018.
 *
 * @version 1.0
 */
@Entity(tableName = "ServiceProvider")
public class ServiceProvider {

    @PrimaryKey
    public long id;

    public String name;

    public String location;

    public boolean isAvailable;

    public boolean requestStatus;

    public ServiceProvider(long id, String name, String location, boolean isAvailable, boolean requestStatus) {
        this.id = id;
        this.name = name;
        this.location = location;
        this.isAvailable = isAvailable;
        this.requestStatus = requestStatus;
    }
}


