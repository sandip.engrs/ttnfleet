package com.ttnfleetsolutions.ttnfleet.data.model.api.event.survey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Sandip Chaulagain on 6/18/2018.
 *
 * @version 1.0
 */
public class SurveyQuestionsResponse {

    @SerializedName("Result")
    @Expose
    private List<Result> result = null;

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

    public class Result implements Serializable {

        @SerializedName("surveyquestionid")
        @Expose
        private Integer surveyquestionid;
        @SerializedName("question")
        @Expose
        private String question;
        @SerializedName("ismandatory")
        @Expose
        private Integer ismandatory;

        private float rating;

        public Integer getSurveyquestionid() {
            return surveyquestionid;
        }

        public void setSurveyquestionid(Integer surveyquestionid) {
            this.surveyquestionid = surveyquestionid;
        }

        public String getQuestion() {
            return question;
        }

        public void setQuestion(String question) {
            this.question = question;
        }

        public Integer getIsmandatory() {
            return ismandatory;
        }

        public void setIsmandatory(Integer ismandatory) {
            this.ismandatory = ismandatory;
        }

        public float getRating() {
            return rating;
        }

        public void setRating(float rating) {
            this.rating = rating;
        }
    }
}
