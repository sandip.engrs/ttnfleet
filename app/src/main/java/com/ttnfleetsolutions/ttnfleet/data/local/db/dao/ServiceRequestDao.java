package com.ttnfleetsolutions.ttnfleet.data.local.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.ServiceRequest;
import com.ttnfleetsolutions.ttnfleet.data.model.ServiceRequestSummary;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 11/10/2017.
 *
 * @version 1.0
 */
@Dao
public interface ServiceRequestDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(ServiceRequest serviceRequest);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<ServiceRequest> serviceRequests);

    @Query("SELECT service_request.*, vehicles.*, location.* FROM service_request INNER JOIN vehicles ON service_request.vehicleID = vehicles.id INNER JOIN location ON service_request.locationID = location.id where status != 2")
    LiveData<List<ServiceRequestSummary>> loadAll();

    @Query("SELECT service_request.*, vehicles.*, location.* FROM service_request INNER JOIN vehicles ON service_request.vehicleID = vehicles.id INNER JOIN location ON service_request.locationID = location.id where status = 2")
    LiveData<List<ServiceRequestSummary>> loadCompleted();

    @Query("SELECT service_request.*, vehicles.*, location.* FROM service_request INNER JOIN vehicles ON service_request.vehicleID = vehicles.id INNER JOIN location ON service_request.locationID = location.id WHERE service_request.id LIKE :id LIMIT 1")
    ServiceRequestSummary getServiceRequestSummary(long id);
}
