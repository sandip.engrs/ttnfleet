package com.ttnfleetsolutions.ttnfleet.data.model.api.event.create;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandip Chaulagain on 6/1/2018.
 *
 * @version 1.0
 */
public class CreateEventResponse {

    @SerializedName("Result")
    @Expose
    private Result result;

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public class Result {

        @SerializedName("locationId")
        @Expose
        private Integer locationId;
        @SerializedName("eventid")
        @Expose
        private Integer eventid;

        public Integer getLocationId() {
            return locationId;
        }

        public void setLocationId(Integer locationId) {
            this.locationId = locationId;
        }

        public Integer getEventid() {
            return eventid;
        }

        public void setEventid(Integer eventid) {
            this.eventid = eventid;
        }

    }
}
