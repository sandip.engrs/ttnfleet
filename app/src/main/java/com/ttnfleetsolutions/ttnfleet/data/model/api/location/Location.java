package com.ttnfleetsolutions.ttnfleet.data.model.api.location;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Sandip Chaulagain on 5/28/2018.
 *
 * @version 1.0
 */
public class Location implements Serializable {


    @SerializedName("locationid")
    @Expose
    private Integer locationid;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("address")
    @Expose
    private String address;

    @SerializedName("userid")
    @Expose
    private Integer userid;
    @SerializedName("locationtypeid")
    @Expose
    private Integer locationtypeid;
    @SerializedName("LocationType")
    @Expose
    private LocationType locationType;

    public Location(String address) {
        this.address = address;
    }

    public Location() {
    }

    public Integer getLocationid() {
        return locationid;
    }

    public void setLocationid(Integer locationid) {
        this.locationid = locationid;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public Integer getLocationtypeid() {
        return locationtypeid;
    }

    public void setLocationtypeid(Integer locationtypeid) {
        this.locationtypeid = locationtypeid;
    }

    public LocationType getLocationType() {
        return locationType;
    }

    public void setLocationType(LocationType locationType) {
        this.locationType = locationType;
    }

    public class LocationType implements Serializable {

        @SerializedName("locationtypeid")
        @Expose
        private Integer locationtypeid;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("eventtypeid")
        @Expose
        private Integer eventtypeid;
        @SerializedName("alwayslive")
        @Expose
        private Integer alwayslive;
        @SerializedName("LocationType")
        @Expose
        private LocationType locationType;

        public Integer getLocationtypeid() {
            return locationtypeid;
        }

        public void setLocationtypeid(Integer locationtypeid) {
            this.locationtypeid = locationtypeid;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public Integer getEventtypeid() {
            return eventtypeid;
        }

        public void setEventtypeid(Integer eventtypeid) {
            this.eventtypeid = eventtypeid;
        }

        public Integer getAlwayslive() {
            return alwayslive;
        }

        public void setAlwayslive(Integer alwayslive) {
            this.alwayslive = alwayslive;
        }

    }
}
