package com.ttnfleetsolutions.ttnfleet.data.model.api.event.survey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandip Chaulagain on 6/18/2018.
 *
 * @version 1.0
 */
public class SubmitSurveyResponse {

    @SerializedName("Result")
    @Expose
    private Result result;

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public class Result {

        @SerializedName("SurveyResultId")
        @Expose
        private Integer surveyResultId;
        @SerializedName("SurveyId")
        @Expose
        private Integer surveyId;

        public Integer getSurveyResultId() {
            return surveyResultId;
        }

        public void setSurveyResultId(Integer surveyResultId) {
            this.surveyResultId = surveyResultId;
        }

        public Integer getSurveyId() {
            return surveyId;
        }

        public void setSurveyId(Integer surveyId) {
            this.surveyId = surveyId;
        }

    }
}
