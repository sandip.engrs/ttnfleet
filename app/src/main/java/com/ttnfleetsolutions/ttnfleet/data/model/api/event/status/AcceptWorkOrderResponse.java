package com.ttnfleetsolutions.ttnfleet.data.model.api.event.status;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 6/8/2018.
 *
 * @version 1.0
 */
public class AcceptWorkOrderResponse {


    @SerializedName("Result")
    @Expose
    private AcceptWorkOrders result;

    public AcceptWorkOrders getResult() {
        return result;
    }

    public void setResult(AcceptWorkOrders result) {
        this.result = result;
    }

    public class AcceptWorkOrders {

        @SerializedName("WorkOredrIds")
        @Expose
        private List<Integer> workOredrIds = null;
        @SerializedName("JobIds")
        @Expose
        private List<Integer> jobIds = null;

        public List<Integer> getWorkOredrIds() {
            return workOredrIds;
        }

        public void setWorkOredrIds(List<Integer> workOredrIds) {
            this.workOredrIds = workOredrIds;
        }

        public List<Integer> getJobIds() {
            return jobIds;
        }

        public void setJobIds(List<Integer> jobIds) {
            this.jobIds = jobIds;
        }

    }
}
