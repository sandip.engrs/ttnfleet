package com.ttnfleetsolutions.ttnfleet.data.model.api.event.status;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sandip Chaulagain on 6/11/2018.
 *
 * @version 1.0
 */
public class RejectionTypesResponse {

    @SerializedName("Result")
    @Expose
    private List<RejectionType> result = null;

    public List<RejectionType> getResult() {
        return result;
    }

    public void setResult(List<RejectionType> result) {
        this.result = result;
    }

    public void addRejectionType(String title, int id) {
        if(result == null) result = new ArrayList<>();
        result.add(new RejectionType(id, title));
    }

    public class RejectionType implements Serializable {

        @SerializedName("reqrejectiontypeid")
        @Expose
        private Integer reqrejectiontypeid;
        @SerializedName("title")
        @Expose
        private String title;

        public RejectionType(Integer reqrejectiontypeid, String title) {
            this.reqrejectiontypeid = reqrejectiontypeid;
            this.title = title;
        }

        public Integer getReqrejectiontypeid() {
            return reqrejectiontypeid;
        }

        public void setReqrejectiontypeid(Integer reqrejectiontypeid) {
            this.reqrejectiontypeid = reqrejectiontypeid;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }
}
