package com.ttnfleetsolutions.ttnfleet.data.model.api.event.create;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 6/8/2018.
 *
 * @version 1.0
 */
public class TruckLoadResponse {

    @SerializedName("Result")
    @Expose
    private List<TruckLoad> result = null;

    public List<TruckLoad> getResult() {
        return result;
    }

    public void setResult(List<TruckLoad> result) {
        this.result = result;
    }

    public void addTruckLoad(int id, String plan) {
        if(getResult() != null) {
            getResult().add(0, new TruckLoad(id, plan));
        }
    }

    public class TruckLoad {

        @SerializedName("LoadId")
        @Expose
        private Integer loadId;
        @SerializedName("LoadPlans")
        @Expose
        private String loadPlans;

        public TruckLoad(Integer loadId, String loadPlans) {
            this.loadId = loadId;
            this.loadPlans = loadPlans;
        }

        public Integer getLoadId() {
            return loadId;
        }

        public void setLoadId(Integer loadId) {
            this.loadId = loadId;
        }

        public String getLoadPlans() {
            return loadPlans;
        }

        public void setLoadPlans(String loadPlans) {
            this.loadPlans = loadPlans;
        }

        @Override
        public String toString() {
            return loadPlans;
        }
    }

}
