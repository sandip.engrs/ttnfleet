package com.ttnfleetsolutions.ttnfleet.data.model.api.event.create;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandip Chaulagain on 6/16/2018.
 *
 * @version 1.0
 */
public class AdditionalJobRequest {

    @SerializedName("WorkorderId")
    @Expose
    private Integer workorderId;
    @SerializedName("EventId")
    @Expose
    private Integer eventId;
    @SerializedName("TechnicianId")
    @Expose
    private Integer technicianId;
    @SerializedName("JobStatusId")
    @Expose
    private Integer jobStatusId;
    @SerializedName("SystemCode")
    @Expose
    private SystemCode systemCode;
    @SerializedName("AdditionalJobNote")
    @Expose
    private String additionalJobNote;

    public Integer getWorkorderId() {
        return workorderId;
    }

    public void setWorkorderId(Integer workorderId) {
        this.workorderId = workorderId;
    }

    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    public Integer getTechnicianId() {
        return technicianId;
    }

    public void setTechnicianId(Integer technicianId) {
        this.technicianId = technicianId;
    }

    public Integer getJobStatusId() {
        return jobStatusId;
    }

    public void setJobStatusId(Integer jobStatusId) {
        this.jobStatusId = jobStatusId;
    }

    public SystemCode getSystemCode() {
        return systemCode;
    }

    public void setSystemCode(SystemCode systemCode) {
        this.systemCode = systemCode;
    }

    public String getAdditionalJobNote() {
        return additionalJobNote;
    }

    public void setAdditionalJobNote(String additionalJobNote) {
        this.additionalJobNote = additionalJobNote;
    }

    public class SystemCode {

        @SerializedName("Code1")
        @Expose
        private String code1;
        @SerializedName("AssemblyCode")
        @Expose
        private AssemblyCode assemblyCode;

        public String getCode1() {
            return code1;
        }

        public void setCode1(String code1) {
            this.code1 = code1;
        }

        public AssemblyCode getAssemblyCode() {
            return assemblyCode;
        }

        public void setAssemblyCode(AssemblyCode assemblyCode) {
            this.assemblyCode = assemblyCode;
        }

        public class AssemblyCode {

            @SerializedName("Code2")
            @Expose
            private String code2;

            public String getCode2() {
                return code2;
            }

            public void setCode2(String code2) {
                this.code2 = code2;
            }

        }
    }
}
