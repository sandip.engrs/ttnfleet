package com.ttnfleetsolutions.ttnfleet.data.model;

/**
 * Created by Sandip Chaulagain on 2/27/2018.
 *
 * @version 1.0
 */

public class NotificationModel {

    private long id;
    private String message;
    private String date;
    private String actionNegative;
    private String actionPositive;
    private boolean isRead;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getActionNegative() {
        return actionNegative;
    }

    public void setActionNegative(String actionNegative) {
        this.actionNegative = actionNegative;
    }

    public String getActionPositive() {
        return actionPositive;
    }

    public void setActionPositive(String actionPositive) {
        this.actionPositive = actionPositive;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }

    public NotificationModel(long id, String message, String date, String actionNegative, String actionPositive, boolean isRead) {
        this.id = id;
        this.message = message;
        this.date = date;
        this.actionNegative = actionNegative;
        this.actionPositive = actionPositive;
        this.isRead = isRead;
    }
}
