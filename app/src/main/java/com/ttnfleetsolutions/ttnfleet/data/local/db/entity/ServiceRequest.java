package com.ttnfleetsolutions.ttnfleet.data.local.db.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by Sandip Chaulagain on 11/10/2017.
 *
 * @version 1.0
 */
@Entity(tableName = "service_request")
public class ServiceRequest {

    @PrimaryKey
    public long id;

    public String serviceType;

    public String name;
    public long vehicleID;
    public long locationID;


    public String serviceIssue;
    public String ETA;
    public String ATA;
    public String ATC;
    public String ETC;

    public String datetime;

    public int status;

    //Drop-off & Pick-up
    public String dropoff;
    public String pickup;

    public ServiceRequest(long id, String serviceType, String name, long vehicleID, long locationID, String serviceIssue, String ETA, String ATA, String ATC, String ETC, String datetime, int status, String dropoff, String pickup) {
        this.id = id;
        this.serviceType = serviceType;
        this.name = name;
        this.vehicleID = vehicleID;
        this.locationID = locationID;
        this.serviceIssue = serviceIssue;
        this.ETA = ETA;
        this.ATA = ATA;
        this.ATC = ATC;
        this.ETC = ETC;
        this.datetime = datetime;
        this.status = status;
        this.dropoff = dropoff;
        this.pickup = pickup;
    }
}
