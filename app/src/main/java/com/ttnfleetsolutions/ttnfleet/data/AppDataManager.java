/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.ttnfleetsolutions.ttnfleet.data;

import android.arch.lifecycle.LiveData;
import android.content.Context;

import com.ttnfleetsolutions.ttnfleet.data.local.db.DbHelper;
import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.Location;
import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.Repair;
import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.ServiceProvider;
import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.ServiceRequest;
import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.User;
import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.Vehicle;
import com.ttnfleetsolutions.ttnfleet.data.local.prefs.PreferencesHelper;
import com.ttnfleetsolutions.ttnfleet.data.model.ServiceRequestSummary;
import com.ttnfleetsolutions.ttnfleet.data.model.api.asset.AssetsListResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.AddNoteResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.AdditionalJobResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.CreateEventResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.DriverListResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.LocationTypeResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.ServiceCodeResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.TruckLoadResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.listing.EventDetailsResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.listing.EventResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.requirements.ImageRequirement;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.requirements.RequireImagesResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.AcceptWorkOrderResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.CompleteJobResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.ConfirmBreakdownResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.DispatchEventResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.EnrouteResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.RejectionTypesResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.survey.SubmitSurveyResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.survey.SurveyQuestionsResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.login.LoginRequest;
import com.ttnfleetsolutions.ttnfleet.data.model.api.login.LoginResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.login.TokenResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.serviceprovider.ServiceProviderListResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.technician.TechnicianListResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.theme.ThemeResponse;
import com.ttnfleetsolutions.ttnfleet.data.remote.ApiHeader;
import com.ttnfleetsolutions.ttnfleet.data.remote.ApiHelper;

import org.json.JSONObject;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

/**
 * Created by Sandip Chaulagain on 10/4/2017.
 * AppDataManager manages operations of local-db, preferences and API Calls.
 * @version 1.0
 */

@Singleton
public class AppDataManager implements DataManager{

    private final Context mContext;
    private final PreferencesHelper mPreferencesHelper;
    private final DbHelper mDbHelper;
    private final ApiHelper mApiHelper;

    @Inject
    AppDataManager(Context context, DbHelper dbHelper, PreferencesHelper preferencesHelper, ApiHelper apiHelper) {
        mContext = context;
        mDbHelper = dbHelper;
        mPreferencesHelper = preferencesHelper;
        mApiHelper = apiHelper;
    }

    //Network
    @Override
    public ApiHeader getApiHeader() {
        return mApiHelper.getApiHeader();
    }

    @Override
    public void updateApiHeader(String accessToken) {
        mApiHelper.getApiHeader().setAccessToken(accessToken);
    }

    @Override
    public Observable<TokenResponse> getAccessTokenApiCall() {
        return mApiHelper.getAccessTokenApiCall();
    }

    @Override
    public Observable<ThemeResponse> getThemeApiCall(String companyId) {
        return mApiHelper.getThemeApiCall(companyId);
    }

    @Override
    public Observable<LoginResponse> doServerLoginApiCall(LoginRequest request) {
        return mApiHelper.doServerLoginApiCall(request);
    }

    @Override
    public Observable<LoginResponse> doServerAuthenticationApiCall(String GUID) {
        return mApiHelper.doServerAuthenticationApiCall(GUID);
    }

    /* DRIVER MODULE APIs */

    @Override
    public Observable<ServiceCodeResponse> doServerGetSystemListApiCall() {
        return mApiHelper.doServerGetSystemListApiCall();
    }

    @Override
    public Observable<ServiceCodeResponse> doServerGetAssemblyListApiCall(String systemCode) {
        return mApiHelper.doServerGetAssemblyListApiCall(systemCode);
    }

    @Override
    public Observable<ServiceCodeResponse> doServerGetComponentListApiCall(String systemCode, String assemblyCode) {
        return mApiHelper.doServerGetComponentListApiCall(systemCode, assemblyCode);
    }

    @Override
    public Observable<TruckLoadResponse> doServerGetTruckLLoadTypesApiCall() {
        return mApiHelper.doServerGetTruckLLoadTypesApiCall();
    }

    @Override
    public Observable<AssetsListResponse> doServerGetAssetsApiCall(int userId) {
        return mApiHelper.doServerGetAssetsApiCall(userId);
    }

    @Override
    public Observable<LocationTypeResponse> doServerLocationTypeApiCall(String eventType, int userId) {
        return mApiHelper.doServerLocationTypeApiCall(eventType, userId);
    }

    @Override
    public Observable<EventResponse> doServerGetEventsApiCall(int userId) {
        return mApiHelper.doServerGetEventsApiCall(userId);
    }

    @Override
    public Observable<CreateEventResponse> doServerCreateEventsApiCall(JSONObject eventRequestData) {
        return mApiHelper.doServerCreateEventsApiCall(eventRequestData);
    }

    @Override
    public Observable<EventResponse> doServerGetClosedEventsApiCall(int userId) {
        return mApiHelper.doServerGetClosedEventsApiCall(userId);
    }

    @Override
    public Observable<SurveyQuestionsResponse> doServerGetSurveyQuestionsApiCall() {
        return mApiHelper.doServerGetSurveyQuestionsApiCall();
    }

    @Override
    public Observable<SubmitSurveyResponse> doServerSubmitSurveyApiCall(JSONObject surveyRequestData) {
        return mApiHelper.doServerSubmitSurveyApiCall(surveyRequestData);
    }

    /* FLEET DISPATCHER MODULE APIs */

    @Override
    public Observable<EventResponse> doServerEventListingFleetDispatcherApiCall(int userId, int eventStatus) {
        return mApiHelper.doServerEventListingFleetDispatcherApiCall(userId, eventStatus);
    }

    @Override
    public Observable<ServiceProviderListResponse> doServerServiceProviderListingApiCall() {
        return mApiHelper.doServerServiceProviderListingApiCall();
    }

    @Override
    public Observable<DispatchEventResponse> doServerDispatchEventApiCall(JSONObject dispatchRequest) {
        return mApiHelper.doServerDispatchEventApiCall(dispatchRequest);
    }

    @Override
    public Observable<DriverListResponse> doServerGetDriverListingApiCall(int companyId) {
        return mApiHelper.doServerGetDriverListingApiCall(companyId);
    }

    @Override
    public Observable<AddNoteResponse> doServerAddNoteFDApiCall(JSONObject addNoteRequest) {
        return mApiHelper.doServerAddNoteFDApiCall(addNoteRequest
        );
    }

    /* SERVICE PROVIDER MODULE APIs */

    @Override
    public Observable<EventResponse> doServerEventListingServiceProviderApiCall(int userId, int eventStatus) {
        return mApiHelper.doServerEventListingServiceProviderApiCall(userId, eventStatus);
    }

    @Override
    public Observable<TechnicianListResponse> doServerTechnicianListingApiCall(int serviceProviderId) {
        return mApiHelper.doServerTechnicianListingApiCall(serviceProviderId);
    }

    @Override
    public Observable<AcceptWorkOrderResponse> doServerAcceptWorkOrderApiCall(JSONObject acceptWorkOrderData) {
        return mApiHelper.doServerAcceptWorkOrderApiCall(acceptWorkOrderData);
    }

    @Override
    public Observable<RejectionTypesResponse> doServerGetRejectTypesApiCall() {
        return mApiHelper.doServerGetRejectTypesApiCall();
    }

    @Override
    public Observable<AcceptWorkOrderResponse> doServerRejectWorkOrderApiCall(JSONObject rejectWorkOrderData) {
        return mApiHelper.doServerRejectWorkOrderApiCall(rejectWorkOrderData);
    }

    @Override
    public Observable<AcceptWorkOrderResponse> doServerAcceptAssignWorkOrderApiCall(JSONObject acceptAssignWorkOrderData) {
        return mApiHelper.doServerAcceptAssignWorkOrderApiCall(acceptAssignWorkOrderData);
    }

    /* TECHNICIAN MODULE APIs */
    @Override
    public Observable<EventResponse> doServerEventListingTechnicianApiCall(int userId, int eventStatus) {
        return mApiHelper.doServerEventListingTechnicianApiCall(userId, eventStatus);
    }

    @Override
    public Observable<AcceptWorkOrderResponse> doServerAcceptRejectJobApiCall(JSONObject acceptRejectJobData) {
        return mApiHelper.doServerAcceptRejectJobApiCall(acceptRejectJobData);
    }

    @Override
    public Observable<RejectionTypesResponse> doServerGetRejectTypesTechApiCall() {
        return mApiHelper.doServerGetRejectTypesTechApiCall();
    }

    @Override
    public Observable<AdditionalJobResponse> doServerCreateAdditionalJobApiCall(JSONObject createAdditionalJobData) {
        return mApiHelper.doServerCreateAdditionalJobApiCall(createAdditionalJobData);
    }

    @Override
    public Observable<CompleteJobResponse> doServerCompleteJobApiCall(JSONObject completeJobRequest) {
        return mApiHelper.doServerCompleteJobApiCall(completeJobRequest);
    }

    @Override
    public Observable<ConfirmBreakdownResponse> doServerConfirmJobApiCall(int jobId) {
        return mApiHelper.doServerConfirmJobApiCall(jobId);
    }

    @Override
    public Observable<EnrouteResponse> doServerEnrouteApiCall(JSONObject enrouteRequest) {
        return mApiHelper.doServerEnrouteApiCall(enrouteRequest);
    }

    @Override
    public Observable<String> doServerUploadRequirementsApiCall(List<ImageRequirement> imageRequirementList, int eventId, int jobId, int userId, String OdometerReading) {
        return mApiHelper.doServerUploadRequirementsApiCall(imageRequirementList, eventId, jobId, userId, OdometerReading);
    }

    /* COMMON APIs */
    @Override
    public Observable<RequireImagesResponse> doServerGetRequirementImagesApiCall(int jobId) {
        return mApiHelper.doServerGetRequirementImagesApiCall(jobId);
    }

    @Override
    public Observable<EventDetailsResponse> doServerGetEventDetailsApiCall(int eventId) {
        return mApiHelper.doServerGetEventDetailsApiCall(eventId);
    }

    @Override
    public String getGUID() {
        return mPreferencesHelper.getGUID();
    }

    @Override
    public void setGUID(String GUID) {
        mPreferencesHelper.setGUID(GUID);
    }

    @Override
    public String getAccessToken() {
        return mPreferencesHelper.getAccessToken();
    }

    @Override
    public void setAccessToken(String accessToken) {
        mPreferencesHelper.setAccessToken(accessToken);
        mApiHelper.getApiHeader().setAccessToken(accessToken);
    }

    //DB Helper Methods
    @Override
    public Observable<Boolean> insertUser(User user) {
        return mDbHelper.insertUser(user);
    }

    @Override
    public Observable<List<User>> getAllUsers() {
        return mDbHelper.getAllUsers();
    }

    /* PREFERENCE HELPER METHODS*/
    @Override
    public int getCurrentUserLoggedInMode() {
        return mPreferencesHelper.getCurrentUserLoggedInMode();
    }

    @Override
    public void setCurrentUserLoggedInMode(LoggedInMode mode) {
        mPreferencesHelper.setCurrentUserLoggedInMode(mode);
    }

    @Override
    public void setCurrentUserRole(Role role) {
        mPreferencesHelper.setCurrentUserRole(role);
    }

    @Override
    public int getCurrentUserRole() {
        return mPreferencesHelper.getCurrentUserRole();
    }

    @Override
    public String getCurrentUserFullName() {
        return mPreferencesHelper.getCurrentUserFullName();
    }

    @Override
    public void setCurrentUserFullName(String fullName) {
        mPreferencesHelper.setCurrentUserFullName(fullName);
    }

    @Override
    public String getCurrentUserRoleName() {
        return mPreferencesHelper.getCurrentUserRoleName();
    }

    @Override
    public void setCurrentUserRoleName(String roleName) {
        mPreferencesHelper.setCurrentUserRoleName(roleName);
    }

    @Override
    public String getCurrentUserProfileImage() {
        return mPreferencesHelper.getCurrentUserProfileImage();
    }

    @Override
    public void setCurrentUserProfileImage(String imageUrl) {
        mPreferencesHelper.setCurrentUserProfileImage(imageUrl);
    }

    @Override
    public String getCurrentUserEmail() {
        return mPreferencesHelper.getCurrentUserEmail();
    }

    @Override
    public void setCurrentUserEmail(String email) {
        mPreferencesHelper.setCurrentUserEmail(email);
    }

    @Override
    public String getCurrentUserPassword() {
        return mPreferencesHelper.getCurrentUserPassword();
    }

    @Override
    public void setCurrentUserPassword(String password) {
        mPreferencesHelper.setCurrentUserPassword(password);
    }

    @Override
    public int getCurrentUserID() {
        return mPreferencesHelper.getCurrentUserID();
    }

    @Override
    public void setCurrentUserID(int userID) {
        mPreferencesHelper.setCurrentUserID(userID);
    }

    @Override
    public boolean isDataAvailable() {
        return mPreferencesHelper.isDataAvailable();
    }

    @Override
    public void setIsDataAdded(boolean added) {
        mPreferencesHelper.setIsDataAdded(added);
    }

    @Override
    public void setUserAsLoggedOut() {
        updateUserInfo(null,
                DataManager.LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT,
                null,
                null,
                null);
    }

    @Override
    public void updateUserInfo(String accessToken, LoggedInMode loggedInMode, String email, String password, Role role) {
        setAccessToken(accessToken);
        setCurrentUserLoggedInMode(loggedInMode);
        setCurrentUserEmail(email);
        setCurrentUserPassword(password);
        setCurrentUserRole(role);
    }

    @Override
    public int getThemeID() {
        return mPreferencesHelper.getThemeID();
    }

    @Override
    public void setThemeID(int id) {
        mPreferencesHelper.setThemeID(id);
    }

    @Override
    public String getThemeLogo() {
        return mPreferencesHelper.getThemeLogo();
    }

    @Override
    public void setThemeLogo(String logo) {
        mPreferencesHelper.setThemeLogo(logo);
    }

    @Override
    public int getThemeStatusBarColor() {
        return mPreferencesHelper.getThemeStatusBarColor();
    }

    @Override
    public void setThemeStatusBarColor(int color) {
        mPreferencesHelper.setThemeStatusBarColor(color);
    }

    @Override
    public int getThemeTitleBarColor() {
        return mPreferencesHelper.getThemeTitleBarColor();
    }

    @Override
    public void setThemeTitleBarColor(int color) {
        mPreferencesHelper.setThemeTitleBarColor(color);
    }

    @Override
    public int getThemeTransparentColor() {
        return mPreferencesHelper.getThemeTransparentColor();
    }

    @Override
    public void setThemeTransparentColor(int color) {
        mPreferencesHelper.setThemeTransparentColor(color);
    }

    @Override
    public String getThemeBgImage() {
        return mPreferencesHelper.getThemeBgImage();
    }

    @Override
    public void setThemeBgImage(String image) {
        mPreferencesHelper.setThemeBgImage(image);
    }

    @Override
    public int getThemeButtonColor() {
        return mPreferencesHelper.getThemeButtonColor();
    }

    @Override
    public void setThemeButtonColor(int color) {
        mPreferencesHelper.setThemeButtonColor(color);
    }

    @Override
    public int getThemeTextColor() {
        return mPreferencesHelper.getThemeTextColor();
    }

    @Override
    public void setThemeTextColor(int color) {
        mPreferencesHelper.setThemeTextColor(color);
    }

    @Override
    public int getThemeCompanyID() {
        return mPreferencesHelper.getThemeCompanyID();
    }

    @Override
    public void setThemeCompanyID(int id) {
        mPreferencesHelper.setThemeCompanyID(id);
    }

    @Override
    public String getCallForHelpLabel() {
        return mPreferencesHelper.getCallForHelpLabel();
    }

    @Override
    public void setCallForHelpLabel(String label) {
        mPreferencesHelper.setCallForHelpLabel(label);
    }

    @Override
    public int getThemeFooterDisplayFlag() {
        return mPreferencesHelper.getThemeFooterDisplayFlag();
    }

    @Override
    public void setThemeFooterDisplayFlag(int flag) {
        mPreferencesHelper.setThemeFooterDisplayFlag(flag);
    }

    @Override
    public int getThemeFooterTextColor() {
        return mPreferencesHelper.getThemeFooterTextColor();
    }

    @Override
    public void setThemeFooterTextColor(int color) {
        mPreferencesHelper.setThemeFooterTextColor(color);
    }

    @Override
    public void setEventRequest(String eventRequest) {
        mPreferencesHelper.setEventRequest(eventRequest);
    }

    @Override
    public String getEventRequest() {
        return mPreferencesHelper.getEventRequest();
    }

    @Override
    public void setAdditionalJobRequest(String additionalJobRequest) {
        mPreferencesHelper.setAdditionalJobRequest(additionalJobRequest);
    }

    @Override
    public String getAdditionalJobRequest() {
        return mPreferencesHelper.getAdditionalJobRequest();
    }

    /* DATABASE HELPER METHODS*/
    @Override
    public Observable<Boolean> insertRepair(Repair repair) {
        return mDbHelper.insertRepair(repair);
    }

    @Override
    public Observable<LiveData<List<Repair>>> getAllRepairs() {
        return mDbHelper.getAllRepairs();
    }

    @Override
    public Observable<Boolean> insertAllRepairs(List<Repair> repairs) {
        return mDbHelper.insertAllRepairs(repairs);
    }

    @Override
    public Observable<Boolean> insertServiceRequest(ServiceRequest serviceRequest) {
        return mDbHelper.insertServiceRequest(serviceRequest);
    }

    @Override
    public Observable<Boolean> insertAllServiceRequests(List<ServiceRequest> serviceRequests) {
        return mDbHelper.insertAllServiceRequests(serviceRequests);
    }

    @Override
    public Observable<LiveData<List<ServiceRequestSummary>>> getAllServiceRequests() {
        return mDbHelper.getAllServiceRequests();
    }

    @Override
    public Observable<LiveData<List<ServiceRequestSummary>>> getCompletedServiceRequests() {
        return mDbHelper.getCompletedServiceRequests();
    }

    @Override
    public Observable<ServiceRequestSummary> getServiceRequestById(long _id) {
        return mDbHelper.getServiceRequestById(_id);
    }

    @Override
    public Observable<Boolean> insertVehicle(Vehicle vehicle) {
        return mDbHelper.insertVehicle(vehicle);
    }

    @Override
    public Observable<LiveData<List<Vehicle>>> getAllVehicles() {
        return mDbHelper.getAllVehicles();
    }

    @Override
    public Observable<LiveData<Vehicle>> getVehicleById(long _id) {
        return mDbHelper.getVehicleById(_id);
    }

    @Override
    public Observable<Vehicle> getVehicleDetailsById(long _id) {
        return mDbHelper.getVehicleDetailsById(_id);
    }

    @Override
    public Observable<Boolean> insertLocation(Location location) {
        return mDbHelper.insertLocation(location);
    }

    @Override
    public Observable<LiveData<List<Location>>> getLocation() {
        return mDbHelper.getLocation();
    }

    @Override
    public Observable<LiveData<Location>> getLocationById(long _id) {
        return mDbHelper.getLocationById(_id);
    }

    @Override
    public Observable<Boolean> insertAllServiceProviders(List<ServiceProvider> serviceProviders) {
        return mDbHelper.insertAllServiceProviders(serviceProviders);
    }

    @Override
    public Observable<LiveData<List<ServiceProvider>>> getAllServiceProviders() {
        return mDbHelper.getAllServiceProviders();
    }

    @Override
    public Observable<Boolean> insertServiceProviders(ServiceProvider serviceProvider) {
        return mDbHelper.insertServiceProviders(serviceProvider);
    }

    @Override
    public int getDriverCount() {
        return mPreferencesHelper.getDriverCount();
    }

    @Override
    public void setDriverCount(int count) {
        mPreferencesHelper.setDriverCount(count);
    }
}

