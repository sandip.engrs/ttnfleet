package com.ttnfleetsolutions.ttnfleet.data.local.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.ttnfleetsolutions.ttnfleet.data.local.db.dao.LocationDao;
import com.ttnfleetsolutions.ttnfleet.data.local.db.dao.RepairDao;
import com.ttnfleetsolutions.ttnfleet.data.local.db.dao.ServiceProviderDao;
import com.ttnfleetsolutions.ttnfleet.data.local.db.dao.ServiceRequestDao;
import com.ttnfleetsolutions.ttnfleet.data.local.db.dao.UserDao;
import com.ttnfleetsolutions.ttnfleet.data.local.db.dao.VehicleDao;
import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.Location;
import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.Repair;
import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.ServiceProvider;
import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.ServiceRequest;
import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.User;
import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.Vehicle;


/**
 * Created by Sandip Chaulagain on 11/2/2017.
 *
 * @version 1.0
 */

@Database(entities = {User.class, Repair.class, ServiceRequest.class, Vehicle.class, Location.class, ServiceProvider.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    public abstract UserDao userDao();

    public abstract RepairDao repairDao();

    public abstract ServiceRequestDao serviceRequestDao();

    public abstract VehicleDao vehicleDao();

    public abstract LocationDao locationDao();

    public abstract ServiceProviderDao serviceProviderDao();


}
