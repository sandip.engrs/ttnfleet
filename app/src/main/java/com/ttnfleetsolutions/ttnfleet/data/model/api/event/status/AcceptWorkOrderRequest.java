package com.ttnfleetsolutions.ttnfleet.data.model.api.event.status;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandip Chaulagain on 6/8/2018.
 *
 * @version 1.0
 */
public class AcceptWorkOrderRequest {


    @SerializedName("EventId")
    @Expose
    private Integer eventId;
    @SerializedName("ServiceProviderId")
    @Expose
    private Integer serviceProviderId;

    public AcceptWorkOrderRequest(Integer eventId, Integer serviceProviderId) {
        this.eventId = eventId;
        this.serviceProviderId = serviceProviderId;
    }

    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    public Integer getServiceProviderId() {
        return serviceProviderId;
    }

    public void setServiceProviderId(Integer serviceProviderId) {
        this.serviceProviderId = serviceProviderId;
    }

}
