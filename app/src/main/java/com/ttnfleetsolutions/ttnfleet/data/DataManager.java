package com.ttnfleetsolutions.ttnfleet.data;

import com.ttnfleetsolutions.ttnfleet.data.local.db.DbHelper;
import com.ttnfleetsolutions.ttnfleet.data.local.prefs.PreferencesHelper;
import com.ttnfleetsolutions.ttnfleet.data.remote.ApiHelper;


public interface DataManager extends DbHelper, PreferencesHelper, ApiHelper {

    void updateApiHeader(String accessToken);

    void setUserAsLoggedOut();

    void updateUserInfo(
            String accessToken,
            LoggedInMode loggedInMode,
            String email,
            String password,
            Role role);

    enum LoggedInMode {

        LOGGED_IN_MODE_LOGGED_OUT(0),
        LOGGED_IN_MODE_SERVER(3);

        private final int mType;

        LoggedInMode(int type) {
            mType = type;
        }

        public int getType() {
            return mType;
        }
    }

    enum FilterDashboardHistory {
        DASHBOARD(1),
        HISTORY(2),
        HISTORY_FD(4),
        NEW(2),
        ACTIVE(3);

        public final int mType;

        FilterDashboardHistory(int type) {
            mType = type;
        }

        public int getType() {
            return mType;
        }
    }

    enum Role {
        TRUCKER(1),
        FLEET_DISPATCHER(2),
        SERVICE_DISPATCHER(3),
        SERVICE_REPRESENTATIVE(4);

        public final int mType;

        Role(int type) {
            mType = type;
        }

        public int getType() {
            return mType;
        }
    }

    enum EventStatus {
        REQUESTED(1),
        ACTIVE(2),
        COMPLETE(3),
        CLOSED(4),
        CANCELED(5);

        private final int mStatus;

        EventStatus(int status) {
            mStatus = status;
        }

        public final int getStatus() {
            return mStatus;
        }
    }

    enum WorkOrderStatus {
        PENDING_DISPATCH_APPROVAL(1),
        PENDING_DISPATCH(2),
        PENDING_REDISPATCH(3),
        PROVIDER_SEARCH_IN_PROCESS(4),
        SCHEDULED(5),
        DISPATCHED_PENDING_ENROUTE_CONFIRMATION(6),
        DISPATCHED_ENROUTE_CONFIRMED(7),
        AT_PROVIDER_AWAITING_BAY(8),
        IN_PROVIDER_BAY_PENDING_DIAGNOSIS(9),
        ONSITE_CONFIRMED_PENDING_DIAGNOSIS(10),
        DIAGNOSIS_CONFIRMED_PENDING_AUTHORIZATION(11),
        SECONDARY_DIAGNOSIS_PENDING(12),
        AT_PROVIDER_AWAITING_PARTS(13),
        ONSITE_DELAYED_AWAITING_PARTS(14),
        AT_PROVIDER_REPAIRS_ONGOING(15),
        ONSITE_REPAIRS_ONGING(16),
        AT_PROVIDER_AWAITING_FINAL_TESTING(17),
        ONSITE_AWAITING_FINAL_TESTING(18),
        AT_PROVIDER_AWAITING_FINAL_TESTING_(19),
        COMPLETE_PENDING_REQUIREMENTS_OR_PAPERWORK(20),
        INCOMPLETE_CUSTODY_TRANSFERRED(21),
        COMPLETE_AWAITING_INVOICE(22),
        COMPLETE_AWAITING_ACCOUNTING(23),
        CLOSED_SENT_TO_ACCOUNTING(24),
        CLOSED_WITHOUT_FEE(25);

        private final int mStatus;

        WorkOrderStatus(int status) {
            mStatus = status;
        }

        public int getStatus() {
            return mStatus;
        }
    }

    enum JobOrderStatus {
        APPROVED_NOT_STARTED(1),
        REPAIR_APPROVAL_REQUESTED(2),
        AWATING_PROVIDER_ESTIMATE(3),
        AWAITING_CUSTOMER_APPROVAL(4),
        DELAYED_AWAITING_PARTS(5),
        DELAYED_AWAITING_DIAGNOSIS(6),
        REPAIR_ONGOING(7),
        REPAIR_COMPLETE_REQUEST_CLOSE(8),
        APPROVED_TO_CLOSE(9),
        APPROVED_TO_PAY(10),
        DISPUTED(11),
        CANCELED(12),
        DENIED_DO_NOT_REPAIR(13),
        DENIED_PEND_FUTURE_REPAIR(14),
        PROVIDER_REFUSAL(15);

        private final int mStatus;

        JobOrderStatus(int status) {
            mStatus = status;
        }

        public int getStatus() {
            return mStatus;
        }
    }

    enum Status {
        DISPATCH(1),
        ACCEPT_REJECT_SP(4),
        ACCEPT_REJECT_TECH(1),
        ACCEPTED_SHOW_ENROUTE(2),
        ENROUTED(18),
        REACHED(19),
        CONFIRMED_BREAKDOWN(20),
        COMPLETED(10),
        REQUESTED(22),
        ASSIGN(25),
        ACCEPT_ASSIGN(26),
        ACCEPTED(27),
        REJECTED(28);
        private final int mStatus;

        Status(int status) {
            mStatus = status;
        }

        public int getStatus() {
            return mStatus;
        }
    }

    public enum ErrorState {
        NO_DATA,
        API_FAIL,
        NETWORK_ERROR
    }
}
