package com.ttnfleetsolutions.ttnfleet.data.model.api.event.status;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandip Chaulagain on 6/8/2018.
 *
 * @version 1.0
 */
public class RejectWorkOrderRequest {

    @SerializedName("UserId")
    @Expose
    private Integer userId;
    @SerializedName("EventId")
    @Expose
    private Integer eventId;
    @SerializedName("WorkorderId")
    @Expose
    private Integer workorderId;
    @SerializedName("RejectionDescription")
    @Expose
    private String rejectionDescription;
    @SerializedName("RejectionTypeId")
    @Expose
    private Integer rejectionTypeId;
    @SerializedName("WorkOrderStatusId")
    @Expose
    private Integer workOrderStatusId;

    public RejectWorkOrderRequest(Integer userId, Integer eventId, Integer workorderId, String rejectionDescription, Integer rejectionTypeId, Integer workOrderStatusId) {
        this.userId = userId;
        this.eventId = eventId;
        this.workorderId = workorderId;
        this.rejectionDescription = rejectionDescription;
        this.rejectionTypeId = rejectionTypeId;
        this.workOrderStatusId = workOrderStatusId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    public Integer getWorkorderId() {
        return workorderId;
    }

    public void setWorkorderId(Integer workorderId) {
        this.workorderId = workorderId;
    }

    public String getRejectionDescription() {
        return rejectionDescription;
    }

    public void setRejectionDescription(String rejectionDescription) {
        this.rejectionDescription = rejectionDescription;
    }

    public Integer getRejectionTypeId() {
        return rejectionTypeId;
    }

    public void setRejectionTypeId(Integer rejectionTypeId) {
        this.rejectionTypeId = rejectionTypeId;
    }

    public Integer getWorkOrderStatusId() {
        return workOrderStatusId;
    }

    public void setWorkOrderStatusId(Integer workOrderStatusId) {
        this.workOrderStatusId = workOrderStatusId;
    }

}
