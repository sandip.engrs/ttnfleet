/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.ttnfleetsolutions.ttnfleet.data.remote;

import com.ttnfleetsolutions.ttnfleet.BuildConfig;

/**
 * Created by Sandip Chaulagain on 5/18/2018.
 *  URL endpoints and image dir URLs which use to make network calls.
 * @version 1.0
 */

public class ApiEndPoint {

    static final String ENDPOINT_SERVER_GET_TOKEN = BuildConfig.BASE_URL + "api/Users/GetToken";

    /**
     * IMAGE URL
     */

    public static final String DIR_IMAGE_VMRS_PARENT = "Icons/VMRSParent/";

    public static final String DIR_IMAGE_TECHNICIAN = "Icons/Technicians/";

    public static final String DIR_IMAGE_THEME_LOGO = BuildConfig.BASE_URL + "Theme/Logo/";

    public static final String DIR_IMAGE_THEME_BG = BuildConfig.BASE_URL + "Theme/BackGround/";

    public static final String DIR_IMAGE_REQUIREMENT = BuildConfig.BASE_URL + "JobImages/";

    public static final String DIR_IMAGE_PROFILE = BuildConfig.BASE_URL + "ProfileImage/";

    /**
     * APP THEME
     */

    public static final String ENDPOINT_SERVER_GET_APP_THEME = BuildConfig.BASE_URL + "api/Appthemes/GetAppthemes/";

    /**
     * AUTHENTICATION
     */

    public static final String ENDPOINT_SERVER_LOGIN = BuildConfig.BASE_URL;

    /**
     * DRIVER MODEULE APIs
     */

    public static final String ENDPOINT_SERVER_GET_LOCATION_TYPE = BuildConfig.BASE_URL + "api/Locationtypes/GetLocationType/";

    public static final String ENDPOINT_SERVER_GET_SYSTEM_LIST_TABLE = BuildConfig.BASE_URL + "api/Servicecodes/GetServiceCodes/";

    public static final String ENDPOINT_SERVER_GET_ASSEMBLY_LIST_TABLE = BuildConfig.BASE_URL + "api/Servicecodes/GetSubServiceCodes/";

    public static final String ENDPOINT_SERVER_GET_COMPONENT_LIST_TABLE = BuildConfig.BASE_URL + "api/Servicecodes/GetIssueCodes/";

    public static final String ENDPOINT_SERVER_GET_TRUCK_LOAD_TYPES = BuildConfig.BASE_URL + "api/TruckLoadTypes/GetTruckLoadTypes/";

    public static final String ENDPOINT_SERVER_GET_ASSETS = BuildConfig.BASE_URL + "api/Vehicles/GetVehicles/";

    public static final String ENDPOINT_SERVER_GET_SURVEY_QUESTIONS = BuildConfig.BASE_URL + "api/Surveys/GetSurveyQuestions/";

    public static final String ENDPOINT_SERVER_SUBMIT_SURVEY = BuildConfig.BASE_URL + "api/Surveys/SubmitSurvey/";

    public static final String ENDPOINT_SERVER_CREATE_EVENT = BuildConfig.BASE_URL + "api/Activeevents/CreateEvent/";

    public static final String ENDPOINT_SERVER_GET_ACTIVE_EVENTS = BuildConfig.BASE_URL + "api/Activeevents/GetActiveEvents/";

    public static final String ENDPOINT_SERVER_GET_CLOSED_EVENTS = BuildConfig.BASE_URL + "api/Activeevents/GetClosedEvents/";


    /**
     * FLEET DISPACTHER MODEULE APIs
     */

    public static final String ENDPOINT_SERVER_GET_EVENTS_FD = BuildConfig.BASE_URL + "api/Activeevents/GetActiveEventsForFD/";

    public static final String ENDPOINT_SERVER_GET_SERVICE_PROVIDER = BuildConfig.BASE_URL + "api/Serviceproviders/GetSecviceProviders/";

    public static final String ENDPOINT_SERVER_DISPATCH_EVENT = BuildConfig.BASE_URL + "api//Workorders/AssignServiceProvider/";

    public static final String ENDPOINT_SERVER_GET_DRIVERS = BuildConfig.BASE_URL + "api/Users/GetDriverList/";

    public static final String ENDPOINT_SERVER_ADD_NOTE_FD = BuildConfig.BASE_URL + "api/Activeevents/AddFDNote/";


    /**
     * SERVICE PROVIDER MODEULE APIs
     */

    public static final String ENDPOINT_SERVER_GET_EVENTS_SP = BuildConfig.BASE_URL + "api/Activeevents/GetActiveEventsForSP/";

    public static final String ENDPOINT_SERVER_GET_TECHNICIANS = BuildConfig.BASE_URL + "api/Technicians/GetTechnicians/";

    public static final String ENDPOINT_SERVER_ACCEPT_WORK_ORDER = BuildConfig.BASE_URL + "api/Workorders/AcceptWorkOrder/";

    public static final String ENDPOINT_SERVER_ACCEPT_ASSIGN_WORK_ORDER = BuildConfig.BASE_URL + "api//Workorders/AssignAndAccept";

    public static final String ENDPOINT_SERVER_REJECT_TYPES = BuildConfig.BASE_URL + "api/Reqrejectiontypes/GetRejectionTypes/";

    public static final String ENDPOINT_SERVER_REJECT_WORK_ORDER = BuildConfig.BASE_URL + "api/WorkOrders/RejectWorkOrder/";

    /**
     * TECHNICIAN MODEULE APIs
     */

    public static final String ENDPOINT_SERVER_GET_EVENTS_TECH = BuildConfig.BASE_URL + "api/Activeevents/GetActiveEventsForTechnician/";

    public static final String ENDPOINT_SERVER_ACCEPT_REJECT_JOB = BuildConfig.BASE_URL + "api/Jobs/AcceptOrRejectJob/";

    public static final String ENDPOINT_SERVER_REJECT_TYPES_TECH = BuildConfig.BASE_URL + "api/Reqrejectiontypes/GetRejectionTypesForTechnician/";

    public static final String ENDPOINT_SERVER_CREATE_ADDITIONAL_JOB = BuildConfig.BASE_URL + "api/Jobs/CreateAdditionalJob/";

    public static final String ENDPOINT_SERVER_CONFIRM_BREAKDOWN = BuildConfig.BASE_URL + "api/Jobs/ConfirmBreakdown/";

    public static final String ENDPOINT_SERVER_COMPLETE_JOB = BuildConfig.BASE_URL + "api/Jobs/CompleteJob/";

    public static final String ENDPOINT_SERVER_ENROUTE = BuildConfig.BASE_URL + "api/Jobs/Enroute/";

    public static final String ENDPOINT_UPLOAD_REQUIREMENTS = BuildConfig.BASE_URL + "UploadMultipart/";

    /**
     * GENERAL APIs
     */
    public static final String ENDPOINT_SERVER_GET_EVENT_DETAILS = BuildConfig.BASE_URL + "api/Activeevents/GetActiveEventDetails/";

    public static final String ENDPOINT_SERVER_GET_REQUIREMENTS_PHOTOS = BuildConfig.BASE_URL + "api/Eventphotos/GetPhotosList/";

    private ApiEndPoint() {
        // This class is not publicly instantiable
    }

}
