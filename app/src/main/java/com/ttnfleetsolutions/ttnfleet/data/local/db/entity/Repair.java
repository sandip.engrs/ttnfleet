package com.ttnfleetsolutions.ttnfleet.data.local.db.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by Sandip Chaulagain on 11/10/2017.
 *
 * @version 1.0
 */
@Entity(tableName = "repairs")
public class Repair {

    @PrimaryKey
    public long id;

    public String name;

    public String VMRS;

    public String time;
    public String duration;
    public String cost;
    public String desc;

    public int approvalStatus;

    public String parts;

    public Repair(long id, String name, String time, String duration, String cost, int approvalStatus, String parts) {
        this.id = id;
        this.name = name;
        this.VMRS = "";
        this.time = time;
        this.duration = duration;
        this.cost = cost;
        this.desc = "";
        this.approvalStatus = approvalStatus;
        this.parts = parts;
    }
}
