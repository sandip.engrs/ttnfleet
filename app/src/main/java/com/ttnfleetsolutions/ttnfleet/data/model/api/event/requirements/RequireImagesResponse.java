package com.ttnfleetsolutions.ttnfleet.data.model.api.event.requirements;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 6/26/2018.
 *
 * @version 1.0
 */
public class RequireImagesResponse {

    @SerializedName("Result")
    @Expose
    private Result result;

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public class Result {

        @SerializedName("Images")
        @Expose
        private List<Image> images = null;
        @SerializedName("OdometerReading")
        @Expose
        private String odometerReading;

        public List<Image> getImages() {
            return images;
        }

        public void setImages(List<Image> images) {
            this.images = images;
        }

        public String getOdometerReading() {
            return odometerReading;
        }

        public void setOdometerReading(String odometerReading) {
            this.odometerReading = odometerReading;
        }

    }

    public class Image {

        @SerializedName("eventphototypeid")
        @Expose
        private Integer eventphototypeid;
        @SerializedName("photoname")
        @Expose
        private String photoname;
        @SerializedName("odometerreading")
        @Expose
        private String odometerreading;

        public Integer getEventphototypeid() {
            return eventphototypeid;
        }

        public void setEventphototypeid(Integer eventphototypeid) {
            this.eventphototypeid = eventphototypeid;
        }

        public String getPhotoname() {
            return photoname;
        }

        public void setPhotoname(String photoname) {
            this.photoname = photoname;
        }

        public String getOdometerreading() {
            return odometerreading;
        }

        public void setOdometerreading(String odometerreading) {
            this.odometerreading = odometerreading;
        }

    }
}
