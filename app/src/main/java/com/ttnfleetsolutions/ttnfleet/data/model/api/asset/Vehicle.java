package com.ttnfleetsolutions.ttnfleet.data.model.api.asset;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandip Chaulagain on 5/29/2018.
 *
 * @version 1.0
 */
public class Vehicle {


    @SerializedName("vehicleid")
    @Expose
    private Integer vehicleid;
    @SerializedName("userid")
    @Expose
    private Integer userid;
    @SerializedName("vehiclename")
    @Expose
    private String vehiclename;
    @SerializedName("manufacturingyear")
    @Expose
    private Integer manufacturingyear;
    @SerializedName("brand")
    @Expose
    private String brand;
    @SerializedName("licenseplateno")
    @Expose
    private String licenseplateno;
    @SerializedName("fueltype")
    @Expose
    private String fueltype;
    @SerializedName("configurationcode")
    @Expose
    private String configurationcode;
    @SerializedName("vin")
    @Expose
    private String vin;

    public Vehicle(Integer vehicleid, String vehiclename) {
        this.vehicleid = vehicleid;
        this.vehiclename = vehiclename;
    }

    public Integer getVehicleid() {
        return vehicleid;
    }

    public void setVehicleid(Integer vehicleid) {
        this.vehicleid = vehicleid;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getVehiclename() {
        return vehiclename;
    }

    public void setVehiclename(String vehiclename) {
        this.vehiclename = vehiclename;
    }

    public Integer getManufacturingyear() {
        return manufacturingyear;
    }

    public void setManufacturingyear(Integer manufacturingyear) {
        this.manufacturingyear = manufacturingyear;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getLicenseplateno() {
        return licenseplateno;
    }

    public void setLicenseplateno(String licenseplateno) {
        this.licenseplateno = licenseplateno;
    }

    public String getFueltype() {
        return fueltype;
    }

    public void setFueltype(String fueltype) {
        this.fueltype = fueltype;
    }

    public String getConfigurationcode() {
        return configurationcode;
    }

    public void setConfigurationcode(String configurationcode) {
        this.configurationcode = configurationcode;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    @Override
    public String toString() {
        return vehiclename;
    }
}
