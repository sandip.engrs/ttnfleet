package com.ttnfleetsolutions.ttnfleet.data.model.api.event.listing;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ttnfleetsolutions.ttnfleet.data.model.api.asset.Vehicle;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.TruckLoadResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.location.Location;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 5/25/2018.
 *
 * @version 1.0
 */
public class Event {

    @SerializedName("eventid")
    @Expose
    private Integer eventid;
    @SerializedName("userid")
    @Expose
    private Integer userid;
    @SerializedName("vehicleid")
    @Expose
    private Integer vehicleid;
    @SerializedName("fleetdispacterid")
    @Expose
    private Object fleetdispacterid;
    @SerializedName("locationid")
    @Expose
    private Integer locationid;
    @SerializedName("statusid")
    @Expose
    private Integer statusid;
    @SerializedName("eventtypeid")
    @Expose
    private Integer eventtypeid;
    @SerializedName("vehicleloaded")
    @Expose
    private int vehicleloaded;
    @SerializedName("breakdownnote")
    @Expose
    private String breakdownnote;
    @SerializedName("fdnote")
    @Expose
    private String fdnote;
    @SerializedName("rta")
    @Expose
    private String rta;
    @SerializedName("rtc")
    @Expose
    private String rtc;
    @SerializedName("createdby")
    @Expose
    private Object createdby;
    @SerializedName("createddatetime")
    @Expose
    private String createddatetime;
    @SerializedName("lastupdateddatetime")
    @Expose
    private String lastupdateddatetime;
    @SerializedName("compeleteddatetime")
    @Expose
    private Object compeleteddatetime;
    @SerializedName("EventType")
    @Expose
    private EventType eventType;
    @SerializedName("WorkOrders")
    @Expose
    private List<WorkOrder> workOrders = null;
    @SerializedName("Status")
    @Expose
    private Status status;
    @SerializedName("Location")
    @Expose
    private Location location;
    @SerializedName("Vehicle")
    @Expose
    private Vehicle vehicle;


    @SerializedName("VehicleLoad")
    @Expose
    private TruckLoadResponse.TruckLoad vehicleLoad;

    private boolean isSelected;

    public Integer getEventid() {
        return eventid;
    }

    public void setEventid(Integer eventid) {
        this.eventid = eventid;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public Integer getVehicleid() {
        return vehicleid;
    }

    public void setVehicleid(Integer vehicleid) {
        this.vehicleid = vehicleid;
    }

    public Object getFleetdispacterid() {
        return fleetdispacterid;
    }

    public void setFleetdispacterid(Object fleetdispacterid) {
        this.fleetdispacterid = fleetdispacterid;
    }

    public Integer getLocationid() {
        return locationid;
    }

    public void setLocationid(Integer locationid) {
        this.locationid = locationid;
    }

    public Integer getStatusid() {
        return statusid;
    }

    public void setStatusid(Integer statusid) {
        this.statusid = statusid;
    }

    public Integer getEventtypeid() {
        return eventtypeid;
    }

    public void setEventtypeid(Integer eventtypeid) {
        this.eventtypeid = eventtypeid;
    }

    public int getVehicleloaded() {
        return vehicleloaded;
    }

    public void setVehicleloaded(int vehicleloaded) {
        this.vehicleloaded = vehicleloaded;
    }

    public String getBreakdownnote() {
        return breakdownnote;
    }

    public void setBreakdownnote(String breakdownnote) {
        this.breakdownnote = breakdownnote;
    }

    public String getFdnote() {
        return fdnote;
    }

    public void setFdnote(String fdnote) {
        this.fdnote = fdnote;
    }

    public String getRta() {
        return rta;
    }

    public void setRta(String rta) {
        this.rta = rta;
    }

    public String getRtc() {
        return rtc;
    }

    public void setRtc(String rtc) {
        this.rtc = rtc;
    }

    public Object getCreatedby() {
        return createdby;
    }

    public void setCreatedby(Object createdby) {
        this.createdby = createdby;
    }

    public String getCreateddatetime() {
        return createddatetime;
    }

    public void setCreateddatetime(String createddatetime) {
        this.createddatetime = createddatetime;
    }

    public String getLastupdateddatetime() {
        return lastupdateddatetime;
    }

    public void setLastupdateddatetime(String lastupdateddatetime) {
        this.lastupdateddatetime = lastupdateddatetime;
    }

    public Object getCompeleteddatetime() {
        return compeleteddatetime;
    }

    public void setCompeleteddatetime(Object compeleteddatetime) {
        this.compeleteddatetime = compeleteddatetime;
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public List<WorkOrder> getWorkOrders() {
        return workOrders;
    }

    public void setWorkOrders(List<WorkOrder> workOrders) {
        this.workOrders = workOrders;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public TruckLoadResponse.TruckLoad getVehicleLoad() {
        return vehicleLoad;
    }

    public void setVehicleLoad(TruckLoadResponse.TruckLoad vehicleLoad) {
        this.vehicleLoad = vehicleLoad;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
