package com.ttnfleetsolutions.ttnfleet.data.local.db.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by Sandip Chaulagain on 1/11/2018.
 *
 * @version 1.0
 */
@Entity(tableName = "location")
public class Location {

    @PrimaryKey
    public long id;

    public double latitude;
    public double longitude;

    public String address;

    public Location(long id, double latitude, double longitude, String address) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.address = address;
    }
}
