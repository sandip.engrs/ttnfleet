package com.ttnfleetsolutions.ttnfleet.data.model.api.theme;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 5/23/2018.
 *
 * @version 1.0
 */
public class ThemeResponse {

    @SerializedName("Result")
    @Expose
    private List<Theme> result = null;

    public List<Theme> getResult() {
        return result;
    }

    public void setResult(List<Theme> result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "ThemeResponse{" +
                "result=" + result +
                '}';
    }
}
