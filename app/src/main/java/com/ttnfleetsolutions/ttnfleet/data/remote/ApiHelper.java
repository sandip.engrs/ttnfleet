/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.ttnfleetsolutions.ttnfleet.data.remote;

import com.ttnfleetsolutions.ttnfleet.data.model.api.asset.AssetsListResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.AddNoteResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.AdditionalJobResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.CreateEventResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.DriverListResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.LocationTypeResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.ServiceCodeResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.TruckLoadResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.listing.EventDetailsResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.listing.EventResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.requirements.ImageRequirement;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.requirements.RequireImagesResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.AcceptWorkOrderResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.CompleteJobResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.ConfirmBreakdownResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.DispatchEventResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.EnrouteResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.RejectionTypesResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.survey.SubmitSurveyResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.survey.SurveyQuestionsResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.login.LoginRequest;
import com.ttnfleetsolutions.ttnfleet.data.model.api.login.LoginResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.login.TokenResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.serviceprovider.ServiceProviderListResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.technician.TechnicianListResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.theme.ThemeResponse;

import org.json.JSONObject;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Sandip Chaulagain on 5/18/2018.
 * ApiHelper contains networks calls definition with Request and Response data types.
 * @version 1.0
 */

public interface ApiHelper {

    ApiHeader getApiHeader();

    Observable<LoginResponse> doServerLoginApiCall(LoginRequest request);

    Observable<LoginResponse> doServerAuthenticationApiCall(String GUID);

    Observable<TokenResponse> getAccessTokenApiCall();

    Observable<ThemeResponse> getThemeApiCall(String userId);

    /* DRIVER MODULE APIs */

    Observable<ServiceCodeResponse> doServerGetSystemListApiCall();

    Observable<ServiceCodeResponse> doServerGetAssemblyListApiCall(String systemCode);

    Observable<ServiceCodeResponse> doServerGetComponentListApiCall(String systemCode, String assemblyCode);

    Observable<TruckLoadResponse> doServerGetTruckLLoadTypesApiCall();

    Observable<AssetsListResponse> doServerGetAssetsApiCall(int userId);

    Observable<LocationTypeResponse> doServerLocationTypeApiCall(String eventType, int userId);

    Observable<EventResponse> doServerGetEventsApiCall(int userId);

    Observable<CreateEventResponse> doServerCreateEventsApiCall(JSONObject eventRequestData);

    Observable<EventResponse> doServerGetClosedEventsApiCall(int userId);

    Observable<SurveyQuestionsResponse> doServerGetSurveyQuestionsApiCall();

    Observable<SubmitSurveyResponse> doServerSubmitSurveyApiCall(JSONObject surveyRequestData);

    /* FLEET DISPATCHER MODULE APIs */

    Observable<EventResponse> doServerEventListingFleetDispatcherApiCall(int userId, int eventStatus);

    Observable<ServiceProviderListResponse> doServerServiceProviderListingApiCall();

    Observable<DispatchEventResponse> doServerDispatchEventApiCall(JSONObject dispatchRequest);

    Observable<DriverListResponse> doServerGetDriverListingApiCall(int companyId);

    Observable<AddNoteResponse> doServerAddNoteFDApiCall(JSONObject addNoteRequest);

    /* SERVICE PROVIDER MODULE APIs */

    Observable<EventResponse> doServerEventListingServiceProviderApiCall(int userId, int eventStatus);

    Observable<TechnicianListResponse> doServerTechnicianListingApiCall(int serviceProviderId);

    Observable<AcceptWorkOrderResponse> doServerAcceptWorkOrderApiCall(JSONObject acceptWorkOrderData);

    Observable<AcceptWorkOrderResponse> doServerAcceptAssignWorkOrderApiCall(JSONObject acceptAssignWorkOrderData);

    Observable<RejectionTypesResponse> doServerGetRejectTypesApiCall();

    Observable<AcceptWorkOrderResponse> doServerRejectWorkOrderApiCall(JSONObject rejectWorkOrderData);

    /* TECHNICIAN MODULE APIs */

    Observable<EventResponse> doServerEventListingTechnicianApiCall(int userId, int eventStatus);

    Observable<AcceptWorkOrderResponse> doServerAcceptRejectJobApiCall(JSONObject acceptRejectJobData);

    Observable<RejectionTypesResponse> doServerGetRejectTypesTechApiCall();

    Observable<AdditionalJobResponse> doServerCreateAdditionalJobApiCall(JSONObject createAdditionalJobData);

    Observable<CompleteJobResponse> doServerCompleteJobApiCall(JSONObject completeJobRequest);

    Observable<ConfirmBreakdownResponse> doServerConfirmJobApiCall(int jobId);

    Observable<EnrouteResponse> doServerEnrouteApiCall(JSONObject enrouteRequest);

    Observable<String> doServerUploadRequirementsApiCall(List<ImageRequirement> imageRequirementList, int eventId, int jobId, int userId, String OdometerReading);

    /* COMMON APIs */

    Observable<EventDetailsResponse> doServerGetEventDetailsApiCall(int eventId);

    Observable<RequireImagesResponse> doServerGetRequirementImagesApiCall(int jobId);
}
