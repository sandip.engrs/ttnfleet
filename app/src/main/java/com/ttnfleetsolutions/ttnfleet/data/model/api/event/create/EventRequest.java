package com.ttnfleetsolutions.ttnfleet.data.model.api.event.create;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class EventRequest implements Serializable {

    @SerializedName("eventid")
    private Integer eventId;

    @SerializedName("eventtypeid")
    private Integer eventType;

    @SerializedName("userid")
    private Integer userId;

    @SerializedName("vehicleid")
    private Integer assetId;

    @SerializedName("statusid")
    private Integer statusId;

    @SerializedName("createdby")
    private Integer createdby;

    //location
    @SerializedName("locationid")
    private Integer locationid;

    @SerializedName("locationtypeid")
    private Integer locationTypeId;

    @SerializedName("alwayslive")
    private Integer alwayslive;

    @SerializedName("latitude")
    private String latitude;

    @SerializedName("longitude")
    private String longitude;

    @SerializedName("address")
    private String address;

    private String addressNote;

    private String street1;

    private String street2;

    private String city;

    private String state;

    //codes
    @SerializedName("systemcode")
    @Expose
    private List<SystemCode> systemCodeList;

    @SerializedName("vehicleloaded")
    private int loadId;

    @SerializedName("breakdownnote")
    private String remarks;

    @SerializedName("rta")
    private String RTA;

    @SerializedName("rtc")
    private String RTC;

    public int checkIfSystemCodeExists(String systemCode) {
        if (systemCodeList != null && systemCodeList.size() > 0)
            for (int i = 0; i < systemCodeList.size(); i++) {
                if (systemCode.equalsIgnoreCase(systemCodeList.get(i).getCode1()))
                    return i;
            }
        return -1;
    }

    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    public Integer getEventType() {
        return eventType;
    }

    public void setEventType(Integer eventType) {
        this.eventType = eventType;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getAssetId() {
        return assetId == null ? 0 : assetId;
    }

    public void setAssetId(Integer assetId) {
        this.assetId = assetId;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusid) {
        this.statusId = statusid;
    }

    public Integer getCreatedby() {
        return createdby;
    }

    public void setCreatedby(Integer createdby) {
        this.createdby = createdby;
    }

    public Integer getLocationid() {
        return locationid;
    }

    public void setLocationid(Integer locationid) {
        this.locationid = locationid;
    }

    public Integer getLocationTypeId() {
        return locationTypeId;
    }

    public void setLocationTypeId(Integer locationTypeId) {
        this.locationTypeId = locationTypeId;
    }

    public Integer getAlwayslive() {
        return alwayslive;
    }

    public void setAlwayslive(Integer alwayslive) {
        this.alwayslive = alwayslive;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddressNote() {
        return addressNote;
    }

    public void setAddressNote(String addressNote) {
        this.addressNote = addressNote;
    }

    public String getStreet1() {
        return street1;
    }

    public void setStreet1(String street1) {
        this.street1 = street1;
    }

    public String getStreet2() {
        return street2;
    }

    public void setStreet2(String street2) {
        this.street2 = street2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public List<SystemCode> getSystemCodeList() {
        return systemCodeList;
    }

    public void setSystemCodeList(List<SystemCode> systemCodeList) {
        this.systemCodeList = systemCodeList;
    }

    public int getLoadId() {
        return loadId;
    }

    public void setLoadId(int loadId) {
        this.loadId = loadId;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getRTA() {
        return RTA;
    }

    public void setRTA(String RTA) {
        this.RTA = RTA;
    }

    public String getRTC() {
        return RTC;
    }

    public void setRTC(String RTC) {
        this.RTC = RTC;
    }
}
