package com.ttnfleetsolutions.ttnfleet.data.model.api.event.listing;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ttnfleetsolutions.ttnfleet.data.model.api.technician.Profile;

/**
 * Created by Sandip Chaulagain on 7/20/2018.
 *
 * @version 1.0
 */
public class Technicians {

    @SerializedName("technicianid")
    @Expose
    private Integer technicianid;
    @SerializedName("serviceproviderid")
    @Expose
    private Integer serviceproviderid;
    @SerializedName("locationid")
    @Expose
    private Integer locationid;
    @SerializedName("availabilitystatusid")
    @Expose
    private Integer availabilitystatusid;
    @SerializedName("userid")
    @Expose
    private Integer userid;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("User")
    @Expose
    private Profile user;

    public Integer getTechnicianid() {
        return technicianid;
    }

    public void setTechnicianid(Integer technicianid) {
        this.technicianid = technicianid;
    }

    public Integer getServiceproviderid() {
        return serviceproviderid;
    }

    public void setServiceproviderid(Integer serviceproviderid) {
        this.serviceproviderid = serviceproviderid;
    }

    public Integer getLocationid() {
        return locationid;
    }

    public void setLocationid(Integer locationid) {
        this.locationid = locationid;
    }

    public Integer getAvailabilitystatusid() {
        return availabilitystatusid;
    }

    public void setAvailabilitystatusid(Integer availabilitystatusid) {
        this.availabilitystatusid = availabilitystatusid;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Profile getUser() {
        return user;
    }

    public void setUser(Profile user) {
        this.user = user;
    }
}
