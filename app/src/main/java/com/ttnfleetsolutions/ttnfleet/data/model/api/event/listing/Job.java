package com.ttnfleetsolutions.ttnfleet.data.model.api.event.listing;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.ServiceCode;

/**
 * Created by Sandip Chaulagain on 6/11/2018.
 *
 * @version 1.0
 */
public class Job {

    @SerializedName("jobid")
    @Expose
    private Integer jobid;
    @SerializedName("workorderid")
    @Expose
    private Integer workorderid;
    @SerializedName("technicianid")
    @Expose
    private Integer technicianid;
    @SerializedName("eventid")
    @Expose
    private Integer eventid;
    @SerializedName("vmrs")
    @Expose
    private String vmrs;
    @SerializedName("isadditional")
    @Expose
    private Integer isadditional;
    @SerializedName("ata")
    @Expose
    private Object ata;
    @SerializedName("atc")
    @Expose
    private Object atc;
    @SerializedName("partslist")
    @Expose
    private Object partslist;
    @SerializedName("materiallist")
    @Expose
    private Object materiallist;
    @SerializedName("labourcharges")
    @Expose
    private Object labourcharges;
    @SerializedName("jobnotes")
    @Expose
    private String jobnotes;
    @SerializedName("updatedby")
    @Expose
    private Object updatedby;
    @SerializedName("updatedtime")
    @Expose
    private String updatedtime;
    @SerializedName("statusid")
    @Expose
    private Integer statusid;
    @SerializedName("isactual")
    @Expose
    private Object isactual;
    @SerializedName("servicecode")
    @Expose
    private Integer servicecode;
    @SerializedName("Status")
    @Expose
    private Status status;
    @SerializedName("Technicians")
    @Expose
    private Technicians technicians;
    @SerializedName("ServiceCode")
    @Expose
    private ServiceCode serviceCode;

    public Integer getJobid() {
        return jobid;
    }

    public void setJobid(Integer jobid) {
        this.jobid = jobid;
    }

    public Integer getWorkorderid() {
        return workorderid;
    }

    public void setWorkorderid(Integer workorderid) {
        this.workorderid = workorderid;
    }

    public Integer getTechnicianid() {
        return technicianid;
    }

    public void setTechnicianid(Integer technicianid) {
        this.technicianid = technicianid;
    }

    public Integer getEventid() {
        return eventid;
    }

    public void setEventid(Integer eventid) {
        this.eventid = eventid;
    }

    public String getVmrs() {
        return vmrs;
    }

    public void setVmrs(String vmrs) {
        this.vmrs = vmrs;
    }

    public Integer getIsadditional() {
        return isadditional;
    }

    public void setIsadditional(Integer isadditional) {
        this.isadditional = isadditional;
    }

    public Object getAta() {
        return ata;
    }

    public void setAta(Object ata) {
        this.ata = ata;
    }

    public Object getAtc() {
        return atc;
    }

    public void setAtc(Object atc) {
        this.atc = atc;
    }

    public Object getPartslist() {
        return partslist;
    }

    public void setPartslist(Object partslist) {
        this.partslist = partslist;
    }

    public Object getMateriallist() {
        return materiallist;
    }

    public void setMateriallist(Object materiallist) {
        this.materiallist = materiallist;
    }

    public Object getLabourcharges() {
        return labourcharges;
    }

    public void setLabourcharges(Object labourcharges) {
        this.labourcharges = labourcharges;
    }

    public String getJobnotes() {
        return jobnotes;
    }

    public void setJobnotes(String jobnotes) {
        this.jobnotes = jobnotes;
    }

    public Object getUpdatedby() {
        return updatedby;
    }

    public void setUpdatedby(Object updatedby) {
        this.updatedby = updatedby;
    }

    public String getUpdatedtime() {
        return updatedtime;
    }

    public void setUpdatedtime(String updatedtime) {
        this.updatedtime = updatedtime;
    }

    public Integer getStatusid() {
        return statusid;
    }

    public void setStatusid(Integer statusid) {
        this.statusid = statusid;
    }

    public Object getIsactual() {
        return isactual;
    }

    public void setIsactual(Object isactual) {
        this.isactual = isactual;
    }

    public Integer getServicecode() {
        return servicecode;
    }

    public void setServicecode(Integer servicecode) {
        this.servicecode = servicecode;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Technicians getTechnicians() {
        return technicians;
    }

    public void setTechnicians(Technicians technicians) {
        this.technicians = technicians;
    }

    public ServiceCode getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(ServiceCode serviceCode) {
        this.serviceCode = serviceCode;
    }

}
