package com.ttnfleetsolutions.ttnfleet.data.model.api.technician;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 5/28/2018.
 *
 * @version 1.0
 */
public class TechnicianListResponse {

    @SerializedName("Result")
    @Expose
    private List<Technician> result = null;

    public List<Technician> getResult() {
        return result;
    }

    public void setResult(List<Technician> result) {
        this.result = result;
    }

}
