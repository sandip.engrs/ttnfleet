package com.ttnfleetsolutions.ttnfleet.data.model.api.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandip Chaulagain on 5/18/2018.
 *
 * @version 1.0
 */
public class TokenResponse {

    @SerializedName("Result")
    @Expose
    private String result;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
