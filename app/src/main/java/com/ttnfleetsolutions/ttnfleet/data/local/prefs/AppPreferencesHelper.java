/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.ttnfleetsolutions.ttnfleet.data.local.prefs;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;

import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.di.PreferenceInfo;

import javax.inject.Inject;

/**
 * Created by Sandip Chaulagain on 10/3/2017.
 *
 * @version 1.0
 */

public class AppPreferencesHelper implements com.ttnfleetsolutions.ttnfleet.data.local.prefs.PreferencesHelper {

    private static final String PREF_KEY_GUID = "PREF_KEY_GUID";

    private static final String PREF_KEY_USER_LOGGED_IN_MODE = "PREF_KEY_USER_LOGGED_IN_MODE";
    private static final String PREF_KEY_CURRENT_USER_FULL_NAME = "PREF_KEY_CURRENT_USER_FULL_NAME";
    private static final String PREF_KEY_CURRENT_USER_ROLE_NAME = "PREF_KEY_CURRENT_USER_ROLE_NAME";
    private static final String PREF_KEY_CURRENT_USER_PROGILE_IMAGE = "PREF_KEY_CURRENT_USER_PROGILE_IMAGE";
    private static final String PREF_KEY_CURRENT_USER_EMAIL = "PREF_KEY_CURRENT_USER_EMAIL";
    private static final String PREF_KEY_CURRENT_USER_PASSWORD = "PREF_KEY_CURRENT_USER_PASSWORD";
    private static final String PREF_KEY_CURRENT_USER_ID = "PREF_KEY_CURRENT_USER_ID";
    private static final String PREF_KEY_USER_ROLE = "PREF_KEY_USER_ROLE";
    private static final String PREF_KEY_DRIVER_LOGIN_COUNT = "PREF_KEY_DRIVER_LOGIN_COUNT";
    private static final String PREF_KEY_DATA_AVAILABLE = "PREF_KEY_DATA_AVAILABLE";
    private static final String PREF_KEY_ACCESS_TOKEN = "PREF_KEY_ACCESS_TOKEN";

    //THEME
    private static final String PREF_KEY_THEME_ID = "PREF_KEY_THEME_ID";
    private static final String PREF_KEY_THEME_LOGO = "PREF_KEY_THEME_LOGO";
    private static final String PREF_KEY_THEME_STATUS_BAR_COLOR = "PREF_KEY_THEME_STATUS_BAR_COLOR";
    private static final String PREF_KEY_THEME_TITLE_BAR_COLOR = "PREF_KEY_THEME_TITLE_BAR_COLOR";
    private static final String PREF_KEY_THEME_TRANSPARENT_COLOR = "PREF_KEY_THEME_TRANSPARENT_COLOR";
    private static final String PREF_KEY_THEME_BG_IMAGE = "PREF_KEY_THEME_BG_IMAGE";
    private static final String PREF_KEY_THEME_BUTTON_COLOR = "PREF_KEY_THEME_BUTTON_COLOR";
    private static final String PREF_KEY_THEME_TEXT_COLOR = "PREF_KEY_THEME_TEXT_COLOR";
    private static final String PREF_KEY_THEME_COMPANY_ID = "PREF_KEY_THEME_COMPANY_ID";
    private static final String PREF_KEY_CALL_FOR_HELP = "PREF_KEY_CALL_FOR_HELP";
    private static final String PREF_KEY_THEME_FOOTER_DISPLAY_FLAG = "PREF_KEY_THEME_FOOTER_DISPLAY_FLAG";
    private static final String PREF_KEY_THEME_FOOTER_TEXT_COLOR = "PREF_KEY_THEME_FOOTER_TEXT_COLOR";

    //Event
    private static final String PREF_KEY_EVENT_REQUEST = "PREF_KEY_EVENT_REQUEST";
    private static final String PREF_KEY_ADDITIONAL_JOB_REQUEST = "PREF_KEY_ADDITIONAL_JOB_REQUEST";

    private final SharedPreferences mPrefs;

    @Inject
    public AppPreferencesHelper(Context context,
                                @PreferenceInfo String prefFileName) {
        mPrefs = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
    }

    @Override
    public String getGUID() {
        return mPrefs.getString(PREF_KEY_GUID, "");
    }

    @Override
    public void setGUID(String GUID) {
        mPrefs.edit().putString(PREF_KEY_GUID, GUID).apply();
    }

    @Override
    public String getAccessToken() {
        return mPrefs.getString(PREF_KEY_ACCESS_TOKEN, null);
    }

    @Override
    public void setAccessToken(String accessToken) {
        mPrefs.edit().putString(PREF_KEY_ACCESS_TOKEN, accessToken).apply();
    }

    @Override
    public int getCurrentUserLoggedInMode() {
        return mPrefs.getInt(PREF_KEY_USER_LOGGED_IN_MODE,
                DataManager.LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT.getType());
    }

    @Override
    public void setCurrentUserLoggedInMode(DataManager.LoggedInMode mode) {
        mPrefs.edit().putInt(PREF_KEY_USER_LOGGED_IN_MODE, mode.getType()).apply();
    }

    @Override
    public String getCurrentUserFullName() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_FULL_NAME, "");
    }

    @Override
    public void setCurrentUserFullName(String fullName) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_FULL_NAME, fullName).apply();
    }

    @Override
    public String getCurrentUserRoleName() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_ROLE_NAME, "");
    }

    @Override
    public void setCurrentUserRoleName(String roleName) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_ROLE_NAME, roleName).apply();
    }

    @Override
    public String getCurrentUserProfileImage() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_PROGILE_IMAGE, "");
    }

    @Override
    public void setCurrentUserProfileImage(String imageUrl) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_PROGILE_IMAGE, imageUrl).apply();
    }

    @Override
    public String getCurrentUserEmail() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_EMAIL, null);
    }

    @Override
    public void setCurrentUserEmail(String email) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_EMAIL, email).apply();
    }

    @Override
    public String getCurrentUserPassword() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_PASSWORD, null);
    }

    @Override
    public void setCurrentUserPassword(String password) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_PASSWORD, password).apply();
    }

    @Override
    public int getCurrentUserID() {
        return mPrefs.getInt(PREF_KEY_CURRENT_USER_ID, 0);
    }

    @Override
    public void setCurrentUserID(int userID) {
        mPrefs.edit().putInt(PREF_KEY_CURRENT_USER_ID, userID).apply();
    }

    @Override
    public int getCurrentUserRole() {
        return mPrefs.getInt(PREF_KEY_USER_ROLE, -1);
    }

    @Override
    public void setCurrentUserRole(DataManager.Role role) {
        mPrefs.edit().putInt(PREF_KEY_USER_ROLE, role.getType()).apply();
    }

    @Override
    public int getDriverCount() {
        return mPrefs.getInt(PREF_KEY_DRIVER_LOGIN_COUNT, -1);
    }

    @Override
    public void setDriverCount(int count) {
        mPrefs.edit().putInt(PREF_KEY_DRIVER_LOGIN_COUNT, count).apply();
    }

    @Override
    public boolean isDataAvailable() {
        return mPrefs.getBoolean(PREF_KEY_DATA_AVAILABLE, false);
    }

    @Override
    public void setIsDataAdded(boolean added) {
        mPrefs.edit().putBoolean(PREF_KEY_DATA_AVAILABLE, added).apply();
    }

    /** APP THEME */
    @Override
    public int getThemeID() {
        return mPrefs.getInt(PREF_KEY_THEME_ID, 0);
    }

    @Override
    public void setThemeID(int id) {
        mPrefs.edit().putInt(PREF_KEY_THEME_ID, id).apply();
    }

    @Override
    public String getThemeLogo() {
        return mPrefs.getString(PREF_KEY_THEME_LOGO, "");
    }

    @Override
    public void setThemeLogo(String logo) {
        mPrefs.edit().putString(PREF_KEY_THEME_LOGO, logo).apply();
    }

    @Override
    public int getThemeStatusBarColor() {
        return mPrefs.getInt(PREF_KEY_THEME_STATUS_BAR_COLOR, Color.parseColor("#2a865f"));
    }

    @Override
    public void setThemeStatusBarColor(int color) {
        mPrefs.edit().putInt(PREF_KEY_THEME_STATUS_BAR_COLOR, color).apply();
    }

    @Override
    public int getThemeTitleBarColor() {
        return mPrefs.getInt(PREF_KEY_THEME_TITLE_BAR_COLOR, Color.parseColor("#34a975"));
    }

    @Override
    public void setThemeTitleBarColor(int color) {
        mPrefs.edit().putInt(PREF_KEY_THEME_TITLE_BAR_COLOR, color).apply();
    }

    @Override
    public int getThemeTransparentColor() {
        return mPrefs.getInt(PREF_KEY_THEME_TRANSPARENT_COLOR, Color.parseColor("#4D34a975"));
    }

    @Override
    public void setThemeTransparentColor(int color) {
        mPrefs.edit().putInt(PREF_KEY_THEME_TRANSPARENT_COLOR, color).apply();
    }

    @Override
    public String getThemeBgImage() {
        return mPrefs.getString(PREF_KEY_THEME_BG_IMAGE, "");
    }

    @Override
    public void setThemeBgImage(String image) {
        mPrefs.edit().putString(PREF_KEY_THEME_BG_IMAGE, image).apply();
    }

    @Override
    public int getThemeButtonColor() {
        return mPrefs.getInt(PREF_KEY_THEME_BUTTON_COLOR, 0);
    }

    @Override
    public void setThemeButtonColor(int color) {
        mPrefs.edit().putInt(PREF_KEY_THEME_BUTTON_COLOR, color).apply();
    }

    @Override
    public int getThemeTextColor() {
        return mPrefs.getInt(PREF_KEY_THEME_TEXT_COLOR, Color.parseColor("#212121"));
    }

    @Override
    public void setThemeTextColor(int color) {
        mPrefs.edit().putInt(PREF_KEY_THEME_TEXT_COLOR, color).apply();
    }

    @Override
    public int getThemeCompanyID() {
        return mPrefs.getInt(PREF_KEY_THEME_COMPANY_ID, 0);
    }

    @Override
    public void setThemeCompanyID(int id) {
        mPrefs.edit().putInt(PREF_KEY_THEME_COMPANY_ID, id).apply();
    }

    @Override
    public String getCallForHelpLabel() {
        return mPrefs.getString(PREF_KEY_CALL_FOR_HELP, "Call for help");
    }

    @Override
    public void setCallForHelpLabel(String label) {
        mPrefs.edit().putString(PREF_KEY_CALL_FOR_HELP, label).apply();
    }

    @Override
    public int getThemeFooterDisplayFlag() {
        return mPrefs.getInt(PREF_KEY_THEME_FOOTER_DISPLAY_FLAG, 1);
    }

    @Override
    public void setThemeFooterDisplayFlag(int flag) {
        mPrefs.edit().putInt(PREF_KEY_THEME_FOOTER_DISPLAY_FLAG, flag).apply();
    }

    @Override
    public int getThemeFooterTextColor() {
        return mPrefs.getInt(PREF_KEY_THEME_FOOTER_TEXT_COLOR, Color.WHITE);
    }

    @Override
    public void setThemeFooterTextColor(int color) {
        mPrefs.edit().putInt(PREF_KEY_THEME_FOOTER_TEXT_COLOR, color).apply();
    }

    /** Event Request */
    @Override
    public String getEventRequest() {
        return mPrefs.getString(PREF_KEY_EVENT_REQUEST, "");
    }

    @Override
    public void setEventRequest(String eventRequest) {
        mPrefs.edit().putString(PREF_KEY_EVENT_REQUEST, eventRequest).apply();
    }

    /** Additional Job Request */
    @Override
    public void setAdditionalJobRequest(String additionalJobRequest) {
        mPrefs.edit().putString(PREF_KEY_ADDITIONAL_JOB_REQUEST, additionalJobRequest).apply();
    }

    @Override
    public String getAdditionalJobRequest() {
        return mPrefs.getString(PREF_KEY_ADDITIONAL_JOB_REQUEST, "");
    }
}
