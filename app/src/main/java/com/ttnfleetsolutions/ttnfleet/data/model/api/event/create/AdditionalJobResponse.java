package com.ttnfleetsolutions.ttnfleet.data.model.api.event.create;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandip Chaulagain on 6/16/2018.
 *
 * @version 1.0
 */
public class AdditionalJobResponse {

    @SerializedName("Result")
    @Expose
    private Result result;

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public class Result {

        @SerializedName("jobid")
        @Expose
        private Integer jobid;
        @SerializedName("workorderid")
        @Expose
        private Integer workorderid;
        @SerializedName("technicianid")
        @Expose
        private Integer technicianid;
        @SerializedName("eventid")
        @Expose
        private Integer eventid;
        @SerializedName("vmrs")
        @Expose
        private String vmrs;
        @SerializedName("isadditional")
        @Expose
        private Integer isadditional;
        @SerializedName("statusid")
        @Expose
        private Integer statusid;
        @SerializedName("LastUpdatedDatetime")
        @Expose
        private String lastUpdatedDatetime;

        public Integer getJobid() {
            return jobid;
        }

        public void setJobid(Integer jobid) {
            this.jobid = jobid;
        }

        public Integer getWorkorderid() {
            return workorderid;
        }

        public void setWorkorderid(Integer workorderid) {
            this.workorderid = workorderid;
        }

        public Integer getTechnicianid() {
            return technicianid;
        }

        public void setTechnicianid(Integer technicianid) {
            this.technicianid = technicianid;
        }

        public Integer getEventid() {
            return eventid;
        }

        public void setEventid(Integer eventid) {
            this.eventid = eventid;
        }

        public String getVmrs() {
            return vmrs;
        }

        public void setVmrs(String vmrs) {
            this.vmrs = vmrs;
        }

        public Integer getIsadditional() {
            return isadditional;
        }

        public void setIsadditional(Integer isadditional) {
            this.isadditional = isadditional;
        }

        public Integer getStatusid() {
            return statusid;
        }

        public void setStatusid(Integer statusid) {
            this.statusid = statusid;
        }

        public String getLastUpdatedDatetime() {
            return lastUpdatedDatetime;
        }

        public void setLastUpdatedDatetime(String lastUpdatedDatetime) {
            this.lastUpdatedDatetime = lastUpdatedDatetime;
        }
    }
}
