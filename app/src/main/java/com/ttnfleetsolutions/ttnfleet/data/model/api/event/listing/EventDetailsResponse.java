package com.ttnfleetsolutions.ttnfleet.data.model.api.event.listing;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandip Chaulagain on 5/25/2018.
 *
 * @version 1.0
 */
public class EventDetailsResponse {

    @SerializedName("Result")
    @Expose
    private Event result;

    public Event getResult() {
        return result;
    }

    public void setResult(Event result) {
        this.result = result;
    }

}
