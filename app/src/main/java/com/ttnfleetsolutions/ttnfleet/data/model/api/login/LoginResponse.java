package com.ttnfleetsolutions.ttnfleet.data.model.api.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandip Chaulagain on 5/18/2018.
 *
 * @version 1.0
 */
public class LoginResponse {

    @SerializedName("UserId")
    @Expose
    private Integer userId;
    @SerializedName("UserType")
    @Expose
    private Integer userType;
    @SerializedName("UserTypeTitle")
    @Expose
    private String userTypeTitle;
    @SerializedName("CompanyId")
    @Expose
    private Integer companyId;
    @SerializedName("Image")
    @Expose
    private String image;
    @SerializedName("FullName")
    @Expose
    private String fullName;
    @SerializedName("token")
    @Expose
    private String token;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public String getUserTypeTitle() {
        return userTypeTitle;
    }

    public void setUserTypeTitle(String userTypeTitle) {
        this.userTypeTitle = userTypeTitle;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
