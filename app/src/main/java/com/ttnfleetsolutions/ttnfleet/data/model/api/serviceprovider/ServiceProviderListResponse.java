package com.ttnfleetsolutions.ttnfleet.data.model.api.serviceprovider;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 6/14/2018.
 *
 * @version 1.0
 */
public class ServiceProviderListResponse {

    @SerializedName("Result")
    @Expose
    private List<ServiceProvider> result = null;

    public List<ServiceProvider> getResult() {
        return result;
    }

    public void setResult(List<ServiceProvider> result) {
        this.result = result;
    }
}
