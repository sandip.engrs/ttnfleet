package com.ttnfleetsolutions.ttnfleet.data.model.api.event.create;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandip Chaulagain on 7/9/2018.
 *
 * @version 1.0
 */
public class AddNoteRequest {

    @SerializedName("EventId")
    @Expose
    private Integer eventId;
    @SerializedName("FleetDispatcherNote")
    @Expose
    private String fleetDispatcherNote;
    @SerializedName("FleetDispacterId")
    @Expose
    private Integer fleetDispacterId;

    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    public String getFleetDispatcherNote() {
        return fleetDispatcherNote;
    }

    public void setFleetDispatcherNote(String fleetDispatcherNote) {
        this.fleetDispatcherNote = fleetDispatcherNote;
    }

    public Integer getFleetDispacterId() {
        return fleetDispacterId;
    }

    public void setFleetDispacterId(Integer fleetDispacterId) {
        this.fleetDispacterId = fleetDispacterId;
    }
}
