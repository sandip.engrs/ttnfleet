package com.ttnfleetsolutions.ttnfleet.data.model.api.serviceprovider;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ttnfleetsolutions.ttnfleet.data.model.api.location.Location;

/**
 * Created by Sandip Chaulagain on 6/14/2018.
 *
 * @version 1.0
 */
public class ServiceProvider {

    @SerializedName("serviceproviderid")
    @Expose
    private Integer serviceproviderid;
    @SerializedName("locationid")
    @Expose
    private Integer locationid;
    @SerializedName("userid")
    @Expose
    private Integer userid;
    @SerializedName("spcontracttypeid")
    @Expose
    private Object spcontracttypeid;
    @SerializedName("User")
    @Expose
    private Profile user;
    @SerializedName("Location")
    @Expose
    private Location location;

    public Integer getServiceproviderid() {
        return serviceproviderid;
    }

    public void setServiceproviderid(Integer serviceproviderid) {
        this.serviceproviderid = serviceproviderid;
    }

    public Integer getLocationid() {
        return locationid;
    }

    public void setLocationid(Integer locationid) {
        this.locationid = locationid;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public Object getSpcontracttypeid() {
        return spcontracttypeid;
    }

    public void setSpcontracttypeid(Object spcontracttypeid) {
        this.spcontracttypeid = spcontracttypeid;
    }

    public Profile getUser() {
        return user;
    }

    public void setUser(Profile user) {
        this.user = user;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
