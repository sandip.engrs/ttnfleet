package com.ttnfleetsolutions.ttnfleet.data.model.api.login;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandip Chaulagain on 5/18/2018.
 *
 * @version 1.0
 */
public class LoginRequest {

    @SerializedName("userName")
    private String userName;
    @SerializedName("password")
    private String password;

    public LoginRequest(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }
}
