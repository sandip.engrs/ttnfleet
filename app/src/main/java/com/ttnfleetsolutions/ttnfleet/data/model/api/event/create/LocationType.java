package com.ttnfleetsolutions.ttnfleet.data.model.api.event.create;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ttnfleetsolutions.ttnfleet.data.model.api.location.Location;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Sandip Chaulagain on 5/21/2018.
 *
 * @version 1.0
 */
public class LocationType implements Serializable {

    @SerializedName("locationtypeid")
    @Expose
    private Integer locationtypeid;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("eventtypeid")
    @Expose
    private Integer eventtypeid;
    @SerializedName("alwayslive")
    @Expose
    private Integer alwayslive;
    @SerializedName("Locations")
    @Expose
    private List<Location> locations = null;

    public Integer getLocationtypeid() {
        return locationtypeid;
    }

    public void setLocationtypeid(Integer locationtypeid) {
        this.locationtypeid = locationtypeid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getEventtypeid() {
        return eventtypeid;
    }

    public void setEventtypeid(Integer eventtypeid) {
        this.eventtypeid = eventtypeid;
    }

    public Integer getAlwayslive() {
        return alwayslive;
    }

    public void setAlwayslive(Integer alwayslive) {
        this.alwayslive = alwayslive;
    }

    public List<Location> getLocations() {
        return locations;
    }

    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }
}
