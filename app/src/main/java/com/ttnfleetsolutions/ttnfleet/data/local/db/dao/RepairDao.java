package com.ttnfleetsolutions.ttnfleet.data.local.db.dao;


import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.Repair;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 11/10/2017.
 *
 * @version 1.0
 */
@Dao
public interface RepairDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Repair repair);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Repair> repairs);

    @Query("SELECT * FROM repairs")
    LiveData<List<Repair>> loadAll();
}
