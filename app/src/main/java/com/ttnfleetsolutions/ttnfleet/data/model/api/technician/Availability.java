package com.ttnfleetsolutions.ttnfleet.data.model.api.technician;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandip Chaulagain on 5/28/2018.
 *
 * @version 1.0
 */
public class Availability {

    @SerializedName("availabilitystatusid")
    @Expose
    private Integer availabilitystatusid;
    @SerializedName("title")
    @Expose
    private String title;

    public Integer getAvailabilitystatusid() {
        return availabilitystatusid;
    }

    public void setAvailabilitystatusid(Integer availabilitystatusid) {
        this.availabilitystatusid = availabilitystatusid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
