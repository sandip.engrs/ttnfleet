package com.ttnfleetsolutions.ttnfleet.data.local.db.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by Sandip Chaulagain on 1/11/2018.
 *
 * @version 1.0
 */
@Entity(tableName = "vehicles")
public class Vehicle {

    @PrimaryKey
    public long id;

    public String vehicle_name;
    public String vin;
    public String licence;
    public String brand;
    public String year;
    public String fuelType;

    public Vehicle(long id, String vehicle_name, String vin, String licence, String brand, String year, String fuelType) {
        this.id = id;
        this.vehicle_name = vehicle_name;
        this.vin = vin;
        this.licence = licence;
        this.brand = brand;
        this.year = year;
        this.fuelType = fuelType;
    }
}
