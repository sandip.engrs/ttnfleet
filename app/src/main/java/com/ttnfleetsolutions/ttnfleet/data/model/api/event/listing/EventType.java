package com.ttnfleetsolutions.ttnfleet.data.model.api.event.listing;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandip Chaulagain on 6/2/2018.
 *
 * @version 1.0
 */
public class EventType {

    @SerializedName("eventtypeid")
    @Expose
    private Integer eventtypeid;
    @SerializedName("title")
    @Expose
    private String title = "";

    public Integer getEventtypeid() {
        return eventtypeid;
    }

    public void setEventtypeid(Integer eventtypeid) {
        this.eventtypeid = eventtypeid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
