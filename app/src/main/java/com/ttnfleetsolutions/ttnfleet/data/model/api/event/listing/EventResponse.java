package com.ttnfleetsolutions.ttnfleet.data.model.api.event.listing;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 5/25/2018.
 *
 * @version 1.0
 */
public class EventResponse {

    @SerializedName("Result")
    @Expose
    private List<Event> result = null;

    public List<Event> getResult() {
        return result;
    }

    public void setResult(List<Event> result) {
        this.result = result;
    }
}
