package com.ttnfleetsolutions.ttnfleet.data.model.api.asset;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 6/28/2018.
 *
 * @version 1.0
 */
public class AssetsListResponse {

    @SerializedName("Result")
    @Expose
    private List<Vehicle> result = null;

    public List<Vehicle> getResult() {
        return result;
    }

    public void setResult(List<Vehicle> result) {
        this.result = result;
    }

    public void addAsset(int id, String vehiclename) {
        if(getResult() != null) {
            getResult().add(0, new Vehicle(id, vehiclename));
        }
    }
}
