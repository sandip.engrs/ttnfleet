package com.ttnfleetsolutions.ttnfleet.data.local.db;


import android.arch.lifecycle.LiveData;


import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.Location;
import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.Repair;
import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.ServiceProvider;
import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.ServiceRequest;
import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.User;
import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.Vehicle;
import com.ttnfleetsolutions.ttnfleet.data.model.ServiceRequestSummary;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Sandip Chaulagain on 11/2/2017.
 *
 * @version 1.0
 */

public interface DbHelper {

    Observable<Boolean> insertUser(final User user);

    Observable<List<User>> getAllUsers();

    Observable<Boolean> insertRepair(final Repair repair);

    Observable<Boolean> insertAllRepairs(final List<Repair> repairs);

    Observable<LiveData<List<Repair>>> getAllRepairs();

    Observable<Boolean> insertServiceRequest(final ServiceRequest serviceRequest);

    Observable<Boolean> insertAllServiceRequests(final List<ServiceRequest> serviceRequests);

    Observable<LiveData<List<ServiceRequestSummary>>> getAllServiceRequests();

    Observable<LiveData<List<ServiceRequestSummary>>> getCompletedServiceRequests();

    Observable<ServiceRequestSummary> getServiceRequestById(long _id);

    Observable<Boolean> insertVehicle(final Vehicle vehicle);

    Observable<LiveData<List<Vehicle>>> getAllVehicles();

    Observable<LiveData<Vehicle>> getVehicleById(long _id);

    Observable<Vehicle> getVehicleDetailsById(long _id);

    Observable<Boolean> insertLocation(final Location location);

    Observable<LiveData<List<Location>>> getLocation();

    Observable<LiveData<Location>> getLocationById(long _id);

    Observable<Boolean> insertAllServiceProviders(final List<ServiceProvider> serviceProviders);

    Observable<Boolean> insertServiceProviders(final ServiceProvider serviceProvider);

    Observable<LiveData<List<ServiceProvider>>> getAllServiceProviders();
}
