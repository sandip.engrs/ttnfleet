package com.ttnfleetsolutions.ttnfleet.ui.trucker.vehicle.details;

import android.arch.lifecycle.LiveData;

import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.Vehicle;

/**
 * Created by Sandip Chaulagain on 1/10/2018.
 *
 * @version 1.0
 */

interface VehicleDetailsNavigator {

    void updateVehicleDetails(LiveData<Vehicle> liveData);

    void updateVehicleDetails(Vehicle vehicle);

    void handleError(Throwable throwable);

    void vehicleAdded();
}
