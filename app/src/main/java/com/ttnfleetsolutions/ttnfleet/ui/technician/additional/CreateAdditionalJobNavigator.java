package com.ttnfleetsolutions.ttnfleet.ui.technician.additional;

/**
 * Created by Sandip Chaulagain on 1/10/2018.
 *
 * @version 1.0
 */

public interface CreateAdditionalJobNavigator {

    void handleError(Throwable throwable);

}
