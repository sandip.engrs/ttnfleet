package com.ttnfleetsolutions.ttnfleet.ui.fleet;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.request.RequestOptions;
import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.BR;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.AddNoteResponse;
import com.ttnfleetsolutions.ttnfleet.databinding.ActivityFleetDispatcherDashboardBinding;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseActivity;
import com.ttnfleetsolutions.ttnfleet.data.model.api.location.Location;
import com.ttnfleetsolutions.ttnfleet.ui.chat.ChatFragment;
import com.ttnfleetsolutions.ttnfleet.ui.chat.MainChatFragment;
import com.ttnfleetsolutions.ttnfleet.ui.chat.ServiceDetailsChatFragment;
import com.ttnfleetsolutions.ttnfleet.ui.fleet.provider.ServiceProviderFragment;
import com.ttnfleetsolutions.ttnfleet.ui.fleet.services.note.AddNoteFragment;
import com.ttnfleetsolutions.ttnfleet.ui.login.LoginActivity;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.TrackLocationFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.form.CreateServiceRequestFormFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.history.ServicesHistoryFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.MainServicesFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.details.CreateEventFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.driver.SelectDriverFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.vmrs.assembly.SelectAssemblyFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.vmrs.component.SelectComponentFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.vmrs.system.SelectServiceFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.notification.NotificationsFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.servicerequest.ServiceDetailsFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.servicerequest.callback.UpdateEventDetailsCallbackFD;
import com.ttnfleetsolutions.ttnfleet.utils.CommonUtils;
import com.ttnfleetsolutions.ttnfleet.utils.GlideApp;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;
import pl.com.salsoft.sqlitestudioremote.SQLiteStudioService;

import static com.ttnfleetsolutions.ttnfleet.data.remote.ApiEndPoint.DIR_IMAGE_PROFILE;
import static com.ttnfleetsolutions.ttnfleet.data.remote.ApiEndPoint.DIR_IMAGE_THEME_LOGO;

/**
 * Created by Sandip Chaulagain on 10/7/2017.
 *
 * @version 1.0
 */

public class DashboardActivity extends BaseActivity<ActivityFleetDispatcherDashboardBinding, DashboardViewModel>
        implements NavigationView.OnNavigationItemSelectedListener, HasSupportFragmentInjector, UpdateEventDetailsCallbackFD, DashboardNavigator {

    public Toolbar mToolbar;
    private NavigationView mNavigationView;
    private DrawerLayout mDrawerLayout;
    private MenuItem menuItemDone, menuItemNotification, menuItemSearch;
    private SearchView mSearchView;
    private View.OnClickListener onClickListener;
    private ActionBarDrawerToggle mDrawerToggle;

    @Inject
    DashboardViewModel mDashboardViewModel;

    private ActivityFleetDispatcherDashboardBinding mActivityFleetDispatcherDashboardBinding;

    @Inject
    DispatchingAndroidInjector<Fragment> fragmentDispatchingAndroidInjector;

    @Override
    public DashboardViewModel getViewModel() {
        return mDashboardViewModel;
    }

    @Override
    protected int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_fleet_dispatcher_dashboard;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityFleetDispatcherDashboardBinding = getViewDataBinding();
        mDashboardViewModel.setNavigator(this);

        setUp();
    }

    private void setUp() {
        SQLiteStudioService.instance().start(this);
        SQLiteStudioService.instance().setPort(9999);

        mToolbar = findViewById(R.id.baseToolbar);
        mToolbar.setTitle("Home");
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayUseLogoEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        setTheme();
        initComponents();
    }

    public void setTheme() {
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(getViewModel().getDataManager().getThemeStatusBarColor());

        mToolbar.setBackgroundColor(getViewModel().getDataManager().getThemeTitleBarColor());
    }

    /*private void addDataToDB() {
        String[] names = getResources().getStringArray(R.array.services_provider);
        String[] location = getResources().getStringArray(R.array.services_provider_location);
        for (int i = 0; i < 5; i++) {
            mDashboardViewModel.insertServiceProvider(new ServiceProvider(i, names[i], location[i], true, false));
        }
        mDashboardViewModel.insertServiceProvider(new ServiceProvider(4, names[4], location[4], false, false));
    }*/

    public static Intent getStartIntent(Context context) {
        return new Intent(context, DashboardActivity.class);
    }

    private void replaceFragment(Fragment fragment) {
        findViewById(R.id.container).setVisibility(View.VISIBLE);
        invalidateOptionsMenu();

        String backStateName = fragment.getClass().getName();

        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.container, fragment, backStateName);
            ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            ft.addToBackStack(backStateName);
            ft.commit();
            getSupportFragmentManager().executePendingTransactions();
        }

        if (isLandscape()) {
            if (fragment instanceof JobDashboardFragment) {
                /*ServiceDetailsFragment orderDetailsFragment = new ServiceDetailsFragment();
                orderDetailsFragment.updateIndex(1, 1);
                replaceFragmentDetails(orderDetailsFragment, false);
                findViewById(R.id.details_container).setVisibility(View.VISIBLE);*/
            } else if (fragment instanceof ServicesHistoryFragment) {
                /*ServiceDetailsFragment orderDetailsFragment = new ServiceDetailsFragment();
                orderDetailsFragment.updateIndex(-1, 3);
                replaceFragmentDetails(orderDetailsFragment, false);
                findViewById(R.id.details_container).setVisibility(View.VISIBLE);*/
            } else if (fragment instanceof MainChatFragment) {
                ServiceDetailsChatFragment serviceDetailsChatFragment = new ServiceDetailsChatFragment();
                replaceFragmentDetails(serviceDetailsChatFragment, false);
                findViewById(R.id.details_container).setVisibility(View.VISIBLE);
            } else {
                findViewById(R.id.details_container).setVisibility(View.GONE);
            }
        }
    }

    private void replaceFragmentDetails(Fragment fragment, boolean fragmentPopped) {
        String backStateName = fragment.getClass().getName();
        FragmentManager manager = getSupportFragmentManager();
//        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);
//        boolean fragmentPopped = false;

        if (!fragmentPopped) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.details_container, fragment, backStateName);
            ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
        updateSubTitle(fragment);
    }

    @Override
    public void onBackPressed() {
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.container);
        if (isLandscape()) {
            Fragment f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
            if (f != null && f1 != null) {
                if (f instanceof JobDashboardFragment && f1 instanceof ServiceDetailsFragment) {
//                    getSupportFragmentManager().popBackStack();
                    finish();
                } else if (f instanceof ServicesHistoryFragment && f1 instanceof ServiceDetailsFragment) {
                    getSupportFragmentManager().popBackStack();
                } else if (f instanceof MainChatFragment && f1 instanceof ServiceDetailsChatFragment) {
                    getSupportFragmentManager().popBackStack();
                }
                updateSubTitle(f1);
            }
        }
        if (f instanceof JobDashboardFragment && !isLandscape()) {
            finish();
        } else {
            super.onBackPressed();
            f = getSupportFragmentManager().findFragmentById(R.id.container);
            if (f != null) {
                updateTitleAndDrawer(f);
            }
        }
        /*if (tabLayout.getVisibility() == View.VISIBLE) {
            finish();
            return;
        }*//*
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.container);
        if (isLandscape()) {
            Fragment f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
            if (f != null && f1 != null) {
                if (f instanceof ServiceHistorySPFragment && f1 instanceof OrderDetailsFragment) {
                    getSupportFragmentManager().popBackStack();
                } else if (f instanceof MainChatFragment && f1 instanceof ServiceDetailsChatFragment) {
                    getSupportFragmentManager().popBackStack();
                }
            }
        }
        super.onBackPressed();
        Fragment f2 = getSupportFragmentManager().findFragmentById(R.id.container);
        if (f2 != null) {
            if (f2.getClass() == f.getClass() && !isLandscape()) {
                mToolbar.setTitle("Dashboard");
//                tabLayout.setVisibility(View.VISIBLE);
                findViewById(R.id.container).setVisibility(View.GONE);
                mNavigationView.setCheckedItem(R.id.navItemJobRoster);
            } else {
                updateTitleAndDrawer(f2);
            }
        } *//*else if (tabLayout.getVisibility() == View.GONE) {
            tabLayout.setVisibility(View.VISIBLE);
            findViewById(R.id.container).setVisibility(View.GONE);
            mNavigationView.setCheckedItem(R.id.navItemJobRoster);
        }*/
    }

    private void updateTitleAndDrawer(Fragment fragment) {
        invalidateOptionsMenu();
        CommonUtils.hideKeyboard(this);
        String fragClassName = fragment.getClass().getName();
      /*  int size = mNavigationView.getMenu().size();
        for (int i = 0; i < size; i++) {
            mNavigationView.getMenu().getItem(i).setChecked(false);
        }*/
        if (findViewById(R.id.details_container) != null)
            findViewById(R.id.details_container).setVisibility(View.GONE);

        if (fragClassName.equals(JobDashboardFragment.class.getName())) {
            mToolbar.setTitle("Dashboard");
            mNavigationView.setCheckedItem(R.id.navItemJobRoster);
            if (findViewById(R.id.details_container) != null)
                findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else if (fragClassName.equals(NotificationsFragment.class.getName())) {
            mToolbar.setTitle("Notifications");
            mNavigationView.setCheckedItem(R.id.navItemNotifications);
        } else if (fragClassName.equals(MainChatFragment.class.getName())) {
            mToolbar.setTitle("Chat");
            mNavigationView.setCheckedItem(R.id.navItemChat);
            if (findViewById(R.id.details_container) != null)
                findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else if (fragClassName.equals(ServicesHistoryFragment.class.getName())) {
            mToolbar.setTitle("Event History");
            mNavigationView.setCheckedItem(R.id.navItemHistory);
            if (findViewById(R.id.details_container) != null)
                findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else if (fragClassName.equals(ServiceProviderFragment.class.getName())) {
            if (((ServiceProviderFragment) fragment).isFlag())
                mToolbar.setTitle("Service Providers");
            else mToolbar.setTitle("Service Providers");
            mNavigationView.setCheckedItem(R.id.navItemServiceDispatcherList);
        } else if (fragClassName.equals(MainServicesFragment.class.getName())) {
            mToolbar.setTitle("Select Event Type");
            mNavigationView.setCheckedItem(R.id.navItemAdd);
        } else if (fragClassName.equals(SelectDriverFragment.class.getName())) {
            mToolbar.setTitle("Select Driver");
            mNavigationView.setCheckedItem(R.id.navItemAdd);
        } else if (fragClassName.equals(SelectServiceFragment.class.getName())) {
            mToolbar.setTitle("Select System Code");
            mNavigationView.setCheckedItem(R.id.navItemAdd);
        } else if (fragClassName.equals(SelectAssemblyFragment.class.getName())) {
            mToolbar.setTitle("Select Assembly Code");
            mNavigationView.setCheckedItem(R.id.navItemAdd);
        } else if (fragClassName.equals(SelectComponentFragment.class.getName())) {
            mToolbar.setTitle("Select Component Code");
            mNavigationView.setCheckedItem(R.id.navItemAdd);
        } else if (fragClassName.equals(CreateEventFragment.class.getName())) {
            mToolbar.setTitle("Create Event");
            mNavigationView.setCheckedItem(R.id.navItemAdd);
        } else if (fragClassName.equals(SelectServiceFragment.class.getName())) {
            mToolbar.setTitle("Select Service Type");
            mNavigationView.setCheckedItem(R.id.navItemAdd);
        } else if (fragClassName.equals(CreateServiceRequestFormFragment.class.getName())) {
            mToolbar.setTitle("Create Service Request");
            mNavigationView.setCheckedItem(R.id.navItemAdd);
        } /*else if (fragClassName.equals(ServiceDetailsFragment.class.getName())) {
            mToolbar.setTitle("Order Details");
            mNavigationView.setCheckedItem(R.id.navItemJobRoster);
        } */ else if (fragClassName.equals(ServiceDetailsChatFragment.class.getName())) {
//            mToolbar.setTitle("Service Details Chat");
            mNavigationView.setCheckedItem(R.id.navItemChat);
        } else if (fragClassName.equals(ChatFragment.class.getName())) {
//            mToolbar.setTitle("Service Details 1");
            mNavigationView.setCheckedItem(R.id.navItemChat);
        } else if (fragClassName.equals(TrackLocationFragment.class.getName())) {
            mToolbar.setTitle("Track Location");
            mNavigationView.setCheckedItem(R.id.navItemJobRoster);
        } else if (fragClassName.equals(AddNoteFragment.class.getName())) {
//            mToolbar.setTitle("Add Note");
            mNavigationView.setCheckedItem(R.id.navItemJobRoster);
        }
    }

    private void updateSubTitle(Fragment fragment) {
        String fragClassName = fragment.getClass().getName();
       /* if (fragClassName.equals(ServiceDetailsFragment.class.getName())) {
            setSubTitle("Order Details");
        } else*/
        if (fragClassName.equals(ServiceDetailsChatFragment.class.getName())) {
//            setSubTitle("Chat");
        } else if (fragClassName.equals(ChatFragment.class.getName())) {
//            setSubTitle("Chat");
        } else if (fragClassName.equals(TrackLocationFragment.class.getName())) {
            setSubTitle("Track Location");
        } else if (fragClassName.equals(AddNoteFragment.class.getName())) {
//            setSubTitle("Add Note");
        } else if (fragClassName.equals(ServiceProviderFragment.class.getName())) {
            if (((ServiceProviderFragment) fragment).isFlag())
                setSubTitle("Service Providers");
            else setSubTitle("Service Providers");
        }
    }

    public void setSubTitle(String subTitle) {
        if (!TextUtils.isEmpty(subTitle)) {
            ((TextView) findViewById(R.id.subTitle)).setText(subTitle);
            findViewById(R.id.subTitle).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.subTitle).setVisibility(View.GONE);
        }
    }

    public void setTitle(String title) {
        if (!TextUtils.isEmpty(title)) {
            mToolbar.setTitle(title);
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);

        if (fragment != null)
            if (isLandscape()) {
                Fragment subfragment = getSupportFragmentManager().findFragmentById(R.id.details_container);
                if (fragment instanceof SelectDriverFragment || fragment instanceof SelectServiceFragment
                        || fragment instanceof SelectAssemblyFragment || fragment instanceof SelectComponentFragment) {
                    if (menuItemSearch != null)
                        menuItemSearch.setVisible(true);
                    if (menuItemDone != null)
                        menuItemDone.setVisible(false);
                    if (menuItemNotification != null)
                        menuItemNotification.setVisible(false);
                } else if ((fragment instanceof JobDashboardFragment && subfragment instanceof AddNoteFragment)
                        || fragment instanceof CreateEventFragment) {
                    if (menuItemDone != null)
                        menuItemDone.setVisible(true);
                    if (menuItemNotification != null)
                        menuItemNotification.setVisible(false);
                    if (menuItemSearch != null)
                        menuItemSearch.setVisible(false);
                } else if (fragment instanceof JobDashboardFragment && subfragment instanceof ServiceProviderFragment
                        && ((ServiceProviderFragment) subfragment).isFlag()) {
                    if (menuItemDone != null)
                        menuItemDone.setVisible(false);
                    if (menuItemNotification != null)
                        menuItemNotification.setVisible(false);
                    if (menuItemSearch != null)
                        menuItemSearch.setVisible(false);
                } else if (fragment instanceof MainServicesFragment) {
                    if (menuItemDone != null)
                        menuItemDone.setVisible(false);
                    if (menuItemNotification != null)
                        menuItemNotification.setVisible(false);
                    if (menuItemSearch != null)
                        menuItemSearch.setVisible(false);
                } else {
                    if (menuItemDone != null)
                        menuItemDone.setVisible(false);
                    if (menuItemNotification != null)
                        menuItemNotification.setVisible(true);
                }
            } else {
                if (fragment instanceof SelectServiceFragment || fragment instanceof SelectAssemblyFragment || fragment instanceof SelectComponentFragment
                        || fragment instanceof CreateEventFragment || fragment instanceof SelectDriverFragment || fragment instanceof MainServicesFragment
                        || fragment instanceof AddNoteFragment) {

                    if (fragment instanceof MainServicesFragment) {
                        if (menuItemSearch != null)
                            menuItemSearch.setVisible(false);
                        if (menuItemDone != null)
                            menuItemDone.setVisible(false);
                    } else if (fragment instanceof CreateEventFragment || fragment instanceof AddNoteFragment) {
                        if (menuItemSearch != null)
                            menuItemSearch.setVisible(false);
                        if (menuItemDone != null)
                            menuItemDone.setVisible(true);
                    } else {
                        if (menuItemSearch != null)
                            menuItemSearch.setVisible(true);
                        if (menuItemDone != null)
                            menuItemDone.setVisible(false);
                    }

                    if (menuItemNotification != null)
                        menuItemNotification.setVisible(false);
                } else if (fragment instanceof ServiceProviderFragment && ((ServiceProviderFragment) fragment).isFlag()) {
                    if (menuItemDone != null)
                        menuItemDone.setVisible(false);
                    if (menuItemNotification != null)
                        menuItemNotification.setVisible(false);
                    if (menuItemSearch != null)
                        menuItemSearch.setVisible(false);
                } else {

                    if (menuItemDone != null)
                        menuItemDone.setVisible(false);
                    if (menuItemNotification != null)
                        menuItemNotification.setVisible(true);
                }
            }
        return true;
    }

    private void initComponents() {
        mToolbar = findViewById(R.id.baseToolbar);
        mToolbar.setTitle("Dashboard");
        setSupportActionBar(mToolbar);

        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mDrawerLayout = findViewById(R.id.drawerLayout);
        mNavigationView = findViewById(R.id.navigationView);

        int[][] state = new int[][]{
                new int[]{android.R.attr.state_checked},
                new int[]{android.R.attr.state_enabled},
                new int[]{android.R.attr.state_pressed},
                new int[]{android.R.attr.state_focused},
                new int[]{android.R.attr.state_pressed}
        };

        int[] color = new int[]{
                getViewModel().getDataManager().getThemeStatusBarColor(),
                Color.DKGRAY,
                Color.DKGRAY,
                Color.DKGRAY,
                Color.DKGRAY
        };

        //Defining ColorStateList for menu item Icon
        ColorStateList navMenuIconList = new ColorStateList(
                new int[][]{
                        new int[]{android.R.attr.state_checked},
                        new int[]{android.R.attr.state_enabled},
                        new int[]{android.R.attr.state_pressed},
                        new int[]{android.R.attr.state_focused},
                        new int[]{android.R.attr.state_pressed}
                },
                new int[]{
                        getViewModel().getDataManager().getThemeStatusBarColor(),
                        getViewModel().getDataManager().getThemeTitleBarColor(),
                        getViewModel().getDataManager().getThemeTitleBarColor(),
                        getViewModel().getDataManager().getThemeTitleBarColor(),
                        getViewModel().getDataManager().getThemeTitleBarColor()
                }
        );
        ColorStateList ColorStateList1 = new ColorStateList(state, color);
        mNavigationView.setItemTextColor(ColorStateList1);
//        mNavigationView.setItemIconTintList(ColorStateList.valueOf(getViewModel().getDataManager().getThemeStatusBarColor())));
        mNavigationView.setItemIconTintList(navMenuIconList);

        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawerLayout,
                mToolbar,
                R.string.open_drawer,
                R.string.close_drawer) {
        };
        mDrawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
        mNavigationView.setNavigationItemSelectedListener(this);

        onClickListener = mDrawerToggle.getToolbarNavigationClickListener();
        getSupportFragmentManager().addOnBackStackChangedListener(() -> {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
            if (fragment != null && findViewById(R.id.container).getVisibility() == View.VISIBLE)
                if (fragment instanceof SelectServiceFragment || fragment instanceof SelectAssemblyFragment
                        || fragment instanceof SelectComponentFragment || fragment instanceof CreateEventFragment
                        || fragment instanceof CreateServiceRequestFormFragment
                        || fragment instanceof ChatFragment || fragment instanceof ServiceDetailsChatFragment
                        || fragment instanceof TrackLocationFragment || fragment instanceof ServiceDetailsFragment
                        || fragment instanceof ServiceProviderFragment && ((ServiceProviderFragment) fragment).isFlag()
                        || fragment instanceof SelectDriverFragment || fragment instanceof MainServicesFragment
                        || fragment instanceof AddNoteFragment) {
                    mDrawerToggle.setDrawerIndicatorEnabled(false);
                    mDrawerToggle.setToolbarNavigationClickListener(v -> onBackPressed());
                } else {
                    mDrawerToggle.setDrawerIndicatorEnabled(true);
                    mDrawerToggle.setToolbarNavigationClickListener(onClickListener);
                }

            Fragment f = getSupportFragmentManager().findFragmentById(R.id.container);
            if (f != null && findViewById(R.id.container).getVisibility() == View.VISIBLE) {
                updateTitleAndDrawer(f);
            }
            if (isLandscape()) {
                Fragment f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
                if (f1 != null) {
                    updateSubTitle(f1);
                }
            }/*else if (tabLayout.getVisibility() == View.GONE) {
                int size = mNavigationView.getMenu().size();
                for (int i = 0; i < size; i++) {
                    mNavigationView.getMenu().getItem(i).setChecked(false);
                }
                mNavigationView.getMenu().getItem(1).setChecked(true);
                mToolbar.setTitle("Dashboard");
                tabLayout.setVisibility(View.VISIBLE);
                findViewById(R.id.container).setVisibility(View.GONE);
            }*/
        });

        /*getSupportFragmentManager().addOnBackStackChangedListener(() -> {
            Fragment f = getSupportFragmentManager().findFragmentById(R.id.container);
            if (f != null) {
                updateTitleAndDrawer(f);
            } else if (tabLayout.getVisibility() == View.GONE) {
                int size = mNavigationView.getMenu().size();
                for (int i = 0; i < size; i++) {
                    mNavigationView.getMenu().getItem(i).setChecked(false);
                }
                mNavigationView.getMenu().getItem(1).setChecked(true);
                mToolbar.setTitle("Dashboard");
                tabLayout.setVisibility(View.VISIBLE);
                findViewById(R.id.container).setVisibility(View.GONE);
            }
        });*/

        /*ViewPager viewPager = findViewById(R.id.viewpager);
        setupViewPager(viewPager);*/

     /*   tabLayout = findViewById(R.id.tabs);
        tabLayout.setVisibility(View.VISIBLE);
        tabLayout.setupWithViewPager(viewPager);*/

        /*mDrawerLayout = findViewById(R.id.drawerLayout);
        mNavigationView = findViewById(R.id.navigationView);
        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawerLayout,
                mToolbar,
                R.string.open_drawer,
                R.string.close_drawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };
        mDrawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        mNavigationView.setNavigationItemSelectedListener(mOnNavigationItemSelectedListener);*/
        mNavigationView.setCheckedItem(R.id.navItemJobRoster);

        View header = mNavigationView.getHeaderView(0);
        ImageView imageViewLogo = header.findViewById(R.id.logo);
        ImageView imageViewProfile = header.findViewById(R.id.profileImageView);
        header.findViewById(R.id.drawerHeaderBg).setBackgroundColor(getViewModel().getDataManager().getThemeTitleBarColor());

        ((TextView) header.findViewById(R.id.name)).setText(getViewModel().getDataManager().getCurrentUserFullName());
        header.findViewById(R.id.number).setVisibility(TextUtils.isEmpty(getViewModel().getDataManager().getCurrentUserRoleName()) ? View.GONE : View.VISIBLE);
        ((TextView) header.findViewById(R.id.number)).setText(getViewModel().getDataManager().getCurrentUserRoleName());

        GlideApp.with(this)
                .load(DIR_IMAGE_PROFILE + getViewModel().getDataManager().getCurrentUserID() + "/" + getViewModel().getDataManager().getCurrentUserProfileImage())
                .apply(RequestOptions.circleCropTransform())
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .into(imageViewProfile);

        GlideApp.with(this)
                .load(DIR_IMAGE_THEME_LOGO + getViewModel().getDataManager().getThemeLogo())
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .fitCenter()
                .into(imageViewLogo);

        Fragment fragment = new JobDashboardFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("colorPrimary", getViewModel().getDataManager().getThemeTitleBarColor());
        bundle.putInt("colorPrimaryDark", getViewModel().getDataManager().getThemeStatusBarColor());
        fragment.setArguments(bundle);
        replaceFragment(fragment);
    }

    public void showOngoingServices() {
        FragmentManager fm = getSupportFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
        getSupportFragmentManager().beginTransaction().remove(getSupportFragmentManager().findFragmentById(R.id.container)).commit();
        Fragment fragment = new JobDashboardFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("colorPrimary", getViewModel().getDataManager().getThemeTitleBarColor());
        bundle.putInt("colorPrimaryDark", getViewModel().getDataManager().getThemeStatusBarColor());
        fragment.setArguments(bundle);
        replaceFragment(fragment);
        mNavigationView.setCheckedItem(R.id.navItemJobRoster);

//        mToolbar.setTitle("Dashboard");
//        findViewById(R.id.container).setVisibility(View.GONE);
//        mDrawerToggle.setDrawerIndicatorEnabled(true);
//        mDrawerToggle.setToolbarNavigationClickListener(onClickListener);
//        invalidateOptionsMenu();
//        mNavigationView.setCheckedItem(R.id.navItemJobRoster);
    }

    public void showChatDetails(int from, String title) {
        if (isLandscape()) {
            Fragment f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
            if (f1 instanceof ChatFragment) {
                final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ((ChatFragment) f1).setReceiver(title);
                ft.detach(f1);
                ft.attach(f1);
                ft.commit();
            } else {
                ChatFragment chatFragment = new ChatFragment();
                chatFragment.setReceiver(title);
                replaceFragmentDetails(chatFragment, false);
            }
            setSubTitle(title);
            //update details only
            findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else {
            mToolbar.setTitle(title);
            ChatFragment chatFragment = new ChatFragment();
            chatFragment.setReceiver(title);
            replaceFragment(chatFragment);
        }
        mNavigationView.setCheckedItem(R.id.navItemChat);
    }

    public void showServiceChatThreadDetails(int from, String title) {
        if (isLandscape()) {
            Fragment f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
            if (f1 instanceof ServiceDetailsChatFragment) {
                final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//                ((ServiceDetailsChatFragment) f1).updateIndex(index);
                ft.detach(f1);
                ft.attach(f1);
                ft.commit();
            } else {
                ServiceDetailsChatFragment serviceDetailsChatFragment = new ServiceDetailsChatFragment();
                String backStateName = serviceDetailsChatFragment.getClass().getName();
                FragmentManager manager = getSupportFragmentManager();
                boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);
                replaceFragmentDetails(serviceDetailsChatFragment, fragmentPopped);
            }
            setSubTitle(title);
            //update details only
            findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else {
            mToolbar.setTitle(title);
            ServiceDetailsChatFragment serviceDetailsChatFragment = new ServiceDetailsChatFragment();
            replaceFragment(serviceDetailsChatFragment);
        }
        mNavigationView.setCheckedItem(R.id.navItemChat);
    }

    public void trackLocation(int from) {
        if (isLandscape()) {
            Fragment f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
            if (f1 instanceof TrackLocationFragment) {
                final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//                ((ServiceDetailsChatFragment) f1).updateIndex(index);
                ft.detach(f1);
                ft.attach(f1);
                ft.commit();
            } else {
                TrackLocationFragment trackLocationFragment = new TrackLocationFragment();
                replaceFragmentDetails(trackLocationFragment, false);
            }

            //update details only
            findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else {
            TrackLocationFragment trackLocationFragment = new TrackLocationFragment();
            replaceFragment(trackLocationFragment);
        }
        mNavigationView.setCheckedItem(R.id.navItemChat);
    }

    /*@Override
    public void onBackPressed() {
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.container);
        if (f instanceof NotificationsFragment) {
            findViewById(R.id.container).setVisibility(View.GONE);
            tabLayout.setVisibility(View.VISIBLE);
            mToolbar.setTitle("Dashboard");
        }
        super.onBackPressed();
    }*/

    //NEW - 0
    //ONGOING - 1
    //COMPLETED - 2
    //ARCHIVE - 3
    /*public void showOrderDetails(int type) {
        if (type != 10)
            startActivity(new Intent(this, OrderDetailsActivity.class).putExtra("type", type));
        else
            startActivity(new Intent(this, OrderDetailsActivity.class).putExtra("type", type));
    }*/
    public void goToOrderDetails(int eventId) {
        if (isLandscape()) {
            loadSplitViewIfLandscape(eventId);
            /*Fragment f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
            if (f1 instanceof ServiceDetailsFragment) {
                final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ((ServiceDetailsFragment) f1).updateIndex(eventId, index);
                ft.detach(f1);
                ft.attach(f1);
                ft.commit();
            } else {
                ServiceDetailsFragment orderDetailsFragment = new ServiceDetailsFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("eventId", eventId);
                orderDetailsFragment.setArguments(bundle);
//                orderDetailsFragment.updateIndex(from, index);
                String backStateName = orderDetailsFragment.getClass().getName();
                FragmentManager manager = getSupportFragmentManager();
                boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);
                replaceFragmentDetails(orderDetailsFragment, fragmentPopped);
                if (fragmentPopped) {
                    f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
                    if (f1 instanceof ServiceDetailsFragment) {
                        final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                        ((ServiceDetailsFragment) f1).updateIndex(eventId, index);
                        ft.detach(f1);
                        ft.attach(f1);

                        ft.commit();
                    }
                }
            }*/

            //update details only
            findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else {
            ServiceDetailsFragment orderDetailsFragment = new ServiceDetailsFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("eventId", eventId);
            orderDetailsFragment.setArguments(bundle);
//            orderDetailsFragment.updateIndex(from, index);
            replaceFragment(orderDetailsFragment);
        }
        mNavigationView.setCheckedItem(R.id.navItemJobRoster);
    }

    /*public void goToOrderDetailsFromSwipe(int eventId) {
        if (isLandscape()) {
            loadSplitViewIfLandscape(eventId);
        }
    }*/

    public void goToOrderDetailsFromNotifiction(int from, int index) {
        if (isLandscape()) {
            Fragment fragment = new JobDashboardFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("colorPrimary", getViewModel().getDataManager().getThemeTitleBarColor());
            bundle.putInt("colorPrimaryDark", getViewModel().getDataManager().getThemeStatusBarColor());
            fragment.setArguments(bundle);
            replaceFragment(fragment);
        } else {
            ServiceDetailsFragment orderDetailsFragment = new ServiceDetailsFragment();
            orderDetailsFragment.updateIndex(from, index);
            replaceFragment(orderDetailsFragment);
        }
    }

    /*public void showChatDetails(String title) {
        startActivity(new Intent(this, ServiceRequestChatActivity.class).putExtra("from", 1).putExtra("title", title));
    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.fd_dashboard, menu);
        menuItemDone = menu.findItem(R.id.action_done);
        menuItemNotification = menu.findItem(R.id.action_notifications);

        View count = menu.findItem(R.id.action_notifications).getActionView();
        count.setOnClickListener(view -> replaceFragment(new NotificationsFragment()));

        menuItemSearch = menu.findItem(R.id.action_search);
        mSearchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        mSearchView.setMaxWidth(Integer.MAX_VALUE);
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
                if (fragment != null)
                    if (fragment instanceof SelectServiceFragment)
                        ((SelectServiceFragment) fragment).searchQuery(newText);
                    else if (fragment instanceof SelectAssemblyFragment)
                        ((SelectAssemblyFragment) fragment).searchQuery(newText);
                    else if (fragment instanceof SelectComponentFragment)
                        ((SelectComponentFragment) fragment).searchQuery(newText);
                    else if (fragment instanceof SelectDriverFragment)
                        ((SelectDriverFragment) fragment).searchQuery(newText);
                return true;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Drawable drawable = item.getIcon();
        if (drawable instanceof Animatable) {
            ((Animatable) drawable).start();
        }
        switch (item.getItemId()) {
            /*case R.id.action_logout:
                getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE).edit().putInt("PREF_KEY_USER_LOGGED_IN_MODE", 0).apply();
                Intent intent = LoginActivity.getStartIntent(DashboardActivity.this);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                return true;*/
           /* case R.id.action_new:
                showForm();
                return true;*/
            case R.id.action_notifications:
                replaceFragment(new NotificationsFragment());
                return true;
            case R.id.action_done:
                // handle confirmation button click here
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
                if (fragment != null)
                    if (isLandscape()) {
                        Fragment subFragment = getSupportFragmentManager().findFragmentById(R.id.details_container);
                        if (fragment instanceof SelectServiceFragment)
                            showForm(null);
                        else if (fragment instanceof CreateEventFragment)
                            ((CreateEventFragment) fragment).onSubmitEventRequest();
                        else if (fragment instanceof JobDashboardFragment && subFragment instanceof ServiceProviderFragment)
                            onBackPressed();
                        else if (fragment instanceof JobDashboardFragment && subFragment instanceof AddNoteFragment)
                            ((AddNoteFragment) subFragment).actionDone();
                    } else {
                        if (fragment instanceof SelectServiceFragment)
                            showForm(null);
                        else if (fragment instanceof CreateEventFragment)
                            ((CreateEventFragment) fragment).onSubmitEventRequest();
                        else if (fragment instanceof ServiceProviderFragment)
                            onBackPressed();
                        else if (fragment instanceof AddNoteFragment)
                            ((AddNoteFragment) fragment).actionDone();
                    }
                /*if(menuItem != null) {
                    menuItem.setVisible(false);
                }*/
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /*private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new NewServiceFragment(), "NEW(1)");
        adapter.addFragment(new OngoingServiceFragment(), "ONGOING(1)");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<android.support.v4.app.Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        ViewPagerAdapter(android.support.v4.app.FragmentManager manager) {
            super(manager);
        }

        @Override
        public android.support.v4.app.Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFragment(android.support.v4.app.Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

    }*/

    public void showForm(Location location) {
        if (location != null)
            getViewModel().updateLocation(location);

//        FragmentManager fragmentManager = getSupportFragmentManager();
        /*CreateServiceRequestFormFragment newFragment = new CreateServiceRequestFormFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("type", type);
        bundle.putString("service", issueType);
        newFragment.setArguments(bundle);
        *//*FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(android.R.id.content, newFragment).addToBackStack(null).commit();*//*
        replaceFragment(newFragment);*/

        Fragment newFragment = new CreateEventFragment();
        replaceFragment(newFragment);
    }

    public void showLocationTypes() {
        Fragment newFragment = new MainServicesFragment();
        replaceFragment(newFragment);
    }

    public void showSystemListing() {
        Fragment newFragment = new SelectServiceFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("from", 1);
        newFragment.setArguments(bundle);
        replaceFragment(newFragment);
    }

    public void showAssemblyListing(String systemCode, String systemCodeName) {
        Fragment newFragment = new SelectAssemblyFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("from", 1);
        bundle.putString("system", systemCode);
        bundle.putString("systemName", systemCodeName);
        newFragment.setArguments(bundle);
        replaceFragment(newFragment);
    }

    public void showComponentListing(String systemCode, String assemblyCode, String systemCodeName, String assemblyCodeName) {
        Fragment newFragment = new SelectComponentFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("from", 1);
        bundle.putString("system", systemCode);
        bundle.putString("assembly", assemblyCode);
        bundle.putString("systemName", systemCodeName);
        bundle.putString("assemblyName", assemblyCodeName);
        newFragment.setArguments(bundle);
        replaceFragment(newFragment);
    }

    public void dispatchService(int eventId, boolean flag) {
        if (isLandscape()) {
            Fragment f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
            if (f1 instanceof ServiceProviderFragment) {
                final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ((ServiceProviderFragment) f1).setUpdateEventDetailsCallback(this);
                ft.detach(f1);
                ft.attach(f1);
                ft.commit();
            } else {
                ServiceProviderFragment serviceProviderFragment = new ServiceProviderFragment();
                serviceProviderFragment.setUpdateEventDetailsCallback(this);
                Bundle bundle = new Bundle();
                bundle.putInt("eventId", eventId);
                bundle.putBoolean("flag", flag);
                serviceProviderFragment.setArguments(bundle);
                replaceFragmentDetails(serviceProviderFragment, false);
            }

            //update details only
            findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else {
            ServiceProviderFragment serviceProviderFragment = new ServiceProviderFragment();
            serviceProviderFragment.setUpdateEventDetailsCallback(this);
            Bundle bundle = new Bundle();
            bundle.putInt("eventId", eventId);
            bundle.putBoolean("flag", flag);
            serviceProviderFragment.setArguments(bundle);
            replaceFragment(serviceProviderFragment);
        }
        mNavigationView.setCheckedItem(R.id.navItemJobRoster);
/*
        Fragment fragment = new ServiceProviderFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean("flag", true);
        fragment.setArguments(bundle);
        replaceFragment(fragment);*/
    }

    public void addNoteToEvent(int eventId, String note) {
        if (isLandscape()) {
            Fragment f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
            if (f1 instanceof AddNoteFragment) {
                final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.detach(f1);
                ft.attach(f1);
                ft.commit();
            } else {
                AddNoteFragment addNoteFragment = new AddNoteFragment();
//                serviceProviderFragment.setUpdateEventDetailsCallback(this);
                Bundle bundle = new Bundle();
                bundle.putInt("eventId", eventId);
                bundle.putString("note", note);
                addNoteFragment.setArguments(bundle);
                replaceFragmentDetails(addNoteFragment, false);
            }

            //update details only
            findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else {
            AddNoteFragment addNoteFragment = new AddNoteFragment();
//            serviceProviderFragment.setUpdateEventDetailsCallback(this);
            Bundle bundle = new Bundle();
            bundle.putInt("eventId", eventId);
            bundle.putString("note", note);
            addNoteFragment.setArguments(bundle);
            replaceFragment(addNoteFragment);
        }
        mNavigationView.setCheckedItem(R.id.navItemJobRoster);
/*
        Fragment fragment = new ServiceProviderFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean("flag", true);
        fragment.setArguments(bundle);
        replaceFragment(fragment);*/
    }

    public void showIssueList(int from, String service) {
        Fragment newFragment = new SelectServiceFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("from", from);
        bundle.putString("service", service);
        newFragment.setArguments(bundle);
        replaceFragment(newFragment);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        mDrawerLayout.closeDrawer(GravityCompat.START);

            /*int size = mNavigationView.getMenu().size();
            for (int i = 0; i < size; i++) {
                mNavigationView.getMenu().getItem(i).setChecked(false);
            }*/
        switch (item.getItemId()) {
            case R.id.navItemAdd:
//                startActivity(new Intent(DashboardActivity.this, OrderDetailsActivityNew.class));
                getViewModel().createNewEventRequest();
                replaceFragment(new SelectDriverFragment());
                return true;
            case R.id.navItemJobRoster:
                Fragment fragment = new JobDashboardFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("colorPrimary", getViewModel().getDataManager().getThemeTitleBarColor());
                bundle.putInt("colorPrimaryDark", getViewModel().getDataManager().getThemeStatusBarColor());
                fragment.setArguments(bundle);
                replaceFragment(fragment);
//                    tabLayout.setVisibility(View.VISIBLE);
//                    findViewById(R.id.container).setVisibility(View.GONE);
                return true;
            case R.id.navItemHistory:
                fragment = new ServicesHistoryFragment();
                bundle = new Bundle();
                bundle.putInt("from", 1);
                fragment.setArguments(bundle);
                replaceFragment(fragment);
                return true;
            case R.id.navItemNotifications:
                replaceFragment(new NotificationsFragment());
                return true;
            case R.id.navItemChat:
                replaceFragment(new MainChatFragment());
                return true;
            case R.id.navItemServiceDispatcherList:
                replaceFragment(new ServiceProviderFragment());
                return true;
            case R.id.navItemLogout:
                setDefaultTheme();
                Intent intent = LoginActivity.getStartIntent(DashboardActivity.this);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                return true;
            default:
                return false;

        }
    }

    @Override
    public void onDispatchedEvent() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        if (fragment == null) return;
        if (isLandscape()) {
            Fragment subFragment = getSupportFragmentManager().findFragmentById(R.id.details_container);
            if (fragment instanceof JobDashboardFragment && subFragment instanceof ServiceDetailsFragment) {
                ((ServiceDetailsFragment) subFragment).onDispatchedEvent();
                ((JobDashboardFragment) fragment).onDispatchedEventFromDetails();
            }
        } else {
            if (fragment instanceof JobDashboardFragment) {
                ((JobDashboardFragment) fragment).onDispatchedEvent();
            } else if (fragment instanceof ServiceDetailsFragment) {
                ((ServiceDetailsFragment) fragment).onDispatchedEvent();
                FragmentManager fragmentManager = getSupportFragmentManager();
                Fragment fragment1 = fragmentManager.findFragmentByTag(JobDashboardFragment.class.getName());
                if (fragment1 instanceof JobDashboardFragment) {
                    ((JobDashboardFragment) fragment1).onDispatchedEventFromDetails();
                }
            }
        }
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentDispatchingAndroidInjector;
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAddedNote(AddNoteResponse addNoteResponse) {
        Toast.makeText(this, getString(R.string.note_added), Toast.LENGTH_SHORT).show();
        onBackPressed();
        onDispatchedEvent();
    }

    public void loadSplitViewIfLandscape(int eventId) {
        ServiceDetailsFragment serviceDetailsFragment = new ServiceDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("eventId", eventId);
        serviceDetailsFragment.setArguments(bundle);
        replaceFragmentDetails(serviceDetailsFragment, false);
        findViewById(R.id.details_container).setVisibility(View.VISIBLE);
    }
}
