package com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.vmrs.component;

import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.View;

import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.BR;
import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.DriverListResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.ServiceCode;
import com.ttnfleetsolutions.ttnfleet.databinding.FragmentIssueListNewBinding;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseFragment;
import com.ttnfleetsolutions.ttnfleet.ui.fleet.DashboardActivity;
import com.ttnfleetsolutions.ttnfleet.ui.technician.TechnicianDashboardActivity;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.MainActivityNew;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.vmrs.system.SelectServiceItemViewModel;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.vmrs.system.SelectServiceNavigator;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.vmrs.system.SelectServiceViewModel;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.vmrs.system.ServiceRecyclerViewAdapter;
import com.ttnfleetsolutions.ttnfleet.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by Sandip Chaulagain on 11/22/2017.
 *
 * @version 1.0
 */

public class SelectComponentFragment extends BaseFragment<FragmentIssueListNewBinding, SelectServiceViewModel> implements SelectServiceNavigator, SelectServiceItemViewModel.ServiceItemViewModelListener {


    @Inject
    SelectServiceViewModel mSelectServiceViewModel;

    FragmentIssueListNewBinding mFragmentIssueListNewBinding;

    @Inject
    ServiceRecyclerViewAdapter mAdapter;

    private String mSystemCode, mAssemblyCode, mSystemCodeName, mAssemblyCodeName;

    private int mFrom;

    @Override
    protected SelectServiceViewModel getViewModel() {
        return mSelectServiceViewModel;
    }

    @Override
    protected int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_issue_list_new;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSelectServiceViewModel.setNavigator(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFragmentIssueListNewBinding = getViewDataBinding();
        setUp();
    }

    private void setUp() {
        if (getArguments() != null) {
            mFrom = getArguments().getInt("from");
            mSystemCode = getArguments().getString("system");
            mAssemblyCode = getArguments().getString("assembly");
            mSystemCodeName = getArguments().getString("systemName");
            mAssemblyCodeName = getArguments().getString("assemblyName");
        }
        mFragmentIssueListNewBinding.nextButton.setBackgroundTintList(ColorStateList.valueOf(getViewModel().getDataManager().getThemeStatusBarColor()));
        mFragmentIssueListNewBinding.nextButton.setVisibility(View.VISIBLE);

        mFragmentIssueListNewBinding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mFragmentIssueListNewBinding.recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        mAdapter.setEmptyItemViewModelListener(this::getListing);
        mAdapter.setCodeStep(3);
        mAdapter.setListener(this);

        if (mFrom != 2) {
            mAdapter.setMultipleSelectionEnabled(true);
        }
        getListing();
    }

    private void getListing() {
        if (!isNetworkConnected()) {
            mAdapter.setEmptyItem(CommonUtils.getEmptyItem(getActivity(), DataManager.ErrorState.NETWORK_ERROR));
            mFragmentIssueListNewBinding.recyclerView.setAdapter(mAdapter);
        } else
            getViewModel().getComponentList(mSystemCode, mAssemblyCode);
    }

    @Override
    public void handleError(Throwable throwable) {
        mAdapter.setEmptyItem(CommonUtils.getEmptyItem(getActivity(), DataManager.ErrorState.API_FAIL));
        mFragmentIssueListNewBinding.recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void setSystemList(List<ServiceCode> systemList) {
        if (systemList != null && systemList.size() > 0) {
            mAdapter.addItems(systemList);
            mFragmentIssueListNewBinding.recyclerView.setAdapter(mAdapter);
        } else onNext();
    }

    @Override
    public void onNext() {
        generateComponentCodes();
        if (mFrom != 2)
            getViewModel().saveCode(mSystemCode, mSystemCodeName, mAssemblyCode, mAssemblyCodeName, componentCodeStr.toString(), componentCodeNameArray);
        if (getActivity() instanceof MainActivityNew)
            ((MainActivityNew) getActivity()).showForm(null);
        else if (getActivity() instanceof DashboardActivity)
            ((DashboardActivity) getActivity()).showForm(null);
        else if (getActivity() instanceof TechnicianDashboardActivity)
            ((TechnicianDashboardActivity) getActivity()).addNoteForAdditionalJob(mSystemCode, mSystemCodeName, mAssemblyCode, mAssemblyCodeName, componentCodeStr.toString(), componentCodeNameArray);
    }

    @Override
    public void onItemClick(String systemCode, String assemblyCode, String componentCode, String systemCodeName, String assemblyCodeName) {
        generateComponentCodes();
        if (getActivity() instanceof TechnicianDashboardActivity)
            ((TechnicianDashboardActivity) getActivity()).addNoteForAdditionalJob(mSystemCode, mSystemCodeName, mAssemblyCode, mAssemblyCodeName, componentCodeStr.toString(), componentCodeNameArray);
    }

    @Override
    public void onItemSelection(int position, boolean isSelected) {
        mAdapter.getItem(position).setSelected(!isSelected);
        mAdapter.notifyDataSetChanged();
    }

    public void searchQuery(String query) {
        List<ServiceCode> temp = new ArrayList();
        for (ServiceCode serviceCode : getViewModel().getCodeList()) {
            //or use .equal(text) with you want equal match
            //use .toLowerCase() for better matches
            if (serviceCode.getDisplaytitle().toLowerCase().contains(query.toLowerCase())) {
                temp.add(serviceCode);
            }
        }
        //update recyclerview
        mAdapter.addItems(temp);
    }

    StringBuilder componentCodeStr = new StringBuilder();
    List<String> componentCodeNameArray = new ArrayList<>();

    private void generateComponentCodes() {
        //generate and save code

        String prefix = "";

        for (ServiceCode serviceCode : getViewModel().getCodeList()) {
            if (serviceCode.isSelected()) {
                //append code
                componentCodeStr.append(prefix);
                prefix = ",";
                componentCodeStr.append(serviceCode.getCode3());
                //add title
                componentCodeNameArray.add(serviceCode.getDisplaytitle());
            }
        }
        if (TextUtils.isEmpty(componentCodeStr)) {
            componentCodeStr.append("000");
            componentCodeNameArray.add("000");
        }
    }

    @Override
    public void setDriverList(List<DriverListResponse.Driver> driverList) {

    }

    @Override
    public void handleErrorDriverListing(Throwable throwable) {

    }
}

