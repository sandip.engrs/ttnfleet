package com.ttnfleetsolutions.ttnfleet.ui.login;

/**
 * Created by Sandip Chaulagain on 10/4/2017.
 *
 * @version 1.0
 */

interface LoginNavigator {

    void openTruckerActivity();

    void openFleetDispatcherActivity();

    void openServiceProviderActivity();

    void openServiceRepActivity();

    void handleError(Throwable throwable);

    void login();
}
