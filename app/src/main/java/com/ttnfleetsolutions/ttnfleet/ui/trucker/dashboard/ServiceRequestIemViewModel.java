package com.ttnfleetsolutions.ttnfleet.ui.trucker.dashboard;

import android.databinding.BindingAdapter;
import android.databinding.ObservableField;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.listing.Event;
import com.ttnfleetsolutions.ttnfleet.utils.CommonUtils;

/**
 * Created by Sandip Chaulagain on 1/19/2018.
 *
 * @version 1.0
 */

public class ServiceRequestIemViewModel {

    private Event mEvent;
    public ObservableField<Integer> id;
    public ObservableField<String> name;
    public ObservableField<String> serviceType;
    public ObservableField<Integer> vehicleID;
    public ObservableField<Integer> locationID;

    public ObservableField<String> serviceIssue;
    public ObservableField<String> RTA;
    public ObservableField<String> ATA;
    public ObservableField<String> ATC;
    public ObservableField<String> RTC;

    public ObservableField<String> datetime;

    public ObservableField<Integer> status;

    //Drop-off & Pick-up
    public ObservableField<String> note;
    public ObservableField<String> dropoff;
    public ObservableField<String> pickup;
    public ObservableField<String> statusText;
    public ObservableField<String> vehicleName;
    public ObservableField<String> VIN;
    public ObservableField<String> address;
    public ObservableField<Boolean> isDispatchVisible;
    public ObservableField<Boolean> isAddNoteVisible;
    public ObservableField<Boolean> isSelected;
    public int mFrom;

    private ServiceRequestItemViewModelListener mListener;

    ServiceRequestIemViewModel(Event event, ServiceRequestItemViewModelListener listener, int from, boolean isLandscape) {
        this.mEvent = event;
        this.mListener = listener;
        this.mFrom = from;
        id = new ObservableField<>(event.getEventid());
        name = new ObservableField<>("Event #" + event.getEventid());
        if (event.getEventType() != null)
            serviceType = new ObservableField<>(event.getEventType().getTitle());
        vehicleID = new ObservableField<>(event.getVehicleid());
        locationID = new ObservableField<>(event.getLocationid());
        note = new ObservableField<>(event.getFdnote());

        StringBuilder stringBuilder = new StringBuilder();
        String prefix = "";
        if (event.getWorkOrders() != null && event.getWorkOrders().size() > 0) {
            for (int i = 0; i < event.getWorkOrders().size(); i++) {
                if (event.getWorkOrders().get(i).getServiceCode() != null && !TextUtils.isEmpty(event.getWorkOrders().get(i).getServiceCode().getDisplaytitle())) {
                    //append code
                    stringBuilder.append(prefix);
                    prefix = "\n";
                    stringBuilder.append(i + 1);
                    stringBuilder.append(". ");
                    stringBuilder.append(event.getWorkOrders().get(i).getServiceCode().getDisplaytitle());
                }
            }
        }
        isDispatchVisible = new ObservableField<>(false);
        isAddNoteVisible = new ObservableField<>(false);

        if (mFrom == 1 && !isLandscape) {
            isAddNoteVisible = new ObservableField<>(true);

            if (event.getWorkOrders() != null && event.getWorkOrders().size() > 0) {
                int workOrderStatusId = event.getWorkOrders().get(0).getStatusid();
                if (workOrderStatusId == DataManager.Status.DISPATCH.getStatus())
                    isDispatchVisible = new ObservableField<>(true);
            }
        }

        if (isLandscape) {
            isSelected = new ObservableField<>(event.isSelected());
        }
        serviceIssue = new ObservableField<>(stringBuilder.toString());

        RTA = new ObservableField<>(CommonUtils.getFromattedDate(event.getRta()));
        ATA = new ObservableField<>("");
        ATC = new ObservableField<>("");
        RTC = new ObservableField<>(CommonUtils.getFromattedDate(event.getRtc()));

        datetime = new ObservableField<>(CommonUtils.getPrettyTime(event.getCreateddatetime()));
        status = new ObservableField<>(1);
         /*dropoff = new ObservableField<>(event.dropoff);
        pickup = new ObservableField<>(event.pickup);
*/
        vehicleName = new ObservableField<>(event.getVehicle().getVehiclename());
        VIN = new ObservableField<>(event.getVehicle().getVin());
        try {
            address = new ObservableField<>(event.getLocation().getAddress());
        } catch (Exception e) {
            e.printStackTrace();
        }
        statusText = new ObservableField<>(event.getStatus().getTitle());
        status = new ObservableField<>(event.getStatusid());
        /*if (status != null)
            switch (status.get()) {
                case 0:
                    statusText = new ObservableField<>("NEW");
                    break;
                case 1:
                    statusText = new ObservableField<>("ONGOING");
                    break;
                case 2:
                    statusText = new ObservableField<>("COMPLETED");
                    break;
            }*/
    }

    public void onItemClick() {
        mListener.onItemClick(mEvent.getEventid());
    }

    public void onDispatchClick() {
        mListener.onDispatchClick(mEvent.getEventid());
    }

    public void onAddNoteClick() {
        mListener.onAddNoteClick(mEvent.getEventid(), mEvent.getFdnote());
    }

    public interface ServiceRequestItemViewModelListener {

        void onItemClick(int id);

        void onDispatchClick(int id);

        void onAddNoteClick(int id, String note);
    }

    @BindingAdapter({"characterBackground"})
    public static void characterBackground(View view, int status) {
        switch (status) {
            case 1:
                view.setBackgroundColor(ContextCompat.getColor(view.getContext(), android.R.color.holo_orange_dark));
                break;
            case 2:
                view.setBackgroundColor(ContextCompat.getColor(view.getContext(), android.R.color.holo_green_dark));
                break;
            case 3:
            case 4:
                view.setBackgroundColor(ContextCompat.getColor(view.getContext(), android.R.color.holo_blue_dark));
                break;
            case 5:
                view.setBackgroundColor(ContextCompat.getColor(view.getContext(), android.R.color.darker_gray));
                break;
            default:
                view.setBackgroundColor(ContextCompat.getColor(view.getContext(), android.R.color.holo_orange_dark));
                break;
        }
    }

    /*@BindingAdapter({"serviceType"})
    public static void serviceType(View view, ServiceRequestIemViewModel serviceRequestIemViewModel) {
        *//*if (serviceRequestIemViewModel.serviceType.get().equalsIgnoreCase("Drop off & Pick up")) {
            ((TextView) view).setText("Service Type: " + serviceRequestIemViewModel.serviceType.get());
        } else {
            ((TextView) view).setText(serviceRequestIemViewModel.serviceIssue.get());
        }*//*
        try {
            ((TextView) view).setText(serviceRequestIemViewModel.serviceIssue.get());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    @BindingAdapter({"text"})
    public static void arrivalRepairInfo(View view, ServiceRequestIemViewModel serviceRequestIemViewModel) {
        StringBuilder info = new StringBuilder();
       /* if (serviceRequestIemViewModel.serviceType.get().equalsIgnoreCase("Drop off & Pick up")) {
            if (!TextUtils.isEmpty(serviceRequestIemViewModel.dropoff.get())) {
                info.append("Drop off:" + serviceRequestIemViewModel.dropoff.get() + " | ");
            }
            if (!TextUtils.isEmpty(serviceRequestIemViewModel.pickup.get())) {
                info.append("Pick up:" + serviceRequestIemViewModel.pickup.get() + " | ");
            }
        } else*/
        {
            if (!TextUtils.isEmpty(serviceRequestIemViewModel.RTA.get())) {
                info.append("RTA: " + serviceRequestIemViewModel.RTA.get() + " | ");
            }
            if (!TextUtils.isEmpty(serviceRequestIemViewModel.ATA.get())) {
                info.append("ATA:" + serviceRequestIemViewModel.ATA.get() + " | ");
            }
            if (!TextUtils.isEmpty(serviceRequestIemViewModel.RTC.get())) {
                info.append("RTC: " + serviceRequestIemViewModel.RTC.get() + " | ");
            }
            if (!TextUtils.isEmpty(serviceRequestIemViewModel.ATC.get())) {
                info.append("ATC:" + serviceRequestIemViewModel.ATC.get() + " | ");
            }
        }
        if (info.length() > 2)
            info.delete(info.length() - 2, info.length() - 1);

        // Initialize a new SpannableStringBuilder instance
        SpannableStringBuilder ssBuilder = new SpannableStringBuilder(info);

        // Apply the bold text style span
        if (info.toString().contains("RTA"))
            ssBuilder.setSpan(
                    new StyleSpan(Typeface.BOLD), // Span to add
                    info.indexOf("RTA:"), // Start of the span (inclusive)
                    info.indexOf("RTA:") + String.valueOf("RTA:").length(), // End of the span (exclusive)
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE // Do not extend the span when text add later
            );

        // Apply the bold text style span
        if (info.toString().contains("ATA"))
            ssBuilder.setSpan(
                    new StyleSpan(Typeface.BOLD), // Span to add
                    info.indexOf("ATA:"), // Start of the span (inclusive)
                    info.indexOf("ATA:") + String.valueOf("ATA:").length(), // End of the span (exclusive)
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE // Do not extend the span when text add later
            );

        // Apply the bold text style span
        if (info.toString().contains("RTC"))
            ssBuilder.setSpan(
                    new StyleSpan(Typeface.BOLD), // Span to add
                    info.indexOf("RTC:"), // Start of the span (inclusive)
                    info.indexOf("RTC:") + String.valueOf("RTC:").length(), // End of the span (exclusive)
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE // Do not extend the span when text add later
            );
        // Apply the bold text style span
        if (info.toString().contains("ATC"))
            ssBuilder.setSpan(
                    new StyleSpan(Typeface.BOLD), // Span to add
                    info.indexOf("ATC:"), // Start of the span (inclusive)
                    info.indexOf("ATC:") + String.valueOf("ATC:").length(), // End of the span (exclusive)
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE // Do not extend the span when text add later
            );

        ((TextView) view).setText(ssBuilder);

    }

    @BindingAdapter({"arrowColor"})
    public static void arrowColor(View view, int status) {
        ImageView imageView = (ImageView) view;
        switch (status) {
            case 1:
                imageView.setColorFilter(ContextCompat.getColor(view.getContext(), android.R.color.holo_orange_dark));
                break;
            case 2:
                imageView.setColorFilter(ContextCompat.getColor(view.getContext(), android.R.color.holo_green_dark));
                break;
            case 3:
            case 4:
                imageView.setColorFilter(ContextCompat.getColor(view.getContext(), android.R.color.holo_blue_dark));
                break;
            case 5:
                imageView.setColorFilter(ContextCompat.getColor(view.getContext(), android.R.color.darker_gray));
                break;
            default:
                imageView.setColorFilter(ContextCompat.getColor(view.getContext(), android.R.color.holo_orange_dark));
                break;
        }
    }

    @BindingAdapter({"titleTextColor"})
    public static void titleTextColor(View view, int status) {
        TextView textView = (TextView) view;
        switch (status) {
            case 1:
                textView.setTextColor(ContextCompat.getColor(view.getContext(), android.R.color.holo_orange_dark));
                break;
            case 2:
                textView.setTextColor(ContextCompat.getColor(view.getContext(), android.R.color.holo_green_dark));
                break;
            case 3:
            case 4:
                textView.setTextColor(ContextCompat.getColor(view.getContext(), android.R.color.holo_blue_dark));
                break;
            case 5:
                textView.setTextColor(ContextCompat.getColor(view.getContext(), android.R.color.darker_gray));
                break;
            default:
                textView.setTextColor(ContextCompat.getColor(view.getContext(), android.R.color.holo_orange_dark));
                break;
        }
    }

    @BindingAdapter({"statusBackgroundColor"})
    public static void statusBackgroundColor(View view, int status) {
        TextView textView = (TextView) view;
        switch (status) {
            case 1:
                textView.setBackgroundTintList(view.getContext().getResources().getColorStateList(android.R.color.holo_orange_dark));
//                ((TextView) view).setText(R.string.new_job);
                break;
            case 2:
                textView.setBackgroundTintList(view.getContext().getResources().getColorStateList(android.R.color.holo_green_dark));
//                ((TextView) view).setText(R.string.ongoing);
                break;
            case 3:
            case 4:
                textView.setBackgroundTintList(view.getContext().getResources().getColorStateList(android.R.color.holo_blue_dark));
//                ((TextView) view).setText(R.string.completed);
                break;
            case 5:
                textView.setBackgroundTintList(view.getContext().getResources().getColorStateList(android.R.color.darker_gray));
//                ((TextView) view).setText(R.string.completed);
                break;
            default:
                textView.setBackgroundTintList(view.getContext().getResources().getColorStateList(android.R.color.holo_orange_dark));
//                ((TextView) view).setText(R.string.new_job);
                break;
        }
    }

    /*@BindingAdapter({"serviceStatus"})
    public static void serviceType(View view, int status) {
        *//*Drawable img = view.getContext().getResources().getDrawable(R.drawable.circle);
        switch (status) {
            case 0:
                img.setTint(ContextCompat.getColor(view.getContext(), android.R.color.holo_orange_dark));
                ((TextView) view).setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                ((TextView) view).setText(R.string.new_job);
                ((TextView) view).setTextColor(ContextCompat.getColor(view.getContext(), android.R.color.holo_orange_dark));
                break;
            case 1:
                img.setTint(ContextCompat.getColor(view.getContext(), android.R.color.holo_green_light));
                ((TextView) view).setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                ((TextView) view).setText(R.string.ongoing);
                ((TextView) view).setTextColor(ContextCompat.getColor(view.getContext(), android.R.color.holo_green_dark));
                break;
            case 2:
                img = view.getContext().getResources().getDrawable(R.drawable.ic_done_all);
                img.setTint(ContextCompat.getColor(view.getContext(), android.R.color.holo_blue_dark));
                ((TextView) view).setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                ((TextView) view).setText(R.string.completed);
                ((TextView) view).setTextColor(ContextCompat.getColor(view.getContext(), android.R.color.holo_blue_dark));
                break;
        }*//*
    }*/

}
