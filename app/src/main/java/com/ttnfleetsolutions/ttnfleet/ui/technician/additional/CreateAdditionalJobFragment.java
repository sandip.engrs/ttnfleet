package com.ttnfleetsolutions.ttnfleet.ui.technician.additional;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.ttnfleetsolutions.ttnfleet.BR;
import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.databinding.FragmentCreateAdditionalJobBinding;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseFragment;
import com.ttnfleetsolutions.ttnfleet.ui.technician.TechnicianDashboardActivity;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by Sandip Chaulagain on 11/22/2017.
 *
 * @version 1.0
 */

public class CreateAdditionalJobFragment extends BaseFragment<FragmentCreateAdditionalJobBinding, CreateAdditionalJobViewModel> implements CreateAdditionalJobNavigator {

    @Inject
    CreateAdditionalJobViewModel mViewModel;

    FragmentCreateAdditionalJobBinding mFragmentBinding;

    private String mSystemCode, mAssemblyCode, mSystemCodeName, mAssemblyCodeName, mComponentCodeStr;

    private ArrayList<String> mComponentCodeNameArray;

    @Override
    protected CreateAdditionalJobViewModel getViewModel() {
        return mViewModel;
    }

    @Override
    protected int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_create_additional_job;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModel.setNavigator(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFragmentBinding = getViewDataBinding();
        setUp();
    }

    private void setUp() {
        if (getArguments() != null) {
            mSystemCode = getArguments().getString("system");
            mAssemblyCode = getArguments().getString("assembly");
            mSystemCodeName = getArguments().getString("systemName");
            mAssemblyCodeName = getArguments().getString("assemblyName");
            mComponentCodeStr = getArguments().getString("componentCodeStr");
            mComponentCodeNameArray = getArguments().getStringArrayList("componentCodeNameArray");
        }
    }

    @Override
    public void handleError(Throwable throwable) {

    }

    public void actionDone() {
        ((TechnicianDashboardActivity) getActivity()).createAdditionalJobAPI(mFragmentBinding.breakdownReport.getText().toString(), mSystemCode, mAssemblyCode, mComponentCodeStr);
    }
}

