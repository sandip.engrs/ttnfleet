package com.ttnfleetsolutions.ttnfleet.ui.trucker.servicerequest.additional;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ttnfleetsolutions.ttnfleet.R;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */

public class AdditionalJobsRecyclerViewAdapter extends RecyclerView.Adapter<AdditionalJobsRecyclerViewAdapter.MyViewHolder> {

    private final String[] times;
    private final String[] desc;

    public AdditionalJobsRecyclerViewAdapter(Context context) {
        Context mContext = context;
        String[] tasks = mContext.getResources().getStringArray(R.array.additional_job_task);
        times = mContext.getResources().getStringArray(R.array.additional_job_task_duration);
        desc = mContext.getResources().getStringArray(R.array.additional_job_task_desc);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_additional_job_new, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
//        holder.task.setText(tasks[position]);
        holder.duration.setText(times[position]);
        holder.desc.setText(desc[position]);
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public final TextView duration;
        public final TextView desc;

        public MyViewHolder(View view) {
            super(view);
            duration = view.findViewById(R.id.datetime);
            desc = view.findViewById(R.id.issueDescription);
        }
    }
}
