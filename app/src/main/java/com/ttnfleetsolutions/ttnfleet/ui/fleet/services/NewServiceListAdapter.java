package com.ttnfleetsolutions.ttnfleet.ui.fleet.services;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseActivity;
import com.ttnfleetsolutions.ttnfleet.ui.fleet.DashboardActivity;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */

public class NewServiceListAdapter extends RecyclerView.Adapter<NewServiceListAdapter.MyViewHolder> {

    private Context mContext;
    private String mStatus;
    private String[] dates, numbers, issues, locations, models, vin;

    Integer selectedItemPosition = null;

    public NewServiceListAdapter(Context context, String status) {
        this.mContext = context;
        this.mStatus = status;
        dates = context.getResources().getStringArray(R.array.times);
        numbers = context.getResources().getStringArray(R.array.services);
        issues = context.getResources().getStringArray(R.array.services_issue_details);
        locations = mContext.getResources().getStringArray(R.array.locations);
        models = mContext.getResources().getStringArray(R.array.truck_array);
        vin = mContext.getResources().getStringArray(R.array.truck_number);
        if (((BaseActivity) mContext).isLandscape()) {
            selectedItemPosition = 0;
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_service_request_new, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        if (selectedItemPosition != null && selectedItemPosition == position) {
            // Here I am just highlighting the background
            holder.rowLayout.setBackgroundColor(mContext.getResources().getColor(R.color.tr_gray));
        } else {
            holder.rowLayout.setBackgroundColor(Color.TRANSPARENT);
        }

        if (mStatus.equalsIgnoreCase("new")) {
            holder.coloredLine.setBackgroundColor(ContextCompat.getColor(mContext, android.R.color.holo_orange_dark));
            Drawable img = mContext.getResources().getDrawable(R.drawable.circle2);
            img.setTint(ContextCompat.getColor(mContext, android.R.color.holo_orange_dark));
            holder.status.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
            holder.status.setTextColor(ContextCompat.getColor(mContext, android.R.color.holo_orange_dark));
            holder.rowLayout.setOnClickListener(view -> {
                if (selectedItemPosition != null) {
                    notifyItemChanged(selectedItemPosition);
                }
                selectedItemPosition = position;
                notifyItemChanged(selectedItemPosition);
                ((DashboardActivity) mContext).goToOrderDetails(position + 1);
            });
            if(position == 1) {
                holder.arrivalRepairTimingInfo.setVisibility(View.VISIBLE);
                holder.arrivalRepairTimingInfo.setText("Drop off: 8th Dec 12:45  |  Pick up: 19th Dec 08:00");
                holder.serviceIssue.setText("Service Type: Drop off & Pick up");
                holder.datetime.setText("Just now");
            } else {
                holder.arrivalRepairTimingInfo.setVisibility(View.GONE);
                holder.serviceIssue.setText("Front axle left wheel tyre damage");
                holder.datetime.setText("10 mins ago");
            }
            holder.serviceRequestName.setText(numbers[position]);
            holder.location.setText(locations[position]);
            holder.vehicleName.setText(models[position]);
            holder.vinNo.setText(vin[position]);
        } else {
            holder.coloredLine.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
            Drawable img = mContext.getResources().getDrawable(R.drawable.circle);
            img.setTint(ContextCompat.getColor(mContext, R.color.colorPrimary));
            holder.status.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
            holder.status.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
            holder.rowLayout.setOnClickListener(view -> {
                if (selectedItemPosition != null) {
                    notifyItemChanged(selectedItemPosition);
                }
                selectedItemPosition = position;
                notifyItemChanged(selectedItemPosition);
                ((DashboardActivity) mContext).goToOrderDetails(position + 1);
            });


            if(position == 0) {
                holder.arrivalRepairTimingInfo.setText("ETA: 17:10  |  ETC: 1 Hour");
                holder.datetime.setText("40 mins ago");
            } else {
                holder.arrivalRepairTimingInfo.setText("ATA: 17:10  |  ETC: 2 Hours");
                holder.datetime.setText("2 hours ago");
            }
            holder.serviceRequestName.setText(numbers[position + 2]);
            holder.location.setText(locations[position + 2]);
            holder.vehicleName.setText(models[position + 2]);
            holder.vinNo.setText(vin[position + 2]);
            holder.serviceIssue.setText(issues[position + 1]);
        }
        holder.status.setText(mStatus);

//        holder.date.setText(dates[position]);

        /*if(position > 0) {
            holder.status.setText("COMPLETED");
            holder.status.setTextColor(ContextCompat.getColor(mContext, android.R.color.holo_blue_dark));
            holder.count.setVisibility(View.GONE);
            holder.View.setBackgroundColor(ContextCompat.getColor(mContext, android.R.color.holo_blue_dark));
            Drawable img = mContext.getResources().getDrawable( R.drawable.ic_done_all );
            img.setTint(ContextCompat.getColor(mContext, android.R.color.holo_blue_dark));
            holder.status.setCompoundDrawablesWithIntrinsicBounds( img, null, null, null );
            holder.rowLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mContext.startActivity(new Intent(mContext, ServiceDetailsActivity.class).putExtra("index", 0));
                }
            });
        } else {
            holder.status.setText("ONGOING");
            holder.count.setVisibility(View.VISIBLE);
            holder.status.setTextColor(ContextCompat.getColor(mContext, android.R.color.holo_green_light));
            holder.View.setBackgroundColor(ContextCompat.getColor(mContext, android.R.color.holo_green_light));
            Drawable img = mContext.getResources().getDrawable( R.drawable.circle );
            holder.status.setCompoundDrawablesWithIntrinsicBounds( img, null, null, null );

            holder.rowLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mContext.startActivity(new Intent(mContext, ServiceDetailsActivity.class).putExtra("index", 0));
                }
            });
        }*/
//        holder.issue.setText(issues[position]);

    }

    @Override
    public int getItemCount() {
        if (mStatus.equalsIgnoreCase("new"))
            return 2;
        else
            return 2;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView status, serviceRequestName, datetime, serviceIssue, location, vehicleName, vinNo, arrivalRepairTimingInfo;
        public View coloredLine;
        public ConstraintLayout rowLayout;

        public MyViewHolder(View view) {
            super(view);
            coloredLine = view.findViewById(R.id.coloredLine);
            status = view.findViewById(R.id.status);
            serviceRequestName = view.findViewById(R.id.serviceRequestName);
            datetime = view.findViewById(R.id.datetime);
            serviceIssue = view.findViewById(R.id.serviceIssue);
            location = view.findViewById(R.id.location);
            vehicleName = view.findViewById(R.id.vehicleName);
            vinNo = view.findViewById(R.id.vinNo);
            rowLayout = view.findViewById(R.id.rowLayout);
            arrivalRepairTimingInfo = view.findViewById(R.id.arrivalRepairTimingInfo);
        }
    }
}
