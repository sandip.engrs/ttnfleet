package com.ttnfleetsolutions.ttnfleet.ui.fleet.services;

import com.ttnfleetsolutions.ttnfleet.data.model.api.event.listing.Event;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 4/4/2018.
 *
 * @version 1.0
 */
interface NewServiceFragmentNavigator {

    void getEvents(List<Event> eventList);

    void handleError(Throwable throwable);
}
