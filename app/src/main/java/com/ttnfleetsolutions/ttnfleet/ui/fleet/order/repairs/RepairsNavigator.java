package com.ttnfleetsolutions.ttnfleet.ui.fleet.order.repairs;

import android.arch.lifecycle.LiveData;

import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.Repair;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 11/10/2017.
 *
 * @version 1.0
 */

interface RepairsNavigator {

    void updateRepairs(LiveData<List<Repair>> repairs);

    void handleError(Throwable throwable);

}
