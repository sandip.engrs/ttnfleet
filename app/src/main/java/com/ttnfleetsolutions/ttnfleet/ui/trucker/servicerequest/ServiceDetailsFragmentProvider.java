package com.ttnfleetsolutions.ttnfleet.ui.trucker.servicerequest;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Sandip Chaulagain on 2/2/2018.
 *
 * @version 1.0
 */
@Module
public abstract class ServiceDetailsFragmentProvider {

    @ContributesAndroidInjector(modules = ServiceDetailsFragmentModule.class)
    abstract ServiceDetailsFragment provideServiceDetailsFragmentFactory();
}
