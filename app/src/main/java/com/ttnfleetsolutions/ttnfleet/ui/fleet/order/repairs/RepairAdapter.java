package com.ttnfleetsolutions.ttnfleet.ui.fleet.order.repairs;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.Repair;
import com.ttnfleetsolutions.ttnfleet.databinding.ItemRepairViewBinding;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseViewHolder;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 11/10/2017.
 *
 * @version 1.0
 */

public class RepairAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private List<Repair> mRepairList;

    private RepairItemViewModel.RepairItemViewModelListener mListener;

    public void setListener(RepairItemViewModel.RepairItemViewModelListener listener) {
        this.mListener = listener;
    }

    public RepairAdapter(List<Repair> repairList) {
        this.mRepairList = repairList;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemRepairViewBinding repairViewBinding = ItemRepairViewBinding.inflate(LayoutInflater.from(parent.getContext()),
                parent, false);
        return new RepairViewHolder(repairViewBinding);
        /*switch (viewType) {
            case VIEW_TYPE_NORMAL:

            case VIEW_TYPE_EMPTY:
            default:
                ItemBlogEmptyViewBinding emptyViewBinding = ItemBlogEmptyViewBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
                return new EmptyViewHolder(emptyViewBinding);
        }*/
    }

    @Override
    public int getItemViewType(int position) {
        if (mRepairList != null && mRepairList.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public int getItemCount() {
        if (mRepairList != null && mRepairList.size() > 0) {
            return mRepairList.size();
        } else {
            return 0;
        }
    }

    public void addItems(List<Repair> repairList) {
        mRepairList = repairList;
        notifyDataSetChanged();
    }

    public class RepairViewHolder extends BaseViewHolder implements RepairItemViewModel.RepairItemViewModelListener {

        private ItemRepairViewBinding mBinding;

        private RepairItemViewModel mRepairItemViewModel;

        public RepairViewHolder(ItemRepairViewBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {

            final Repair repair = mRepairList.get(position);

            mRepairItemViewModel = new RepairItemViewModel(repair, this);

            mBinding.setViewModel(mRepairItemViewModel);
          /*  switch (position) {
                case 0:
                    mBinding.approvalStatus.setText("Approved by you");
//                    mBinding.approvalStatus.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimaryDark));
//                    mBinding.view.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorPrimaryDark));
                    mBinding.threshold.setText("");
                    break;
                case 1:
                    mBinding.approvalStatus.setText("Need your approval");
//                    mBinding.approvalStatus.setTextColor(ContextCompat.getColor(mContext, android.R.color.holo_orange_dark));
//                    mBinding.view.setBackgroundColor(ContextCompat.getColor(mContext, android.R.color.holo_orange_dark));
                    mBinding.threshold.setText("(Threshold exceeded)");
                    break;
                case 2:
                case 4:
                    mBinding.approvalStatus.setText("Fix this later");
//                    mBinding.approvalStatus.setTextColor(ContextCompat.getColor(mContext, android.R.color.darker_gray));
//                    mBinding.view.setBackgroundColor(ContextCompat.getColor(mContext, android.R.color.darker_gray));
                    mBinding.threshold.setText("");

                    break;
            }*/
            // Immediate Binding
            // When a variable or observable changes, the binding will be scheduled to change before
            // the next frame. There are times, however, when binding must be executed immediately.
            // To force execution, use the executePendingBindings() method.
            mBinding.executePendingBindings();

        }

        @Override
        public void onItemClick(long id) {
            mListener.onItemClick(id);
        }
    }

    /*public class EmptyViewHolder extends BaseViewHolder implements RepairEmptyItemViewModel.RepairEmptyItemViewModelListener {

        private ItemBlogEmptyViewBinding mBinding;

        public EmptyViewHolder(ItemBlogEmptyViewBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {
            BlogEmptyItemViewModel emptyItemViewModel = new BlogEmptyItemViewModel(this);
            mBinding.setViewModel(emptyItemViewModel);
        }

        @Override
        public void onRetryClick() {
            mListener.onRetryClick();
        }
    }

    public interface BlogAdapterListener {
        void onRetryClick();
    }*/
}