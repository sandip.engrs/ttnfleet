package com.ttnfleetsolutions.ttnfleet.ui.trucker.history;

import com.ttnfleetsolutions.ttnfleet.ui.serviceprovider.history.ServiceHistoryFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Sandip Chaulagain on 11/10/2017.
 *
 * @version 1.0
 */
@Module
public abstract class ServiceHistoryFragmentProvider {

    @ContributesAndroidInjector(modules = ServiceHistoryFragmentModule.class)
    abstract ServicesHistoryFragment provideServicesHistoryFragmentFactory();

    @ContributesAndroidInjector(modules = ServiceHistoryFragmentModule.class)
    abstract ServiceHistoryFragment provideServicesHistoryFragmentSPFactory();

    @ContributesAndroidInjector(modules = ServiceHistoryFragmentModule.class)
    abstract com.ttnfleetsolutions.ttnfleet.ui.technician.history.ServiceHistoryFragment provideServicesHistoryFragmentTechFactory();
}
