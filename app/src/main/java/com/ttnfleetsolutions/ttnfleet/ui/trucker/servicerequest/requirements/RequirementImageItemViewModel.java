package com.ttnfleetsolutions.ttnfleet.ui.trucker.servicerequest.requirements;

import android.databinding.BindingAdapter;
import android.databinding.ObservableField;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.requirements.RequireImagesResponse;
import com.ttnfleetsolutions.ttnfleet.utils.GlideApp;

/**
 * Created by Sandip Chaulagain on 1/19/2018.
 *
 * @version 1.0
 */

public class RequirementImageItemViewModel {

    private RequireImagesResponse.Image mRequirementImage;
    private final ItemViewModelListener mListener;
    public ObservableField<String> url;
    public ObservableField<Integer> id;

    RequirementImageItemViewModel(RequireImagesResponse.Image requirementImage, ItemViewModelListener listener) {
        this.mRequirementImage = requirementImage;
        this.url = new ObservableField<>(requirementImage.getPhotoname());
        this.id = new ObservableField<>(requirementImage.getEventphototypeid());
        this.mListener = listener;
    }

    public void onItemClick(View view) {
        mListener.onItemClick(view, mRequirementImage.getPhotoname());
    }

    public interface ItemViewModelListener {
        void onItemClick(View thumbView, String url);
    }

    @BindingAdapter({"resource"})
    public static void setImageViewResource(View view, String url) {
        Log.e("TAG", "url: " + url);
        GlideApp.with(view.getContext())
                .load(url)
                .placeholder(R.mipmap.logo)
                .error(R.mipmap.logo)
                .centerCrop()
                .into((ImageView) view);
    }

    @BindingAdapter({"textType"})
    public static void setTextType(View view, int id) {
        String[] array = view.getContext().getResources().getStringArray(R.array.images);
        ((TextView) view).setText(array[id - 1]);
    }
}

