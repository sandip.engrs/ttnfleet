package com.ttnfleetsolutions.ttnfleet.ui.trucker.notification;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Sandip Chaulagain on 2/2/2018.
 *
 * @version 1.0
 */
@Module
public abstract class NotificationsFragmentProvider {

    @ContributesAndroidInjector(modules = NotificationsFragmentModule.class)
    abstract NotificationsFragment provideNotificationsFragmentFactory();
}
