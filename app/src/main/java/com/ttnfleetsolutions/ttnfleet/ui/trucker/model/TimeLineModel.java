package com.ttnfleetsolutions.ttnfleet.ui.trucker.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */
public class TimeLineModel implements Parcelable {

    public long id;
    private String mMessage;
    private String mDate;
    private String mAction;
    private OrderStatus mStatus;

    public TimeLineModel() {
    }

    public TimeLineModel(String message, String date, OrderStatus status, String action) {
        this.mMessage = message;
        this.mDate = date;
        this.mStatus = status;
        this.mAction = action;
    }

    public TimeLineModel(String message, String date, OrderStatus status) {
        this.mMessage = message;
        this.mDate = date;
        this.mStatus = status;
    }

    public String getMessage() {
        return mMessage;
    }

    public void semMessage(String message) {
        this.mMessage = message;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        this.mDate = date;
    }

    public OrderStatus getStatus() {
        return mStatus;
    }

    public void setStatus(OrderStatus mStatus) {
        this.mStatus = mStatus;
    }

    public String getAction() {
        return mAction;
    }

    public void setAction(String mAction) {
        this.mAction = mAction;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mMessage);
        dest.writeString(this.mDate);
        dest.writeInt(this.mStatus == null ? -1 : this.mStatus.ordinal());
    }

    private TimeLineModel(Parcel in) {
        this.mMessage = in.readString();
        this.mDate = in.readString();
        int tmpMStatus = in.readInt();
        this.mStatus = tmpMStatus == -1 ? null : OrderStatus.values()[tmpMStatus];
    }

    public static final Parcelable.Creator<TimeLineModel> CREATOR = new Parcelable.Creator<TimeLineModel>() {
        @Override
        public TimeLineModel createFromParcel(Parcel source) {
            return new TimeLineModel(source);
        }

        @Override
        public TimeLineModel[] newArray(int size) {
            return new TimeLineModel[size];
        }
    };
}
