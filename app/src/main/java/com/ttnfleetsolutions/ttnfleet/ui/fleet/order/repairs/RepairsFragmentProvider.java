package com.ttnfleetsolutions.ttnfleet.ui.fleet.order.repairs;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Sandip Chaulagain on 11/10/2017.
 *
 * @version 1.0
 */
@Module
public abstract class RepairsFragmentProvider {

    @ContributesAndroidInjector(modules = RepairsFragmentModule.class)
    abstract RepairsFragment provideRepairsFragmentFactory();
}
