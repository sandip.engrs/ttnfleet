package com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.driver;

import android.databinding.BindingAdapter;
import android.databinding.ObservableField;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.request.RequestOptions;
import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.DriverListResponse;
import com.ttnfleetsolutions.ttnfleet.data.remote.ApiEndPoint;
import com.ttnfleetsolutions.ttnfleet.utils.GlideApp;

/**
 * Created by Sandip Chaulagain on 1/19/2018.
 *
 * @version 1.0
 */

public class SelectDriverItemViewModel {

    private DriverListResponse.Driver mDriver;
    public ObservableField<String> name, icon;
    private ItemViewModelListener mListener;

    SelectDriverItemViewModel(DriverListResponse.Driver driver, ItemViewModelListener listener) {
        this.mDriver = driver;
        this.mListener = listener;
        name = new ObservableField<>(driver.getDisplaytitle());
        icon = new ObservableField<>(ApiEndPoint.DIR_IMAGE_PROFILE + driver.getUserid() + "/" + driver.getImage());
    }

    public void onItemClick() {
        mListener.onItemClick(mDriver.getUserid());
    }

    public interface ItemViewModelListener {
        void onItemClick(int driverId);
    }

    @BindingAdapter({"profileImage"})
    public static void setImageViewResource(View view, String url) {
        GlideApp.with(view.getContext())
                .load(url)
                .apply(RequestOptions.circleCropTransform())
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .into((ImageView) view);
    }
}
