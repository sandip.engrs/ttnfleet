package com.ttnfleetsolutions.ttnfleet.ui.technician.inspection;


import android.databinding.ObservableField;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Toast;

import com.ttnfleetsolutions.ttnfleet.BR;
import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.ConfirmBreakdownResponse;
import com.ttnfleetsolutions.ttnfleet.databinding.FragmentJobInspectionTechBinding;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.servicerequest.callback.UpdateEventDetailsCallbackTech;

import javax.inject.Inject;

/**
 * A simple {@link Fragment} subclass.
 */
public class JobInspectionTechFragment extends BaseFragment<FragmentJobInspectionTechBinding, JobInspectionViewModel> implements JobInspectionNavigator {

    @Inject
    JobInspectionViewModel mViewModel;

    FragmentJobInspectionTechBinding mFragmentBinding;

    @Override
    protected JobInspectionViewModel getViewModel() {
        return mViewModel;
    }

    @Override
    protected int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_job_inspection_tech;
    }

    private int mJobId;

    private String mEventType, mEventBreakdownNote;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModel.setNavigator(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFragmentBinding = getViewDataBinding();
        setUp();
    }

    private void setUp() {
        if (getArguments() != null) {
            mJobId = getArguments().getInt("jobId");
            mEventType = getArguments().getString("eventType");
            mEventBreakdownNote = getArguments().getString("breakdownNote");
        }
        /*ColorStateList colorStateList = new ColorStateList(
                new int[][]{
                        new int[]{-android.R.attr.state_checked}, // unchecked
                        new int[]{android.R.attr.state_checked}  // checked
                },
                new int[]{
                        Color.DKGRAY,
                        getViewModel().getDataManager().getThemeTitleBarColor()
                }
        );
        mFragmentBinding.checkBoxProblem.setButtonTintList(colorStateList);*/
        getViewModel().setEventType(new ObservableField<>(mEventType));
        getViewModel().setBreakdownType(new ObservableField<>(mEventBreakdownNote));

        mFragmentBinding.editJob.setVisibility(View.GONE);
        mFragmentBinding.additionalJob.setVisibility(View.GONE);
    }

    public void actionDone() {
        if (mFragmentBinding.checkBoxProblem.isChecked())
            getViewModel().confirmBreakdown(mJobId);
        else getActivity().onBackPressed();
    }

    @Override
    public void onConfirmBreakdownResponseReceived(ConfirmBreakdownResponse confirmBreakdownResponse) {
        getActivity().onBackPressed();
        mUpdateEventDetailsCallbackTech.onConfirmedBreakdown();
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(getActivity(), getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
    }

    private UpdateEventDetailsCallbackTech mUpdateEventDetailsCallbackTech;

    public void setUpdateEventDetailsCallback(UpdateEventDetailsCallbackTech mUpdateEventDetailsCallbackTech) {
        this.mUpdateEventDetailsCallbackTech = mUpdateEventDetailsCallbackTech;
    }
    /*private TextView editJob;
    private View jobInspectionTechView;
    private Handler mHandler = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        jobInspectionTechView = inflater.inflate(R.layout.fragment_job_inspection_tech, container, false);

        editJob = jobInspectionTechView.findViewById(R.id.editJob);
        editJob.setOnClickListener(view -> {
            EditReportDialogFragment fragment = new EditReportDialogFragment();
            fragment.show(getActivity().getSupportFragmentManager(), "Edit Report");
        });

//        jobInspectionTechView.findViewById(R.id.additionalJob).setOnClickListener(view -> startActivity(new Intent(getActivity(), AdditionalJobActivity.class)));
        jobInspectionTechView.findViewById(R.id.additionalJob).setOnClickListener(view -> ((TechnicianDashboardActivity) getActivity()).additionalJobs(0));

//        final Drawable img = getResources().getDrawable(R.drawable.ic_action_edit);
        editJob.setEnabled(false);
//        img.setTint(ContextCompat.getColor(JobInspectionTechActivity.this, android.R.color.darker_gray));
//        mEditDetails.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
//        mEditDetails.setTextColor(ContextCompat.getColor(JobInspectionTechActivity.this, android.R.color.darker_gray));


        final CheckBox checkBox = jobInspectionTechView.findViewById(R.id.checkBoxProblem);
        checkBox.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                editJob.setEnabled(false);
                jobInspectionTechView.findViewById(R.id.editedByTechnician).setVisibility(View.GONE);
//                    img.setTint(ContextCompat.getColor(JobInspectionTechActivity.this, android.R.color.darker_gray));
//                    mEditDetails.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                editJob.setBackgroundColor(ContextCompat.getColor(getActivity(), android.R.color.darker_gray));
            } else {
                jobInspectionTechView.findViewById(R.id.editedByTechnician).setVisibility(View.VISIBLE);
                editJob.setEnabled(true);
//                    img.setTint(ContextCompat.getColor(JobInspectionTechActivity.this, android.R.color.holo_orange_dark));
//                    mEditDetails.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                editJob.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
            }
        });

        QueryBuilder queryBuilder = new QueryBuilder()
                .withWeight();
        String query = queryBuilder.build();

        FontRequest request = new FontRequest(
                "com.google.android.gms.fonts",
                "com.google.android.gms",
                query,
                R.array.com_google_android_gms_fonts_certs);

        FontsContractCompat.FontRequestCallback callback = new FontsContractCompat
                .FontRequestCallback() {
            @Override
            public void onTypefaceRetrieved(Typeface typeface) {
                checkBox.setTypeface(typeface);
            }

            @Override
            public void onTypefaceRequestFailed(int reason) {
            }
        };
        FontsContractCompat.requestFont(getActivity(), request, callback, getHandlerThreadHandler());

        return jobInspectionTechView;
    }

    private Handler getHandlerThreadHandler() {
        if (mHandler == null) {
            HandlerThread handlerThread = new HandlerThread("fonts");
            handlerThread.start();
            mHandler = new Handler(handlerThread.getLooper());
        }
        return mHandler;
    }
*/
}
