package com.ttnfleetsolutions.ttnfleet.ui.serviceprovider.technician;

import android.util.Log;

import com.google.gson.Gson;
import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.AcceptWorkOrderRequest;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.AssignAcceptWorkOrderRequest;
import com.ttnfleetsolutions.ttnfleet.data.model.api.technician.Technician;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseViewModel;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.details.CreateEventFragment;
import com.ttnfleetsolutions.ttnfleet.utils.rx.SchedulerProvider;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 1/22/2018.
 *
 * @version 1.0
 */

public class AllocateTechnicianViewModel extends BaseViewModel<AllocateTechnicianNavigator> {

    AllocateTechnicianViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    private List<Technician> mTechnicianList;

    public List<Technician> getTechnicianList() {
        return mTechnicianList;
    }

    public void setTechnicianList(List<Technician> mTechnicianList) {
        this.mTechnicianList = mTechnicianList;
    }

    void getTechnicianListAPICall() {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .doServerTechnicianListingApiCall(getDataManager().getCurrentUserID())
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    if (response != null) {
                        setTechnicianList(response.getResult());
                        getNavigator().getTechnicianList(response.getResult());
                    }
                    setIsLoading(false);
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                }));
    }

   private JSONObject getAcceptWorkOrderRequest(int eventId, int serviceProviderId) {
        AcceptWorkOrderRequest acceptWorkOrderRequest = new AcceptWorkOrderRequest(eventId, serviceProviderId);
        try {
            JSONObject jsonObject = new JSONObject(new Gson().toJson(acceptWorkOrderRequest, AcceptWorkOrderRequest.class));
            JSONObject dataObject = new JSONObject();
            dataObject.put("data", jsonObject);
            Log.e(CreateEventFragment.class.getSimpleName(), "Json -- " + dataObject.toString());
            return dataObject;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private JSONObject getAssignAcceptWorkOrderRequest(int workOrderStatusId, boolean isAccepted, int technicianId) {
        AssignAcceptWorkOrderRequest assignAcceptWorkOrderRequest = new AssignAcceptWorkOrderRequest(workOrderStatusId, isAccepted, technicianId);
        try {
            JSONObject jsonObject = new JSONObject(new Gson().toJson(assignAcceptWorkOrderRequest, AssignAcceptWorkOrderRequest.class));
            JSONObject dataObject = new JSONObject();
            dataObject.put("data", jsonObject);
            Log.e(CreateEventFragment.class.getSimpleName(), "Json -- " + dataObject.toString());
            return dataObject;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    void acceptWorkOrdersAPICall(int eventId, int serviceProviderId) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .doServerAcceptWorkOrderApiCall(getAcceptWorkOrderRequest(eventId, serviceProviderId))
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    getNavigator().onAcceptWorkOrders(response);
                    setIsLoading(false);
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleErrorOnAccept(throwable);
                }));
    }

    void acceptAssignWorkOrdersAPICall(int workOrderId, boolean isAccepted, int technicianId) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .doServerAcceptAssignWorkOrderApiCall(getAssignAcceptWorkOrderRequest(workOrderId, isAccepted, technicianId))
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    getNavigator().onAcceptWorkOrders(response);
                    setIsLoading(false);
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleErrorOnAcceptAssign(throwable);
                }));
    }
}
