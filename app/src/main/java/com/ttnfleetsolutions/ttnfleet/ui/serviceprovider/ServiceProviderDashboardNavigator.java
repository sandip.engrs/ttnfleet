package com.ttnfleetsolutions.ttnfleet.ui.serviceprovider;

import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.AcceptWorkOrderResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.RejectionTypesResponse;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 1/10/2018.
 *
 * @version 1.0
 */

public interface ServiceProviderDashboardNavigator {

    void handleError(Throwable throwable);

    void getRejectionTypes(int eventId, int workOrderId, int workOrderStatusId, List<RejectionTypesResponse.RejectionType> rejectionTypeList);

    void onRejectWorkOrders(AcceptWorkOrderResponse acceptWorkOrderResponse);
}
