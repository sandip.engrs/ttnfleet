package com.ttnfleetsolutions.ttnfleet.ui.fleet.services;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.ttnfleetsolutions.ttnfleet.BR;
import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.listing.Event;
import com.ttnfleetsolutions.ttnfleet.databinding.FragmentFleetNewServiceBinding;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseActivity;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseFragment;
import com.ttnfleetsolutions.ttnfleet.ui.fleet.DashboardActivity;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.dashboard.OngoingServiceAdapter;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.dashboard.ServiceRequestIemViewModel;
import com.ttnfleetsolutions.ttnfleet.utils.CommonUtils;
import com.ttnfleetsolutions.ttnfleet.utils.VerticalSpaceItemDecoration;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import static com.ttnfleetsolutions.ttnfleet.utils.AppConstants.VERTICAL_LIST_ITEM_SPACE;

/**
 * Created by Sandip Chaulagain on 10/7/2017.
 *
 * @version 1.0
 */

public class NewServiceFragment extends BaseFragment<FragmentFleetNewServiceBinding, NewServiceFragmentViewModel> implements NewServiceFragmentNavigator {

    @Inject
    NewServiceFragmentViewModel mNewServiceFragmentViewModel;

    FragmentFleetNewServiceBinding mFragmentFleetNewServiceBinding;

    @Inject
    OngoingServiceAdapter mAdapter;

    private int mStatus;

    @Override
    protected NewServiceFragmentViewModel getViewModel() {
        return mNewServiceFragmentViewModel;
    }

    @Override
    protected int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_fleet_new_service;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mNewServiceFragmentViewModel.setNavigator(this);
        if (getArguments() != null)
            mStatus = getArguments().getInt("status");
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFragmentFleetNewServiceBinding = getViewDataBinding();
        setUp();
    }

    private void setUp() {
        mFragmentFleetNewServiceBinding.swiperefresh.setOnRefreshListener(
                () -> getEvents()
        );
        mFragmentFleetNewServiceBinding.servicesRecyclerViewFleet.setLayoutManager(new LinearLayoutManager(getActivity()));
        mFragmentFleetNewServiceBinding.servicesRecyclerViewFleet.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_LIST_ITEM_SPACE));
        mAdapter.setFrom(1);
        mAdapter.setIsLandscape(((BaseActivity) getActivity()).isLandscape());
        mAdapter.setEmptyItemViewModelListener(this::getEvents);
        mAdapter.setListener(listener);

        if (getViewModel().getEventList() != null && getViewModel().getEventList().size() > 0 && !((DashboardActivity) getActivity()).getViewModel().isReloadNeeded) {
            getEvents(getViewModel().getEventList());
        } else {
            getEvents();
        }
    }

    private void getEvents() {
        if (!isNetworkConnected()) {
            mAdapter.setEmptyItem(CommonUtils.getEmptyItem(getActivity(), DataManager.ErrorState.NETWORK_ERROR));
            mFragmentFleetNewServiceBinding.servicesRecyclerViewFleet.setAdapter(mAdapter);
        } else {
            mFragmentFleetNewServiceBinding.swiperefresh.setRefreshing(true);
            getViewModel().getEvents(mStatus);
        }
    }

    private ServiceRequestIemViewModel.ServiceRequestItemViewModelListener listener = new ServiceRequestIemViewModel.ServiceRequestItemViewModelListener() {
        @Override
        public void onItemClick(int id) {
            ((DashboardActivity) Objects.requireNonNull(getActivity())).goToOrderDetails(id);
            for (Event event : getViewModel().getEventList()) {
                if (event.getEventid() == id)
                    event.setSelected(true);
                else event.setSelected(false);
            }
            mAdapter.notifyDataSetChanged();
        }

        @Override
        public void onDispatchClick(int id) {
            ((DashboardActivity) Objects.requireNonNull(getActivity())).dispatchService(id, true);
        }

        @Override
        public void onAddNoteClick(int id, String note) {
            ((DashboardActivity) Objects.requireNonNull(getActivity())).addNoteToEvent(id, note);
        }
    };

    @Override
    public void getEvents(List<Event> eventList) {
        mFragmentFleetNewServiceBinding.swiperefresh.setRefreshing(false);
        ((DashboardActivity) getActivity()).getViewModel().isReloadNeeded = false;
        if (eventList != null) {
            mAdapter.addItems(eventList);
            mFragmentFleetNewServiceBinding.servicesRecyclerViewFleet.setAdapter(mAdapter);
            if (mStatus == DataManager.FilterDashboardHistory.NEW.getType() && ((BaseActivity) getActivity()).isLandscape() && eventList.size() > 0) {
                listener.onItemClick(eventList.get(0).getEventid());
            }
        } else {
            mAdapter.setEmptyItem(CommonUtils.getEmptyItem(getActivity(), DataManager.ErrorState.NO_DATA));
            mFragmentFleetNewServiceBinding.servicesRecyclerViewFleet.setAdapter(mAdapter);
        }
    }

    public void loadFirstItemId() {
        try {
            mFragmentFleetNewServiceBinding.swiperefresh.setRefreshing(false);
            if (getViewModel() != null && getViewModel().getEventList() != null && getViewModel().getEventList().size() > 0) {
                mFragmentFleetNewServiceBinding.servicesRecyclerViewFleet.scrollToPosition(0);
                listener.onItemClick(getViewModel().getEventList().get(0).getEventid());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void handleError(Throwable throwable) {
        mAdapter.setEmptyItem(CommonUtils.getEmptyItem(getActivity(), DataManager.ErrorState.API_FAIL));
        mFragmentFleetNewServiceBinding.servicesRecyclerViewFleet.setAdapter(mAdapter);
    }
}