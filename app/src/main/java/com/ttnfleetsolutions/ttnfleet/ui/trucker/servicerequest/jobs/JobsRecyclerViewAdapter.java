package com.ttnfleetsolutions.ttnfleet.ui.trucker.servicerequest.jobs;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.github.vipulasri.timelineview.TimelineView;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.listing.Job;
import com.ttnfleetsolutions.ttnfleet.databinding.RowEventDetailsJobBinding;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseViewHolder;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */

public class JobsRecyclerViewAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private List<Job> mList;

    private JobItemViewModel.ItemViewModelListener mListener;

    public void setListener(JobItemViewModel.ItemViewModelListener listener) {
        this.mListener = listener;
    }

    public JobsRecyclerViewAdapter(List<Job> list) {
        this.mList = list;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RowEventDetailsJobBinding rowBinding = RowEventDetailsJobBinding.inflate(LayoutInflater.from(parent.getContext()),
                parent, false);
        return new MyViewHolder(rowBinding);
    }

    @Override
    public int getItemViewType(int position) {
        return TimelineView.getTimeLineViewType(position, getItemCount());
    }

    /*@Override
    public int getItemViewType(int position) {
        if (mList != null && mList.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }*/

    @Override
    public int getItemCount() {
        if (mList != null && mList.size() > 0) {
            return mList.size();
        } else {
            return 0;
        }
    }

    public void addItems(List<Job> list) {
        mList = list;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends BaseViewHolder implements JobItemViewModel.ItemViewModelListener {

        private RowEventDetailsJobBinding mBinding;

        private JobItemViewModel mItemViewModel;

        public MyViewHolder(RowEventDetailsJobBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {

            final Job job = mList.get(position);

            mItemViewModel = new JobItemViewModel(job, this);

            mBinding.setViewModel(mItemViewModel);

            // Immediate Binding
            // When a variable or observable changes, the binding will be scheduled to change before
            // the next frame. There are times, however, when binding must be executed immediately.
            // To force execution, use the executePendingBindings() method.
            mBinding.executePendingBindings();

        }

        @Override
        public void onItemClick(int id) {

        }
    }
}
