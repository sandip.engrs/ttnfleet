package com.ttnfleetsolutions.ttnfleet.ui.technician.requirements;

import android.databinding.BindingAdapter;
import android.databinding.ObservableField;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.requirements.ImageRequirement;
import com.ttnfleetsolutions.ttnfleet.utils.GlideApp;

import java.io.File;

/**
 * Created by Sandip Chaulagain on 6/22/2018.
 *
 * @version 1.0
 */
public class ImageRequirementItemViewModel {

    private ImageRequirement mImageRequirement;
    public ObservableField<Integer> index;
    public ObservableField<String> label;
    public ObservableField<Uri> fileName;

    private ItemViewModelListener mListener;

    public ImageRequirementItemViewModel(ImageRequirement imageRequirement, ItemViewModelListener listener) {
        this.mImageRequirement = imageRequirement;
        this.mListener = listener;
        index = new ObservableField<>(imageRequirement.getIndex());
        label = new ObservableField<>(imageRequirement.getLabel());
        fileName = new ObservableField<>(imageRequirement.getFilePath());
    }

    @BindingAdapter({"resource"})
    public static void setImageViewResource(View view, Uri path) {
        if (path == null) return;
        Log.e("TAG", "url " + path);
        if (path.getPath().contains("/data/data/" + view.getContext().getPackageName() + "/files/")) {
            GlideApp.with(view.getContext())
                    .load(new File(path.getPath()))
                    .placeholder(R.color.colorPrimary)
                    .error(R.color.colorPrimary)
                    .fitCenter()
                    .into((ImageView) view);
        } else {
            GlideApp.with(view.getContext())
                    .load(path)
                    .placeholder(R.color.colorPrimary)
                    .error(R.color.colorPrimary)
                    .fitCenter()
                    .into((ImageView) view);
        }

    }

    public void onItemClick() {
        mListener.onItemClick(mImageRequirement.getIndex());
    }

    public interface ItemViewModelListener {
        void onItemClick(int index);
    }
}
