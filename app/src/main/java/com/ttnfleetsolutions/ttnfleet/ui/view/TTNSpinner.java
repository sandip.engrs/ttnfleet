package com.ttnfleetsolutions.ttnfleet.ui.view;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.util.AttributeSet;

import com.ttnfleetsolutions.ttnfleet.R;

/**
 * Created by Sandip Chaulagain on 6/7/2018.
 *
 * @version 1.0
 */
public class TTNSpinner extends android.support.v7.widget.AppCompatSpinner {

    public TTNSpinner(Context context) {
        super(context);
        init();
    }

    public TTNSpinner(Context context, int mode) {
        super(context, mode);
    }

    public TTNSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TTNSpinner(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public TTNSpinner(Context context, AttributeSet attrs, int defStyleAttr, int mode) {
        super(context, attrs, defStyleAttr, mode);
    }

    public TTNSpinner(Context context, AttributeSet attrs, int defStyleAttr, int mode, Resources.Theme popupTheme) {
        super(context, attrs, defStyleAttr, mode, popupTheme);
    }

    void init() {
        setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.orange)));
    }
}
