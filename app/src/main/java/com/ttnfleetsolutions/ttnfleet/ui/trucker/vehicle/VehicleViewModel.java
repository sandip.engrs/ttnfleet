package com.ttnfleetsolutions.ttnfleet.ui.trucker.vehicle;

import android.arch.lifecycle.LiveData;

import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.Vehicle;
import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseViewModel;
import com.ttnfleetsolutions.ttnfleet.utils.rx.SchedulerProvider;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 1/22/2018.
 *
 * @version 1.0
 */

public class VehicleViewModel extends BaseViewModel<VehicleNavigator> {

    public VehicleViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    private LiveData<List<Vehicle>> mVehicleLiveData;

    public LiveData<List<Vehicle>> getVehicleLiveData() {
        return mVehicleLiveData;
    }

    void getVehicles() {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .getAllVehicles()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(listLiveData -> {
                    if (listLiveData != null) {
                        mVehicleLiveData = listLiveData;
                        getNavigator().updateVehicles(mVehicleLiveData);
                    }
                    setIsLoading(false);
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                }));
    }
}
