package com.ttnfleetsolutions.ttnfleet.ui.fleet;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.github.vipulasri.timelineview.TimelineView;
import com.ttnfleetsolutions.ttnfleet.R;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */
public class TimeLineViewHolder extends RecyclerView.ViewHolder {

    TextView mDate;
    TextView mMessage, button;
    TimelineView mTimelineView;

    public TimeLineViewHolder(View itemView, int viewType) {
        super(itemView);
        mDate = itemView.findViewById(R.id.timelineDate);
        mMessage = itemView.findViewById(R.id.timelineMessage);
        button = itemView.findViewById(R.id.confirm);
        mTimelineView = itemView.findViewById(R.id.time_marker);
        mTimelineView.initLine(viewType);
    }
}
