package com.ttnfleetsolutions.ttnfleet.ui.fleet.provider;

import android.util.Log;

import com.google.gson.Gson;
import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.DispatchEventRequest;
import com.ttnfleetsolutions.ttnfleet.data.model.api.serviceprovider.ServiceProvider;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseViewModel;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.details.CreateEventFragment;
import com.ttnfleetsolutions.ttnfleet.utils.rx.SchedulerProvider;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 4/4/2018.
 *
 * @version 1.0
 */
public class ServiceProviderFragmentViewModel extends BaseViewModel<ServiceProviderFragmentNavigator> {

    ServiceProviderFragmentViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    private List<ServiceProvider> mServiceProviderList;

    public List<ServiceProvider> getServiceProviderList() {
        return mServiceProviderList;
    }

    public void setServiceProviderList(List<ServiceProvider> mServiceProviderList) {
        this.mServiceProviderList = mServiceProviderList;
    }

    void getServiceProviderListAPICall() {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .doServerServiceProviderListingApiCall()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    setServiceProviderList(response.getResult());
                    getNavigator().loadServiceProviderList(response.getResult());
                    setIsLoading(false);
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                }));
    }

    private JSONObject getDispatchEventRequest(int eventId, int serviceProviderId) {
        DispatchEventRequest dispatchEventRequest = new DispatchEventRequest(eventId, serviceProviderId);
        try {
            JSONObject jsonObject = new JSONObject(new Gson().toJson(dispatchEventRequest, DispatchEventRequest.class));
            JSONObject dataObject = new JSONObject();
            dataObject.put("data", jsonObject);
            Log.e(CreateEventFragment.class.getSimpleName(), "Json -- " + dataObject.toString());
            return dataObject;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    void dispatchEventAPICall(int eventId, int serviceProviderId) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .doServerDispatchEventApiCall(getDispatchEventRequest(eventId, serviceProviderId))
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    getNavigator().onDispatchedResponseReceived(response);
                    setIsLoading(false);
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleErrorDispatch(throwable);
                }));
    }
}
