package com.ttnfleetsolutions.ttnfleet.ui.fleet.eventdetails;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.ttnfleetsolutions.ttnfleet.BR;
import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.data.model.ServiceRequestSummary;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.listing.Event;
import com.ttnfleetsolutions.ttnfleet.databinding.FragmentFdOrderDetailsNewBinding;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseActivity;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseFragment;
import com.ttnfleetsolutions.ttnfleet.ui.fleet.DashboardActivity;
import com.ttnfleetsolutions.ttnfleet.ui.fleet.EstimationRecyclerViewAdapter;
import com.ttnfleetsolutions.ttnfleet.ui.fleet.TaskRecyclerViewAdapter;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.model.OrderStatus;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.model.TimeLineModel;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.servicerequest.timeline.TimelineAdapter;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.servicerequest.timeline.TimelineItemViewModel;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.servicerequest.workorder.WorkOrderRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * A simple {@link Fragment} subclass.
 */
public class EventDetailsFragment extends BaseFragment<FragmentFdOrderDetailsNewBinding, EventDetailsViewModel> implements EventDetailsNavigator {

    @Inject
    EventDetailsViewModel mEventDetailsViewModel;

    FragmentFdOrderDetailsNewBinding mFragmentFdOrderDetailsNewBinding;

    @Inject
    WorkOrderRecyclerViewAdapter mWorkOrderRecyclerViewAdapter;

    private int mEventId;

    @Override
    protected EventDetailsViewModel getViewModel() {
        return mEventDetailsViewModel;
    }

    @Override
    protected int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_fd_order_details_new;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mEventDetailsViewModel.setNavigator(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFragmentFdOrderDetailsNewBinding = getViewDataBinding();
        setUp();
    }

    private void setUp() {
        if (getArguments() != null) {
            mEventId = getArguments().getInt("eventId");
        }
        if (!((BaseActivity) getActivity()).isLandscape())
            ((DashboardActivity) getActivity()).mToolbar.setTitle("Event #" + mEventId);
        else
            ((TextView) getActivity().findViewById(R.id.subTitle)).setText("Event #" + mEventId);

        mFragmentFdOrderDetailsNewBinding.workOrderRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mFragmentFdOrderDetailsNewBinding.workOrderRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));

        getViewModel().getEventDetailsAPICall(mEventId);
        mFragmentFdOrderDetailsNewBinding.chatFloatingActionButton.setOnClickListener(view -> ((DashboardActivity) getActivity()).showServiceChatThreadDetails(0, "Service Request 1"));

        switch (index) {
            case 1:
                mFragmentFdOrderDetailsNewBinding.status.setText("NEW");
                mFragmentFdOrderDetailsNewBinding.status.setBackgroundTintList(getResources().getColorStateList(android.R.color.holo_orange_dark));

                mFragmentFdOrderDetailsNewBinding.editedByTechnicianLabel.setVisibility(View.GONE);
                mFragmentFdOrderDetailsNewBinding.tasksRecyclerView.setVisibility(View.GONE);
                mFragmentFdOrderDetailsNewBinding.effortsBackground.setVisibility(View.GONE);
                mFragmentFdOrderDetailsNewBinding.totalTime.setVisibility(View.GONE);
                mFragmentFdOrderDetailsNewBinding.totalCost.setVisibility(View.GONE);

                mFragmentFdOrderDetailsNewBinding.serviceProviderDetailsLabel.setVisibility(View.GONE);
                mFragmentFdOrderDetailsNewBinding.serviceDispatcherName.setVisibility(View.GONE);
                mFragmentFdOrderDetailsNewBinding.technicianDetails.setVisibility(View.GONE);

                if (from == 2) {
                    //drop off and pick up - new
                    mFragmentFdOrderDetailsNewBinding.datetime.setText("Just now");
                    mFragmentFdOrderDetailsNewBinding.breakdownReportLabel.setText(getString(R.string.service_required));
                    mFragmentFdOrderDetailsNewBinding.description.setText("Service Type: Drop off & Pick up");
                    mFragmentFdOrderDetailsNewBinding.issueDescription.setText("Drop off: 8th Dec 12:45  |  Pick up: 19th Dec 08:00");
                    mFragmentFdOrderDetailsNewBinding.serviceIssue.setText(getString(R.string.dummy_services));

                    mFragmentFdOrderDetailsNewBinding.estimationDetailsLabel.setVisibility(View.VISIBLE);
                    mFragmentFdOrderDetailsNewBinding.estimationDetailsRecyclerView.setVisibility(View.VISIBLE);
                    mFragmentFdOrderDetailsNewBinding.estimationDetailsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                    mFragmentFdOrderDetailsNewBinding.estimationDetailsRecyclerView.setHasFixedSize(true);
                    mFragmentFdOrderDetailsNewBinding.estimationDetailsRecyclerView.setNestedScrollingEnabled(false);
                    mFragmentFdOrderDetailsNewBinding.estimationDetailsRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
                    mFragmentFdOrderDetailsNewBinding.estimationDetailsRecyclerView.setAdapter(new EstimationRecyclerViewAdapter(getActivity()));

                } else {
                    mFragmentFdOrderDetailsNewBinding.datetime.setText("10 mins ago");
                    mFragmentFdOrderDetailsNewBinding.location.setText("3884 Eu Road, Boston, MA, 43448");
                    mFragmentFdOrderDetailsNewBinding.vehicleName.setText("Maan Truck 8.136 FAE 4X4");
                    mFragmentFdOrderDetailsNewBinding.vinNo.setText("WVM56502016034590");
                }

                break;
            case 2:
                mFragmentFdOrderDetailsNewBinding.status.setText("ONGOING");
                mFragmentFdOrderDetailsNewBinding.status.setBackgroundTintList(getResources().getColorStateList(R.color.colorPrimary));
                if (from == 1) {
                    mFragmentFdOrderDetailsNewBinding.editedByTechnicianLabel.setVisibility(View.GONE);
                    mFragmentFdOrderDetailsNewBinding.tasksRecyclerView.setVisibility(View.GONE);
                    mFragmentFdOrderDetailsNewBinding.effortsBackground.setVisibility(View.GONE);
                    mFragmentFdOrderDetailsNewBinding.totalTime.setVisibility(View.GONE);
                    mFragmentFdOrderDetailsNewBinding.totalCost.setVisibility(View.GONE);
                    mFragmentFdOrderDetailsNewBinding.datetime.setText("40 mins ago");

                    mFragmentFdOrderDetailsNewBinding.location.setText("State Highway 580, Tampa, FL, USA");
                    mFragmentFdOrderDetailsNewBinding.vehicleName.setText("Mercedes-Benz Unimog");
                    mFragmentFdOrderDetailsNewBinding.vinNo.setText("WVM58956444545545");
                } else if (from == 2) {
                    mFragmentFdOrderDetailsNewBinding.editedByTechnicianLabel.setVisibility(View.VISIBLE);
                    mFragmentFdOrderDetailsNewBinding.tasksRecyclerView.setVisibility(View.VISIBLE);
                    mFragmentFdOrderDetailsNewBinding.effortsBackground.setVisibility(View.VISIBLE);
                    mFragmentFdOrderDetailsNewBinding.totalTime.setVisibility(View.VISIBLE);
                    mFragmentFdOrderDetailsNewBinding.totalCost.setVisibility(View.VISIBLE);
                    mFragmentFdOrderDetailsNewBinding.datetime.setText("2 hours ago");

                    mFragmentFdOrderDetailsNewBinding.location.setText("484–2500 Ullamcorper. Road, Houston, Texas, 31515");
                    mFragmentFdOrderDetailsNewBinding.vehicleName.setText("Ford F-650 Supertruck");
                    mFragmentFdOrderDetailsNewBinding.vinNo.setText("WVM25454545454525");
                    mFragmentFdOrderDetailsNewBinding.serviceIssue.setText("Front axle brake jam issue");
                    mFragmentFdOrderDetailsNewBinding.issueDescription.setText("Brakes are squealing, caused by a worn down brake pad, which comes into direct contact with the rotor.");
                }
                break;
            case 3:
                mFragmentFdOrderDetailsNewBinding.status.setText("COMPLETED");
                mFragmentFdOrderDetailsNewBinding.status.setBackgroundTintList(getResources().getColorStateList(android.R.color.holo_blue_dark));
                mFragmentFdOrderDetailsNewBinding.editedByTechnicianLabel.setVisibility(View.GONE);
                mFragmentFdOrderDetailsNewBinding.tasksRecyclerView.setVisibility(View.GONE);
                mFragmentFdOrderDetailsNewBinding.effortsBackground.setVisibility(View.GONE);
                mFragmentFdOrderDetailsNewBinding.totalTime.setVisibility(View.GONE);
                mFragmentFdOrderDetailsNewBinding.totalCost.setVisibility(View.GONE);
                break;
        }

        mFragmentFdOrderDetailsNewBinding.timelineRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mFragmentFdOrderDetailsNewBinding.timelineRecyclerView.setHasFixedSize(true);
        mFragmentFdOrderDetailsNewBinding.timelineRecyclerView.setNestedScrollingEnabled(false);

        mFragmentFdOrderDetailsNewBinding.tasksRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mFragmentFdOrderDetailsNewBinding.tasksRecyclerView.setHasFixedSize(true);
        mFragmentFdOrderDetailsNewBinding.tasksRecyclerView.setNestedScrollingEnabled(false);
        mFragmentFdOrderDetailsNewBinding.tasksRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        initView();
//        generateLocalNotification();
        mFragmentFdOrderDetailsNewBinding.nestedScrollView.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
            if (scrollY > oldScrollY) {
                mFragmentFdOrderDetailsNewBinding.chatFloatingActionButton.hide();
            } else {
                mFragmentFdOrderDetailsNewBinding.chatFloatingActionButton.show();
            }
        });
    }

    @Override
    public void loadEventDetails(Event event) {
        mWorkOrderRecyclerViewAdapter.addItems(event.getWorkOrders());
        mFragmentFdOrderDetailsNewBinding.workOrderRecyclerView.setAdapter(mWorkOrderRecyclerViewAdapter);
    }

    @Override
    public void updateEventRequestDetails(ServiceRequestSummary serviceRequestSummary) {

    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(getActivity(), getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
    }

    private int index = 0, from = 0;

    private List<TimeLineModel> mDataList = new ArrayList<>();
    private NotificationManager notificationManager;
    private NotificationCompat.Builder notificationBuilder;

    @SuppressWarnings("deprecation")
    private void generateLocalNotification() {
        notificationManager = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationBuilder = new NotificationCompat.Builder(getActivity())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(this.getResources(),
                        R.mipmap.ic_launcher))
                .setContentTitle("Additional Tasks Approval")
                .setPriority(Notification.PRIORITY_MAX)
                .setStyle(new NotificationCompat.BigTextStyle().bigText("Service provider approved additional tasks for service request-1"))
                .setContentText("Service provider approved additional tasks for service request-1");

        Intent answerIntent = new Intent(getActivity(), DashboardActivity.class);
        answerIntent.setAction("Yes");
        PendingIntent pendingIntentYes = PendingIntent.getActivity(getActivity(), 1, answerIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        notificationBuilder.addAction(R.drawable.common_google_signin_btn_icon_dark, "Approve", pendingIntentYes);

        sendNotification();
    }

    private void sendNotification() {
        Intent notificationIntent = new Intent(getActivity(), DashboardActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(getActivity(), 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        notificationBuilder.setContentIntent(contentIntent);
        Notification notification = notificationBuilder.build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notification.defaults |= Notification.DEFAULT_SOUND;
        int notificationId = 11;

        notificationManager.notify(notificationId, notification);
    }

    private void initView() {
//        mRootView.findViewById(R.id.chatFloatingActionButton).setOnClickListener(view -> ((ServiceProviderDashboardActivity) getActivity()).showServiceChatThreadDetails(0, "Service Request 1"));

        setDataListItems();
        TimelineAdapter mTimeLineAdapter = new TimelineAdapter(mDataList);
        mTimeLineAdapter.setListener(mTimelineItemViewModelListener);
        mFragmentFdOrderDetailsNewBinding.timelineRecyclerView.setAdapter(mTimeLineAdapter);


//        TODO: Add Task Adapter
        mFragmentFdOrderDetailsNewBinding.tasksRecyclerView.setAdapter(new TaskRecyclerViewAdapter(getActivity()));
    }

    private void setDataListItems() {
        mDataList.clear();
        if (index == 1) {
            for (int i = 0; i < 1; i++) {
                mDataList.add(new TimeLineModel(getResources().getStringArray(R.array.fleet_dispatcher_order_timeline)[i], getResources().getStringArray(R.array.fleet_dispatcher_order_time)[i], OrderStatus.INACTIVE));
            }
            mDataList.get(0).setStatus(OrderStatus.ACTIVE);
            if (from == 2) {
                mDataList.get(0).setStatus(OrderStatus.COMPLETED);
                mDataList.add(new TimeLineModel("Estimation received from Service Dispatchers", "2017-10-11 16:55", OrderStatus.ACTIVE));
            }
        } else if (index == 2) {
            if (from == 1) {
                for (int i = 0; i < 4; i++) {
                    mDataList.add(new TimeLineModel(getResources().getStringArray(R.array.fleet_dispatcher_order_timeline)[i], getResources().getStringArray(R.array.fleet_dispatcher_order_time)[i], OrderStatus.INACTIVE));
                }
                mDataList.get(0).setAction("Dispatch");
                mDataList.get(0).setStatus(OrderStatus.COMPLETED);
                mDataList.get(1).setStatus(OrderStatus.COMPLETED);
                mDataList.get(2).setStatus(OrderStatus.COMPLETED);
                mDataList.get(3).setStatus(OrderStatus.ACTIVE);
            } else {
                for (int i = 0; i < 6; i++) {
                    mDataList.add(new TimeLineModel(getResources().getStringArray(R.array.fleet_dispatcher_order_timeline)[i], getResources().getStringArray(R.array.fleet_dispatcher_order_time)[i], OrderStatus.INACTIVE));
                }
                mDataList.get(0).setStatus(OrderStatus.COMPLETED);
                mDataList.get(1).setStatus(OrderStatus.COMPLETED);
                mDataList.get(2).setStatus(OrderStatus.COMPLETED);
                mDataList.get(3).setStatus(OrderStatus.COMPLETED);
                mDataList.get(4).setStatus(OrderStatus.COMPLETED);
                mDataList.get(5).setStatus(OrderStatus.ACTIVE);
            }
        } else {
            for (int i = 0; i < getResources().getStringArray(R.array.fleet_dispatcher_order_timeline).length; i++) {
                mDataList.add(new TimeLineModel(getResources().getStringArray(R.array.fleet_dispatcher_order_timeline)[i], getResources().getStringArray(R.array.fleet_dispatcher_order_time)[i], OrderStatus.INACTIVE));
            }

            mDataList.get(0).setStatus(OrderStatus.COMPLETED);
            mDataList.get(1).setStatus(OrderStatus.COMPLETED);
            mDataList.get(2).setStatus(OrderStatus.COMPLETED);
            mDataList.get(3).setStatus(OrderStatus.COMPLETED);
            mDataList.get(4).setStatus(OrderStatus.COMPLETED);
            mDataList.get(5).setStatus(OrderStatus.COMPLETED);
            mDataList.get(6).setStatus(OrderStatus.COMPLETED);
            mDataList.get(7).setStatus(OrderStatus.COMPLETED);
            mDataList.get(8).setStatus(OrderStatus.ACTIVE);
        }
        /*mDataList.get(9).setStatus(OrderStatus.COMPLETED);
        mDataList.get(10).setStatus(OrderStatus.COMPLETED);
        mDataList.get(11).setStatus(OrderStatus.ACTIVE);*/
    }

    private TimelineItemViewModel.TimelineItemViewModelListener mTimelineItemViewModelListener = action -> {
        if (!TextUtils.isEmpty(action)) {
            if (action.equalsIgnoreCase("Track Location")) {
                ((DashboardActivity) getActivity()).trackLocation(0);
            } else if (action.equalsIgnoreCase("Dispatch")) {
                ((DashboardActivity) getActivity()).dispatchService(0, true);
            }
        }
    };

    public void updateIndex(int from, int index) {
        this.from = from;
        this.index = index;
    }

//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//        if (findViewById(R.id.chatFloatingActionButton).getVisibility() == View.GONE) {
//            findViewById(R.id.chatFloatingActionButton).setVisibility(View.VISIBLE);
//            getToolbar().setTitle("Service Request 1");
//            menuItemStatus.setVisible(true);
//            menuItemDone.setVisible(false);
//        }
//    }
}
