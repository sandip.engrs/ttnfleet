package com.ttnfleetsolutions.ttnfleet.ui.serviceprovider;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ttnfleetsolutions.ttnfleet.R;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */

public class NotificationRecyclerViewAdapter extends RecyclerView.Adapter<NotificationRecyclerViewAdapter.MyViewHolder> {

    private Context mContext;

    private String[] times, texts;

    NotificationRecyclerViewAdapter(Context context) {
        this.mContext = context;
        times = mContext.getResources().getStringArray(R.array.times2);
        texts = mContext.getResources().getStringArray(R.array.sp_notifications);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_notification_tech, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        if (position > 0) {
            holder.rowLayout.setBackgroundColor(ContextCompat.getColor(mContext, android.R.color.transparent));
        } else {
            holder.rowLayout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.tr_gray));
        }

        switch (position) {
            case 5:
                holder.acceptButton.setVisibility(View.GONE);
                break;
            case 4:
                holder.acceptButton.setVisibility(View.GONE);
                holder.acceptButton.setText(R.string.track_location);
                break;
            case 3:
                holder.acceptButton.setVisibility(View.GONE);
                holder.acceptButton.setText(R.string.confirm);
                break;
            case 2:
                holder.acceptButton.setVisibility(View.GONE);
                holder.acceptButton.setText(R.string.approve);
                break;
            case 1:
                holder.acceptButton.setVisibility(View.GONE);
                holder.acceptButton.setText(R.string.complete_order);
                break;
            case 0:
             /*   holder.acceptButton.setText("FEEDBACK");
                holder.acceptButton.setVisibility(View.VISIBLE);*/
                holder.acceptButton.setVisibility(View.GONE);
                holder.acceptButton.setText(R.string.approve);
                break;
        }
        holder.acceptButton.setTag(position);
        holder.acceptButton.setOnClickListener(view -> {
            switch ((int) view.getTag()) {
                case 4:
//                        mContext.startActivity(new Intent(mContext, TrackLocationActivity.class));
                    break;
                case 3:
                   /* AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    builder.setMessage("Please acceptButton technician arrival");
                    builder.setCancelable(false);
                    builder.setPositiveButton("acceptButton", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    builder.setNegativeButton("CANCEL", null);
                    builder.show();*/
                    break;
                case 1:
                    break;
                case 0:
//                    ((ServiceProviderDashboardActivity) mContext).allocateJobToTechnician(0);
//                    mContext.startActivity(new Intent(mContext, AllocateJobToTechnicianActivity.class));
                    /*FeedbackDialogFragment fragment = new FeedbackDialogFragment();
                    fragment.show(((AppCompatActivity) mContext).getSupportFragmentManager()(), "feedback");*/
                   /* FragmentManager fragmentManager = ((AppCompatActivity) mContext).getSupportFragmentManager()();
                    Fragment newFragment = new FeedbackFragment();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
                    transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                    transaction.replace(R.id.container, newFragment).addToBackStack("FEEDBACK").commit();*/
                    break;
            }
        });
        holder.time.setText(times[position]);
        holder.message.setText(Html.fromHtml(texts[position]));
        holder.rejectButton.setVisibility(View.GONE);
        holder.rowLayout.setTag(position);
        holder.rowLayout.setOnClickListener(view -> ((ServiceProviderDashboardActivity) mContext).showOrderDetailsFromNotifications(-1, 3));
    }

    @Override
    public int getItemCount() {
        return texts.length;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView message, time, acceptButton, rejectButton, serviceRequestName;
        public ConstraintLayout rowLayout;

        public MyViewHolder(View view) {
            super(view);
            serviceRequestName = view.findViewById(R.id.serviceRequestName);
            rowLayout = view.findViewById(R.id.rowLayout);
            message = view.findViewById(R.id.notificationMessage);
            time = view.findViewById(R.id.datetime);
            acceptButton = view.findViewById(R.id.acceptButton);
            rejectButton = view.findViewById(R.id.rejectButton);
        }
    }
}
