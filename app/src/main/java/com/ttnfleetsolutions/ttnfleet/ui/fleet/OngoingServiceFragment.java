package com.ttnfleetsolutions.ttnfleet.ui.fleet;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.ui.fleet.services.NewServiceListAdapter;
import com.ttnfleetsolutions.ttnfleet.utils.VerticalSpaceItemDecoration;

import static com.ttnfleetsolutions.ttnfleet.utils.AppConstants.VERTICAL_LIST_ITEM_SPACE;

/**
 * Created by Sandip Chaulagain on 10/7/2017.
 *
 * @version 1.0
 */

public class OngoingServiceFragment extends Fragment {

    public OngoingServiceFragment() {
        // Required empty public constructor
    }

    private RecyclerView recyclerView;
    private NewServiceListAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_fleet_ongoing_service, container, false);

        recyclerView = rootView.findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_LIST_ITEM_SPACE));

        mAdapter = new NewServiceListAdapter(getActivity(), "ONGOING");
        recyclerView.setAdapter(mAdapter);
        return rootView;
    }

    void updateAdapter() {
        if (mAdapter != null) {
//            mAdapter.selectedItemPosition = 0;
            mAdapter.notifyDataSetChanged();
        }
    }
}