/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.ttnfleetsolutions.ttnfleet.ui.splash;

import android.os.Handler;

import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseViewModel;
import com.ttnfleetsolutions.ttnfleet.utils.rx.SchedulerProvider;

import static com.ttnfleetsolutions.ttnfleet.utils.AppConstants.SPLASH_DELAY;

/**
 * Created by Sandip Chaulagain on 10/4/2017.
 *
 * @version 1.0
 */
public class SplashViewModel extends BaseViewModel<SplashNavigator> {

    public SplashViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    public void startSeeding() {
        /*getCompositeDisposable().add(getDataManager()
                .seedDatabaseQuestions()
                .flatMap(new Function<Boolean, ObservableSource<Boolean>>() {
                    @Override
                    public ObservableSource<Boolean> apply(Boolean aBoolean) throws Exception {
                        return getDataManager().seedDatabaseOptions();
                    }
                })
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<Boolean>() {
                    @Override
                    public void accept(Boolean aBoolean) throws Exception {
                        decideNextActivity();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        decideNextActivity();
                    }
                }));*/
        new Handler().postDelayed(this::decideNextActivity, SPLASH_DELAY);
    }

    private void decideNextActivity() {
        if (getDataManager().getCurrentUserLoggedInMode()
                == DataManager.LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT.getType()) {
            getNavigator().openLoginActivity();
        } else {
            switch (getDataManager().getCurrentUserRole()) {
                case 1:
                    getNavigator().openTruckerActivity();
                    break;
                case 2:
                    getNavigator().openFleetDispatcherActivity();
                    break;
                case 3:
                    getNavigator().openServiceProviderActivity();
                    break;
                case 4:
                    getNavigator().openServiceRepActivity();
                    break;
            }
        }
    }

}
