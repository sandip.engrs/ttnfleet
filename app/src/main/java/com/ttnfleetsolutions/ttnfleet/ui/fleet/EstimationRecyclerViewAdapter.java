package com.ttnfleetsolutions.ttnfleet.ui.fleet;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.EstimationDetailsDialogFragment;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */

public class EstimationRecyclerViewAdapter extends RecyclerView.Adapter<EstimationRecyclerViewAdapter.MyViewHolder> {

    private Context mContext;

    public EstimationRecyclerViewAdapter(Context context) {
        this.mContext = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_estimation_summary_details, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        switch (position) {
            case 0:
                holder.name.setText("Gulf Coast Auto &amp; Truck Repair");
                holder.location.setText("Location: 281 TX-3, League City, TX 77573, USA");
                holder.estimation.setText("Total Time: 04.30 Hours | Total Cost: $1600");
                break;
            case 1:
                holder.name.setText("Wheeler Truck Sales &amp; Service. Inc");
                holder.location.setText("Location: 610 N Prospect Ave, Kansas City, MO 64120, USA");
                holder.estimation.setText("Total Time: 06.30 Hours | Total Cost: $1200");
                break;
        }
    }

    @Override
    public int getItemCount() {
        return 2;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, location, estimation;
        public ConstraintLayout rowLayout;

        public MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.serviceDispatcherName);
            location = view.findViewById(R.id.serviceDispatcherLocation);
            estimation = view.findViewById(R.id.serviceDispatcherEstimatation);
            rowLayout = view.findViewById(R.id.rowLayout);

            rowLayout.setOnClickListener(view1 -> {
                EstimationDetailsDialogFragment fragment = new EstimationDetailsDialogFragment();
                fragment.show(((AppCompatActivity) mContext).getSupportFragmentManager(), "Task Approval");
            });
        }
    }
}
