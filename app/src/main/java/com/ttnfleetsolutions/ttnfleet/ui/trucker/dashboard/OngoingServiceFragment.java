package com.ttnfleetsolutions.ttnfleet.ui.trucker.dashboard;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.BR;
import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.listing.Event;
import com.ttnfleetsolutions.ttnfleet.databinding.FragmentOngoingServiceBinding;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseActivity;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.MainActivityNew;
import com.ttnfleetsolutions.ttnfleet.utils.CommonUtils;
import com.ttnfleetsolutions.ttnfleet.utils.VerticalSpaceItemDecoration;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import static com.ttnfleetsolutions.ttnfleet.utils.AppConstants.VERTICAL_LIST_ITEM_SPACE;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */

public class OngoingServiceFragment extends BaseFragment<FragmentOngoingServiceBinding, OngoingServicesViewModel> implements OngoingServicesNavigator {

    @Inject
    OngoingServicesViewModel mOngoingServicesViewModel;

    FragmentOngoingServiceBinding mFragmentOngoingServiceBinding;

    @Inject
    OngoingServiceAdapter mAdapter;

    @Override
    protected OngoingServicesViewModel getViewModel() {
        return mOngoingServicesViewModel;
    }

    @Override
    protected int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_ongoing_service;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mOngoingServicesViewModel.setNavigator(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFragmentOngoingServiceBinding = getViewDataBinding();
        setUp();
    }

    private void setUp() {
        mFragmentOngoingServiceBinding.swiperefresh.setOnRefreshListener(
                () -> getEvents()
        );
        mFragmentOngoingServiceBinding.ongoingServicesRecyclerViewTrucker.setLayoutManager(new LinearLayoutManager(getActivity()));
        mFragmentOngoingServiceBinding.ongoingServicesRecyclerViewTrucker.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_LIST_ITEM_SPACE));
        mAdapter.setEmptyItemViewModelListener(this::getEvents);
        mAdapter.setIsLandscape(((BaseActivity) getActivity()).isLandscape());
        mAdapter.setListener(listener);

        if (getViewModel().getEventList() != null && getViewModel().getEventList().size() > 0 && !((BaseActivity) getActivity()).isLandscape()) {
            getActiveEvents(getViewModel().getEventList());
        } else {
            getEvents();
        }
    }

    private void getEvents() {
        if (!isNetworkConnected()) {
            mAdapter.setEmptyItem(CommonUtils.getEmptyItem(getActivity(), DataManager.ErrorState.NETWORK_ERROR));
            mFragmentOngoingServiceBinding.ongoingServicesRecyclerViewTrucker.setAdapter(mAdapter);
        } else {
            mFragmentOngoingServiceBinding.swiperefresh.setRefreshing(true);
            getViewModel().getActiveEvents();
        }
    }

    @Override
    public void getActiveEvents(List<Event> eventList) {
        mFragmentOngoingServiceBinding.swiperefresh.setRefreshing(false);
        if (eventList != null && eventList.size() > 0) {
            mAdapter.addItems(eventList);
            mFragmentOngoingServiceBinding.ongoingServicesRecyclerViewTrucker.setAdapter(mAdapter);
            if (((BaseActivity) getActivity()).isLandscape()) {
                listener.onItemClick(eventList.get(0).getEventid());
            }
        } else {
            mAdapter.setEmptyItem(CommonUtils.getEmptyItem(getActivity(), DataManager.ErrorState.NO_DATA));
            mFragmentOngoingServiceBinding.ongoingServicesRecyclerViewTrucker.setAdapter(mAdapter);
            if(((BaseActivity) getActivity()).isLandscape()) {
                listener.onItemClick(0);
            }
        }
    }

    @Override
    public void handleError(Throwable throwable) {
        mFragmentOngoingServiceBinding.swiperefresh.setRefreshing(false);
        mAdapter.setEmptyItem(CommonUtils.getEmptyItem(getActivity(), DataManager.ErrorState.API_FAIL));
        mFragmentOngoingServiceBinding.ongoingServicesRecyclerViewTrucker.setAdapter(mAdapter);
    }

    private ServiceRequestIemViewModel.ServiceRequestItemViewModelListener listener = new ServiceRequestIemViewModel.ServiceRequestItemViewModelListener() {
        @Override
        public void onItemClick(int id) {
            ((MainActivityNew) Objects.requireNonNull(getActivity())).goToServiceDetails(id);
            for (Event event : getViewModel().getEventList()) {
                if (event.getEventid() == id)
                    event.setSelected(true);
                else event.setSelected(false);
            }
            mAdapter.notifyDataSetChanged();
        }

        @Override
        public void onDispatchClick(int id) {

        }

        @Override
        public void onAddNoteClick(int id, String note) {

        }
    };
}
