package com.ttnfleetsolutions.ttnfleet.ui.serviceprovider.jobroster;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.ttnfleetsolutions.ttnfleet.data.model.api.event.listing.Event;
import com.ttnfleetsolutions.ttnfleet.databinding.RowEmptyViewBinding;
import com.ttnfleetsolutions.ttnfleet.databinding.RowServiceRequestSpBinding;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseViewHolder;
import com.ttnfleetsolutions.ttnfleet.ui.empty.EmptyItem;
import com.ttnfleetsolutions.ttnfleet.ui.empty.EmptyItemViewModel;
import com.ttnfleetsolutions.ttnfleet.ui.empty.EmptyViewHolder;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */

public class JobRosterAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private List<Event> mList;

    private int mFrom;

    private boolean mIsLandscape = false;

    private JobRosterItemViewModel.ItemViewModelListener mListener;
    private EmptyItemViewModel.EmptyItemViewModelListener mEmptyItemViewModelListener;
    private EmptyItem mEmptyItem;

    public void setEmptyItem(EmptyItem mEmptyItem) {
        this.mEmptyItem = mEmptyItem;
    }

    public void setEmptyItemViewModelListener(EmptyItemViewModel.EmptyItemViewModelListener mEmptyItemViewModelListener) {
        this.mEmptyItemViewModelListener = mEmptyItemViewModelListener;
    }

    public void setListener(JobRosterItemViewModel.ItemViewModelListener listener) {
        this.mListener = listener;
    }

    public JobRosterAdapter(List<Event> list) {
        this.mList = list;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                RowServiceRequestSpBinding rowBinding = RowServiceRequestSpBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
                return new MyViewHolder(rowBinding);
            case VIEW_TYPE_EMPTY:
            default:
                RowEmptyViewBinding emptyViewBinding = RowEmptyViewBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
                return new EmptyViewHolder(emptyViewBinding, mEmptyItem, mEmptyItemViewModelListener);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mList != null && mList.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public int getItemCount() {
        if (mList != null && mList.size() > 0) {
            return mList.size();
        } else {
            return 1;
        }
    }

    public void addItems(List<Event> list) {
        mList = list;
        notifyDataSetChanged();
    }

    public boolean isIsLandscape() {
        return mIsLandscape;
    }

    public void setIsLandscape(boolean mIsLandscape) {
        this.mIsLandscape = mIsLandscape;
    }

    public int getFrom() {
        return mFrom;
    }

    public void setFrom(int mFrom) {
        this.mFrom = mFrom;
    }

    public class MyViewHolder extends BaseViewHolder implements JobRosterItemViewModel.ItemViewModelListener {

        private RowServiceRequestSpBinding mBinding;

        private JobRosterItemViewModel mJobRosterItemViewModel;

        public MyViewHolder(RowServiceRequestSpBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {
            final Event event = mList.get(position);
            mJobRosterItemViewModel = new JobRosterItemViewModel(event, this, getFrom(), isIsLandscape());
            mBinding.setViewModel(mJobRosterItemViewModel);

            // Immediate Binding
            // When a variable or observable changes, the binding will be scheduled to change before
            // the next frame. There are times, however, when binding must be executed immediately.
            // To force execution, use the executePendingBindings() method.
            mBinding.executePendingBindings();
        }

        @Override
        public void onItemClick(int id) {
            mListener.onItemClick(id);
        }

        @Override
        public void onAcceptButtonClick(int eventId, int workOrderId, int workOrderStatus, int jobId, int jobStatusId) {
            mListener.onAcceptButtonClick(eventId, workOrderId, workOrderStatus, jobId, jobStatusId);
        }

        @Override
        public void onRejectButtonClick(int eventId, int workOrderId, int workOrderStatus, int jobId, int jobStatusId) {
            mListener.onRejectButtonClick(eventId, workOrderId, workOrderStatus, jobId, jobStatusId);
        }

        @Override
        public void onEstimateButtonClick(int eventId, int workOrderId, int workOrderStatus, int jobId, int jobStatusId) {
            mListener.onEstimateButtonClick(eventId, workOrderId, workOrderStatus, jobId, jobStatusId);
        }

        @Override
        public void onEnrouteButtonClick(int eventId, int workOrderId, int workOrderStatus, int jobId, int jobStatusId) {
            mListener.onEnrouteButtonClick(eventId, workOrderId, workOrderStatus, jobId, jobStatusId);
        }

        @Override
        public void onConfirmBreakReportButtonClick(int eventId, int workOrderId, int workOrderStatus, int jobId, int jobStatusId, String eventType, String breakdown) {
            mListener.onConfirmBreakReportButtonClick(eventId, workOrderId, workOrderStatus, jobId, jobStatusId, eventType, breakdown);
        }

        @Override
        public void onAdditionalJobButtonClick(int eventId, int workOrderId, int workOrderStatus, int jobId, int jobStatusId) {
            mListener.onAdditionalJobButtonClick(eventId, workOrderId, workOrderStatus, jobId, jobStatusId);
        }

        @Override
        public void onCompleteJobButtonClick(int eventId, int workOrderId, int workOrderStatus, int jobId, int jobStatusId) {
            mListener.onCompleteJobButtonClick(eventId, workOrderId, workOrderStatus, jobId, jobStatusId);
        }
    }
}
