package com.ttnfleetsolutions.ttnfleet.ui.trucker;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.ui.chat.ChatFragment;
import com.ttnfleetsolutions.ttnfleet.ui.chat.MainChatFragment;
import com.ttnfleetsolutions.ttnfleet.ui.chat.ServiceDetailsChatFragment;
import com.ttnfleetsolutions.ttnfleet.ui.login.LoginActivity;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.dashboard.OngoingServiceFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.form.CreateServiceRequestFormFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.history.ServicesHistoryFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.MainServicesFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.vmrs.system.SelectServiceFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.notification.NotificationsFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.servicerequest.ServiceDetailsFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.vehicle.VehicleFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.vehicle.details.VehicleDetailsFragment;
import com.ttnfleetsolutions.ttnfleet.utils.AppConstants;
import com.ttnfleetsolutions.ttnfleet.utils.CommonUtils;

import static android.content.res.Configuration.ORIENTATION_LANDSCAPE;
import static com.ttnfleetsolutions.ttnfleet.utils.AppConstants.PERMISSION_REQUEST_CALL_PHONE;
import static com.ttnfleetsolutions.ttnfleet.utils.AppConstants.PREF_NAME;

/**
 * Created by Sandip Chaulagain on 10/12/2017.
 *
 * @version 1.0
 */

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    private Toolbar mToolbar;
    private static final String TAG = MainActivity.class.getSimpleName();
    private DrawerLayout mDrawerLayout;
    private NavigationView mNavigationView;
    private MenuItem menuItemDone, menuItemNotification;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.primary_dark));*/

        if (getResources().getBoolean(R.bool.portrait_mode)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
        setContentView(R.layout.activity_main_trucker);

        mToolbar = findViewById(R.id.baseToolbar);
//        mToolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.primary));
        mToolbar.setTitle("Home");
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayUseLogoEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        initComponents();
    }

    public static Intent getStartIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }

    private void replaceFragment(Fragment fragment) {
        /*if (fragment instanceof IssueListFragment) {
            if (menuItemDone != null)
                menuItemDone.setVisible(true);
        } else {
            if (menuItemDone != null)
                menuItemDone.setVisible(false);
            invalidateOptionsMenu();
        }*/
        invalidateOptionsMenu();
        String backStateName = fragment.getClass().getName();
        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.container, fragment, backStateName);
            ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
        if (isLandscape()) {
            if (fragment instanceof OngoingServiceFragment) {
                ServiceDetailsFragment serviceDetailsFragment = new ServiceDetailsFragment();
                /*Bundle bundle = new Bundle();
                bundle.putInt("index", 5);
                serviceDetailsFragment.setArguments(bundle);*/
                serviceDetailsFragment.updateIndex(1, 1);
                replaceFragmentDetails(serviceDetailsFragment, false);
                findViewById(R.id.details_container).setVisibility(View.VISIBLE);
            } else if (fragment instanceof VehicleFragment) {
                VehicleDetailsFragment fragmentAddVehicle = new VehicleDetailsFragment();
                fragmentAddVehicle.updateIndex(0);
                replaceFragmentDetails(fragmentAddVehicle, false);
                findViewById(R.id.details_container).setVisibility(View.VISIBLE);
            } else if (fragment instanceof ServicesHistoryFragment) {
                ServiceDetailsFragment serviceDetailsFragment = new ServiceDetailsFragment();
                /*Bundle bundle = new Bundle();
                bundle.putInt("index", 5);
                serviceDetailsFragment.setArguments(bundle);*/
                serviceDetailsFragment.updateIndex(-1, 3);
                replaceFragmentDetails(serviceDetailsFragment, false);
                findViewById(R.id.details_container).setVisibility(View.VISIBLE);
            } else if (fragment instanceof MainChatFragment) {
                ServiceDetailsChatFragment serviceDetailsChatFragment = new ServiceDetailsChatFragment();
                /*Bundle bundle = new Bundle();
                bundle.putInt("index", 5);
                serviceDetailsFragment.setArguments(bundle);*/
//                serviceDetailsFragment.updateIndex(0);
                replaceFragmentDetails(serviceDetailsChatFragment, false);
                findViewById(R.id.details_container).setVisibility(View.VISIBLE);
            } else {
                findViewById(R.id.details_container).setVisibility(View.GONE);
            }
        }

    }

    private void replaceFragmentDetails(Fragment fragment, boolean fragmentPopped) {
        String backStateName = fragment.getClass().getName();
        FragmentManager manager = getSupportFragmentManager();
//        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);
//        boolean fragmentPopped = false;

        if (!fragmentPopped) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.details_container, fragment, backStateName);
            ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
        updateSubTitle(fragment);
    }

    private void updateTitleAndDrawer(Fragment fragment) {
        /*if (fragment instanceof IssueListFragment) {
            if (menuItemDone != null)
                menuItemDone.setVisible(true);
        } else {
            if (menuItemDone != null)
                menuItemDone.setVisible(false);
            invalidateOptionsMenu();
        }*/
        invalidateOptionsMenu();
        CommonUtils.hideKeyboard(this);
        if (findViewById(R.id.details_container) != null)
            findViewById(R.id.details_container).setVisibility(View.GONE);

//        findViewById(R.id.floatingActionsMenu).setVisibility(View.GONE);
        String fragClassName = fragment.getClass().getName();
        if (fragClassName.equals(MainServicesFragment.class.getName())) {
            /*if (fragment.getTargetFragment() != null)
                fragment.getTargetFragment().setMenuVisibility(false);*/
            mToolbar.setTitle("Home");
            mNavigationView.setCheckedItem(R.id.navItemFillForm);
        } else if (fragClassName.equals(SelectServiceFragment.class.getName())) {
            mToolbar.setTitle("Select Service Type");
            mNavigationView.setCheckedItem(R.id.navItemFillForm);
        } else if (fragClassName.equals(OngoingServiceFragment.class.getName())) {
            mToolbar.setTitle("Ongoing Services");
            mNavigationView.setCheckedItem(R.id.navItemOngoing);
            if (findViewById(R.id.details_container) != null)
                findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else if (fragClassName.equals(NotificationsFragment.class.getName())) {
            mToolbar.setTitle("Notifications");
            mNavigationView.setCheckedItem(R.id.navItemNotifications);
        } else if (fragClassName.equals(MainChatFragment.class.getName())) {
            mToolbar.setTitle("Chat");
            mNavigationView.setCheckedItem(R.id.navItemChat);
            if (findViewById(R.id.details_container) != null)
                findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else if (fragClassName.equals(ServicesHistoryFragment.class.getName())) {
            mToolbar.setTitle("Service History");
            mNavigationView.setCheckedItem(R.id.navItemHistory);
            if (findViewById(R.id.details_container) != null)
                findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else if (fragClassName.equals(CreateServiceRequestFormFragment.class.getName())) {
            mToolbar.setTitle("Create Service Request");
            mNavigationView.setCheckedItem(R.id.navItemFillForm);
        } else if (fragClassName.equals(VehicleFragment.class.getName())) {
            mToolbar.setTitle("My Vehicles");
            mNavigationView.setCheckedItem(R.id.navItemVehicle);
//            findViewById(R.id.floatingActionsMenu).setVisibility(View.VISIBLE);
            if (findViewById(R.id.details_container) != null)
                findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else if (fragClassName.equals(VehicleDetailsFragment.class.getName())) {
//            mToolbar.setTitle("Add Vehicle");
            mNavigationView.setCheckedItem(R.id.navItemVehicle);
        } else if (fragClassName.equals(ServiceDetailsFragment.class.getName())) {
            mToolbar.setTitle("Service Details");
            mNavigationView.setCheckedItem(R.id.navItemOngoing);
        } else if (fragClassName.equals(ServiceDetailsChatFragment.class.getName())) {
//            mToolbar.setTitle("Service Details Chat");
            mNavigationView.setCheckedItem(R.id.navItemChat);
        } else if (fragClassName.equals(ChatFragment.class.getName())) {
//            mToolbar.setTitle("Service Details 1");
            mNavigationView.setCheckedItem(R.id.navItemChat);
        } else if (fragClassName.equals(TrackLocationFragment.class.getName())) {
            mToolbar.setTitle("Track Location");
            mNavigationView.setCheckedItem(R.id.navItemOngoing);
        }
    }

    public void setTitle(String title) {
        if (!TextUtils.isEmpty(title)) {
            mToolbar.setTitle(title);
        }
    }

    private void updateSubTitle(Fragment fragment) {
        String fragClassName = fragment.getClass().getName();
        if (fragClassName.equals(VehicleDetailsFragment.class.getName())) {
//            setSubTitle("Add Vehicle");
        } else if (fragClassName.equals(ServiceDetailsFragment.class.getName())) {
            setSubTitle("Service Details");
        } else if (fragClassName.equals(ServiceDetailsChatFragment.class.getName())) {
//            setSubTitle("Chat");
        } else if (fragClassName.equals(ChatFragment.class.getName())) {
//            setSubTitle("Chat");
        } else if (fragClassName.equals(TrackLocationFragment.class.getName())) {
            setSubTitle("Track Location");
        }
    }

    public void setSubTitle(String subTitle) {
        if (!TextUtils.isEmpty(subTitle)) {
            ((TextView) findViewById(R.id.subTitle)).setText(subTitle);
            findViewById(R.id.subTitle).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.subTitle).setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);

        if (fragment != null) {
            if (isLandscape()) {
                Fragment subFragment = getSupportFragmentManager().findFragmentById(R.id.details_container);
                if (fragment instanceof SelectServiceFragment || fragment instanceof CreateServiceRequestFormFragment
                        || (fragment instanceof VehicleFragment && subFragment != null && subFragment instanceof VehicleDetailsFragment)) {
                    if (menuItemDone != null)
                        menuItemDone.setVisible(true);
                    if (menuItemNotification != null)
                        menuItemNotification.setVisible(false);
                } else {
                    if (menuItemDone != null)
                        menuItemDone.setVisible(false);
                    if (menuItemNotification != null)
                        menuItemNotification.setVisible(true);
                }
            } else {
                if (fragment instanceof SelectServiceFragment || fragment instanceof CreateServiceRequestFormFragment
                        || fragment instanceof VehicleDetailsFragment) {
                    if (menuItemDone != null)
                        menuItemDone.setVisible(true);
                    if (menuItemNotification != null)
                        menuItemNotification.setVisible(false);
                } else {
                    if (menuItemDone != null)
                        menuItemDone.setVisible(false);
                    if (menuItemNotification != null)
                        menuItemNotification.setVisible(true);
                }
            }
            /*if (fragment instanceof OngoingServiceFragment) {
                findViewById(R.id.floatingActionsMenu).setVisibility(View.GONE);
            }*/
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.container);
        if (isLandscape()) {
            Fragment f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
            if (f != null && f1 != null) {
                if (f instanceof OngoingServiceFragment && f1 instanceof ServiceDetailsFragment) {
                    getSupportFragmentManager().popBackStack();
                } else if (f instanceof VehicleFragment && f1 instanceof VehicleDetailsFragment) {
                    getSupportFragmentManager().popBackStack();
//                    findViewById(R.id.floatingActionsMenu).setVisibility(View.GONE);
                } else if (f instanceof ServicesHistoryFragment && f1 instanceof ServiceDetailsFragment) {
                    getSupportFragmentManager().popBackStack();
                } else if (f instanceof MainChatFragment && f1 instanceof ServiceDetailsChatFragment) {
                    getSupportFragmentManager().popBackStack();
                }
                updateSubTitle(f1);
            }
        }
        if (f != null && f instanceof MainServicesFragment) {
            finish();
        } else {
            super.onBackPressed();
            f = getSupportFragmentManager().findFragmentById(R.id.container);
            if (f != null) {
                updateTitleAndDrawer(f);
            }
        }
    }

    private void initComponents() {
        mDrawerLayout = findViewById(R.id.drawerLayout);
        mNavigationView = findViewById(R.id.navigationView);

        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawerLayout,
                mToolbar,
                R.string.open_drawer,
                R.string.close_drawer) {
        };
        mDrawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
        mNavigationView.setNavigationItemSelectedListener(this);

        View.OnClickListener onClickListener = mDrawerToggle.getToolbarNavigationClickListener();
        getSupportFragmentManager().addOnBackStackChangedListener(() -> {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
            if (fragment != null)
                if (fragment instanceof SelectServiceFragment || fragment instanceof CreateServiceRequestFormFragment
                        || fragment instanceof VehicleDetailsFragment || fragment instanceof ServiceDetailsFragment
                        || fragment instanceof ChatFragment || fragment instanceof ServiceDetailsChatFragment
                        || fragment instanceof TrackLocationFragment) {
                    mDrawerToggle.setDrawerIndicatorEnabled(false);
                    mDrawerToggle.setToolbarNavigationClickListener(v -> onBackPressed());
                } else {
                    mDrawerToggle.setDrawerIndicatorEnabled(true);
                    mDrawerToggle.setToolbarNavigationClickListener(onClickListener);
                }

            Fragment f = getSupportFragmentManager().findFragmentById(R.id.container);
            if (f != null) {
                updateTitleAndDrawer(f);
            }
            if (isLandscape()) {
                Fragment f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
                if (f1 != null) {
                    updateSubTitle(f1);
                }
            }
        });

        View header = mNavigationView.getHeaderView(0);
        ImageView imageViewLogo = header.findViewById(R.id.logo);
        int count = getSharedPreferences(AppConstants.PREF_NAME, Context.MODE_PRIVATE).getInt("PREF_KEY_DRIVER_LOGIN_COUNT", 0);

        switch (count) {
            case 0:
                imageViewLogo.setImageResource(R.drawable.pam_logo);
                break;
            case 1:
                imageViewLogo.setImageResource(R.drawable.michelin_on_call2);
                break;
            case 2:
                imageViewLogo.setImageResource(R.drawable.schneiderimg_sni_logo);
                break;
            case 3:
                imageViewLogo.setImageResource(R.drawable.shipex_logo);
                break;
            case 4:
                imageViewLogo.setImageResource(R.drawable.sunbelt_logo);
                break;
            case 5:
                imageViewLogo.setImageResource(R.drawable.cr_england_logo_with_red_background);
                imageViewLogo.setBackgroundColor(Color.DKGRAY);
                break;
        }

        replaceFragment(new MainServicesFragment());

        /*findViewById(R.id.actionEnterVIN).setOnClickListener(this);
        findViewById(R.id.actionScanBarcode).setOnClickListener(this);
        findViewById(R.id.actionAddVehicle).setOnClickListener(this);*/
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        mDrawerLayout.closeDrawer(GravityCompat.START);

        /*int size = mNavigationView.getMenu().size();
        for (int i = 0; i < size; i++) {
            mNavigationView.getMenu().getItem(i).setChecked(false);
        }*/
        switch (item.getItemId()) {
            case R.id.navItemPlaceCall:
                showCallDialog();
                return true;
            case R.id.navItemFillForm:
                replaceFragment(new MainServicesFragment());
                return true;
            case R.id.navItemChat:
                replaceFragment(new MainChatFragment());
                return true;
           /* case R.id.navItemFeedback:
                replaceFragment(new FeedbackFragment());
                return true;*/
            case R.id.navItemHistory:
                replaceFragment(new ServicesHistoryFragment());
                return true;
            case R.id.navItemNotifications:
                replaceFragment(new NotificationsFragment());
                return true;
            case R.id.navItemOngoing:
                replaceFragment(new OngoingServiceFragment());
                return true;
            case R.id.navItemVehicle:
                replaceFragment(new VehicleFragment());
//                startActivity(new Intent(MainActivity.this, AddVehicleActivity.class));
                return true;
            case R.id.navItemLogout:
                getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE).edit().putInt("PREF_KEY_USER_LOGGED_IN_MODE", 0).apply();
                Intent intent = LoginActivity.getStartIntent(MainActivity.this);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                return true;
            default:
                return false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.main, menu);
        menuItemDone = menu.findItem(R.id.action_done);
        menuItemNotification = menu.findItem(R.id.navItemNotifications);
        View count = menu.findItem(R.id.navItemNotifications).getActionView();
        count.setOnClickListener(view -> replaceFragment(new NotificationsFragment()));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Drawable drawable = item.getIcon();
        if (drawable instanceof Animatable) {
            ((Animatable) drawable).start();
        }
        switch (item.getItemId()) {
            case R.id.navItemNotifications:
                replaceFragment(new NotificationsFragment());
                return true;
            case R.id.action_done:
                // handle confirmation button click here
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
                if (fragment != null)
                    if (fragment instanceof SelectServiceFragment)
                        showForm(0, "");
                    else if (fragment instanceof CreateServiceRequestFormFragment)
                        showOngoingServices();
                    else if (fragment instanceof VehicleDetailsFragment)
                        replaceFragment(new VehicleFragment());
                /*if(menuItem != null) {
                    menuItem.setVisible(false);
                }*/
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void showChatDetails(int from, String title) {
        if (isLandscape()) {
            Fragment f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
            if (f1 != null && f1 instanceof ChatFragment) {
                final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ((ChatFragment) f1).setReceiver(title);
                ft.detach(f1);
                ft.attach(f1);
                ft.commit();
            } else {
                ChatFragment chatFragment = new ChatFragment();
                chatFragment.setReceiver(title);
                replaceFragmentDetails(chatFragment, false);
            }
            setSubTitle(title);
            //update details only
            findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else {
            mToolbar.setTitle(title);
            ChatFragment chatFragment = new ChatFragment();
            chatFragment.setReceiver(title);
            replaceFragment(chatFragment);
        }
        mNavigationView.setCheckedItem(R.id.navItemChat);
    }

    public void showServiceChatThreadDetails(int from, String title) {
        if (isLandscape()) {
            Fragment f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
            if (f1 != null && f1 instanceof ServiceDetailsChatFragment) {
                final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//                ((ServiceDetailsChatFragment) f1).updateIndex(index);
                ft.detach(f1);
                ft.attach(f1);
                ft.commit();
            } else {
                ServiceDetailsChatFragment serviceDetailsChatFragment = new ServiceDetailsChatFragment();
                String backStateName = serviceDetailsChatFragment.getClass().getName();
                FragmentManager manager = getSupportFragmentManager();
                boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);
                replaceFragmentDetails(serviceDetailsChatFragment, fragmentPopped);
            }
            setSubTitle(title);
            //update details only
            findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else {
            mToolbar.setTitle(title);
            ServiceDetailsChatFragment serviceDetailsChatFragment = new ServiceDetailsChatFragment();
            replaceFragment(serviceDetailsChatFragment);
        }
        mNavigationView.setCheckedItem(R.id.navItemChat);
    }

    public void trackLocation(int from) {
        if (isLandscape()) {
            Fragment f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
            if (f1 != null && f1 instanceof TrackLocationFragment) {
                final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//                ((ServiceDetailsChatFragment) f1).updateIndex(index);
                ft.detach(f1);
                ft.attach(f1);
                ft.commit();
            } else {
                TrackLocationFragment trackLocationFragment = new TrackLocationFragment();
                replaceFragmentDetails(trackLocationFragment, false);
            }

            //update details only
            findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else {
            TrackLocationFragment trackLocationFragment = new TrackLocationFragment();
            replaceFragment(trackLocationFragment);
        }
        mNavigationView.setCheckedItem(R.id.navItemChat);
    }

    public void goToHome() {
        replaceFragment(new MainServicesFragment());
        mNavigationView.setCheckedItem(R.id.navItemFillForm);
    }

    void showForm(int type, String serviceType) {
//        FragmentManager fragmentManager = getSupportFragmentManager();
        CreateServiceRequestFormFragment newFragment = new CreateServiceRequestFormFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("type", type);
        bundle.putString("service", serviceType);
        newFragment.setArguments(bundle);/*
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(android.R.id.content, newFragment).addToBackStack(newFragment.getClass().getName()).commit();
        invalidateOptionsMenu();*/
        replaceFragment(newFragment);
    }

    void showIssueList(int from, String service) {
        Fragment newFragment = new SelectServiceFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("from", from);
        bundle.putString("service", service);
        newFragment.setArguments(bundle);
        replaceFragment(newFragment);
    }

    public void showOngoingServices() {
        FragmentManager fm = getSupportFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
        getSupportFragmentManager().beginTransaction().remove(getSupportFragmentManager().findFragmentById(R.id.container)).commit();
        replaceFragment(new OngoingServiceFragment());
        mNavigationView.setCheckedItem(R.id.navItemOngoing);
    }

    public void goToVehicleDetails(int from, int index) {
        if (isLandscape()) {
            /*ServiceDetailsFragment serviceDetailsFragment = new ServiceDetailsFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("index", index);
            serviceDetailsFragment.setArguments(bundle);
            replaceFragmentDetails(serviceDetailsFragment);*/

            Fragment f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
            if (f1 != null && f1 instanceof VehicleDetailsFragment) {
                final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ((VehicleDetailsFragment) f1).updateIndex(index);
                ft.detach(f1);
                ft.attach(f1);

                ft.commit();
            } else {
               /*ServiceDetailsFragment serviceDetailsFragment = new ServiceDetailsFragment();
               Bundle bundle = new Bundle();
               bundle.putInt("index", index);
               serviceDetailsFragment.setArguments(bundle);
               replaceFragment(serviceDetailsFragment);*/
            }

            //update details only
            findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else {
            VehicleDetailsFragment fragmentAddVehicle = new VehicleDetailsFragment();
            /*Bundle bundle = new Bundle();
            bundle.putInt("index", index);
            serviceDetailsFragment.setArguments(bundle);*/
            fragmentAddVehicle.updateIndex(index);
            replaceFragment(fragmentAddVehicle);
        }
        mNavigationView.setCheckedItem(R.id.navItemVehicle);
    }

    public void goToServiceDetails(int from, int index) {
        if (isLandscape()) {
            Fragment f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
            if (f1 != null && f1 instanceof ServiceDetailsFragment) {
                final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ((ServiceDetailsFragment) f1).updateIndex(from, index);
                ft.detach(f1);
                ft.attach(f1);

                ft.commit();
            } else {
                ServiceDetailsFragment serviceDetailsFragment = new ServiceDetailsFragment();
                serviceDetailsFragment.updateIndex(from, index);
                String backStateName = serviceDetailsFragment.getClass().getName();
                FragmentManager manager = getSupportFragmentManager();
                boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);
                replaceFragmentDetails(serviceDetailsFragment, fragmentPopped);
                if (fragmentPopped) {
                    f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
                    if (f1 != null && f1 instanceof ServiceDetailsFragment) {
                        final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                        ((ServiceDetailsFragment) f1).updateIndex(from, index);
                        ft.detach(f1);
                        ft.attach(f1);

                        ft.commit();
                    }
                }
            }

            //update details only
            findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else {
            ServiceDetailsFragment serviceDetailsFragment = new ServiceDetailsFragment();
            serviceDetailsFragment.updateIndex(from, index);
            replaceFragment(serviceDetailsFragment);
        }
        mNavigationView.setCheckedItem(R.id.navItemOngoing);
    }

    public void goToServiceDetailsFromNotifiction(int from, int index) {
        if (isLandscape()) {
            replaceFragment(new OngoingServiceFragment());
        } else {
            ServiceDetailsFragment serviceDetailsFragment = new ServiceDetailsFragment();
            serviceDetailsFragment.updateIndex(from, index);
            replaceFragment(serviceDetailsFragment);
        }
    }

    public boolean isLandscape() {
        return getResources().getConfiguration().orientation == ORIENTATION_LANDSCAPE;
    }

    public void showCallDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.call_desc);
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.call, (dialogInterface, i) -> {
            if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CALL_PHONE}, PERMISSION_REQUEST_CALL_PHONE);
                return;
            }
            actionCallIntent();
        });
        builder.setNegativeButton(R.string.cancel, null);
        builder.show();
    }

    private void actionCallIntent() {
        if (CommonUtils.isSimAvailable(MainActivity.this)) {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + getString(R.string.call_desc)));
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                startActivity(callIntent);
            }
        } else {
            Toast.makeText(this, "Sim card not available", Toast.LENGTH_LONG).show();
        }

    }

    private void actionDialIntent() {
        if (CommonUtils.isSimAvailable(MainActivity.this)) {
            Intent callIntent = new Intent(Intent.ACTION_DIAL);
            callIntent.setData(Uri.parse("tel:" + getString(R.string.call_desc)));
            startActivity(callIntent);
        } else {
            Toast.makeText(this, "Sim card not available", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST_CALL_PHONE:
                if (grantResults.length <= 0) {
                    actionDialIntent();
                } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    actionCallIntent();
                } else {
                    actionDialIntent();
                }
                break;
        }
    }

    public void addVehicle(int id) {
        switch (id) {
            case R.id.actionEnterVIN:
//                ((FloatingActionsMenu) findViewById(R.id.floatingActionsMenu)).collapse();
                goToVehicleDetails(0, -1);
                break;
            case R.id.actionScanBarcode:
//                ((FloatingActionsMenu) findViewById(R.id.floatingActionsMenu)).collapse();
                new IntentIntegrator(MainActivity.this).initiateScan(); // `this` is the current Activity
                break;
            case R.id.actionAddVehicle:
//                ((FloatingActionsMenu) findViewById(R.id.floatingActionsMenu)).collapse();
                goToVehicleDetails(0, -1);
                break;
        }
    }

    // Get the results:
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {
                Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "Scanned: " + result.getContents(), Toast.LENGTH_LONG).show();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onClick(View view) {
        /*switch (view.getId()) {
            case R.id.actionEnterVIN:
            case R.id.actionScanBarcode:
            case R.id.actionAddVehicle:
                addVehicle(view.getId());
                break;

        }*/
    }
}
