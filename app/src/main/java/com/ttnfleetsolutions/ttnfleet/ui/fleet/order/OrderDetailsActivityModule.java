package com.ttnfleetsolutions.ttnfleet.ui.fleet.order;

import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Sandip Chaulagain on 11/10/2017.
 *
 * @version 1.0
 */
@Module
public class OrderDetailsActivityModule {

    @Provides
    OrderDetailsViewModel provideOrderViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        return new OrderDetailsViewModel(dataManager, schedulerProvider);
    }

    @Provides
    OrderDetailsViewPagerAdapter provideFeedPagerAdapter(OrderDetailsActivityNew activity) {
        return new OrderDetailsViewPagerAdapter(activity.getSupportFragmentManager());
    }

}
