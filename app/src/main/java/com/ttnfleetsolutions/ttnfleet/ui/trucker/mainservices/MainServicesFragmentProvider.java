package com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Sandip Chaulagain on 1/22/2018.
 *
 * @version 1.0
 */
@Module
public abstract class MainServicesFragmentProvider {

    @ContributesAndroidInjector(modules = MainServicesFragmentModule.class)
    abstract MainServicesFragment provideMainServicesFragmentFactory();
}
