package com.ttnfleetsolutions.ttnfleet.ui.fleet.provider;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.Toast;

import com.ttnfleetsolutions.ttnfleet.BR;
import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.DispatchEventResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.serviceprovider.ServiceProvider;
import com.ttnfleetsolutions.ttnfleet.databinding.FragmentServiceProviderBinding;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.servicerequest.callback.UpdateEventDetailsCallbackFD;
import com.ttnfleetsolutions.ttnfleet.utils.CommonUtils;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */

public class ServiceProviderFragment extends BaseFragment<FragmentServiceProviderBinding, ServiceProviderFragmentViewModel> implements ServiceProviderFragmentNavigator {

    @Inject
    ServiceProviderFragmentViewModel mServiceProviderFragmentViewModel;

    FragmentServiceProviderBinding mFragmentServiceProviderBinding;

    @Inject
    ServicesProviderListAdapter mAdapter;

    private int mEventId;

    private boolean mFlag = false;

    @Override
    protected ServiceProviderFragmentViewModel getViewModel() {
        return mServiceProviderFragmentViewModel;
    }

    @Override
    protected int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_service_provider;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mServiceProviderFragmentViewModel.setNavigator(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFragmentServiceProviderBinding = getViewDataBinding();
        setUp();
    }

    private void setUp() {
        if (getArguments() != null) {
            mEventId = getArguments().getInt("eventId");
            mFlag = getArguments().getBoolean("flag");
        }

        mFragmentServiceProviderBinding.serviceProviderRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mFragmentServiceProviderBinding.serviceProviderRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));

        mAdapter.setEmptyItemViewModelListener(this::getList);
        mAdapter.setListener(mListener);

        if (getViewModel().getServiceProviderList() != null && getViewModel().getServiceProviderList().size() > 0) {
            loadServiceProviderList(getViewModel().getServiceProviderList());
        } else
            getList();
    }

    private UpdateEventDetailsCallbackFD mUpdateEventDetailsCallbackFD;

    public void setUpdateEventDetailsCallback(UpdateEventDetailsCallbackFD updateEventDetailsCallbackFD) {
        this.mUpdateEventDetailsCallbackFD = updateEventDetailsCallbackFD;
    }

    private void getList() {
        if (!isNetworkConnected()) {
            mAdapter.setEmptyItem(CommonUtils.getEmptyItem(getActivity(), DataManager.ErrorState.NETWORK_ERROR));
            mFragmentServiceProviderBinding.serviceProviderRecyclerView.setAdapter(mAdapter);
        } else {
            getViewModel().getServiceProviderListAPICall();
        }
    }

    private ServiceProviderFragmentItemViewModel.ItemViewModelListener mListener = (id, name) -> {
        if (!mFlag) return;
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Assign to " + name + "?");
        builder.setPositiveButton("SUBMIT", (dialogInterface, i) -> getViewModel().dispatchEventAPICall(mEventId, id));
        builder.setNegativeButton("CANCEL", (dialogInterface, i) -> dialogInterface.dismiss());
        builder.show();
    };

    @Override
    public void loadServiceProviderList(List<ServiceProvider> serviceProviderList) {
        if (serviceProviderList != null && serviceProviderList.size() > 0) {
            mAdapter.addItems(serviceProviderList);
            mFragmentServiceProviderBinding.serviceProviderRecyclerView.setAdapter(mAdapter);
        } else {
            mAdapter.setEmptyItem(CommonUtils.getEmptyItem(getActivity(), DataManager.ErrorState.NO_DATA));
            mFragmentServiceProviderBinding.serviceProviderRecyclerView.setAdapter(mAdapter);
        }
    }

    @Override
    public void onDispatchedResponseReceived(DispatchEventResponse dispatchEventResponse) {
        getActivity().onBackPressed();
        mUpdateEventDetailsCallbackFD.onDispatchedEvent();
    }

    @Override
    public void handleError(Throwable throwable) {
        mAdapter.setEmptyItem(CommonUtils.getEmptyItem(getActivity(), DataManager.ErrorState.API_FAIL));
        mFragmentServiceProviderBinding.serviceProviderRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void handleErrorDispatch(Throwable throwable) {
        Toast.makeText(getActivity(), getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
    }

    public boolean isFlag() {
        return mFlag;
    }
}
