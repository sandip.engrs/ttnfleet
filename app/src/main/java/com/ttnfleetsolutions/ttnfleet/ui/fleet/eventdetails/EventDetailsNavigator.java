package com.ttnfleetsolutions.ttnfleet.ui.fleet.eventdetails;

import com.ttnfleetsolutions.ttnfleet.data.model.ServiceRequestSummary;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.listing.Event;

/**
 * Created by Sandip Chaulagain on 2/2/2018.
 *
 * @version 1.0
 */

interface EventDetailsNavigator {

    void updateEventRequestDetails(ServiceRequestSummary serviceRequestSummary);

    void handleError(Throwable throwable);

    void loadEventDetails(Event event);

}
