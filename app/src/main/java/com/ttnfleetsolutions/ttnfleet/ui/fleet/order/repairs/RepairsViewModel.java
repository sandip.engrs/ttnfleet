package com.ttnfleetsolutions.ttnfleet.ui.fleet.order.repairs;

import android.arch.lifecycle.LiveData;

import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.Repair;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseViewModel;
import com.ttnfleetsolutions.ttnfleet.utils.rx.SchedulerProvider;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 11/10/2017.
 *
 * @version 1.0
 */

public class RepairsViewModel extends BaseViewModel<RepairsNavigator> {

    private LiveData<List<Repair>> mRepairListLiveData;

    public RepairsViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    void getAllRepairs() {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .getAllRepairs()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(repairs -> {
                    if (repairs != null) {
                        mRepairListLiveData = repairs;
                        getNavigator().updateRepairs(repairs);
                    }
                    setIsLoading(false);
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                }));
    }

    public LiveData<List<Repair>> getRepairListLiveData() {
        return mRepairListLiveData;
    }

    public void insertData(Repair repair) {
        getCompositeDisposable().add(getDataManager().insertRepair(repair)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(aBoolean -> {
                }));
    }
}
