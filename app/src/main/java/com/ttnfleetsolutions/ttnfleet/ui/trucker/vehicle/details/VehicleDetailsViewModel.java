package com.ttnfleetsolutions.ttnfleet.ui.trucker.vehicle.details;

import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.Vehicle;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseViewModel;
import com.ttnfleetsolutions.ttnfleet.utils.rx.SchedulerProvider;

/**
 * Created by Sandip Chaulagain on 1/22/2018.
 *
 * @version 1.0
 */

public class VehicleDetailsViewModel extends BaseViewModel<VehicleDetailsNavigator> {

    public VehicleDetailsViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    private Vehicle mVehicle;

    public Vehicle getVehicleDetails() {
        return mVehicle;
    }

    void getVehicle(long id) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .getVehicleDetailsById(id)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(vehicle -> {
                    mVehicle = vehicle;
                    getNavigator().updateVehicleDetails(mVehicle);
                    setIsLoading(false);
                }, throwable -> {
                    mVehicle = null;
                    getNavigator().updateVehicleDetails(mVehicle);
                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                }));
    }

    public void insertVehicle(Vehicle vehicle) {
        getCompositeDisposable().add(getDataManager().insertVehicle(vehicle)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe((Boolean aBoolean) -> getNavigator().vehicleAdded(), throwable -> getNavigator().handleError(throwable)));
    }
}
