package com.ttnfleetsolutions.ttnfleet.ui.trucker.notification;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.View;

import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.BR;
import com.ttnfleetsolutions.ttnfleet.data.model.NotificationModel;
import com.ttnfleetsolutions.ttnfleet.databinding.FragmentNotificationsBinding;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseFragment;
import com.ttnfleetsolutions.ttnfleet.ui.fleet.DashboardActivity;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.MainActivityNew;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */

public class NotificationsFragment extends BaseFragment<FragmentNotificationsBinding, NotificationsViewModel> implements NotificationsNavigator, NotificationItemViewModel.NotificationItemViewModelListener {

    @Inject
    NotificationsViewModel mNotificationsViewModel;

    FragmentNotificationsBinding mFragmentNotificationsBinding;

    @Inject
    NotificationRecyclerViewAdapter mAdapter;

    private List<NotificationModel> mNotificationModels = new ArrayList<>();

    @Override
    protected NotificationsViewModel getViewModel() {
        return mNotificationsViewModel;
    }

    @Override
    protected int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_notifications;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mNotificationsViewModel.setNavigator(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFragmentNotificationsBinding = getViewDataBinding();
        setUp();
    }

    private void setUp() {
        mFragmentNotificationsBinding.notificationsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mFragmentNotificationsBinding.notificationsRecyclerView.setHasFixedSize(true);
        mFragmentNotificationsBinding.notificationsRecyclerView.setNestedScrollingEnabled(false);
        mFragmentNotificationsBinding.notificationsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mFragmentNotificationsBinding.notificationsRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));

        String[] times = getResources().getStringArray(R.array.times2);
        String[] texts = getResources().getStringArray(R.array.driver_notifications);
        for (int i = 0; i < texts.length; i++) {
            mNotificationModels.add(new NotificationModel(i, texts[i], times[i], "", "", false));
        }
        mNotificationModels.get(0).setRead(false);
        mNotificationModels.get(0).setActionPositive("Feedback");
        mAdapter.addItems(mNotificationModels);
        mFragmentNotificationsBinding.notificationsRecyclerView.setAdapter(mAdapter);
        mAdapter.setListener(this);
    }

    @Override
    public void onItemClick(long id) {
        if (getActivity() instanceof MainActivityNew) {
            ((MainActivityNew) getActivity()).goToServiceDetails((int) id);
        } else if (getActivity() instanceof DashboardActivity) {
            ((DashboardActivity) getActivity()).goToOrderDetailsFromNotifiction(-1, 3);
        }
    }

    @Override
    public void onItemClickActionPositive(String action) {
        if (getActivity() instanceof MainActivityNew) {
            if (!TextUtils.isEmpty(action)) {
                if (getActivity() instanceof MainActivityNew) {
                    if (action.equalsIgnoreCase("Track Location")) {
                        ((MainActivityNew) getActivity()).trackLocation(0);
                    } else if (action.equalsIgnoreCase("Feedback")) {
                        ((MainActivityNew) getActivity()).showFeedbackDialog();
                    }
                } else if (getActivity() instanceof DashboardActivity) {
                    if (action.equalsIgnoreCase("Track Location")) {
                        ((DashboardActivity) getActivity()).trackLocation(0);
                    }
                }
            }
        }
    }

    @Override
    public void onItemClickActionNegative(String action) {
        if (!TextUtils.isEmpty(action)) {
            if (getActivity() instanceof MainActivityNew) {
                if (action.equalsIgnoreCase("Track Location")) {
                    ((MainActivityNew) getActivity()).trackLocation(0);
                } else if (action.equalsIgnoreCase("Feedback")) {
                    ((MainActivityNew) getActivity()).showFeedbackDialog();
                }
            } else if (getActivity() instanceof DashboardActivity) {
                if (action.equalsIgnoreCase("Track Location")) {
                    ((DashboardActivity) getActivity()).trackLocation(0);
                }
            }
        }
    }

    /*@Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_notifications, container, false);

        RecyclerView mRecyclerView = rootView.findViewById(R.id.notificationsRecyclerView);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setAdapter(new NotificationRecyclerViewAdapter(getActivity()));
        return rootView;
    }*/
}
