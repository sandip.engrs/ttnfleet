package com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.driver;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.DriverListResponse;
import com.ttnfleetsolutions.ttnfleet.databinding.RowDriverBinding;
import com.ttnfleetsolutions.ttnfleet.databinding.RowEmptyViewBinding;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseViewHolder;
import com.ttnfleetsolutions.ttnfleet.ui.empty.EmptyItem;
import com.ttnfleetsolutions.ttnfleet.ui.empty.EmptyItemViewModel;
import com.ttnfleetsolutions.ttnfleet.ui.empty.EmptyViewHolder;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */

public class SelectDriverRecyclerViewAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private List<DriverListResponse.Driver> mList;

    private SelectDriverItemViewModel.ItemViewModelListener mListener;
    private EmptyItemViewModel.EmptyItemViewModelListener mEmptyItemViewModelListener;
    private EmptyItem mEmptyItem;

    public void setEmptyItem(EmptyItem mEmptyItem) {
        this.mEmptyItem = mEmptyItem;
    }

    public void setEmptyItemViewModelListener(EmptyItemViewModel.EmptyItemViewModelListener mEmptyItemViewModelListener) {
        this.mEmptyItemViewModelListener = mEmptyItemViewModelListener;
    }

    public void setListener(SelectDriverItemViewModel.ItemViewModelListener listener) {
        this.mListener = listener;
    }

    public SelectDriverRecyclerViewAdapter(List<DriverListResponse.Driver> list) {
        this.mList = list;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                RowDriverBinding rowBinding = RowDriverBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
                return new MyViewHolder(rowBinding);
            case VIEW_TYPE_EMPTY:
            default:
                RowEmptyViewBinding emptyViewBinding = RowEmptyViewBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
                return new EmptyViewHolder(emptyViewBinding, mEmptyItem, mEmptyItemViewModelListener);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mList != null && mList.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public int getItemCount() {
        if (mList != null && mList.size() > 0) {
            return mList.size();
        } else {
            return 1;
        }
    }

    public DriverListResponse.Driver getItem(int position) {
        return mList.get(position);
    }

    public void addItems(List<DriverListResponse.Driver> list) {
        mList = list;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends BaseViewHolder implements SelectDriverItemViewModel.ItemViewModelListener {

        private RowDriverBinding mBinding;

        private SelectDriverItemViewModel mItemViewModel;

        public MyViewHolder(RowDriverBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {

            final DriverListResponse.Driver serviceCode = mList.get(position);
            mItemViewModel = new SelectDriverItemViewModel(serviceCode, this);
            mBinding.setViewModel(mItemViewModel);

            // Immediate Binding
            // When a variable or observable changes, the binding will be scheduled to change before
            // the next frame. There are times, however, when binding must be executed immediately.
            // To force execution, use the executePendingBindings() method.
            mBinding.executePendingBindings();
        }

        @Override
        public void onItemClick(int driverId) {
            mListener.onItemClick(driverId);
        }
    }
}

