package com.ttnfleetsolutions.ttnfleet.ui.serviceprovider.jobroster;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Sandip Chaulagain on 11/10/2017.
 *
 * @version 1.0
 */
@Module
public abstract class JobRosterFragmentProvider {

    @ContributesAndroidInjector(modules = JobRosterFragmentModule.class)
    abstract JobRosterFragment provideJobRosterFragmentFactory();

    @ContributesAndroidInjector(modules = JobRosterFragmentModule.class)
    abstract com.ttnfleetsolutions.ttnfleet.ui.technician.jobroster.JobRosterFragment provideJobRosterFragmentTechFactory();
}
