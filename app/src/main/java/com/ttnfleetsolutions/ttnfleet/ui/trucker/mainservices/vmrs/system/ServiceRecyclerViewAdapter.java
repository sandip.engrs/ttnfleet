package com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.vmrs.system;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.ServiceCode;
import com.ttnfleetsolutions.ttnfleet.databinding.RowEmptyViewBinding;
import com.ttnfleetsolutions.ttnfleet.databinding.RowServiceBinding;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseViewHolder;
import com.ttnfleetsolutions.ttnfleet.ui.empty.EmptyItem;
import com.ttnfleetsolutions.ttnfleet.ui.empty.EmptyItemViewModel;
import com.ttnfleetsolutions.ttnfleet.ui.empty.EmptyViewHolder;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */

public class ServiceRecyclerViewAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private List<ServiceCode> mList;

    private boolean mMultipleSelectionEnabled = false;
    private int mCodeStep = 0;

    private SelectServiceItemViewModel.ServiceItemViewModelListener mListener;
    private EmptyItemViewModel.EmptyItemViewModelListener mEmptyItemViewModelListener;
    private EmptyItem mEmptyItem;

    public void setEmptyItem(EmptyItem mEmptyItem) {
        this.mEmptyItem = mEmptyItem;
    }

    public void setEmptyItemViewModelListener(EmptyItemViewModel.EmptyItemViewModelListener mEmptyItemViewModelListener) {
        this.mEmptyItemViewModelListener = mEmptyItemViewModelListener;
    }

    public void setListener(SelectServiceItemViewModel.ServiceItemViewModelListener listener) {
        this.mListener = listener;
    }

    public ServiceRecyclerViewAdapter(List<ServiceCode> list) {
        this.mList = list;
    }

    public boolean isMultipleSelectionEnabled() {
        return mMultipleSelectionEnabled;
    }

    public void setMultipleSelectionEnabled(boolean multipleSelectionEnabled) {
        this.mMultipleSelectionEnabled = multipleSelectionEnabled;
    }

    public int getCodeStep() {
        return mCodeStep;
    }

    public void setCodeStep(int mCodeStep) {
        this.mCodeStep = mCodeStep;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                RowServiceBinding rowServiceBinding = RowServiceBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
                return new MyViewHolder(rowServiceBinding);
            case VIEW_TYPE_EMPTY:
            default:
                RowEmptyViewBinding emptyViewBinding = RowEmptyViewBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
                return new EmptyViewHolder(emptyViewBinding, mEmptyItem, mEmptyItemViewModelListener);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mList != null && mList.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public int getItemCount() {
        if (mList != null && mList.size() > 0) {
            return mList.size();
        } else {
            return 1;
        }
    }

    public ServiceCode getItem(int position) {
        return mList.get(position);
    }

    public void addItems(List<ServiceCode> list) {
        mList = list;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends BaseViewHolder implements SelectServiceItemViewModel.ServiceItemViewModelListener {

        private RowServiceBinding mBinding;

        private SelectServiceItemViewModel mSelectServiceItemViewModel;

        public MyViewHolder(RowServiceBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {

            final ServiceCode serviceCode = mList.get(position);
            serviceCode.setPosition(position);
            mSelectServiceItemViewModel = new SelectServiceItemViewModel(serviceCode, mMultipleSelectionEnabled, mCodeStep, this);

            mBinding.setViewModel(mSelectServiceItemViewModel);

            // Immediate Binding
            // When a variable or observable changes, the binding will be scheduled to change before
            // the next frame. There are times, however, when binding must be executed immediately.
            // To force execution, use the executePendingBindings() method.
            mBinding.executePendingBindings();

        }

        /*@Override
        public void onItemClick(String systemCode, String assemblyCode, String componentCode) {
            mListener.onItemClick(systemCode, assemblyCode, componentCode);
        }*/

        @Override
        public void onItemClick(String systemCode, String assemblyCode, String componentCode, String systemCodeName, String assemblyCodeName) {
            mListener.onItemClick(systemCode, assemblyCode, componentCode, systemCodeName, assemblyCodeName);
        }

        @Override
        public void onItemSelection(int position, boolean isSelected) {
            mListener.onItemSelection(position, isSelected);
        }
    }
}

