package com.ttnfleetsolutions.ttnfleet.ui.technician;

import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.AdditionalJobResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.AcceptWorkOrderResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.CompleteJobResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.EnrouteResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.RejectionTypesResponse;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 1/10/2018.
 *
 * @version 1.0
 */

public interface TechnicianDashboardNavigator {

    void handleError(Throwable throwable);

    void getRejectionTypesTech(int eventId, int jobId, int jobStatusId, List<RejectionTypesResponse.RejectionType> rejectionTypeList);

    void onAcceptRejectJob(AcceptWorkOrderResponse acceptWorkOrderResponse);

    void onAdditionalJob(AdditionalJobResponse additionalJobResponse);

    void onCompleteJob(CompleteJobResponse completeJobResponse);

    void onEnroute(EnrouteResponse enrouteResponse);

}
