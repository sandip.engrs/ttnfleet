package com.ttnfleetsolutions.ttnfleet.ui.fleet.order;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.ttnfleetsolutions.ttnfleet.ui.fleet.order.chat.ChatFragment;
import com.ttnfleetsolutions.ttnfleet.ui.fleet.order.repairs.RepairsFragment;
import com.ttnfleetsolutions.ttnfleet.ui.fleet.order.status.StatusFragment;
import com.ttnfleetsolutions.ttnfleet.ui.fleet.order.technician.TechnicianFragment;
import com.ttnfleetsolutions.ttnfleet.ui.fleet.order.vehicle.VehicleFragment;

/**
 * Created by Sandip Chaulagain on 11/10/2017.
 *
 * @version 1.0
 */

class OrderDetailsViewPagerAdapter extends FragmentStatePagerAdapter {

    public OrderDetailsViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return VehicleFragment.newInstance();
            case 1:
                return RepairsFragment.newInstance();
            case 2:
                return StatusFragment.newInstance();
            case 3:
                return TechnicianFragment.newInstance();
            case 4:
                return ChatFragment.newInstance();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 5;
    }
}
