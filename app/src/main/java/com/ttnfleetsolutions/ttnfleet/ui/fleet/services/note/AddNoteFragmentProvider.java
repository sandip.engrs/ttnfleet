package com.ttnfleetsolutions.ttnfleet.ui.fleet.services.note;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Sandip Chaulagain on 1/22/2018.
 *
 * @version 1.0
 */
@Module
public abstract class AddNoteFragmentProvider {

    @ContributesAndroidInjector(modules = AddNoteFragmentModule.class)
    abstract AddNoteFragment provideAddNoteFragmentFactory();
}
