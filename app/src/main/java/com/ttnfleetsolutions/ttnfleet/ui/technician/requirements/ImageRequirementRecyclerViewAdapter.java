package com.ttnfleetsolutions.ttnfleet.ui.technician.requirements;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.ttnfleetsolutions.ttnfleet.data.model.api.event.requirements.ImageRequirement;
import com.ttnfleetsolutions.ttnfleet.databinding.RowUploadRequirementImageBinding;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseViewHolder;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */

public class ImageRequirementRecyclerViewAdapter extends RecyclerView.Adapter<BaseViewHolder>  {

    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private List<ImageRequirement> mList;

    private ImageRequirementItemViewModel.ItemViewModelListener mListener;

    private int mRootViewWidth = 720;

    public void setListener(ImageRequirementItemViewModel.ItemViewModelListener listener) {
        this.mListener = listener;
    }

    public void setRootViewWidth(int mRootViewWidth) {
        this.mRootViewWidth = mRootViewWidth;
    }

    public ImageRequirementRecyclerViewAdapter(List<ImageRequirement> list) {
        this.mList = list;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RowUploadRequirementImageBinding rowBinding = RowUploadRequirementImageBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) rowBinding.selectedImage.getLayoutParams();
//        layoutParams.width = CommonUtils.getScreenWidth()/3 - 60;
        layoutParams.width = mRootViewWidth;
        layoutParams.height = layoutParams.width;
        rowBinding.selectedImage.setLayoutParams(layoutParams);
        rowBinding.imageName.setLayoutParams(layoutParams);
        return new MyViewHolder(rowBinding);
    }

    @Override
    public int getItemViewType(int position) {
        if (mList != null && mList.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public int getItemCount() {
        if (mList != null && mList.size() > 0) {
            return mList.size();
        } else {
            return 0;
        }
    }

    public void addItems(List<ImageRequirement> list) {
        mList = list;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends BaseViewHolder implements ImageRequirementItemViewModel.ItemViewModelListener {

        private RowUploadRequirementImageBinding mBinding;

        private ImageRequirementItemViewModel mItemViewModel;

        public MyViewHolder(RowUploadRequirementImageBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {

            final ImageRequirement imageRequirement = mList.get(position);

            mItemViewModel = new ImageRequirementItemViewModel(imageRequirement, this);

            mBinding.setViewModel(mItemViewModel);

            // Immediate Binding
            // When a variable or observable changes, the binding will be scheduled to change before
            // the next frame. There are times, however, when binding must be executed immediately.
            // To force execution, use the executePendingBindings() method.
            mBinding.executePendingBindings();

        }


        @Override
        public void onItemClick(int index) {
            mListener.onItemClick(index);
        }
    }
}
