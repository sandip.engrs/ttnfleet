package com.ttnfleetsolutions.ttnfleet.ui.trucker.vehicle.details;

import android.arch.lifecycle.LiveData;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Toast;

import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.BR;
import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.Vehicle;
import com.ttnfleetsolutions.ttnfleet.databinding.FragmentAddVehicleBinding;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseActivity;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.MainActivityNew;
import com.ttnfleetsolutions.ttnfleet.utils.CommonUtils;

import javax.inject.Inject;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */

public class VehicleDetailsFragment extends BaseFragment<FragmentAddVehicleBinding, VehicleDetailsViewModel> implements VehicleDetailsNavigator, View.OnClickListener {

    @Inject
    VehicleDetailsViewModel mVehicleDetailsViewModel;

    FragmentAddVehicleBinding mFragmentAddVehicleBinding;

    @Override
    protected VehicleDetailsViewModel getViewModel() {
        return mVehicleDetailsViewModel;
    }

    @Override
    protected int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_add_vehicle;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mVehicleDetailsViewModel.setNavigator(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFragmentAddVehicleBinding = getViewDataBinding();
        setUp();
    }

    private void setUp() {
        initView();
    }

    private long id;
//    private EditText mVinEditText, mVehicleNameEditText, mLicencePlatNoEditText, mBrandEditText;
//    private Spinner mYearSpinner, mFueltypesSpinner;
//    private View mRootView;


    /*@Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_add_vehicle, container, false);
        mVinEditText = mRootView.findViewById(R.id.vinEditText);
        mVehicleNameEditText = mRootView.findViewById(R.id.vehicleNameEditText);
        mLicencePlatNoEditText = mRootView.findViewById(R.id.licencePlatNoEditText);
        mBrandEditText = mRootView.findViewById(R.id.brandEditText);
        mYearSpinner = mRootView.findViewById(R.id.yearSpinner);
        mFueltypesSpinner = mRootView.findViewById(R.id.fueltypesSpinner);

        mRootView.findViewById(R.id.actionEnterVIN).setOnClickListener(this);
        mRootView.findViewById(R.id.actionScanBarcode).setOnClickListener(this);
        mRootView.findViewById(R.id.actionAddVehicle).setOnClickListener(this);

        if (((MainActivityNew) getActivity()).isLandscape()) {
            mRootView.findViewById(R.id.floatingActionsMenu).setVisibility(View.VISIBLE);
        }
        return mRootView;
    }*/

    private void initView() {
        mVehicleDetailsViewModel.getVehicle(id);

      /*  if (mVehicleDetailsViewModel.getVehicleLiveData() != null) {
            mFragmentAddVehicleBinding.vinEditText.setText(mVehicleDetailsViewModel.getVehicleLiveData().getValue().vin);
            mFragmentAddVehicleBinding.vehicleNameEditText.setText(mVehicleDetailsViewModel.getVehicleLiveData().getValue().vehicle_name);
            mFragmentAddVehicleBinding.licencePlatNoEditText.setText(mVehicleDetailsViewModel.getVehicleLiveData().getValue().licence);
            mFragmentAddVehicleBinding.brandEditText.setText(mVehicleDetailsViewModel.getVehicleLiveData().getValue().brand);
            mFragmentAddVehicleBinding.yearSpinner.setSelection(5);
            mFragmentAddVehicleBinding.fueltypesSpinner.setSelection(2);
        }*/

        mFragmentAddVehicleBinding.actionEnterVIN.setOnClickListener(this);
        mFragmentAddVehicleBinding.actionScanBarcode.setOnClickListener(this);
        mFragmentAddVehicleBinding.actionAddVehicle.setOnClickListener(this);

        if (((MainActivityNew) getActivity()).isLandscape()) {
            ((MainActivityNew) getActivity()).setSubTitle("Edit Vehicle");
            mFragmentAddVehicleBinding.floatingActionsMenu.setVisibility(View.VISIBLE);
            if (id < 0) ((MainActivityNew) getActivity()).setSubTitle("Add Vehicle");
        } else if (id > 0)
            ((MainActivityNew) getActivity()).setTitle("Edit Vehicle");
        else ((MainActivityNew) getActivity()).setTitle("Add Vehicle");
        /*switch (index) {
            case -1:

                mVinEditText.setText("");
                mVehicleNameEditText.setText("");
                mLicencePlatNoEditText.setText("");
                mBrandEditText.setText("");
                mYearSpinner.setSelection(0);
                mFueltypesSpinner.setSelection(0);
                if (((MainActivity) getActivity()).isLandscape())
                    ((MainActivity) getActivity()).setSubTitle("Add Vehicle");
                else
                    ((MainActivity) getActivity()).setTitle("Add Vehicle");
                break;
            case 0:
                mVinEditText.setText("WVM56502016034590");
                mVehicleNameEditText.setText("Maan Truck 8.136 FAE 4X4");
                mLicencePlatNoEditText.setText("TX 01 FJ0122");
                mBrandEditText.setText("Maan");
                mYearSpinner.setSelection(5);
                mFueltypesSpinner.setSelection(2);
                if (((MainActivity) getActivity()).isLandscape())
                    ((MainActivity) getActivity()).setSubTitle("Edit Vehicle");
                else
                    ((MainActivity) getActivity()).setTitle("Edit Vehicle");

                break;
            case 1:
                mVinEditText.setText("WVM24389042384849");
                mVehicleNameEditText.setText("IVECO Stralis 440S48 2002 – 2013");
                mLicencePlatNoEditText.setText("WR 22 RK5685");
                mBrandEditText.setText("IVECO Stralis");
                mYearSpinner.setSelection(6);
                mFueltypesSpinner.setSelection(1);
                if (((MainActivity) getActivity()).isLandscape())
                    ((MainActivity) getActivity()).setSubTitle("Edit Vehicle");
                else
                    ((MainActivity) getActivity()).setTitle("Edit Vehicle");
                break;
        }*/
    }

    public void updateIndex(int index) {
        this.id = index;
//        if (mRootView != null) initView();
    }

    public void updateId(long id) {
        this.id = id;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.actionEnterVIN:
            case R.id.actionScanBarcode:
            case R.id.actionAddVehicle:
//                ((FloatingActionsMenu) mRootView.findViewById(R.id.floatingActionsMenu)).collapse();
                ((MainActivityNew) getActivity()).addVehicle(view.getId());
                break;
        }
    }

    @Override
    public void updateVehicleDetails(LiveData<Vehicle> liveData) {
       /* if (liveData != null && liveData.getValue() != null) {
            mFragmentAddVehicleBinding.vinEditText.setText(mVehicleDetailsViewModel.getVehicleLiveData().getValue().vin);
            mFragmentAddVehicleBinding.vehicleNameEditText.setText(mVehicleDetailsViewModel.getVehicleLiveData().getValue().vehicle_name);
            mFragmentAddVehicleBinding.licencePlatNoEditText.setText(mVehicleDetailsViewModel.getVehicleLiveData().getValue().licence);
            mFragmentAddVehicleBinding.brandEditText.setText(mVehicleDetailsViewModel.getVehicleLiveData().getValue().brand);
            mFragmentAddVehicleBinding.yearSpinner.setSelection(5);
            mFragmentAddVehicleBinding.fueltypesSpinner.setSelection(2);
        }*/
    }

    public void updateVehicle() {
        CommonUtils.hideKeyboard(getActivity());
        if (id < 1) id = 6;
        Vehicle vehicle = new Vehicle(id, mFragmentAddVehicleBinding.vehicleNameEditText.getText().toString(),
                mFragmentAddVehicleBinding.vinEditText.getText().toString(), mFragmentAddVehicleBinding.licencePlatNoEditText.getText().toString(),
                mFragmentAddVehicleBinding.brandEditText.getText().toString(), mFragmentAddVehicleBinding.yearSpinner.getSelectedItem().toString(),
                mFragmentAddVehicleBinding.fueltypesSpinner.getSelectedItem().toString());
        mVehicleDetailsViewModel.insertVehicle(vehicle);
    }

    @Override
    public void updateVehicleDetails(Vehicle vehicle) {
        if (vehicle != null) {
            mFragmentAddVehicleBinding.vinEditText.setText(vehicle.vin);
            mFragmentAddVehicleBinding.vehicleNameEditText.setText(vehicle.vehicle_name);
            mFragmentAddVehicleBinding.licencePlatNoEditText.setText(vehicle.licence);
            mFragmentAddVehicleBinding.brandEditText.setText(vehicle.brand);
            mFragmentAddVehicleBinding.yearSpinner.setSelection(5);
            mFragmentAddVehicleBinding.fueltypesSpinner.setSelection(2);
        } else {
            mFragmentAddVehicleBinding.vinEditText.setText("");
            mFragmentAddVehicleBinding.vehicleNameEditText.setText("");
            mFragmentAddVehicleBinding.licencePlatNoEditText.setText("");
            mFragmentAddVehicleBinding.brandEditText.setText("");
            mFragmentAddVehicleBinding.yearSpinner.setSelection(0);
            mFragmentAddVehicleBinding.fueltypesSpinner.setSelection(0);
        }
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(getActivity(), getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void vehicleAdded() {
        Toast.makeText(getActivity(), "Vehicle updated", Toast.LENGTH_SHORT).show();
        if (!((BaseActivity) getActivity()).isLandscape())
            getActivity().onBackPressed();
    }

    /*@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstan
        ceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.vehical, menu);
    }*/
}
