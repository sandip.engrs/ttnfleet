package com.ttnfleetsolutions.ttnfleet.ui.empty;

/**
 * Created by Sandip Chaulagain on 6/27/2018.
 *
 * @version 1.0
 */
public class EmptyItem {

    private String message;

    private boolean isRetryVisible;

    private boolean isLogoVisible;

    public EmptyItem(String message, boolean isRetryVisible, boolean isLogoVisible) {
        this.message = message;
        this.isRetryVisible = isRetryVisible;
        this.isLogoVisible = isLogoVisible;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isRetryVisible() {
        return isRetryVisible;
    }

    public void setRetryVisible(boolean retryVisible) {
        isRetryVisible = retryVisible;
    }

    public boolean isLogoVisible() {
        return isLogoVisible;
    }

    public void setLogoVisible(boolean logoVisible) {
        isLogoVisible = logoVisible;
    }
}
