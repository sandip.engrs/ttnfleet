package com.ttnfleetsolutions.ttnfleet.ui.trucker;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ttnfleetsolutions.ttnfleet.R;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */

public class NavigationDrawerListAdapter extends RecyclerView.Adapter<NavigationDrawerListAdapter.MyViewHolder> {

    private Context mContext;
    private String[] names;
    private TypedArray icons;
    private String tintColor;

    public NavigationDrawerListAdapter(Context context, String tintColor) {
        this.mContext = context;
        this.tintColor =  tintColor;
        names = mContext.getResources().getStringArray(R.array.menu_names);
        icons = mContext.getResources().obtainTypedArray(R.array.menu_icons);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_nav_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.name.setText(names[position]);
        holder.icon.setImageDrawable(icons.getDrawable(position));
        holder.icon.setColorFilter(Color.parseColor(tintColor));
//        holder.rowLayout.setOnClickListener(view -> holder.checkBox.setChecked(!holder.checkBox.isChecked()));
    }

    @Override
    public int getItemCount() {
        return names.length;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public ImageView icon;
        public ConstraintLayout rowLayout;

        public MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.menuName);
            icon = view.findViewById(R.id.menuIcon);
            rowLayout = (ConstraintLayout) view;
        }
    }
}
