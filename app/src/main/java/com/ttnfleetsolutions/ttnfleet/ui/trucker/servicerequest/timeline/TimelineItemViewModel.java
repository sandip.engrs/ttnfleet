package com.ttnfleetsolutions.ttnfleet.ui.trucker.servicerequest.timeline;

import android.databinding.BindingAdapter;
import android.databinding.ObservableField;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.TextView;

import com.github.vipulasri.timelineview.TimelineView;
import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.model.DateTimeUtils;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.model.OrderStatus;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.model.TimeLineModel;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.model.VectorDrawableUtils;

/**
 * Created by Sandip Chaulagain on 1/19/2018.
 *
 * @version 1.0
 */

public class TimelineItemViewModel {

    private TimeLineModel mTimeLineModel;
    public ObservableField<Long> id;
    public ObservableField<String> date, message, action;
    public ObservableField<OrderStatus> status;
    private TimelineItemViewModelListener mListener;

    TimelineItemViewModel(TimeLineModel timeLineModel, TimelineItemViewModelListener listener) {
        this.mTimeLineModel = timeLineModel;
        this.mListener = listener;
        id = new ObservableField<>(timeLineModel.id);
        date = new ObservableField<>(DateTimeUtils.parseDateTime(timeLineModel.getDate()));
        message = new ObservableField<>(timeLineModel.getMessage());
        action = new ObservableField<>(timeLineModel.getAction());
        status = new ObservableField<>(timeLineModel.getStatus());
    }

    public void onItemClick() {
        mListener.onItemClick(mTimeLineModel.getAction());
    }

    public interface TimelineItemViewModelListener {
        void onItemClick(String action);
    }

    @BindingAdapter({"background"})
    public static void setBackground(View view, OrderStatus status) {
        if (view == null) return;
        switch (status) {
            case INACTIVE:
                ((TimelineView) view).setMarker(VectorDrawableUtils.getDrawable(view.getContext(), R.drawable.ic_marker_inactive, android.R.color.darker_gray));
                break;
            case ACTIVE:
                ((TimelineView) view).setMarker(VectorDrawableUtils.getDrawable(view.getContext(), R.drawable.ic_marker_active, R.color.colorPrimary));
                break;
            case COMPLETED:
                ((TimelineView) view).setMarker(VectorDrawableUtils.getDrawable(view.getContext(), R.drawable.ic_marker, R.color.colorPrimary));
                break;
        }
    }

    @BindingAdapter({"textColor"})
    public static void setTextColor(View view, OrderStatus status) {
        switch (status) {
            case INACTIVE:
                ((TextView) view).setTextColor(ContextCompat.getColor(view.getContext(), android.R.color.darker_gray));
                break;
            case ACTIVE:
                ((TextView) view).setTextColor(ContextCompat.getColor(view.getContext(), R.color.colorPrimary));
                break;
            case COMPLETED:
                ((TextView) view).setTextColor(ContextCompat.getColor(view.getContext(), R.color.gray_darker));
                break;
            case DELAYED:
                ((TextView) view).setTextColor(ContextCompat.getColor(view.getContext(), android.R.color.holo_orange_dark));
                break;
        }
    }
}
