package com.ttnfleetsolutions.ttnfleet.ui.serviceprovider;

import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Sandip Chaulagain on 3/7/2018.
 *
 * @version 1.0
 */
@Module
public class ServiceProviderDashboardActivityModule {

    @Provides
    ServiceProviderDashboardActivityModel provideServiceProviderDashboardActivityViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        return new ServiceProviderDashboardActivityModel(dataManager, schedulerProvider);
    }
}
