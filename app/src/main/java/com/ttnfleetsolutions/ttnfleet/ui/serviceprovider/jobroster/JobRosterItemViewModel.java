package com.ttnfleetsolutions.ttnfleet.ui.serviceprovider.jobroster;

import android.databinding.BindingAdapter;
import android.databinding.ObservableField;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.listing.Event;
import com.ttnfleetsolutions.ttnfleet.utils.CommonUtils;

/**
 * Created by Sandip Chaulagain on 1/19/2018.
 *
 * @version 1.0
 */

public class JobRosterItemViewModel {

    private Event mEvent;
    public ObservableField<Integer> id;
    public ObservableField<String> name;
    public ObservableField<String> serviceType;
    public ObservableField<Integer> vehicleID;
    public ObservableField<Integer> locationID;

    public ObservableField<String> serviceIssue;
    public ObservableField<String> RTA;
    public ObservableField<String> ATA;
    public ObservableField<String> ATC;
    public ObservableField<String> RTC;

    public ObservableField<String> datetime;

    public ObservableField<Integer> status;

    //Drop-off & Pick-up
    public ObservableField<String> dropoff;
    public ObservableField<String> pickup;
    public ObservableField<String> statusText;
    public ObservableField<String> vehicleName;
    public ObservableField<String> VIN;
    public ObservableField<String> address;
    public ObservableField<Boolean> isAcceptRejectVisible, isEnrouteVisible, isConfirmBreakdownVisible, isEstimateVisible, isAdditionalJobVisible, isCompleteJobVisible;
    private int mFrom;
    private ItemViewModelListener mListener;
    public ObservableField<Boolean> isSelected;

    JobRosterItemViewModel(Event event, ItemViewModelListener listener, int from, boolean isLandscape) {
        this.mFrom = from;
        this.mEvent = event;
        this.mListener = listener;
        id = new ObservableField<>(event.getEventid());
        name = new ObservableField<>("Event #" + event.getEventid());
        if (event.getEventType() != null)
            serviceType = new ObservableField<>(event.getEventType().getTitle());
        vehicleID = new ObservableField<>(event.getVehicleid());
        locationID = new ObservableField<>(event.getLocationid());

        isAcceptRejectVisible = new ObservableField<>(false);
        isEnrouteVisible = new ObservableField<>(false);
        isConfirmBreakdownVisible = new ObservableField<>(false);
        isEstimateVisible = new ObservableField<>(false);
        isAdditionalJobVisible = new ObservableField<>(false);
        isCompleteJobVisible = new ObservableField<>(false);

        StringBuilder stringBuilder = new StringBuilder();
        String prefix = "";
        if (event.getWorkOrders() != null && event.getWorkOrders().size() > 0) {
            for (int i = 0; i < event.getWorkOrders().size(); i++) {
                if (event.getWorkOrders().get(i).getServiceCode() != null && !TextUtils.isEmpty(event.getWorkOrders().get(i).getServiceCode().getDisplaytitle())) {
                    //append code
                    stringBuilder.append(prefix);
                    prefix = "\n";
                    stringBuilder.append(i + 1);
                    stringBuilder.append(". ");
                    stringBuilder.append(event.getWorkOrders().get(i).getServiceCode().getDisplaytitle());
                }
                /*int statusId = event.getWorkOrders().get(i).getStatusid();
                if (mFrom == 0) {
                    //service provider
                    if (statusId == DataManager.Status.ACCEPT_REJECT.getStatus())
                        isAcceptRejectVisible = new ObservableField<>(true);
                } else if (mFrom == 1) {
                    //technician
                    for (int j = 0; j < event.getWorkOrders().get(i).getJobs().size(); j++) {
                        if (event.getWorkOrders().get(i).getJobs().get(j).getStatusid() == DataManager.Status.ACCEPT_REJECT.getStatus())
                            isAcceptRejectVisible = new ObservableField<>(true);
                    }
                }*/
            }

            //Buttons Visibility
            int workOrderStatusId = event.getWorkOrders().get(0).getStatusid();
            if (mFrom == 0 && !isLandscape) {
                if (workOrderStatusId == DataManager.Status.ACCEPT_REJECT_SP.getStatus())
                    isAcceptRejectVisible = new ObservableField<>(true);
            } else if (mFrom == 1 && !isLandscape) {
                if (event.getWorkOrders().get(0).getJobs() != null && event.getWorkOrders().get(0).getJobs().size() > 0) {
                    int jobStatusId = event.getWorkOrders().get(0).getJobs().get(0).getStatusid();
                    if (jobStatusId == DataManager.Status.ACCEPT_REJECT_TECH.getStatus()) {
                        isAcceptRejectVisible = new ObservableField<>(true);
                    } else if (jobStatusId == DataManager.Status.ACCEPTED_SHOW_ENROUTE.getStatus()) {
                        isEnrouteVisible = new ObservableField<>(true);
                    } else if (jobStatusId == DataManager.Status.ENROUTED.getStatus()) {
                        isConfirmBreakdownVisible = new ObservableField<>(true);
                    } else if (jobStatusId == DataManager.Status.REACHED.getStatus()) {
                        isConfirmBreakdownVisible = new ObservableField<>(true);
                    } else if (jobStatusId == DataManager.Status.CONFIRMED_BREAKDOWN.getStatus()) {
                        isAdditionalJobVisible = new ObservableField<>(true);
                        isCompleteJobVisible = new ObservableField<>(true);
                    }
                }
            }
        }
        if (isLandscape) {
            isSelected = new ObservableField<>(event.isSelected());
        }

        serviceIssue = new ObservableField<>(stringBuilder.toString());

        RTA = new ObservableField<>(CommonUtils.getFromattedDate(event.getRta()));
        ATA = new ObservableField<>("");
        ATC = new ObservableField<>("");
        RTC = new ObservableField<>(CommonUtils.getFromattedDate(event.getRtc()));

        datetime = new ObservableField<>(CommonUtils.getPrettyTime(event.getCreateddatetime()));
//        status = new ObservableField<>(1);
         /*dropoff = new ObservableField<>(event.dropoff);
        pickup = new ObservableField<>(event.pickup);
*/
        vehicleName = new ObservableField<>(event.getVehicle().getVehiclename());
        VIN = new ObservableField<>(event.getVehicle().getVin());
        try {
            address = new ObservableField<>(event.getLocation().getAddress());
        } catch (Exception e) {
            e.printStackTrace();
        }

        /*if(mEvent.getFrom() == 0) {
            try {
                statusText = new ObservableField<>(event.getWorkOrders().get(0).getStatus().getTitle());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if(mEvent.getFrom() == 1) {
            try {
                statusText = new ObservableField<>(event.getWorkOrders().get(0).getJobs().get(0).getStatus().getTitle());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }*/
        try {
            statusText = new ObservableField<>(event.getStatus().getTitle());
        } catch (Exception e) {
            e.printStackTrace();
        }
        status = new ObservableField<>(event.getStatusid());
        /*if (status != null)
            switch (status.get()) {
                case 0:
                    statusText = new ObservableField<>("NEW");
                    break;
                case 1:
                    statusText = new ObservableField<>("ONGOING");
                    break;
                case 2:
                    statusText = new ObservableField<>("COMPLETED");
                    break;
            }*/


    }

    public void onItemClick() {
        mListener.onItemClick(mEvent.getEventid());
    }

    public void onAcceptButtonClick() {
        isAcceptRejectVisible = new ObservableField<>(false);
        if (mFrom == 0)
            mListener.onAcceptButtonClick(mEvent.getEventid(), mEvent.getWorkOrders().get(0).getWorkorderid(), DataManager.Status.ASSIGN.getStatus(), 0, 0);
        else if (mFrom == 1)
            mListener.onAcceptButtonClick(mEvent.getEventid(), mEvent.getWorkOrders().get(0).getWorkorderid(), mEvent.getWorkOrders().get(0).getStatusid(), mEvent.getWorkOrders().get(0).getJobs().get(0).getJobid(), DataManager.Status.ACCEPTED.getStatus());
    }

    public void onRejectButtonClick() {
        isAcceptRejectVisible = new ObservableField<>(false);
        if (mFrom == 0)
            mListener.onRejectButtonClick(mEvent.getEventid(), mEvent.getWorkOrders().get(0).getWorkorderid(), DataManager.Status.REJECTED.getStatus(), 0, 0);
        else if (mFrom == 1)
            mListener.onRejectButtonClick(mEvent.getEventid(), mEvent.getWorkOrders().get(0).getWorkorderid(), mEvent.getWorkOrders().get(0).getStatusid(), mEvent.getWorkOrders().get(0).getJobs().get(0).getJobid(), DataManager.Status.REJECTED.getStatus());
    }

    public void onEstimateButtonClick() {
        isEstimateVisible = new ObservableField<>(false);
        if (mFrom == 0)
            mListener.onEstimateButtonClick(mEvent.getEventid(), mEvent.getWorkOrders().get(0).getWorkorderid(), DataManager.Status.REJECTED.getStatus(), 0, 0);
        else if (mFrom == 1)
            mListener.onEstimateButtonClick(mEvent.getEventid(), mEvent.getWorkOrders().get(0).getWorkorderid(), mEvent.getWorkOrders().get(0).getStatusid(), mEvent.getWorkOrders().get(0).getJobs().get(0).getJobid(), DataManager.Status.REJECTED.getStatus());
    }

    public void onEnrouteButtonClick() {
        isEnrouteVisible = new ObservableField<>(false);
        if (mFrom == 0)
            mListener.onEnrouteButtonClick(mEvent.getEventid(), mEvent.getWorkOrders().get(0).getWorkorderid(), DataManager.Status.ASSIGN.getStatus(), 0, 0);
        else if (mFrom == 1)
            mListener.onEnrouteButtonClick(mEvent.getEventid(), mEvent.getWorkOrders().get(0).getWorkorderid(), mEvent.getWorkOrders().get(0).getStatusid(), mEvent.getWorkOrders().get(0).getJobs().get(0).getJobid(), DataManager.Status.ACCEPTED.getStatus());
    }

    public void onConfirmBreakReportButtonClick() {
        isConfirmBreakdownVisible = new ObservableField<>(false);
        if (mFrom == 0)
            mListener.onConfirmBreakReportButtonClick(mEvent.getEventid(), mEvent.getWorkOrders().get(0).getWorkorderid(), DataManager.Status.REJECTED.getStatus(), 0, 0, mEvent.getEventType().getTitle(), mEvent.getBreakdownnote());
        else if (mFrom == 1)
            mListener.onConfirmBreakReportButtonClick(mEvent.getEventid(), mEvent.getWorkOrders().get(0).getWorkorderid(), mEvent.getWorkOrders().get(0).getStatusid(), mEvent.getWorkOrders().get(0).getJobs().get(0).getJobid(), DataManager.Status.REJECTED.getStatus(), mEvent.getEventType().getTitle(), mEvent.getBreakdownnote());
    }

    public void onAdditionalJobButtonClick() {
//        isAdditionalJobVisible = new ObservableField<>(false);
        if (mFrom == 0)
            mListener.onAdditionalJobButtonClick(mEvent.getEventid(), mEvent.getWorkOrders().get(0).getWorkorderid(), DataManager.Status.ASSIGN.getStatus(), 0, 0);
        else if (mFrom == 1)
            mListener.onAdditionalJobButtonClick(mEvent.getEventid(), mEvent.getWorkOrders().get(0).getWorkorderid(), mEvent.getWorkOrders().get(0).getStatusid(), mEvent.getWorkOrders().get(0).getJobs().get(0).getJobid(), DataManager.Status.ACCEPTED.getStatus());
    }

    public void onCompleteJobButtonClick() {
//        isCompleteJobVisible = new ObservableField<>(false);
        if (mFrom == 0)
            mListener.onCompleteJobButtonClick(mEvent.getEventid(), mEvent.getWorkOrders().get(0).getWorkorderid(), DataManager.Status.REJECTED.getStatus(), 0, 0);
        else if (mFrom == 1)
            mListener.onCompleteJobButtonClick(mEvent.getEventid(), mEvent.getWorkOrders().get(0).getWorkorderid(), mEvent.getWorkOrders().get(0).getStatusid(), mEvent.getWorkOrders().get(0).getJobs().get(0).getJobid(), 9);
    }

    public interface ItemViewModelListener {

        void onItemClick(int id);

        void onAcceptButtonClick(int eventId, int workOrderId, int workOrderStatus, int jobId, int jobStatusId);

        void onRejectButtonClick(int eventId, int workOrderId, int workOrderStatus, int jobId, int jobStatusId);

        void onEstimateButtonClick(int eventId, int workOrderId, int workOrderStatus, int jobId, int jobStatusId);

        void onEnrouteButtonClick(int eventId, int workOrderId, int workOrderStatus, int jobId, int jobStatusId);

        void onConfirmBreakReportButtonClick(int eventId, int workOrderId, int workOrderStatus, int jobId, int jobStatusId, String eventType, String breakdown);

        void onAdditionalJobButtonClick(int eventId, int workOrderId, int workOrderStatus, int jobId, int jobStatusId);

        void onCompleteJobButtonClick(int eventId, int workOrderId, int workOrderStatus, int jobId, int jobStatusId);
    }

    @BindingAdapter({"characterBackground"})
    public static void characterBackground(View view, int status) {
        switch (status) {
            case 1:
                view.setBackgroundColor(ContextCompat.getColor(view.getContext(), android.R.color.holo_orange_dark));
                break;
            case 2:
                view.setBackgroundColor(ContextCompat.getColor(view.getContext(), android.R.color.holo_green_dark));
                break;
            case 3:
            case 4:
                view.setBackgroundColor(ContextCompat.getColor(view.getContext(), android.R.color.holo_blue_dark));
                break;
            case 5:
                view.setBackgroundColor(ContextCompat.getColor(view.getContext(), android.R.color.darker_gray));
                break;
            default:
                view.setBackgroundColor(ContextCompat.getColor(view.getContext(), android.R.color.holo_orange_dark));
                break;
        }
    }

    @BindingAdapter({"serviceType"})
    public static void serviceType(View view, JobRosterItemViewModel serviceRequestIemViewModel) {
        /*if (serviceRequestIemViewModel.serviceType.get().equalsIgnoreCase("Drop off & Pick up")) {
            ((TextView) view).setText("Service Type: " + serviceRequestIemViewModel.serviceType.get());
        } else {
            ((TextView) view).setText(serviceRequestIemViewModel.serviceIssue.get());
        }*/
        try {
            ((TextView) view).setText(serviceRequestIemViewModel.serviceIssue.get());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @BindingAdapter({"text"})
    public static void arrivalRepairInfo(View view, JobRosterItemViewModel serviceRequestIemViewModel) {
        StringBuilder info = new StringBuilder();
       /* if (serviceRequestIemViewModel.serviceType.get().equalsIgnoreCase("Drop off & Pick up")) {
            if (!TextUtils.isEmpty(serviceRequestIemViewModel.dropoff.get())) {
                info.append("Drop off:" + serviceRequestIemViewModel.dropoff.get() + " | ");
            }
            if (!TextUtils.isEmpty(serviceRequestIemViewModel.pickup.get())) {
                info.append("Pick up:" + serviceRequestIemViewModel.pickup.get() + " | ");
            }
        } else*/
        {
            if (!TextUtils.isEmpty(serviceRequestIemViewModel.RTA.get())) {
                info.append("RTA: " + serviceRequestIemViewModel.RTA.get() + " | ");
            }
            if (!TextUtils.isEmpty(serviceRequestIemViewModel.ATA.get())) {
                info.append("ATA:" + serviceRequestIemViewModel.ATA.get() + " | ");
            }
            if (!TextUtils.isEmpty(serviceRequestIemViewModel.RTC.get())) {
                info.append("RTC: " + serviceRequestIemViewModel.RTC.get() + " | ");
            }
            if (!TextUtils.isEmpty(serviceRequestIemViewModel.ATC.get())) {
                info.append("ATC:" + serviceRequestIemViewModel.ATC.get() + " | ");
            }
        }
        if (info.length() > 2)
            info.delete(info.length() - 2, info.length() - 1);

        // Initialize a new SpannableStringBuilder instance
        SpannableStringBuilder ssBuilder = new SpannableStringBuilder(info);

        // Apply the bold text style span
        if (info.toString().contains("RTA"))
            ssBuilder.setSpan(
                    new StyleSpan(Typeface.BOLD), // Span to add
                    info.indexOf("RTA:"), // Start of the span (inclusive)
                    info.indexOf("RTA:") + String.valueOf("RTA:").length(), // End of the span (exclusive)
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE // Do not extend the span when text add later
            );

        // Apply the bold text style span
        if (info.toString().contains("ATA"))
            ssBuilder.setSpan(
                    new StyleSpan(Typeface.BOLD), // Span to add
                    info.indexOf("ATA:"), // Start of the span (inclusive)
                    info.indexOf("ATA:") + String.valueOf("ATA:").length(), // End of the span (exclusive)
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE // Do not extend the span when text add later
            );

        // Apply the bold text style span
        if (info.toString().contains("RTC"))
            ssBuilder.setSpan(
                    new StyleSpan(Typeface.BOLD), // Span to add
                    info.indexOf("RTC:"), // Start of the span (inclusive)
                    info.indexOf("RTC:") + String.valueOf("RTC:").length(), // End of the span (exclusive)
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE // Do not extend the span when text add later
            );
        // Apply the bold text style span
        if (info.toString().contains("ATC"))
            ssBuilder.setSpan(
                    new StyleSpan(Typeface.BOLD), // Span to add
                    info.indexOf("ATC:"), // Start of the span (inclusive)
                    info.indexOf("ATC:") + String.valueOf("ATC:").length(), // End of the span (exclusive)
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE // Do not extend the span when text add later
            );

        ((TextView) view).setText(ssBuilder);

    }

    @BindingAdapter({"arrowColor"})
    public static void arrowColor(View view, int status) {
        ImageView imageView = (ImageView) view;
        switch (status) {
            case 1:
                imageView.setColorFilter(ContextCompat.getColor(view.getContext(), android.R.color.holo_orange_dark));
                break;
            case 2:
                imageView.setColorFilter(ContextCompat.getColor(view.getContext(), android.R.color.holo_green_dark));
                break;
            case 3:
            case 4:
                imageView.setColorFilter(ContextCompat.getColor(view.getContext(), android.R.color.holo_blue_dark));
                break;
            case 5:
                imageView.setColorFilter(ContextCompat.getColor(view.getContext(), android.R.color.darker_gray));
                break;
            default:
                imageView.setColorFilter(ContextCompat.getColor(view.getContext(), android.R.color.holo_orange_dark));
                break;
        }
    }

    @BindingAdapter({"titleTextColor"})
    public static void titleTextColor(View view, int status) {
        TextView textView = (TextView) view;
        switch (status) {
            case 1:
                textView.setTextColor(ContextCompat.getColor(view.getContext(), android.R.color.holo_orange_dark));
                break;
            case 2:
                textView.setTextColor(ContextCompat.getColor(view.getContext(), android.R.color.holo_green_dark));
                break;
            case 3:
            case 4:
                textView.setTextColor(ContextCompat.getColor(view.getContext(), android.R.color.holo_blue_dark));
                break;
            case 5:
                textView.setTextColor(ContextCompat.getColor(view.getContext(), android.R.color.darker_gray));
                break;
            default:
                textView.setTextColor(ContextCompat.getColor(view.getContext(), android.R.color.holo_orange_dark));
                break;
        }
    }

    @BindingAdapter({"statusBackgroundColor"})
    public static void statusBackgroundColor(View view, int status) {
        TextView textView = (TextView) view;
        switch (status) {
            case 1:
                textView.setBackgroundTintList(view.getContext().getResources().getColorStateList(android.R.color.holo_orange_dark));
//                ((TextView) view).setText(R.string.new_job);
                break;
            case 2:
                textView.setBackgroundTintList(view.getContext().getResources().getColorStateList(android.R.color.holo_green_dark));
//                ((TextView) view).setText(R.string.ongoing);
                break;
            case 3:
            case 4:
                textView.setBackgroundTintList(view.getContext().getResources().getColorStateList(android.R.color.holo_blue_dark));
//                ((TextView) view).setText(R.string.completed);
                break;
            case 5:
                textView.setBackgroundTintList(view.getContext().getResources().getColorStateList(android.R.color.darker_gray));
//                ((TextView) view).setText(R.string.completed);
                break;
            default:
                textView.setBackgroundTintList(view.getContext().getResources().getColorStateList(android.R.color.holo_orange_dark));
//                ((TextView) view).setText(R.string.new_job);
                break;
        }
    }

    @BindingAdapter({"serviceStatus"})
    public static void serviceType(View view, int status) {
        /*Drawable img = view.getContext().getResources().getDrawable(R.drawable.circle);
        switch (status) {
            case 0:
                img.setTint(ContextCompat.getColor(view.getContext(), android.R.color.holo_orange_dark));
                ((TextView) view).setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                ((TextView) view).setText(R.string.new_job);
                ((TextView) view).setTextColor(ContextCompat.getColor(view.getContext(), android.R.color.holo_orange_dark));
                break;
            case 1:
                img.setTint(ContextCompat.getColor(view.getContext(), android.R.color.holo_green_light));
                ((TextView) view).setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                ((TextView) view).setText(R.string.ongoing);
                ((TextView) view).setTextColor(ContextCompat.getColor(view.getContext(), android.R.color.holo_green_dark));
                break;
            case 2:
                img = view.getContext().getResources().getDrawable(R.drawable.ic_done_all);
                img.setTint(ContextCompat.getColor(view.getContext(), android.R.color.holo_blue_dark));
                ((TextView) view).setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                ((TextView) view).setText(R.string.completed);
                ((TextView) view).setTextColor(ContextCompat.getColor(view.getContext(), android.R.color.holo_blue_dark));
                break;
        }*/
    }

}
