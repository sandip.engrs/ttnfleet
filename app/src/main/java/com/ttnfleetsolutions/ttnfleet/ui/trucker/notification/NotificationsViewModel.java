package com.ttnfleetsolutions.ttnfleet.ui.trucker.notification;

import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseViewModel;
import com.ttnfleetsolutions.ttnfleet.utils.rx.SchedulerProvider;

/**
 * Created by Sandip Chaulagain on 2/2/2018.
 *
 * @version 1.0
 */

public class NotificationsViewModel extends BaseViewModel<NotificationsNavigator> {

    public NotificationsViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }
}
