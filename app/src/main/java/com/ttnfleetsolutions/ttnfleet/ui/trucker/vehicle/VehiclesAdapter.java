package com.ttnfleetsolutions.ttnfleet.ui.trucker.vehicle;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.Vehicle;
import com.ttnfleetsolutions.ttnfleet.databinding.RowVehicleBinding;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseViewHolder;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */

public class VehiclesAdapter extends RecyclerView.Adapter<BaseViewHolder>  {

    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private List<Vehicle> mList;

    private VehicleItemViewModel.VehicleItemViewModelListener mListener;

    public void setListener(VehicleItemViewModel.VehicleItemViewModelListener listener) {
        this.mListener = listener;
    }

    public VehiclesAdapter(List<Vehicle> list) {
        this.mList = list;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RowVehicleBinding rowVehicleBinding = RowVehicleBinding.inflate(LayoutInflater.from(parent.getContext()),
                parent, false);
        return new MyViewHolder(rowVehicleBinding);
    }

    @Override
    public int getItemViewType(int position) {
        if (mList != null && mList.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public int getItemCount() {
        if (mList != null && mList.size() > 0) {
            return mList.size();
        } else {
            return 0;
        }
    }

    public void addItems(List<Vehicle> list) {
        mList = list;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends BaseViewHolder implements VehicleItemViewModel.VehicleItemViewModelListener {

        private RowVehicleBinding mBinding;

        private VehicleItemViewModel mVehicleViewModel;

        public MyViewHolder(RowVehicleBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {

            final Vehicle vehicle = mList.get(position);

            mVehicleViewModel = new VehicleItemViewModel(vehicle, this);

            mBinding.setViewModel(mVehicleViewModel);

            // Immediate Binding
            // When a variable or observable changes, the binding will be scheduled to change before
            // the next frame. There are times, however, when binding must be executed immediately.
            // To force execution, use the executePendingBindings() method.
            mBinding.executePendingBindings();

        }

        @Override
        public void onItemClick(long id) {
            mListener.onItemClick(id);
        }
    }
}
