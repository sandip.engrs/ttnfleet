package com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.locationtypes;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.LocationType;
import com.ttnfleetsolutions.ttnfleet.data.model.api.location.Location;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseActivity;
import com.ttnfleetsolutions.ttnfleet.ui.fleet.DashboardActivity;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.MainActivityNew;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.SelectLocationActivity;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */

public class SelectLocationTypeDialogFragment extends DialogFragment implements View.OnClickListener {

    private View rootView;
    private int mEventType;
    private int mColorPrimary, mColorPrimaryDark;
    private List<LocationType> mLocationTypeList;
    private TextView mName;
    private ImageView mIcon;
    private RecyclerView mRecyclerView;
    private static int REQUEST_LOCATION = 1001;
    private int mLocationTypeId;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.dialog_fragment_location_types, container, false);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        //to hide keyboard when showing dialog fragment
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        mName = rootView.findViewById(R.id.mainServiceName);
        mIcon = rootView.findViewById(R.id.mainServiceIcon);
        mRecyclerView = rootView.findViewById(R.id.locationsTypeRecyclerView);
        rootView.findViewById(R.id.closeBtn).setOnClickListener(v -> dismiss());

        if (getArguments() != null) {

            mColorPrimary = getArguments().getInt("colorPrimary");
            mColorPrimaryDark = getArguments().getInt("colorPrimaryDark");
            rootView.findViewById(R.id.topBgImg1).setBackgroundTintList(ColorStateList.valueOf(mColorPrimary));
            rootView.findViewById(R.id.mainServiceIconLayout1).setBackgroundTintList(ColorStateList.valueOf(mColorPrimary));
            rootView.findViewById(R.id.bottomBgImg1).setBackgroundTintList(ColorStateList.valueOf(mColorPrimary));

            mName.setTextColor(mColorPrimary);

            mLocationTypeList = (List<LocationType>) getArguments().get("locationTypeList");

            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            mRecyclerView.setAdapter(new LocationTypeAdapter(mLocationTypeList, mOnClickListener, mColorPrimary));


            mEventType = (int) getArguments().get("eventType");
            switch (mEventType) {
                case 1:
                    mName.setText(getString(R.string.scheduled_maintenance));
                    mIcon.setImageResource(R.mipmap.popup_icon_scheduled);
                    break;
                case 2:
                    mName.setText(getString(R.string.emergency_repair));
                    mIcon.setImageResource(R.mipmap.popup_icon_road_service);
                    break;
                case 3:
                    mName.setText(getString(R.string.type_accident));
                    mIcon.setImageResource(R.mipmap.popup_icon_acident);
                    break;
                case 4:
                    mName.setText(getString(R.string.type_towing));
                    mIcon.setImageResource(R.mipmap.popup_icon_towing);
                    break;
            }
        }
        return rootView;
    }

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int position = (int) v.getTag();

            if (mLocationTypeList.get(position).getLocations() != null && mLocationTypeList.get(position).getLocations().size() > 0) {
                dismiss();
                SelectLocationDialogFragment fragment = new SelectLocationDialogFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("eventType", mEventType);
                bundle.putString("locationType", mLocationTypeList.get(position).getTitle());
                bundle.putInt("locationTypeId", mLocationTypeList.get(position).getLocationtypeid());
                bundle.putInt("alwaysLive", mLocationTypeList.get(position).getAlwayslive());
                bundle.putInt("colorPrimary", mColorPrimary);
                bundle.putInt("colorPrimaryDark", mColorPrimaryDark);
                bundle.putSerializable("locationList", (Serializable) mLocationTypeList.get(position).getLocations());
                fragment.setArguments(bundle);
                fragment.show(Objects.requireNonNull(getActivity()).getSupportFragmentManager(), SelectLocationTypeDialogFragment.class.getSimpleName());
            } else {
                Intent intent = new Intent(getActivity(), SelectLocationActivity.class);
                intent.putExtra("colorPrimary", mColorPrimary);
                intent.putExtra("colorPrimaryDark", mColorPrimaryDark);
                intent.putExtra("locationTypeId", mLocationTypeList.get(position).getLocationtypeid());
                intent.putExtra("alwaysLive", mLocationTypeList.get(position).getAlwayslive());
                startActivityForResult(intent, REQUEST_LOCATION);
            }

        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 1001:
                if (data != null && data.getExtras() != null) {
                   /* // Get the location passed to this service through an extra.
                    Location mDefaultLocation = data.getParcelableExtra(AppConstants.LOCATION_DATA_EXTRA);
                    // Display the address string or an error message sent from the intent service.
                    String mAddressOutput = data.getStringExtra(AppConstants.RESULT_DATA_KEY);*/
                    Location selectedLocation = (Location) data.getExtras().get("location");

                    dismiss();
                    if (getActivity() instanceof MainActivityNew)
                        ((MainActivityNew) getActivity()).showForm(selectedLocation);
                    else if (getActivity() instanceof DashboardActivity)
                        ((DashboardActivity) getActivity()).showForm(selectedLocation);
                } else {
                    dismiss();
                }
                break;
        }
    }

    @Override
    public void onClick(View view) {
        dismiss();
    }

    @Override
    public void onResume() {
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        if (((BaseActivity) getActivity()).isLandscape())
            params.width = (int) getResources().getDimension(R.dimen.dialog_min_width);
        else
            params.width = ViewGroup.LayoutParams.MATCH_PARENT;

        params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes((WindowManager.LayoutParams) params);
        super.onResume();
    }
}
