package com.ttnfleetsolutions.ttnfleet.ui.trucker.dashboard;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.ttnfleetsolutions.ttnfleet.data.model.api.event.listing.Event;
import com.ttnfleetsolutions.ttnfleet.databinding.RowEmptyViewBinding;
import com.ttnfleetsolutions.ttnfleet.databinding.RowServiceRequestNewBinding;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseViewHolder;
import com.ttnfleetsolutions.ttnfleet.ui.empty.EmptyItem;
import com.ttnfleetsolutions.ttnfleet.ui.empty.EmptyItemViewModel;
import com.ttnfleetsolutions.ttnfleet.ui.empty.EmptyViewHolder;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */

public class OngoingServiceAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private List<Event> mList;
    private int mFrom = 0;
    private boolean mIsLandscape = false;

    private ServiceRequestIemViewModel.ServiceRequestItemViewModelListener mListener;
    private EmptyItemViewModel.EmptyItemViewModelListener mEmptyItemViewModelListener;
    private EmptyItem mEmptyItem;

    public void setEmptyItem(EmptyItem mEmptyItem) {
        this.mEmptyItem = mEmptyItem;
    }

    public void setEmptyItemViewModelListener(EmptyItemViewModel.EmptyItemViewModelListener mEmptyItemViewModelListener) {
        this.mEmptyItemViewModelListener = mEmptyItemViewModelListener;
    }

    public void setListener(ServiceRequestIemViewModel.ServiceRequestItemViewModelListener listener) {
        this.mListener = listener;
    }

    public OngoingServiceAdapter(List<Event> list) {
        this.mList = list;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                RowServiceRequestNewBinding repairViewBinding = RowServiceRequestNewBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
                return new MyViewHolder(repairViewBinding);
            case VIEW_TYPE_EMPTY:
            default:
                RowEmptyViewBinding emptyViewBinding = RowEmptyViewBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
                return new EmptyViewHolder(emptyViewBinding, mEmptyItem, mEmptyItemViewModelListener);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mList != null && mList.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public int getItemCount() {
        if (mList != null && mList.size() > 0) {
            return mList.size();
        } else {
            return 1;
        }
    }

    public void addItems(List<Event> list) {
        mList = list;
        notifyDataSetChanged();
    }

    public int getFrom() {
        return mFrom;
    }

    public void setFrom(int mFrom) {
        this.mFrom = mFrom;
    }

    public boolean isIsLandscape() {
        return mIsLandscape;
    }

    public void setIsLandscape(boolean mIsLandscape) {
        this.mIsLandscape = mIsLandscape;
    }

    public class MyViewHolder extends BaseViewHolder implements ServiceRequestIemViewModel.ServiceRequestItemViewModelListener {

        private RowServiceRequestNewBinding mBinding;

        private ServiceRequestIemViewModel mServiceRequestViewModel;

        public MyViewHolder(RowServiceRequestNewBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {

            final Event event = mList.get(position);
            mServiceRequestViewModel = new ServiceRequestIemViewModel(event, this, getFrom(), isIsLandscape());

            mBinding.setViewModel(mServiceRequestViewModel);
          /*  switch (position) {
                case 0:
                    mBinding.approvalStatus.setText("Approved by you");
//                    mBinding.approvalStatus.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimaryDark));
//                    mBinding.view.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorPrimaryDark));
                    mBinding.threshold.setText("");
                    break;
                case 1:
                    mBinding.approvalStatus.setText("Need your approval");
//                    mBinding.approvalStatus.setTextColor(ContextCompat.getColor(mContext, android.R.color.holo_orange_dark));
//                    mBinding.view.setBackgroundColor(ContextCompat.getColor(mContext, android.R.color.holo_orange_dark));
                    mBinding.threshold.setText("(Threshold exceeded)");
                    break;
                case 2:
                case 4:
                    mBinding.approvalStatus.setText("Fix this later");
//                    mBinding.approvalStatus.setTextColor(ContextCompat.getColor(mContext, android.R.color.darker_gray));
//                    mBinding.view.setBackgroundColor(ContextCompat.getColor(mContext, android.R.color.darker_gray));
                    mBinding.threshold.setText("");

                    break;
            }*/
            // Immediate Binding
            // When a variable or observable changes, the binding will be scheduled to change before
            // the next frame. There are times, however, when binding must be executed immediately.
            // To force execution, use the executePendingBindings() method.
            mBinding.executePendingBindings();

        }

        @Override
        public void onItemClick(int id) {
            mListener.onItemClick(id);
        }

        @Override
        public void onDispatchClick(int id) {
            mListener.onDispatchClick(id);
        }

        @Override
        public void onAddNoteClick(int id, String note) {
            mListener.onAddNoteClick(id, note);
        }
    }
}
