package com.ttnfleetsolutions.ttnfleet.ui.trucker.dashboard;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Sandip Chaulagain on 11/10/2017.
 *
 * @version 1.0
 */
@Module
public abstract class OngoingServicesFragmentProvider {

    @ContributesAndroidInjector(modules = OngoingServicesFragmentModule.class)
    abstract OngoingServiceFragment provideOngoingServiceFragmentFactory();
}
