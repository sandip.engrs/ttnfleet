package com.ttnfleetsolutions.ttnfleet.ui.empty;

import com.ttnfleetsolutions.ttnfleet.databinding.RowEmptyViewBinding;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseViewHolder;

/**
 * Created by Sandip Chaulagain on 6/27/2018.
 *
 * @version 1.0
 */
public class EmptyViewHolder extends BaseViewHolder implements EmptyItemViewModel.EmptyItemViewModelListener {

    private RowEmptyViewBinding mBinding;
    private EmptyItemViewModel.EmptyItemViewModelListener mListener;
    private EmptyItem mEmptyItem;

    public EmptyViewHolder(RowEmptyViewBinding binding) {
        super(binding.getRoot());
        this.mBinding = binding;
    }

    public EmptyViewHolder(RowEmptyViewBinding binding, EmptyItem emptyItem, EmptyItemViewModel.EmptyItemViewModelListener listener) {
        super(binding.getRoot());
        this.mBinding = binding;
        this.mListener = listener;
        this.mEmptyItem = emptyItem;
    }

    @Override
    public void onBind(int position) {
        EmptyItemViewModel emptyItemViewModel = new EmptyItemViewModel(mEmptyItem, this);
        mBinding.setViewModel(emptyItemViewModel);
    }

    @Override
    public void onRetryClick() {
        mListener.onRetryClick();
    }
}
