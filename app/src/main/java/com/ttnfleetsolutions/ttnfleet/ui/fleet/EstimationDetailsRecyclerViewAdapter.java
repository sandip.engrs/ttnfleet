package com.ttnfleetsolutions.ttnfleet.ui.fleet;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ttnfleetsolutions.ttnfleet.R;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */

public class EstimationDetailsRecyclerViewAdapter extends RecyclerView.Adapter<EstimationDetailsRecyclerViewAdapter.MyViewHolder> {

    private Context mContext;

    public EstimationDetailsRecyclerViewAdapter(Context context) {
        this.mContext = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_detailed_estimation_by_service_provider, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        switch (position) {
            case 0:
                holder.name.setText("A PM - Inspection &amp; Lubrication Dry(142)");
                holder.estimation.setText("Time: 1.30 Hours  |  Cost: $1200");
                break;
            case 1:
                holder.name.setText("N PM - Sparks Plugs &amp; Wires(186)");
                holder.estimation.setText("Time: 2 Hours  |  Cost: $250");
                break;
            case 2:
                holder.name.setText("G PM - Gasoline Filter(182)");
                holder.estimation.setText("Time: 1 Hour  |  Cost: $150");
                break;
        }
    }

    @Override
    public int getItemCount() {
        return 3;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, estimation;

        public MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.serviceName);
            estimation = view.findViewById(R.id.serviceDispatcherEstimatation);
        }
    }
}
