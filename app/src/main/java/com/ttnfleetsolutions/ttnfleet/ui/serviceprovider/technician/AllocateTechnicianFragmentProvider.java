package com.ttnfleetsolutions.ttnfleet.ui.serviceprovider.technician;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Sandip Chaulagain on 1/22/2018.
 *
 * @version 1.0
 */
@Module
public abstract class AllocateTechnicianFragmentProvider {

    @ContributesAndroidInjector(modules = AllocateTechnicianFragmentModule.class)
    abstract AllocateTechnicianFragment provideAllocateTechnicianFragmentFactory();
}
