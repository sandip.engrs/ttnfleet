package com.ttnfleetsolutions.ttnfleet.ui.login;

import android.databinding.Observable;
import android.databinding.ObservableField;
import android.databinding.PropertyChangeRegistry;
import android.graphics.Color;
import android.support.v4.graphics.ColorUtils;
import android.text.TextUtils;
import android.util.Log;

import com.ttnfleetsolutions.ttnfleet.BR;
import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseViewModel;
import com.ttnfleetsolutions.ttnfleet.ui.empty.EmptyItemViewModel;
import com.ttnfleetsolutions.ttnfleet.utils.ColorUtil;
import com.ttnfleetsolutions.ttnfleet.utils.CommonUtils;
import com.ttnfleetsolutions.ttnfleet.utils.rx.SchedulerProvider;

import java.util.Random;

/**
 * Created by Sandip Chaulagain on 10/4/2017.
 *
 * @version 1.0
 */

public class LoginViewModel extends BaseViewModel<LoginNavigator> implements Observable {

    public LoginViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    private PropertyChangeRegistry registry = new PropertyChangeRegistry();

    public void onServerLoginClick() {
        getNavigator().login();
    }

    public boolean isEmailAndPasswordValid(String email, String password) {
        //validate email and password
        if (email == null || email.isEmpty()) {
            return false;
        }
        return CommonUtils.isEmailValid(email) && !(password == null || password.isEmpty());
    }

    public void login(String email, String password, int selectedRole) {
        getDataManager().setCurrentUserEmail(email);
        getDataManager().setCurrentUserPassword(password);
        getDataManager().setCurrentUserLoggedInMode(DataManager.LoggedInMode.LOGGED_IN_MODE_SERVER);
//        getDataManager().setCurrentUserRole(DataManager.Role.values()[selectedRole]);
        /*try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
        int count = getDataManager().getDriverCount();
        count++;
        if (count > 5)
            count = 0;
        getDataManager().setDriverCount(count);
        switch (selectedRole) {
            case 1:
                getNavigator().openTruckerActivity();
                break;
            case 2:
                getNavigator().openFleetDispatcherActivity();
                break;
            case 3:
                getNavigator().openServiceProviderActivity();
                break;
            case 4:
                getNavigator().openServiceRepActivity();
                break;
        }
    }

    void getAuthentication(String email, String password) {

        setIsLoading(true);

        Log.i("on get auth","true");

        getCompositeDisposable().add(getDataManager().doServerAuthenticationApiCall(getDataManager().getGUID())
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
//                    getDataManager().setAccessToken(response.getResult());
                    getDataManager().setCurrentUserRole(getRole(response.getUserType()));
                    getDataManager().setCurrentUserID(response.getUserId());
                    getDataManager().setAccessToken(response.getToken());
                    getDataManager().setCurrentUserFullName(response.getFullName());
                    getDataManager().setCurrentUserRoleName(response.getUserTypeTitle());
                    getDataManager().setCurrentUserProfileImage(response.getImage());

                    setIsLoading(false);
                    /*String[] array = new String[]{"1", "2", "3", "4"};
                    String randomStr = array[new Random().nextInt(array.length)];

                    */

                    getAppTheme(String.valueOf(response.getCompanyId()), email, password, getDataManager().getCurrentUserRole());

                }, throwable -> {

                    setIsLoading(false);
                    getNavigator().handleError(throwable);

                }));
    }

    private DataManager.Role getRole(int type) {
        switch (type) {
            case 1:
                return DataManager.Role.TRUCKER;
            case 2:
                return DataManager.Role.FLEET_DISPATCHER;
            case 3:
                return DataManager.Role.SERVICE_DISPATCHER;
            case 4:
                return DataManager.Role.SERVICE_REPRESENTATIVE;
        }
        return null;
    }


    void getAccessToken(String email, String password, int selectedRole) {

        Log.i("on get access tokne","true");
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager().getAccessTokenApiCall()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    getDataManager().setAccessToken(response.getResult());
                    setIsLoading(false);
                    String[] array = new String[]{"1", "2", "3", "4"};
                    String randomStr = array[new Random().nextInt(array.length)];
                    getDataManager().setCurrentUserRole(getRole(selectedRole));
                    getDataManager().setCurrentUserID(6);
                    getAppTheme(randomStr, email, password, selectedRole);
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                }));
    }

    private void getAppTheme(String userId, String email, String password, int selectedRole) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager().getThemeApiCall(userId)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    setIsLoading(false);

                    Log.i("theme response",response.toString());

                    if (response.getResult() != null && response.getResult().size() > 0) {

                        if (response.getResult().get(0).getThemeId() != null)
                            getDataManager().setThemeID(response.getResult().get(0).getThemeId());

                        if (!TextUtils.isEmpty(response.getResult().get(0).getLogo()))
                            getDataManager().setThemeLogo(response.getResult().get(0).getLogo());

                        if (!TextUtils.isEmpty(response.getResult().get(0).getBackgroundColor())) {
                            getDataManager().setThemeStatusBarColor(ColorUtil.darken(Color.parseColor(response.getResult().get(0).getBackgroundColor()), 12));
                            getDataManager().setThemeTitleBarColor(Color.parseColor(response.getResult().get(0).getBackgroundColor()));
                            int colorTransparent = ColorUtils.setAlphaComponent(getDataManager().getThemeStatusBarColor(), 50);
                            getDataManager().setThemeTransparentColor(colorTransparent);
                        }

                        if (!TextUtils.isEmpty(response.getResult().get(0).getBackgroundImage()))
                            getDataManager().setThemeBgImage(response.getResult().get(0).getBackgroundImage());

                        if (!TextUtils.isEmpty(response.getResult().get(0).getButtonColor()))
                            getDataManager().setThemeButtonColor(Color.parseColor(response.getResult().get(0).getButtonColor()));

                        if (!TextUtils.isEmpty(response.getResult().get(0).getTextColor()))
                            getDataManager().setThemeTextColor(Color.parseColor(response.getResult().get(0).getTextColor()));

                        if (response.getResult().get(0).getCompanyId() != null)
                            getDataManager().setThemeCompanyID(response.getResult().get(0).getCompanyId());

                        if (!TextUtils.isEmpty(response.getResult().get(0).getCallForLabel()))
                            getDataManager().setCallForHelpLabel(response.getResult().get(0).getCallForLabel());

                        if (response.getResult().get(0).getFooterTextDisplay() != null)
                            getDataManager().setThemeFooterDisplayFlag(response.getResult().get(0).getFooterTextDisplay());

                        if (response.getResult().get(0).getFooterTextColor() != null)
                            getDataManager().setThemeFooterTextColor(Color.parseColor(response.getResult().get(0).getFooterTextColor()));
                    }
                    login(email, password, selectedRole);
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                    login(email, password, selectedRole);
                }));
    }

    @Override
    public void addOnPropertyChangedCallback(OnPropertyChangedCallback callback) {
        registry.add(callback);
    }

    @Override
    public void removeOnPropertyChangedCallback(OnPropertyChangedCallback callback) {
        registry.remove(callback);
    }

    //Empty View
    public ObservableField<EmptyItemViewModel> emptyItemViewModel = new ObservableField<>();

    void setEmptyItemViewModel(ObservableField<EmptyItemViewModel> emptyItemViewModel) {
        this.emptyItemViewModel = emptyItemViewModel;
        registry.notifyChange(this, BR._all);
    }
}
