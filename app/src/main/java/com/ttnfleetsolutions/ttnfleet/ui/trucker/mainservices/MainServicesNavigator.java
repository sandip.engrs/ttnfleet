package com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices;

import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.LocationType;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 1/10/2018.
 *
 * @version 1.0
 */

interface MainServicesNavigator {

    void handleError(Throwable throwable);

    void setLocationTypeList(int type, List<LocationType> locationTypeList);
}
