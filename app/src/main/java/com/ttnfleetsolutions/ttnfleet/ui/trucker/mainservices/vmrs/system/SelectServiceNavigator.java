package com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.vmrs.system;

import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.DriverListResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.ServiceCode;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 1/10/2018.
 *
 * @version 1.0
 */

public interface SelectServiceNavigator {

    void handleError(Throwable throwable);

    void setSystemList(List<ServiceCode> systemList);

    void setDriverList(List<DriverListResponse.Driver> driverList);

    void onNext();

    void handleErrorDriverListing(Throwable throwable);

    /*void setComponentList(int type, List<ServiceCode> componentList);

    void setPositionList(int type, List<ServiceCode> positionList);*/
}
