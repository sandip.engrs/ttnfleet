package com.ttnfleetsolutions.ttnfleet.ui.view;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.utils.AppConstants;

/**
 * Created by Sandip Chaulagain on 6/6/2018.
 *
 * @version 1.0
 */
public class TTNEditText extends AppCompatEditText {

    public TTNEditText(Context context) {
        super(context);
        init();
    }

    public TTNEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TTNEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

   /* public TTNEditText(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }*/

    public void init() {
        SharedPreferences mPrefs = getContext().getSharedPreferences(AppConstants.PREF_NAME, Context.MODE_PRIVATE);
//        getBackground().setColorFilter(getResources().getColor(R.color.orange), PorterDuff.Mode.SRC_IN);
        setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.orange)));
//        ColorStateList colorStateList = ColorStateList.valueOf(getResources().getColor(R.color.orange));
    }


/*
    public TTNEditText(Context context) {
        super(context);
        init();
    }

    public TTNEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TTNEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void init() {
        SharedPreferences mPrefs = getContext().getSharedPreferences(AppConstants.PREF_NAME, Context.MODE_PRIVATE);
        getBackground().setColorFilter(getResources().getColor(R.color.orange), PorterDuff.Mode.SRC_IN);
//        setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.orange)));
        ColorStateList colorStateList = ColorStateList.valueOf(getResources().getColor(R.color.orange));
        setSupportBackgroundTintList(colorStateList);
        *//*ViewCompat.setBackgroundTintList(this, colorStateList);*//*
    }*/
}
