package com.ttnfleetsolutions.ttnfleet.ui.fleet.order;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.MenuItem;

import com.ttnfleetsolutions.ttnfleet.BR;
import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.Repair;
import com.ttnfleetsolutions.ttnfleet.databinding.ActivityOrderDetailsNewBinding;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;
import pl.com.salsoft.sqlitestudioremote.SQLiteStudioService;

public class OrderDetailsActivityNew extends BaseActivity<ActivityOrderDetailsNewBinding, OrderDetailsViewModel> implements HasSupportFragmentInjector {


    @Inject
    OrderDetailsViewModel orderDetailsViewModel;

    @Inject
    OrderDetailsViewPagerAdapter mOrderDetailsViewPagerAdapter;

    @Inject
    DispatchingAndroidInjector<Fragment> fragmentDispatchingAndroidInjector;

    private ActivityOrderDetailsNewBinding mActivityOrderDetailsNewBinding;

    private List<Repair> repairs = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityOrderDetailsNewBinding = getViewDataBinding();
        orderDetailsViewModel.setNavigator(this);

        SQLiteStudioService.instance().start(this);
        SQLiteStudioService.instance().setPort(9999);
        setUp();
    }

    private void setUp() {
        mActivityOrderDetailsNewBinding.toolbar.setTitle("Service Request 1");
        mActivityOrderDetailsNewBinding.toolbar.setSubtitle("ONGOING");

//        mActivityOrderDetailsNewBinding.toolbar.setSubtitleTextColor(Color.WHITE);
//        mActivityOrderDetailsNewBinding.toolbar.setTitleTextColor(Color.WHITE);

        setSupportActionBar(mActivityOrderDetailsNewBinding.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
//            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        /*mActivityOrderDetailsNewBinding.appBar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (Math.abs(verticalOffset) == appBarLayout.getTotalScrollRange()) {
                    // Collapsed
                    mActivityOrderDetailsNewBinding.truckImage.setVisibility(View.INVISIBLE);
                } else if (verticalOffset == 0) {
                    // Expanded
                    mActivityOrderDetailsNewBinding.truckImage.setVisibility(View.VISIBLE);
                } else {
                    // Somewhere in between
                }
            }
        });*/

//        setSupportActionBar(mActivityOrderDetailsNewBinding.baseToolbar);
//        if (getSupportActionBar() != null) {
//            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//            getSupportActionBar().setDisplayShowHomeEnabled(true);
//        }
//        BottomNavigationViewHelper.removeShiftMode(mActivityOrderDetailsNewBinding.navigation);
        mActivityOrderDetailsNewBinding.navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        mActivityOrderDetailsNewBinding.orderDetailsViewPager.setAdapter(mOrderDetailsViewPagerAdapter);
        mActivityOrderDetailsNewBinding.orderDetailsViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        mActivityOrderDetailsNewBinding.navigation.setSelectedItemId(R.id.nav_vehicle_details);
                        break;
                    case 1:
                        mActivityOrderDetailsNewBinding.navigation.setSelectedItemId(R.id.nav_breakdown_report);
                        break;
                    case 2:
                        mActivityOrderDetailsNewBinding.navigation.setSelectedItemId(R.id.nav_status_details);
                        break;
                    case 3:
                        mActivityOrderDetailsNewBinding.navigation.setSelectedItemId(R.id.nav_service_provider_details);
                        break;
                    case 4:
                        mActivityOrderDetailsNewBinding.navigation.setSelectedItemId(R.id.nav_chat);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        repairs.add(new Repair(0, "Renew the brake pads (first axle)", "40 min ago", "Total Time (H) : 1.40", "Cost : $500", 0, "Parts required : 2 (Brake pad set, disc brake)"));
        repairs.add(new Repair(1, "Renew front axle left wheel tyre", "15 min ago", "Total Time (H) : 2.00", "Cost : $1000", 1, "Parts required : 1 (Spare Tyre)"));
        repairs.add(new Repair(2, "Remove/refit the parking brake valve", "5 min ago", "Total Time (H) : 0.50", "Cost : $100", 2, "Parts required : 0"));
        orderDetailsViewModel.insertData(repairs);
    }

    @Override
    public OrderDetailsViewModel getViewModel() {
        return orderDetailsViewModel;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_order_details_new;
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.nav_vehicle_details:
                    mActivityOrderDetailsNewBinding.orderDetailsViewPager.setCurrentItem(0);
                    return true;
                case R.id.nav_breakdown_report:
                    mActivityOrderDetailsNewBinding.orderDetailsViewPager.setCurrentItem(1);
                    return true;
                case R.id.nav_status_details:
                    mActivityOrderDetailsNewBinding.orderDetailsViewPager.setCurrentItem(2);
                    return true;
                case R.id.nav_service_provider_details:
                    mActivityOrderDetailsNewBinding.orderDetailsViewPager.setCurrentItem(3);
                    return true;
                case R.id.nav_chat:
                    mActivityOrderDetailsNewBinding.orderDetailsViewPager.setCurrentItem(4);
                    return true;
            }
            return false;

        }
    };

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentDispatchingAndroidInjector;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //Menu
        switch (item.getItemId()) {
            //When home is clicked
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
