package com.ttnfleetsolutions.ttnfleet.ui.trucker.servicerequest;

import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.databinding.Observable;
import android.databinding.ObservableField;
import android.databinding.PropertyChangeRegistry;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.ttnfleetsolutions.ttnfleet.BR;
import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.listing.Event;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.listing.Job;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.requirements.RequireImagesResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.survey.SurveyRequest;
import com.ttnfleetsolutions.ttnfleet.data.remote.ApiEndPoint;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseViewModel;
import com.ttnfleetsolutions.ttnfleet.ui.empty.EmptyItemViewModel;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.details.CreateEventFragment;
import com.ttnfleetsolutions.ttnfleet.utils.CommonUtils;
import com.ttnfleetsolutions.ttnfleet.utils.rx.SchedulerProvider;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Sandip Chaulagain on 2/2/2018.
 *
 * @version 1.0
 */

public class ServiceDetailsViewModel extends BaseViewModel<ServiceDetailsNavigator> implements Observable {

    private PropertyChangeRegistry registry = new PropertyChangeRegistry();

    public ObservableField<String> status = new ObservableField<>();
    public ObservableField<Integer> statusId = new ObservableField<>();
    public ObservableField<String> updatedTime = new ObservableField<>();

    public ObservableField<String> vehicleName = new ObservableField<>();
    public ObservableField<String> vin = new ObservableField<>();
    public ObservableField<String> address = new ObservableField<>();
    public ObservableField<String> locationType = new ObservableField<>();

    public ObservableField<String> eventType = new ObservableField<>();
    public ObservableField<String> breakdownType = new ObservableField<>();
    public ObservableField<String> loadType = new ObservableField<>();
    public ObservableField<String> RTA = new ObservableField<>();
    public ObservableField<String> RTC = new ObservableField<>();
    public ObservableField<String> note = new ObservableField<>();
    public ObservableField<String> odometerReading = new ObservableField<>();
    public ObservableField<Boolean> isRequirementsLabelVisible = new ObservableField<>();

    private ItemViewModelListener mListener;

    public void setListener(ItemViewModelListener listener) {
        this.mListener = listener;
    }

    public void onAcceptButtonClick() {
        mListener.onAcceptButtonClick(mEventDetails.getEventid(), mEventDetails.getWorkOrders().get(0).getWorkorderid(),
                DataManager.Status.ASSIGN.getStatus(), mEventDetails.getWorkOrders().get(0).getJobs().get(0).getJobid(), DataManager.Status.ACCEPTED.getStatus());
    }

    public void onRejectButtonClick() {
        mListener.onRejectButtonClick(mEventDetails.getEventid(), mEventDetails.getWorkOrders().get(0).getWorkorderid(),
                DataManager.Status.REJECTED.getStatus(), mEventDetails.getWorkOrders().get(0).getJobs().get(0).getJobid(), DataManager.Status.REJECTED.getStatus());
    }

    public void onAdditionalJobButtonClick() {
        mListener.onAdditionalJobButtonClick(mEventDetails.getEventid(), mEventDetails.getWorkOrders().get(0).getWorkorderid(),
                DataManager.Status.REJECTED.getStatus(), mEventDetails.getWorkOrders().get(0).getJobs().get(0).getJobid(), DataManager.Status.REJECTED.getStatus());
    }

    public void onCompleteJobButtonClick() {
        mListener.onCompleteJobButtonClick(mEventDetails.getEventid(), mEventDetails.getWorkOrders().get(0).getWorkorderid(),
                DataManager.Status.REJECTED.getStatus(), mEventDetails.getWorkOrders().get(0).getJobs().get(0).getJobid(), DataManager.Status.REJECTED.getStatus());
    }

    public void onEnrouteButtonClick() {
        mListener.onEnrouteButtonClick(mEventDetails.getEventid(), mEventDetails.getWorkOrders().get(0).getWorkorderid(),
                DataManager.Status.REJECTED.getStatus(), mEventDetails.getWorkOrders().get(0).getJobs().get(0).getJobid(), DataManager.Status.REJECTED.getStatus());
    }

    public void onConfirmBreakDownJobButtonClick() {
        mListener.onConfirmBreakReportButtonClick(mEventDetails.getEventid(), mEventDetails.getWorkOrders().get(0).getWorkorderid(),
                DataManager.Status.REJECTED.getStatus(), mEventDetails.getWorkOrders().get(0).getJobs().get(0).getJobid(),
                DataManager.Status.REJECTED.getStatus(), mEventDetails.getEventType().getTitle(), mEventDetails.getBreakdownnote());
    }

    public void onDispatchEventButtonClick() {
        mListener.onDispatchEventButtonClick(mEventDetails.getEventid());
    }

    public void onAddNoteFDButtonClick() {
        mListener.onAddNoteFDButtonClick(mEventDetails.getEventid(), mEventDetails.getFdnote());
    }

    public void onFeedbackButtonClick() {
        mListener.onFeedbackButtonClick(mEventDetails.getEventid(), mEventDetails.getVehicle().getVehiclename(), mEventDetails.getVehicle().getVin(), mEventDetails.getEventType().getTitle(), mEventDetails.getCreateddatetime());
    }

    public ServiceDetailsViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    private Event mEventDetails;

    Event getEventDetails() {
        return mEventDetails;
    }

    private List<RequireImagesResponse.Image> mRequirementImageList;

    public List<RequireImagesResponse.Image> getRequirementImageList() {
        return mRequirementImageList;
    }

    public void setRequirementImageList(List<RequireImagesResponse.Image> mRequirementImageList) {
        this.mRequirementImageList = mRequirementImageList;
    }

    void setEventDetails(Event mEventDetails) {
        this.mEventDetails = mEventDetails;
        getNavigator().loadEventDetails(mEventDetails);

        if (mEventDetails.getStatus() != null)
            setStatus(new ObservableField<>(mEventDetails.getStatus().getTitle()));

        setStatusId(new ObservableField<>(mEventDetails.getStatusid()));
        setUpdatedTime(new ObservableField<>(CommonUtils.getPrettyTime(mEventDetails.getCreateddatetime())));

        if (mEventDetails.getVehicle() != null) {
            setVehicleName(new ObservableField<>(mEventDetails.getVehicle().getVehiclename()));
            setVin(new ObservableField<>(mEventDetails.getVehicle().getVin()));
        }

        if (mEventDetails.getLocation() != null) {
            setAddress(new ObservableField<>(mEventDetails.getLocation().getAddress()));
            if (mEventDetails.getLocation().getLocationType() != null && !TextUtils.isEmpty(mEventDetails.getLocation().getLocationType().getTitle())) {
                setLocationType(new ObservableField<>(mEventDetails.getLocation().getLocationType().getTitle()));
            }
        }

        setEventType(new ObservableField<>(mEventDetails.getEventType().getTitle()));
        setBreakdownType(new ObservableField<>(mEventDetails.getBreakdownnote()));

        if (mEventDetails.getVehicleLoad() != null)
            setLoadType(new ObservableField<>(mEventDetails.getVehicleLoad().getLoadPlans()));

        setRTA(new ObservableField<>(CommonUtils.getFromattedDate(mEventDetails.getRta())));
        setRTC(new ObservableField<>(CommonUtils.getFromattedDate(mEventDetails.getRtc())));
        setNote(new ObservableField<>(mEventDetails.getFdnote()));
    }

    @Bindable
    public ObservableField<String> getStatus() {
        return status;
    }

    public void setStatus(ObservableField<String> status) {
        this.status = status;
        registry.notifyChange(this, BR.status);
    }

    @Bindable
    public ObservableField<Integer> getStatusId() {
        return statusId;
    }

    public void setStatusId(ObservableField<Integer> statusId) {
        this.statusId = statusId;
        registry.notifyChange(this, BR.statusId);
    }

    @Bindable
    public ObservableField<String> getUpdatedTime() {
        return updatedTime;
    }

    private void setUpdatedTime(ObservableField<String> updatedTime) {
        this.updatedTime = updatedTime;
        registry.notifyChange(this, BR.updatedTime);
    }

    @Bindable
    public ObservableField<String> getEventType() {
        return eventType;
    }

    public void setEventType(ObservableField<String> eventType) {
        this.eventType = eventType;
        registry.notifyChange(this, BR.eventType);
    }

    @Bindable
    public ObservableField<String> getBreakdownType() {
        return breakdownType;
    }

    public void setBreakdownType(ObservableField<String> breakdownType) {
        this.breakdownType = breakdownType;
        registry.notifyChange(this, BR.breakdownType);
    }

    @Bindable
    public ObservableField<String> getLoadType() {
        return loadType;
    }

    private void setLoadType(ObservableField<String> loadType) {
        this.loadType = loadType;
        registry.notifyChange(this, BR.loadType);
    }

    @Bindable
    public ObservableField<String> getVehicleName() {
        return vehicleName;
    }

    public void setVehicleName(ObservableField<String> vehicleName) {
        this.vehicleName = vehicleName;
        registry.notifyChange(this, BR.vehicleName);
    }

    @Bindable
    public ObservableField<String> getVin() {
        return vin;
    }

    public void setVin(ObservableField<String> vin) {
        this.vin = vin;
        registry.notifyChange(this, BR.vin);
    }

    @Bindable
    public ObservableField<String> getAddress() {
        return address;
    }

    public void setAddress(ObservableField<String> address) {
        this.address = address;
        registry.notifyChange(this, BR.address);
    }

    @Bindable
    public ObservableField<String> getLocationType() {
        return locationType;
    }

    public void setLocationType(ObservableField<String> locationType) {
        this.locationType = locationType;
        registry.notifyChange(this, BR.locationType);
    }

    @Bindable
    public ObservableField<String> getRTA() {
        return RTA;
    }

    public void setRTA(ObservableField<String> RTA) {
        this.RTA = RTA;
        registry.notifyChange(this, BR.rTA);
    }

    @Bindable
    public ObservableField<String> getRTC() {
        return RTC;
    }

    public void setRTC(ObservableField<String> RTC) {
        this.RTC = RTC;
        registry.notifyChange(this, BR.rTC);
    }

    @Bindable
    public ObservableField<String> getNote() {
        return note;
    }

    public void setNote(ObservableField<String> note) {
        this.note = note;
        registry.notifyChange(this, BR.note);
    }

    @Bindable
    public ObservableField<String> getOdometerReading() {
        return odometerReading;
    }

    public void setOdometerReading(ObservableField<String> odometerReading) {
        this.odometerReading = odometerReading;
        registry.notifyChange(this, BR.odometerReading);
    }

    @Bindable
    public ObservableField<Boolean> getIsRequirementsLabelVisible() {
        return isRequirementsLabelVisible;
    }

    public void setIsRequirementsLabelVisible(ObservableField<Boolean> isRequirementsLabelVisible) {
        this.isRequirementsLabelVisible = isRequirementsLabelVisible;
        registry.notifyChange(this, BR.isRequirementsLabelVisible);
    }

    @BindingAdapter({"statusBackgroundColor"})
    public static void statusBackgroundColor(View view, int status) {
        TextView textView = (TextView) view;
        switch (status) {
            case 1:
                textView.setBackgroundTintList(view.getContext().getResources().getColorStateList(android.R.color.holo_orange_dark));
                break;
            case 2:
                textView.setBackgroundTintList(view.getContext().getResources().getColorStateList(android.R.color.holo_green_dark));
                break;
            case 3:
            case 4:
                textView.setBackgroundTintList(view.getContext().getResources().getColorStateList(android.R.color.holo_blue_dark));
                break;
            case 5:
                textView.setBackgroundTintList(view.getContext().getResources().getColorStateList(android.R.color.darker_gray));
                break;
            default:
                textView.setBackgroundTintList(view.getContext().getResources().getColorStateList(android.R.color.holo_orange_dark));
                break;
        }
    }

    void getEventDetailsAPICall(int eventId) {
//        getSchedulerProvider().io().createWorker();
        setIsLoading(true);
        getCompositeDisposable().clear();
//        getCompositeDisposable().dispose();
        getCompositeDisposable().add(getDataManager()
                .doServerGetEventDetailsApiCall(eventId)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    sortJobsArray(response.getResult());
                    setIsLoading(false);
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleErrorDetails(throwable);
                }));
    }

    void sortJobsArray(Event event) {
        if (event.getWorkOrders() != null && event.getWorkOrders().size() > 0) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Collections.sort(event.getWorkOrders().get(0).getJobs(), Comparator.comparing(Job::getJobid));
            } else {
                Collections.sort(event.getWorkOrders().get(0).getJobs(), (Job one, Job other) -> one.getJobid().compareTo(other.getJobid()));
            }
        }
        setEventDetails(event);
        try {
            getRequirementImagesAPICall(event.getWorkOrders().get(0).getJobs().get(0).getJobid());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void getRequirementImagesAPICall(int jobId) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .doServerGetRequirementImagesApiCall(jobId)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    if (response != null && response.getResult() != null
                            && ((response.getResult().getImages() != null && response.getResult().getImages().size() > 0)) || !TextUtils.isEmpty(response.getResult().getOdometerReading())) {
                        setIsRequirementsLabelVisible(new ObservableField<>(true));
                        for (RequireImagesResponse.Image result : response.getResult().getImages()) {
                            result.setPhotoname(ApiEndPoint.DIR_IMAGE_REQUIREMENT + getEventDetails().getEventid() + "/" + jobId + "/" + result.getPhotoname());
                        }
                        setRequirementImageList(response.getResult().getImages());
                        setOdometerReading(new ObservableField<>(response.getResult().getOdometerReading()));
                        getNavigator().loadJobRequirementImages(response.getResult().getImages(), mEventDetails.getEventid(), jobId);
                    } else {
                        setIsRequirementsLabelVisible(new ObservableField<>(false));
                    }
                    setIsLoading(false);
                }, throwable -> {
                    setIsRequirementsLabelVisible(new ObservableField<>(false));
                    setIsLoading(false);
                    getNavigator().handleErrorRequirementImages(throwable);
                }));
    }

    void getSurveyQuestionsAPICall(int eventId, String vehicleName, String VIN, String serviceIssue, String datetime) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .doServerGetSurveyQuestionsApiCall()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    getNavigator().loadSurveyQuestions(response, eventId, vehicleName, VIN, serviceIssue, datetime);
                    setIsLoading(false);
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleErrorSurveyQuestions(throwable);
                }));
    }

    private JSONObject getSurveyRequestJson(SurveyRequest surveyRequest) {
        try {
            JSONObject jsonObject = new JSONObject(new Gson().toJson(surveyRequest, SurveyRequest.class));
            JSONObject dataObject = new JSONObject();
            dataObject.put("data", jsonObject);
            Log.e(CreateEventFragment.class.getSimpleName(), "Json -- " + dataObject.toString());
            return dataObject;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    void submitSurveyAPICall(SurveyRequest surveyRequest) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .doServerSubmitSurveyApiCall(getSurveyRequestJson(surveyRequest))
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    getNavigator().onSubmitSurveyResponse(response);
                    setIsLoading(false);
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleErrorSubmitSurvey(throwable);
                }));
    }

    @Override
    public void addOnPropertyChangedCallback(OnPropertyChangedCallback callback) {
        registry.add(callback);
    }

    @Override
    public void removeOnPropertyChangedCallback(OnPropertyChangedCallback callback) {
        registry.remove(callback);
    }

    public interface ItemViewModelListener {

        void onAcceptButtonClick(int eventId, int workOrderId, int workOrderStatus, int jobId, int jobStatusId);

        void onRejectButtonClick(int eventId, int workOrderId, int workOrderStatus, int jobId, int jobStatusId);

        void onEstimateButtonClick(int eventId, int workOrderId, int workOrderStatus, int jobId, int jobStatusId);

        void onEnrouteButtonClick(int eventId, int workOrderId, int workOrderStatus, int jobId, int jobStatusId);

        void onConfirmBreakReportButtonClick(int eventId, int workOrderId, int workOrderStatus, int jobId, int jobStatusId, String eventType, String breakdown);

        void onAdditionalJobButtonClick(int eventId, int workOrderId, int workOrderStatus, int jobId, int jobStatusId);

        void onCompleteJobButtonClick(int eventId, int workOrderId, int workOrderStatus, int jobId, int jobStatusId);

        void onFeedbackButtonClick(int eventId, String vehicleName, String VIN, String serviceIssue, String datetime);

        void onDispatchEventButtonClick(int eventId);

        void onAddNoteFDButtonClick(int eventId, String note);
    }

    //Empty View
    public ObservableField<EmptyItemViewModel> emptyItemViewModel = new ObservableField<>();

    void setEmptyItemViewModel(ObservableField<EmptyItemViewModel> emptyItemViewModel) {
        this.emptyItemViewModel = emptyItemViewModel;
        registry.notifyChange(this, BR._all);
    }
}
