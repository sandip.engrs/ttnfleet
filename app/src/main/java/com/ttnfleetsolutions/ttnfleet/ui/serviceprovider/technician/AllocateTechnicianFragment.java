package com.ttnfleetsolutions.ttnfleet.ui.serviceprovider.technician;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.Toast;

import com.ttnfleetsolutions.ttnfleet.BR;
import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.AcceptWorkOrderResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.technician.Technician;
import com.ttnfleetsolutions.ttnfleet.databinding.FragmentAllocateJobToTechnicianBinding;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.servicerequest.callback.UpdateEventDetailsCallbackSP;
import com.ttnfleetsolutions.ttnfleet.utils.CommonUtils;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Sandip Chaulagain on 10/10/2017.
 *
 * @version 1.0
 */

public class AllocateTechnicianFragment extends BaseFragment<FragmentAllocateJobToTechnicianBinding, AllocateTechnicianViewModel> implements AllocateTechnicianNavigator, AllocateTechnicianItemViewModel.ViewModelListener {

    @Inject
    AllocateTechnicianViewModel mViewModel;

    FragmentAllocateJobToTechnicianBinding mFragmentBinding;

    @Inject
    AllocateTechnicianAdapter mAdapter;

    private int mEventId, mWorkOrderId, mServiceProviderId;

    private boolean mFlag = false;

    @Override
    protected AllocateTechnicianViewModel getViewModel() {
        return mViewModel;
    }

    @Override
    protected int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_allocate_job_to_technician;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModel.setNavigator(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFragmentBinding = getViewDataBinding();
        setUp();
    }

    private void setUp() {
        if (getArguments() != null) {
            mEventId = getArguments().getInt("eventId");
            mWorkOrderId = getArguments().getInt("workOrderId");
            mServiceProviderId = getArguments().getInt("serviceProviderId");
            mFlag = getArguments().getBoolean("flag");
        }
        if (!mFlag) mFragmentBinding.acceptOnly.setVisibility(View.GONE);
        mFragmentBinding.acceptOnly.setBackgroundColor(getViewModel().getDataManager().getThemeTitleBarColor());
        mFragmentBinding.acceptOnly.setOnClickListener(v -> getViewModel().acceptWorkOrdersAPICall(mEventId, getViewModel().getDataManager().getCurrentUserID()));

        mFragmentBinding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mFragmentBinding.recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        mAdapter.setEmptyItemViewModelListener(this::getListing);
        mAdapter.setListener(this);

        if (getViewModel().getTechnicianList() != null && getViewModel().getTechnicianList().size() > 0) {
            getTechnicianList(getViewModel().getTechnicianList());
        } else {
            getListing();
        }
    }

    private void getListing() {
        if (!isNetworkConnected()) {
            mAdapter.setEmptyItem(CommonUtils.getEmptyItem(getActivity(), DataManager.ErrorState.NETWORK_ERROR));
            mFragmentBinding.recyclerView.setAdapter(mAdapter);
        } else {
            getViewModel().getTechnicianListAPICall();
        }
    }

    private UpdateEventDetailsCallbackSP mUpdateEventDetailsCallbackSP;

    public void setUpdateEventDetailsCallbackSP(UpdateEventDetailsCallbackSP mUpdateEventDetailsCallbackSP) {
        this.mUpdateEventDetailsCallbackSP = mUpdateEventDetailsCallbackSP;
    }

    @Override
    public void onItemClick(int id, String name) {
        if (!mFlag) return;
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Assign Job to " + name + "?");
        builder.setPositiveButton("ACCEPT & ASSIGN", (dialogInterface, i) -> getViewModel().acceptAssignWorkOrdersAPICall(mWorkOrderId, true, id));
        builder.setNegativeButton("ASSIGN", (dialogInterface, i) -> getViewModel().acceptAssignWorkOrdersAPICall(mWorkOrderId, false, id));
        builder.show();
    }

    @Override
    public void getTechnicianList(List<Technician> technicianList) {
        if (technicianList != null && technicianList.size() > 0) {
            mAdapter.addItems(technicianList);
            mFragmentBinding.recyclerView.setAdapter(mAdapter);
        } else {
            mAdapter.setEmptyItem(CommonUtils.getEmptyItem(getActivity(), DataManager.ErrorState.NO_DATA));
            mFragmentBinding.recyclerView.setAdapter(mAdapter);
        }
    }

    @Override
    public void handleError(Throwable throwable) {
        mAdapter.setEmptyItem(CommonUtils.getEmptyItem(getActivity(), DataManager.ErrorState.API_FAIL));
        mFragmentBinding.recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void handleErrorOnAccept(Throwable throwable) {
        Toast.makeText(getActivity(), "Failed to accept work orders. Try again!", Toast.LENGTH_LONG).show();
    }

    @Override
    public void handleErrorOnAcceptAssign(Throwable throwable) {
        Toast.makeText(getActivity(), "Failed to accept work orders. Try again!", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onAcceptWorkOrders(AcceptWorkOrderResponse acceptWorkOrderResponse) {
        if (acceptWorkOrderResponse != null) {
            getActivity().onBackPressed();
            mUpdateEventDetailsCallbackSP.onAcceptEvent();
        } else
            Toast.makeText(getActivity(), "Failed to accept work orders. Try again!", Toast.LENGTH_LONG).show();
    }

    public boolean isFlag() {
        return mFlag;
    }
}
