package com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.details;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.google.gson.Gson;
import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.BR;
import com.ttnfleetsolutions.ttnfleet.data.model.api.asset.Vehicle;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.CreateEventResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.EventRequest;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.SystemCode;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.TruckLoadResponse;
import com.ttnfleetsolutions.ttnfleet.databinding.FragmentCreateEventBinding;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseFragment;
import com.ttnfleetsolutions.ttnfleet.ui.fleet.DashboardActivity;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.MainActivityNew;

import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by Sandip Chaulagain on 11/22/2017.
 *
 * @version 1.0
 */

public class CreateEventFragment extends BaseFragment<FragmentCreateEventBinding, CreateEventViewModel> implements CreateEventNavigator, SelectedCodeItemViewModel.ItemViewModelListener {

    @Inject
    CreateEventViewModel mViewModel;

    FragmentCreateEventBinding mFragmentBinding;

    @Inject
    SelectedCodeRecyclerViewAdapter mAdapter;

    private String mRTADateStr, mRTCDateStr;

    @Override
    protected CreateEventViewModel getViewModel() {
        return mViewModel;
    }

    @Override
    protected int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_create_event;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModel.setNavigator(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFragmentBinding = getViewDataBinding();
        setUp();
    }

    private void setUp() {
        getViewModel().setCodeList();
//        mFragmentBinding.repairs.setBackgroundColor(getViewModel().getDataManager().getThemeTransparentColor());
        mFragmentBinding.addRepair.setTextColor(getViewModel().getDataManager().getThemeStatusBarColor());
//        mFragmentBinding.vehicleSpinner.setBackgroundTintList(ColorStateList.valueOf(getViewModel().getDataManager().getThemeTitleBarColor()));

        mFragmentBinding.repairsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mFragmentBinding.repairsRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        mAdapter.setListener(this);

        if (getViewModel().getTruckLoadList() != null && getViewModel().getTruckLoadList().size() > 0) {
            setTruckLoadList(getViewModel().getTruckLoadList());
        } else
            getViewModel().getTruckLoadTypes();

        //Assets
        if (getViewModel().getVehicleList() != null && getViewModel().getVehicleList().size() > 0)
            setAssetList(getViewModel().getVehicleList());
        else if (getActivity() instanceof MainActivityNew)
            getViewModel().getAssetsApiCall(getViewModel().getDataManager().getCurrentUserID());
        else if (getActivity() instanceof DashboardActivity)
            getViewModel().getAssetsApiCall(getViewModel().getDriverId());


        getViewModel().loadLocationDetails();

        mFragmentBinding.addRepair.setOnClickListener(v -> {
            getViewModel().saveDetails(mFragmentBinding.vehicleSpinner.getSelectedItemPosition(), mFragmentBinding.RTA.getText().toString(), mFragmentBinding.RTC.getText().toString(),
                    mFragmentBinding.truckLoadSpinner.getSelectedItemPosition(), mFragmentBinding.breakdownReport.getText().toString(), getActivity()  instanceof MainActivityNew ? true : false);
            if (getActivity() instanceof MainActivityNew)
                ((MainActivityNew) getActivity()).showSystemListing();
            else if (getActivity() instanceof DashboardActivity)
                ((DashboardActivity) getActivity()).showSystemListing();
        });

        mFragmentBinding.RTA.setOnClickListener(v -> {
            Calendar now = Calendar.getInstance();
            new DatePickerDialog(getActivity(), (datePicker, i, i1, i2) -> {
                mRTADateStr = datePicker.getYear() + "-" + String.format("%02d", datePicker.getMonth() + 1) + "-" + String.format("%02d", datePicker.getDayOfMonth());
                new TimePickerDialog(getActivity(), (timePicker, i32, i112) -> {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        mRTADateStr = mRTADateStr + " " + String.format("%02d", timePicker.getHour()) + ":" + String.format("%02d", timePicker.getMinute());
                    } else {
                        mRTADateStr = mRTADateStr + " " + String.format("%02d", timePicker.getCurrentHour()) + ":" + String.format("%02d", timePicker.getCurrentMinute());
                    }
                    mFragmentBinding.RTA.setText(mRTADateStr);
                }, now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE), true).show();
            }, now.get(Calendar.YEAR),
                    now.get(Calendar.MONTH),
                    now.get(Calendar.DAY_OF_MONTH)).show();
        });

        mFragmentBinding.RTC.setOnClickListener(v -> {
            Calendar now = Calendar.getInstance();
            new DatePickerDialog(getActivity(), (datePicker, i, i1, i2) -> {
                mRTCDateStr = datePicker.getYear() + "-" + String.format("%02d", datePicker.getMonth() + 1) + "-" + String.format("%02d", datePicker.getDayOfMonth());
                new TimePickerDialog(getActivity(), (timePicker, i3, i11) -> {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        mRTCDateStr = mRTCDateStr + " " + String.format("%02d", timePicker.getHour()) + ":" + String.format("%02d", timePicker.getMinute());
                    } else {
                        mRTCDateStr = mRTCDateStr + " " + String.format("%02d", timePicker.getCurrentHour()) + ":" + String.format("%02d", timePicker.getCurrentMinute());
                    }
                    mFragmentBinding.RTC.setText(mRTCDateStr);
                }, now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE), true).show();
            }, now.get(Calendar.YEAR),
                    now.get(Calendar.MONTH),
                    now.get(Calendar.DAY_OF_MONTH)).show();
        });
    }

    @Override
    public void loadDetails(int assetId, String address, String RTA, String RTC, int loadId, String comments) {
        mFragmentBinding.vehicleSpinner.setSelection(assetId);
        mFragmentBinding.selectedLocation.setText(address);
        mFragmentBinding.RTA.setText(RTA);
        mFragmentBinding.RTC.setText(RTC);
        mFragmentBinding.truckLoadSpinner.setSelection(loadId);
        mFragmentBinding.breakdownReport.setText(comments);
    }

    @Override
    public void loadLocationDetails(String address) {
        mFragmentBinding.selectedLocation.setText(address);
    }

    @Override
    public void setCodeList(List<SystemCode> codeList) {
        mAdapter.addItems(codeList);
        mFragmentBinding.repairsRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(getActivity(), getString(R.string.failed_create_job), Toast.LENGTH_LONG).show();
    }

    @Override
    public void handleErrorAssetList(Throwable throwable) {
        Toast.makeText(getActivity(), getString(R.string.failed_load_assets), Toast.LENGTH_LONG).show();
    }

    @Override
    public void handleErrorTruckLoadTypes(Throwable throwable) {
        Toast.makeText(getActivity(), getString(R.string.failed_load_truckload_types), Toast.LENGTH_LONG).show();
    }

    @Override
    public void setTruckLoadList(List<TruckLoadResponse.TruckLoad> truckLoadList) {
        ArrayAdapter<TruckLoadResponse.TruckLoad> myAdapter = new ArrayAdapter<>(getActivity(), R.layout.row_truckload_spinner, truckLoadList);
        mFragmentBinding.truckLoadSpinner.setAdapter(myAdapter);
    }

    @Override
    public void setAssetList(List<Vehicle> vehicleList) {
        ArrayAdapter<Vehicle> myAdapter = new ArrayAdapter<>(getActivity(), R.layout.row_truckload_spinner, vehicleList);
        mFragmentBinding.vehicleSpinner.setAdapter(myAdapter);
    }

    @Override
    public void onEventCreation(CreateEventResponse eventResponse) {
        if (eventResponse != null && eventResponse.getResult() != null)
            if (getActivity() instanceof MainActivityNew)
                ((MainActivityNew) getActivity()).showOngoingServices();
            else if (getActivity() instanceof DashboardActivity)
                ((DashboardActivity) getActivity()).showOngoingServices();
    }

    @Override
    public void notifyAdapter() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onRemoveItemClick(String systemCode) {
        for (int i = 0; i < getViewModel().getCodeList().size(); i++) {
            if (getViewModel().getCodeList().get(i).getCode1().equalsIgnoreCase(systemCode)) {
                getViewModel().removeCode(i);
                return;
            }
        }
    }

    public void onSubmitEventRequest() {
        if (!isValidate()) return;
        getViewModel().saveDetails(((Vehicle) mFragmentBinding.vehicleSpinner.getSelectedItem()).getVehicleid(), mFragmentBinding.RTA.getText().toString(), mFragmentBinding.RTC.getText().toString(),
                ((TruckLoadResponse.TruckLoad) mFragmentBinding.truckLoadSpinner.getSelectedItem()).getLoadId(), mFragmentBinding.breakdownReport.getText().toString(), getActivity()  instanceof MainActivityNew ? true : false);

        Gson gson = new Gson();

        getViewModel().submitEventAPICall();
    }

    private boolean isValidate() {
        if (mFragmentBinding.vehicleSpinner.getSelectedItem() == null) {
            Toast.makeText(getActivity(), "Please select vehicle", Toast.LENGTH_SHORT).show();
        } else if (mFragmentBinding.truckLoadSpinner.getSelectedItem() == null) {
            Toast.makeText(getActivity(), "Please select truck load type", Toast.LENGTH_SHORT).show();
        } else if (mFragmentBinding.vehicleSpinner.getSelectedItemPosition() == 0) {
            Toast.makeText(getActivity(), "Please select vehicle", Toast.LENGTH_SHORT).show();
            return false;
        } else if (mFragmentBinding.truckLoadSpinner.getSelectedItemPosition() == 0) {
            Toast.makeText(getActivity(), "Please select truck load type", Toast.LENGTH_SHORT).show();
            return false;
        } else if (TextUtils.isEmpty(mFragmentBinding.RTA.getText().toString())) {
            Toast.makeText(getActivity(), "Please select RTA", Toast.LENGTH_SHORT).show();
            return false;
        } else if (TextUtils.isEmpty(mFragmentBinding.RTC.getText().toString())) {
            Toast.makeText(getActivity(), "Please select RTC", Toast.LENGTH_SHORT).show();
            return false;
        } else if (mAdapter.getItemCount() == 0) {
            Toast.makeText(getActivity(), "Please add repair", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
}

