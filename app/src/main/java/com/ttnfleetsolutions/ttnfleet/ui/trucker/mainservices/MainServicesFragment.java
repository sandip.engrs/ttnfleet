package com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices;

import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.BR;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.LocationType;
import com.ttnfleetsolutions.ttnfleet.databinding.FragmentMainServicesBinding;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.locationtypes.SelectLocationTypeDialogFragment;
import com.ttnfleetsolutions.ttnfleet.utils.GlideApp;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import static com.ttnfleetsolutions.ttnfleet.data.remote.ApiEndPoint.DIR_IMAGE_THEME_BG;

/**
 * Created by Sandip Chaulagain on 11/21/2017.
 *
 * @version 1.0
 */

public class MainServicesFragment extends BaseFragment<FragmentMainServicesBinding, MainServicesViewModel> implements MainServicesNavigator {

    @Inject
    MainServicesViewModel mMainServicesViewModel;

    FragmentMainServicesBinding mFragmentMainServicesBinding;

    @Override
    protected MainServicesViewModel getViewModel() {
        return mMainServicesViewModel;
    }

    @Override
    protected int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_main_services;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mMainServicesViewModel.setNavigator(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFragmentMainServicesBinding = getViewDataBinding();
        setUp();
    }

    private void setUp() {
        GlideApp.with(this)
                .load(DIR_IMAGE_THEME_BG + getViewModel().getDataManager().getThemeBgImage())
                .placeholder(R.mipmap.bg_dashboard)
                .error(R.mipmap.bg_dashboard)
                .fitCenter()
                .into(mFragmentMainServicesBinding.bgImage);

        mFragmentMainServicesBinding.typeScheduledPM.setBackgroundTintList(ColorStateList.valueOf(getViewModel().getDataManager().getThemeTitleBarColor()));
        mFragmentMainServicesBinding.typeEmergencyRepair.setBackgroundTintList(ColorStateList.valueOf(getViewModel().getDataManager().getThemeTitleBarColor()));
        mFragmentMainServicesBinding.typeAccident.setBackgroundTintList(ColorStateList.valueOf(getViewModel().getDataManager().getThemeTitleBarColor()));
        mFragmentMainServicesBinding.typeTowing.setBackgroundTintList(ColorStateList.valueOf(getViewModel().getDataManager().getThemeTitleBarColor()));

        if(getViewModel().getDataManager().getThemeFooterDisplayFlag() == 1) {
            mFragmentMainServicesBinding.textViewBottom.setVisibility(View.VISIBLE);
            mFragmentMainServicesBinding.textViewBottom.setTextColor(getViewModel().getDataManager().getThemeFooterTextColor());
        }
        /*mFragmentMainServicesBinding.typeScheduledPM.setOnClickListener(this);
        mFragmentMainServicesBinding.typeEmergencyRepair.setOnClickListener(this);
        mFragmentMainServicesBinding.typeAccident.setOnClickListener(this);
        mFragmentMainServicesBinding.typeTowing.setOnClickListener(this);

        if (getActivity() instanceof ServiceProviderDashboardActivity)
            mFragmentMainServicesBinding.callForHelp.setVisibility(View.GONE);

        mFragmentMainServicesBinding.emergencyRoadService.setBackgroundTintList(ColorStateList.valueOf(Color.argb(153, 126, 211, 33)));
        mFragmentMainServicesBinding.scheduledMaintainanceService.setBackgroundTintList(ColorStateList.valueOf(Color.argb(153, 248, 231, 28)));
        mFragmentMainServicesBinding.accidentNotificationService.setBackgroundTintList(ColorStateList.valueOf(Color.argb(153, 245, 166, 35)));
        mFragmentMainServicesBinding.callForHelp.setBackgroundTintList(ColorStateList.valueOf(Color.argb(153, 210, 31, 31)));*/
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(getActivity(), getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setLocationTypeList(int eventType, List<LocationType> locationTypeList) {
        SelectLocationTypeDialogFragment fragment = new SelectLocationTypeDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("eventType", eventType);
        bundle.putInt("colorPrimary", getViewModel().getDataManager().getThemeTitleBarColor());
        bundle.putInt("colorPrimaryDark", getViewModel().getDataManager().getThemeStatusBarColor());
        bundle.putSerializable("locationTypeList", (Serializable) locationTypeList);
        fragment.setArguments(bundle);
        fragment.show(Objects.requireNonNull(getActivity()).getSupportFragmentManager(), SelectLocationTypeDialogFragment.class.getSimpleName());
    }

    /*@Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.typeScheduledPM: //actually emergency assistance
                BottomSheetDialogFragmentEmergencyRoadSide bottomSheetDialogFragment = new BottomSheetDialogFragmentEmergencyRoadSide();
                Bundle bundle = new Bundle();
                bundle.putInt("from", 0);
                bottomSheetDialogFragment.setArguments(bundle);
                if (getActivity() instanceof MainActivityNew)
                    bottomSheetDialogFragment.show(getActivity().getSupportFragmentManager(), "emergencyRoadService");
                else if (getActivity() instanceof ServiceProviderDashboardActivity)
                    bottomSheetDialogFragment.show(getActivity().getSupportFragmentManager(), "emergencyRoadService");
                break;
            case R.id.typeEmergencyRepair: //actually Schedule Maintenance
                BottomSheetDialogFragmentScheduledMaintenance bottomSheetDialogFragmentScheduledMaintenance = new BottomSheetDialogFragmentScheduledMaintenance();
                bundle = new Bundle();
                bundle.putInt("from", 1);
                bottomSheetDialogFragmentScheduledMaintenance.setArguments(bundle);
                if (getActivity() instanceof MainActivityNew)
                    bottomSheetDialogFragmentScheduledMaintenance.show(getActivity().getSupportFragmentManager(), "scheduledMaintainanceService");
                else if (getActivity() instanceof ServiceProviderDashboardActivity)
                    bottomSheetDialogFragmentScheduledMaintenance.show(getActivity().getSupportFragmentManager(), "scheduledMaintainanceService");

                break;
            case R.id.typeAccident:
                bottomSheetDialogFragment = new BottomSheetDialogFragmentEmergencyRoadSide();
                bundle = new Bundle();
                bundle.putInt("from", 2);
                bottomSheetDialogFragment.setArguments(bundle);
                if (getActivity() instanceof MainActivityNew)
                    bottomSheetDialogFragment.show(getActivity().getSupportFragmentManager(), "accident");
                else if (getActivity() instanceof ServiceProviderDashboardActivity)
                    bottomSheetDialogFragment.show(getActivity().getSupportFragmentManager(), "accident");
                break;
            case R.id.typeTowing:
                bottomSheetDialogFragment = new BottomSheetDialogFragmentEmergencyRoadSide();
                bundle = new Bundle();
                bundle.putInt("from", 2);
                bottomSheetDialogFragment.setArguments(bundle);
                if (getActivity() instanceof MainActivityNew)
                    bottomSheetDialogFragment.show(getActivity().getSupportFragmentManager(), "accident");
                else if (getActivity() instanceof ServiceProviderDashboardActivity)
                    bottomSheetDialogFragment.show(getActivity().getSupportFragmentManager(), "accident");
                break;
        }
    }*/
}
