package com.ttnfleetsolutions.ttnfleet.ui.serviceprovider.jobroster;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.Toast;

import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.BR;
import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.listing.Event;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.AcceptWorkOrderResponse;
import com.ttnfleetsolutions.ttnfleet.databinding.FragmentSpJobRosterBinding;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseActivity;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseFragment;
import com.ttnfleetsolutions.ttnfleet.ui.serviceprovider.RejectWithReasonDialogFragment;
import com.ttnfleetsolutions.ttnfleet.ui.serviceprovider.ServiceProviderDashboardActivity;
import com.ttnfleetsolutions.ttnfleet.utils.CommonUtils;
import com.ttnfleetsolutions.ttnfleet.utils.VerticalSpaceItemDecoration;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import static com.ttnfleetsolutions.ttnfleet.utils.AppConstants.VERTICAL_LIST_ITEM_SPACE;

/**
 * Created by Sandip Chaulagain on 10/10/2017.
 *
 * @version 1.0
 */

public class JobRosterFragment extends BaseFragment<FragmentSpJobRosterBinding, JobRosterViewModel> implements JobRosterNavigator {

    @Inject
    JobRosterViewModel mViewModel;

    FragmentSpJobRosterBinding mFragmentBinding;

    @Inject
    JobRosterAdapter mAdapter;

    @Override
    protected JobRosterViewModel getViewModel() {
        return mViewModel;
    }

    @Override
    protected int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_sp_job_roster;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModel.setNavigator(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFragmentBinding = getViewDataBinding();
        setUp();
    }

    private void setUp() {
        mFragmentBinding.swiperefresh.setOnRefreshListener(
                () -> getEvents()
        );
        mFragmentBinding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mFragmentBinding.recyclerView.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_LIST_ITEM_SPACE));
        mAdapter.setEmptyItemViewModelListener(this::getEvents);
        mAdapter.setIsLandscape(((BaseActivity) getActivity()).isLandscape());
        mAdapter.setListener(mListener);

        if(getViewModel().getEventList() != null && getViewModel().getEventList().size() > 0 && !((ServiceProviderDashboardActivity) getActivity()).getViewModel().isReloadNeeded) {
            getEvents(getViewModel().getEventList());
        } else {
            getEvents();
        }
    }

    public void getEvents() {
        if (!isNetworkConnected()) {
            mAdapter.setEmptyItem(CommonUtils.getEmptyItem(getActivity(), DataManager.ErrorState.NETWORK_ERROR));
            mFragmentBinding.recyclerView.setAdapter(mAdapter);
        } else {
            mFragmentBinding.swiperefresh.setRefreshing(true);
            getViewModel().getEventsSP(DataManager.FilterDashboardHistory.DASHBOARD.getType());
        }
    }

    @Override
    public void getEvents(List<Event> eventList) {
        mFragmentBinding.swiperefresh.setRefreshing(false);
        ((ServiceProviderDashboardActivity) getActivity()).getViewModel().isReloadNeeded = false;
        if (eventList != null) {
            mAdapter.setFrom(0);
            mAdapter.addItems(eventList);
            mFragmentBinding.recyclerView.setAdapter(mAdapter);
            if(((BaseActivity) getActivity()).isLandscape() && eventList.size() > 0) {
                mListener.onItemClick(eventList.get(0).getEventid());
            } else {
                if(((BaseActivity) getActivity()).isLandscape()) {
                    mListener.onItemClick(0);
                }
            }
        } else {
            mAdapter.setEmptyItem(CommonUtils.getEmptyItem(getActivity(), DataManager.ErrorState.NO_DATA));
            mFragmentBinding.recyclerView.setAdapter(mAdapter);
            if(((BaseActivity) getActivity()).isLandscape()) {
                mListener.onItemClick(0);
            }
        }
    }

    @Override
    public void handleError(Throwable throwable) {
        mFragmentBinding.swiperefresh.setRefreshing(false);
        mAdapter.setEmptyItem(CommonUtils.getEmptyItem(getActivity(), DataManager.ErrorState.API_FAIL));
        mFragmentBinding.recyclerView.setAdapter(mAdapter);
    }

    public RejectWithReasonDialogFragment.SubmitListener submitListener = (eventId, workOrderId, workOrderStatusId, jobId, jobStatusId, rejectionDescription, rejectionTypeId) ->   ((ServiceProviderDashboardActivity) Objects.requireNonNull(getActivity())).getViewModel().rejectWorkOrdersAPICall(eventId, workOrderId, workOrderStatusId, rejectionDescription, rejectionTypeId);

    public JobRosterItemViewModel.ItemViewModelListener mListener = new JobRosterItemViewModel.ItemViewModelListener() {

        @Override
        public void onItemClick(int id) {
            ((ServiceProviderDashboardActivity) Objects.requireNonNull(getActivity())).goToServiceDetails(id);
            for (Event event : getViewModel().getEventList()) {
                if(event.getEventid() == id)
                    event.setSelected(true);
                else event.setSelected(false);
            }
            mAdapter.notifyDataSetChanged();
        }

        @Override
        public void onAcceptButtonClick(int eventId, int workOrderId, int workOrderStatus, int jobId, int jobStatusId) {
            ((ServiceProviderDashboardActivity) Objects.requireNonNull(getActivity())).allocateJobToTechnician(eventId, workOrderId, workOrderStatus);
        }

        @Override
        public void onRejectButtonClick(int eventId, int workOrderId, int workOrderStatus, int jobId, int jobStatusId) {
            ((ServiceProviderDashboardActivity) Objects.requireNonNull(getActivity())).getViewModel().getRejectionTypesSP(eventId, workOrderId, workOrderStatus);
        }

        @Override
        public void onEstimateButtonClick(int eventId, int workOrderId, int workOrderStatus, int jobId, int jobStatusId) {

        }

        @Override
        public void onEnrouteButtonClick(int eventId, int workOrderId, int workOrderStatus, int jobId, int jobStatusId) {

        }

        @Override
        public void onConfirmBreakReportButtonClick(int eventId, int workOrderId, int workOrderStatus, int jobId, int jobStatusId, String eventType, String breakdown) {

        }

        @Override
        public void onAdditionalJobButtonClick(int eventId, int workOrderId, int workOrderStatus, int jobId, int jobStatusId) {

        }

        @Override
        public void onCompleteJobButtonClick(int eventId, int workOrderId, int workOrderStatus, int jobId, int jobStatusId) {

        }
    };

    public void onRejectWorkOrders(AcceptWorkOrderResponse acceptWorkOrderResponse) {
        Toast.makeText(getActivity(), "Successfully rejected", Toast.LENGTH_LONG).show();
        getViewModel().getEventsSP(DataManager.FilterDashboardHistory.DASHBOARD.getType());
    }

    public void onAcceptWorkOrders() {
        Toast.makeText(getActivity(), "Successfully accepted", Toast.LENGTH_LONG).show();
        getViewModel().getEventsSP(DataManager.FilterDashboardHistory.DASHBOARD.getType());
    }
}
