package com.ttnfleetsolutions.ttnfleet.ui.fleet.order.repairs;

import android.databinding.BindingAdapter;
import android.databinding.ObservableField;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.TextView;

import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.Repair;

/**
 * Created by Sandip Chaulagain on 11/10/2017.
 *
 * @version 1.0
 */

public class RepairItemViewModel {

    private Repair mRepair;
    public ObservableField<String> id;
    public ObservableField<String> name;
    public ObservableField<String> time;
    public ObservableField<String> duration;
    public ObservableField<String> cost;
    public ObservableField<String> parts;
    public ObservableField<String> approvalStatusText;
    public ObservableField<Integer> approvalStatus;

    private RepairItemViewModelListener mListener;

    RepairItemViewModel(Repair repair, RepairItemViewModelListener listener) {
        this.mRepair = repair;
        this.mListener = listener;
        id = new ObservableField<>(String.format("%s. ", String.valueOf(mRepair.id + 1)));
        name = new ObservableField<>(mRepair.name);
        time = new ObservableField<>(mRepair.time);
        duration = new ObservableField<>(mRepair.duration);
        cost = new ObservableField<>(mRepair.cost);
        parts = new ObservableField<>(mRepair.parts);
        approvalStatus = new ObservableField<>(mRepair.approvalStatus);
        switch (approvalStatus.get()) {
            case 0:
                approvalStatusText = new ObservableField<>("Approved by you");
                break;
            case 1:
                approvalStatusText = new ObservableField<>("Vender estimate requested");
                break;
            case 2:
                approvalStatusText = new ObservableField<>("Fix this later");
                break;
            case 3:
                approvalStatusText = new ObservableField<>("Fix this later");
                break;
            default:
                approvalStatusText = new ObservableField<>("Approved by you");
                break;
        }

    }

    public void onItemClick() {
        mListener.onItemClick(mRepair.id);
    }

    public interface RepairItemViewModelListener {
        void onItemClick(long id);
    }

    @BindingAdapter({"characterBackground"})
    public static void characterBackground(View view, int status) {
        switch (status) {
            case 0:
                view.setBackgroundColor(ContextCompat.getColor(view.getContext(), R.color.colorPrimaryDark));
                break;
            case 1:
                view.setBackgroundColor(ContextCompat.getColor(view.getContext(), android.R.color.holo_orange_dark));
                break;
            case 2:
                view.setBackgroundColor(ContextCompat.getColor(view.getContext(), android.R.color.darker_gray));
                break;
            default:
                view.setBackgroundColor(ContextCompat.getColor(view.getContext(), R.color.colorPrimaryDark));
                break;
        }
    }

    @BindingAdapter({"textBackground"})
    public static void textBackground(TextView view, int status) {
        switch (status) {
            case 0:
                view.setTextColor(ContextCompat.getColor(view.getContext(), R.color.colorPrimaryDark));
                break;
            case 1:
                view.setTextColor(ContextCompat.getColor(view.getContext(), android.R.color.holo_orange_dark));
                break;
            case 2:
                view.setTextColor(ContextCompat.getColor(view.getContext(), android.R.color.darker_gray));
                break;
            default:
                view.setTextColor(ContextCompat.getColor(view.getContext(), R.color.colorPrimaryDark));
                break;
        }
    }
}
