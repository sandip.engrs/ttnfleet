package com.ttnfleetsolutions.ttnfleet.ui.technician;


import android.app.AlertDialog;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.ui.serviceprovider.RejectWithReasonDialogFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.model.OrderStatus;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.model.Orientation;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.model.TimeLineModel;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class JobCardDetailsFragment extends Fragment {

    View mRootView;
    private int index = 0, from = 0;
    private RecyclerView mRecyclerView;
    private List<TimeLineModel> mDataList = new ArrayList<>();
    private Orientation mOrientation = Orientation.VERTICAL;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mRootView = inflater.inflate(R.layout.fragment_job_card_details_tech_new, container, false);
        initComponents();
        return mRootView;
    }

    public void updateIndex(int from, int index) {
        this.from = from;
        this.index = index;
    }

    private void initComponents() {
        NestedScrollView nsv = mRootView.findViewById(R.id.nestedScrollView);
        nsv.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
            if (scrollY > oldScrollY) {
                ((FloatingActionButton) mRootView.findViewById(R.id.chatFloatingActionButton)).hide();
            } else {
                ((FloatingActionButton) mRootView.findViewById(R.id.chatFloatingActionButton)).show();
            }
        });

        mRootView.findViewById(R.id.acceptButton).setOnClickListener(view -> ((TechnicianDashboardActivity) getActivity()).showJobCheckList(0));

        mRootView.findViewById(R.id.rejectButton).setOnClickListener(view -> {
            RejectWithReasonDialogFragment fragment = new RejectWithReasonDialogFragment();
            Bundle bundle = new Bundle();
            bundle.putBoolean("from-sd", false);
            fragment.setArguments(bundle);
            fragment.show(getActivity().getSupportFragmentManager(), "RejectWithReason");
        });

        mRootView.findViewById(R.id.confirmBreakdownReportButton).setOnClickListener(view -> {
            ((TechnicianDashboardActivity) getActivity()).jobInspections(0, "", "");
        });

        mRootView.findViewById(R.id.additionalJob).setOnClickListener(view -> {
            ((TechnicianDashboardActivity) getActivity()).additionalJobs(0);
        });

        mRootView.findViewById(R.id.completeJob).setOnClickListener(view -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("You need to verify details on call (+1 248-481-1201) before you complete the job.");
            builder.setNegativeButton("Cancel", (dialogInterface, i) -> dialogInterface.dismiss());
            builder.setPositiveButton("Make a call", (dialogInterface, i) -> dialogInterface.dismiss());
            builder.show();
        });

        mRootView.findViewById(R.id.enrouteButton).setOnClickListener(view -> {
            //TODO
        });

        mRootView.findViewById(R.id.chatFloatingActionButton).setOnClickListener(view -> ((TechnicianDashboardActivity) getActivity()).showServiceChatThreadDetails(0, "Service Request 1"));

        mRootView.findViewById(R.id.requirementsUpload).setOnClickListener(view -> ((TechnicianDashboardActivity) getActivity()).showJobRequirements(0, 0));

        switch (index) {
            case 1:
                ((TextView) mRootView.findViewById(R.id.status)).setText("NEW");
                mRootView.findViewById(R.id.status).setBackgroundTintList(getResources().getColorStateList(android.R.color.holo_orange_dark));

                mRootView.findViewById(R.id.requiredImages).setVisibility(View.GONE);
                mRootView.findViewById(R.id.imagesRecyclerView).setVisibility(View.GONE);

                mRootView.findViewById(R.id.materialChecklistLabel).setVisibility(View.GONE);
                mRootView.findViewById(R.id.materialChecklistLayout).setVisibility(View.GONE);

                mRootView.findViewById(R.id.bottomLayout).setVisibility(View.VISIBLE);
                mRootView.findViewById(R.id.additionalJobLayout).setVisibility(View.GONE);
                mRootView.findViewById(R.id.enrouteButton).setVisibility(View.GONE);
                mRootView.findViewById(R.id.confirmBreakdownReportButton).setVisibility(View.GONE);

                mRootView.findViewById(R.id.requirementsUpload).setVisibility(View.GONE);

                CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) mRootView.findViewById(R.id.chatFloatingActionButton).getLayoutParams();
                layoutParams.setMargins(0, 0, (int) getResources().getDimension(R.dimen.fab_margin), (int) getResources().getDimension(R.dimen.fab_margin_bottom));
                mRootView.findViewById(R.id.chatFloatingActionButton).setLayoutParams(layoutParams);

                switch (from) {
                    case 2:
                        //Drop off & Pick Up (NEW)

                        ((TextView) mRootView.findViewById(R.id.breakdownReportLabel)).setText(getString(R.string.service_required));
                        ((TextView) mRootView.findViewById(R.id.description)).setText("Service Type: Drop off & Pick up");
                        ((TextView) mRootView.findViewById(R.id.issueDescription)).setText("Drop off: 8th Dec 12:45  |  Pick up: 19th Dec 08:00");
                        ((TextView) mRootView.findViewById(R.id.datetime)).setText("Just now");
                        ((TextView) mRootView.findViewById(R.id.serviceIssue)).setText(getString(R.string.dummy_services));

                        ((TextView) mRootView.findViewById(R.id.location)).setText("3884 Eu Road, Boston, MA, 43448");
                        ((TextView) mRootView.findViewById(R.id.vehicleName)).setText("Maan Truck 8.136 FAE 4X4");
                        ((TextView) mRootView.findViewById(R.id.vinNo)).setText("WVM56502016034590");
                        break;
                    case 1:
                        //Emergency RoadSide (NEW)
                        break;
                }
                break;
            case 21:
                ((TextView) mRootView.findViewById(R.id.status)).setText("ONGOING");
                mRootView.findViewById(R.id.status).setBackgroundTintList(getResources().getColorStateList(R.color.colorPrimary));

                ((TextView) mRootView.findViewById(R.id.datetime)).setText("20 mins ago");

                mRootView.findViewById(R.id.bottomLayout).setVisibility(View.GONE);
                mRootView.findViewById(R.id.additionalJobLayout).setVisibility(View.GONE);
                mRootView.findViewById(R.id.enrouteButton).setVisibility(View.VISIBLE);
                mRootView.findViewById(R.id.confirmBreakdownReportButton).setVisibility(View.GONE);

                mRootView.findViewById(R.id.materialChecklistLabel).setVisibility(View.VISIBLE);
                mRootView.findViewById(R.id.materialChecklistLayout).setVisibility(View.VISIBLE);

                layoutParams = (CoordinatorLayout.LayoutParams) mRootView.findViewById(R.id.chatFloatingActionButton).getLayoutParams();
                layoutParams.setMargins(0, 0, (int) getResources().getDimension(R.dimen.fab_margin), (int) getResources().getDimension(R.dimen.fab_margin_bottom));
                mRootView.findViewById(R.id.chatFloatingActionButton).setLayoutParams(layoutParams);
                break;
            case 22:
                ((TextView) mRootView.findViewById(R.id.status)).setText("ONGOING");
                mRootView.findViewById(R.id.status).setBackgroundTintList(getResources().getColorStateList(R.color.colorPrimary));

                ((TextView) mRootView.findViewById(R.id.datetime)).setText("45 mins ago");

                mRootView.findViewById(R.id.bottomLayout).setVisibility(View.GONE);
                mRootView.findViewById(R.id.additionalJobLayout).setVisibility(View.GONE);
                mRootView.findViewById(R.id.enrouteButton).setVisibility(View.GONE);
                mRootView.findViewById(R.id.confirmBreakdownReportButton).setVisibility(View.VISIBLE);

                mRootView.findViewById(R.id.materialChecklistLabel).setVisibility(View.VISIBLE);
                mRootView.findViewById(R.id.materialChecklistLayout).setVisibility(View.VISIBLE);

                layoutParams = (CoordinatorLayout.LayoutParams) mRootView.findViewById(R.id.chatFloatingActionButton).getLayoutParams();
                layoutParams.setMargins(0, 0, (int) getResources().getDimension(R.dimen.fab_margin), (int) getResources().getDimension(R.dimen.fab_margin_bottom));
                mRootView.findViewById(R.id.chatFloatingActionButton).setLayoutParams(layoutParams);

                ((TextView) mRootView.findViewById(R.id.location)).setText("State Highway 580, Tampa, FL, USA");
                ((TextView) mRootView.findViewById(R.id.vehicleName)).setText("Mercedes-Benz Unimog");
                ((TextView) mRootView.findViewById(R.id.vinNo)).setText("WVM58956444545545");
                ((TextView) mRootView.findViewById(R.id.serviceIssue)).setText("Front axle brake jam issue");
                ((TextView) mRootView.findViewById(R.id.issueDescription)).setText("Brakes are squealing, caused by a worn down brake pad, which comes into direct contact with the rotor.");
                break;
            case 23:
                ((TextView) mRootView.findViewById(R.id.status)).setText("ONGOING");
                mRootView.findViewById(R.id.status).setBackgroundTintList(getResources().getColorStateList(R.color.colorPrimary));

                ((TextView) mRootView.findViewById(R.id.datetime)).setText("2 hours ago");

                mRootView.findViewById(R.id.bottomLayout).setVisibility(View.GONE);
                mRootView.findViewById(R.id.additionalJobLayout).setVisibility(View.VISIBLE);
                mRootView.findViewById(R.id.enrouteButton).setVisibility(View.GONE);
                mRootView.findViewById(R.id.confirmBreakdownReportButton).setVisibility(View.GONE);

                mRootView.findViewById(R.id.materialChecklistLabel).setVisibility(View.VISIBLE);
                mRootView.findViewById(R.id.materialChecklistLayout).setVisibility(View.VISIBLE);

                layoutParams = (CoordinatorLayout.LayoutParams) mRootView.findViewById(R.id.chatFloatingActionButton).getLayoutParams();
                layoutParams.setMargins(0, 0, (int) getResources().getDimension(R.dimen.fab_margin), (int) getResources().getDimension(R.dimen.fab_margin_bottom));
                mRootView.findViewById(R.id.chatFloatingActionButton).setLayoutParams(layoutParams);

                ((TextView) mRootView.findViewById(R.id.location)).setText("484–2500 Ullamcorper. Road, Houston, Texas, 31515");
                ((TextView) mRootView.findViewById(R.id.vehicleName)).setText("Ford F-650 Supertruck");
                ((TextView) mRootView.findViewById(R.id.vinNo)).setText("WVM25454545454525");
                ((TextView) mRootView.findViewById(R.id.serviceIssue)).setText("Clutch Problem");
                ((TextView) mRootView.findViewById(R.id.issueDescription)).setText("The friction material on the clutch plates have worn out.");
                break;
            case 3:
                ((TextView) mRootView.findViewById(R.id.status)).setText("COMPLETED");
                mRootView.findViewById(R.id.status).setBackgroundTintList(getResources().getColorStateList(android.R.color.holo_blue_dark));

                ((TextView) mRootView.findViewById(R.id.datetime)).setText("Just now");

                layoutParams = (CoordinatorLayout.LayoutParams) mRootView.findViewById(R.id.chatFloatingActionButton).getLayoutParams();
                layoutParams.setMargins(0, 0, (int) getResources().getDimension(R.dimen.fab_margin), (int) getResources().getDimension(R.dimen.fab_margin));
                mRootView.findViewById(R.id.chatFloatingActionButton).setLayoutParams(layoutParams);

                mRootView.findViewById(R.id.materialChecklistLabel).setVisibility(View.VISIBLE);
                mRootView.findViewById(R.id.materialChecklistLayout).setVisibility(View.VISIBLE);

                mRootView.findViewById(R.id.bottomLayout).setVisibility(View.GONE);
                mRootView.findViewById(R.id.additionalJobLayout).setVisibility(View.GONE);
                mRootView.findViewById(R.id.enrouteButton).setVisibility(View.GONE);
                mRootView.findViewById(R.id.confirmBreakdownReportButton).setVisibility(View.GONE);

                mRootView.findViewById(R.id.requirementsUpload).setVisibility(View.GONE);
                mRootView.findViewById(R.id.requiredImages).setVisibility(View.VISIBLE);
                mRootView.findViewById(R.id.imagesRecyclerView).setVisibility(View.VISIBLE);
                break;
        }

        mRecyclerView = mRootView.findViewById(R.id.timelineRecyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setNestedScrollingEnabled(false);
        setDataListItems();
        TimeLineAdapter mTimeLineAdapter = new TimeLineAdapter(mDataList, mOrientation);
        mRecyclerView.setAdapter(mTimeLineAdapter);

        RecyclerView imagesRecyclerView = mRootView.findViewById(R.id.imagesRecyclerView);
        imagesRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        imagesRecyclerView.setAdapter(new ImagesRecyclerViewAdapter(getActivity()));
    }


    private void setDataListItems() {
        mDataList.clear();
        switch (index) {
            case 1:
                mDataList.add(new TimeLineModel("Received job request", "2017-12-06 16:35", OrderStatus.ACTIVE));
                break;
            case 21:
                mDataList.add(new TimeLineModel("Received job request", "2017-12-06 16:35", OrderStatus.COMPLETED));
                mDataList.add(new TimeLineModel("You accepted job request.\nETC: 1 Hour", "2017-12-06 16:40", OrderStatus.ACTIVE));
                break;
            case 22:
                mDataList.add(new TimeLineModel("Received job request", "2017-12-06 16:35", OrderStatus.COMPLETED));
                mDataList.add(new TimeLineModel("You accepted job request.\nETC: 1 Hour", "2017-12-06 16:40", OrderStatus.COMPLETED));
                mDataList.add(new TimeLineModel("ETA: 17:00", "2017-12-06 16:40", OrderStatus.COMPLETED));
                mDataList.add(new TimeLineModel("ATA: 17:10", "2017-12-06 17:10", OrderStatus.COMPLETED));
                mDataList.add(new TimeLineModel("Work in progress", "2017-12-06 17:10", OrderStatus.ACTIVE));
                break;
            case 23:
                mDataList.add(new TimeLineModel("Received job request", "2017-12-06 16:35", OrderStatus.COMPLETED));
                mDataList.add(new TimeLineModel("You accepted job request.\nETC: 1 Hour", "2017-12-06 16:40", OrderStatus.COMPLETED));
                mDataList.add(new TimeLineModel("ETA: 17:00", "2017-12-06 16:40", OrderStatus.COMPLETED));
                mDataList.add(new TimeLineModel("ATA: 17:10", "2017-12-06 17:10", OrderStatus.COMPLETED));
                mDataList.add(new TimeLineModel("Work in progress", "2017-12-06 17:10", OrderStatus.ACTIVE));
                break;
            case 3:
                mDataList.add(new TimeLineModel("Received job request", "2017-12-06 16:35", OrderStatus.COMPLETED));
                mDataList.add(new TimeLineModel("You accepted job request.\nETC: 1 Hour", "2017-12-06 16:40", OrderStatus.COMPLETED));
                mDataList.add(new TimeLineModel("ETA: 17:00", "2017-12-06 16:40", OrderStatus.COMPLETED));
                mDataList.add(new TimeLineModel("ATA: 17:10", "2017-12-06 17:10", OrderStatus.COMPLETED));
                mDataList.add(new TimeLineModel("Work in progress", "2017-12-06 17:10", OrderStatus.COMPLETED));
                mDataList.add(new TimeLineModel("Job completed", "2017-12-06 18:20", OrderStatus.ACTIVE));
                break;
        }
    }
}
