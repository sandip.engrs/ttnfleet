package com.ttnfleetsolutions.ttnfleet.ui.technician.history;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.BR;
import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.listing.Event;
import com.ttnfleetsolutions.ttnfleet.databinding.FragmentServiceHistoryBinding;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseActivity;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseFragment;
import com.ttnfleetsolutions.ttnfleet.ui.technician.TechnicianDashboardActivity;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.dashboard.OngoingServiceAdapter;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.dashboard.ServiceRequestIemViewModel;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.history.ServiceHistoryNavigator;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.history.ServiceHistoryViewModel;
import com.ttnfleetsolutions.ttnfleet.utils.CommonUtils;
import com.ttnfleetsolutions.ttnfleet.utils.VerticalSpaceItemDecoration;

import java.util.List;

import javax.inject.Inject;

import static com.ttnfleetsolutions.ttnfleet.utils.AppConstants.VERTICAL_LIST_ITEM_SPACE;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */

public class ServiceHistoryFragment extends BaseFragment<FragmentServiceHistoryBinding, ServiceHistoryViewModel> implements ServiceHistoryNavigator {

    @Inject
    ServiceHistoryViewModel mServiceHistoryViewModel;

    FragmentServiceHistoryBinding mFragmentServiceHistoryBinding;

    @Inject
    OngoingServiceAdapter mAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mServiceHistoryViewModel.setNavigator(this);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFragmentServiceHistoryBinding = getViewDataBinding();
        setUp();
    }

    private void setUp() {
        mFragmentServiceHistoryBinding.swiperefresh.setOnRefreshListener(
                () -> getEvents()
        );
        mFragmentServiceHistoryBinding.serviceHistoryRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mFragmentServiceHistoryBinding.serviceHistoryRecyclerView.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_LIST_ITEM_SPACE));
        mAdapter.setEmptyItemViewModelListener(this::getEvents);
        mAdapter.setIsLandscape(((BaseActivity) getActivity()).isLandscape());
        mAdapter.setListener(listener);

        if(getViewModel().getEventList() != null && getViewModel().getEventList().size() > 0) {
            getClosedEvents(getViewModel().getEventList());
        } else {
            getEvents();
        }
    }

    private void getEvents() {
        if (!isNetworkConnected()) {
            mAdapter.setEmptyItem(CommonUtils.getEmptyItem(getActivity(), DataManager.ErrorState.NETWORK_ERROR));
            mFragmentServiceHistoryBinding.serviceHistoryRecyclerView.setAdapter(mAdapter);
        } else {
            getViewModel().getClosedEventsTech(DataManager.FilterDashboardHistory.HISTORY.getType());
        }
    }

    private ServiceRequestIemViewModel.ServiceRequestItemViewModelListener listener = new ServiceRequestIemViewModel.ServiceRequestItemViewModelListener() {
        @Override
        public void onItemClick(int id) {
            ((TechnicianDashboardActivity) getActivity()).showOrderDetails(id);
            for (Event event : getViewModel().getEventList()) {
                if(event.getEventid() == id)
                    event.setSelected(true);
                else event.setSelected(false);
            }
            mAdapter.notifyDataSetChanged();
        }

        @Override
        public void onDispatchClick(int id) {

        }

        @Override
        public void onAddNoteClick(int id, String note) {

        }
    };

    @Override
    public void handleError(Throwable throwable) {
        mFragmentServiceHistoryBinding.swiperefresh.setRefreshing(false);
        mAdapter.setEmptyItem(CommonUtils.getEmptyItem(getActivity(), DataManager.ErrorState.API_FAIL));
        mFragmentServiceHistoryBinding.serviceHistoryRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void getClosedEvents(List<Event> eventList) {
        mFragmentServiceHistoryBinding.swiperefresh.setRefreshing(false);
        if (eventList != null && eventList.size() > 0) {
            mAdapter.addItems(eventList);
            mFragmentServiceHistoryBinding.serviceHistoryRecyclerView.setAdapter(mAdapter);
            if(((BaseActivity) getActivity()).isLandscape()) {
                listener.onItemClick(eventList.get(0).getEventid());
            }
        } else {
            mAdapter.setEmptyItem(CommonUtils.getEmptyItem(getActivity(), DataManager.ErrorState.NO_DATA));
            mFragmentServiceHistoryBinding.serviceHistoryRecyclerView.setAdapter(mAdapter);
            if(((BaseActivity) getActivity()).isLandscape()) {
                listener.onItemClick(0);
            }
        }
    }

    @Override
    protected ServiceHistoryViewModel getViewModel() {
        return mServiceHistoryViewModel;
    }

    @Override
    protected int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_service_history;
    }
}
