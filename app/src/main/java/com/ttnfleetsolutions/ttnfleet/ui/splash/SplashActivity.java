package com.ttnfleetsolutions.ttnfleet.ui.splash;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import com.ttnfleetsolutions.ttnfleet.databinding.ActivitySplashBinding;
import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.BR;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseActivity;
import com.ttnfleetsolutions.ttnfleet.ui.fleet.DashboardActivity;
import com.ttnfleetsolutions.ttnfleet.ui.login.LoginActivity;
import com.ttnfleetsolutions.ttnfleet.ui.serviceprovider.ServiceProviderDashboardActivity;
import com.ttnfleetsolutions.ttnfleet.ui.technician.TechnicianDashboardActivity;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.MainActivityNew;

import javax.inject.Inject;

/**
 * Created by Sandip Chaulagain on 10/4/2017.
 *
 * @version 1.0
 */

public class SplashActivity extends BaseActivity<ActivitySplashBinding, SplashViewModel> implements SplashNavigator {

    @Inject
    SplashViewModel mSplashViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSplashViewModel.setNavigator(this);

        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
//        String appLinkAction = appLinkIntent.getAction();
            Uri appLinkData = appLinkIntent.getData();
            if (appLinkData != null) {
                Log.e("TAG", "last path " + appLinkData.getLastPathSegment());
                setDefaultTheme();
                getViewModel().getDataManager().setGUID(appLinkData.getLastPathSegment());
            }
        }
        mSplashViewModel.startSeeding();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void openLoginActivity() {
        Intent intent = LoginActivity.getStartIntent(SplashActivity.this);
        startActivity(intent);
        finish();
    }

    @Override
    public void openTruckerActivity() {
        Intent intent = MainActivityNew.getStartIntent(SplashActivity.this);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    public void openFleetDispatcherActivity() {
        Intent intent = DashboardActivity.getStartIntent(SplashActivity.this);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    public void openServiceProviderActivity() {
        Intent intent = ServiceProviderDashboardActivity.getStartIntent(SplashActivity.this);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    public void openServiceRepActivity() {
        Intent intent = TechnicianDashboardActivity.getStartIntent(SplashActivity.this);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    public SplashViewModel getViewModel() {
        return mSplashViewModel;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_splash;
    }
}
