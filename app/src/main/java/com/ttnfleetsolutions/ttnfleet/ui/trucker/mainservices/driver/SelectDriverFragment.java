package com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.driver;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.BR;
import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.DriverListResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.ServiceCode;
import com.ttnfleetsolutions.ttnfleet.databinding.FragmentIssueListNewBinding;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseFragment;
import com.ttnfleetsolutions.ttnfleet.ui.fleet.DashboardActivity;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.vmrs.system.SelectServiceNavigator;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.vmrs.system.SelectServiceViewModel;
import com.ttnfleetsolutions.ttnfleet.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by Sandip Chaulagain on 11/22/2017.
 *
 * @version 1.0
 */

public class SelectDriverFragment extends BaseFragment<FragmentIssueListNewBinding, SelectServiceViewModel> implements SelectServiceNavigator, SelectDriverItemViewModel.ItemViewModelListener {


    @Inject
    SelectServiceViewModel mSelectServiceViewModel;

    FragmentIssueListNewBinding mFragmentIssueListNewBinding;

    @Inject
    SelectDriverRecyclerViewAdapter mAdapter;

    @Override
    protected SelectServiceViewModel getViewModel() {
        return mSelectServiceViewModel;
    }

    @Override
    protected int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_issue_list_new;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSelectServiceViewModel.setNavigator(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFragmentIssueListNewBinding = getViewDataBinding();
        setUp();
    }

    private void setUp() {
        mFragmentIssueListNewBinding.nextButton.setVisibility(View.GONE);

        mFragmentIssueListNewBinding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mFragmentIssueListNewBinding.recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        mAdapter.setEmptyItemViewModelListener(this::getListing);
        mAdapter.setListener(this);

        getListing();
    }

    private void getListing() {
        if (!isNetworkConnected()) {
            mAdapter.setEmptyItem(CommonUtils.getEmptyItem(getActivity(), DataManager.ErrorState.NETWORK_ERROR));
            mFragmentIssueListNewBinding.recyclerView.setAdapter(mAdapter);
        } else
            getViewModel().getDriverListing(getViewModel().getDataManager().getThemeCompanyID());
    }

    @Override
    public void setDriverList(List<DriverListResponse.Driver> driverList) {
        if (driverList != null && driverList.size() > 0) {
            mAdapter.addItems(driverList);
            mFragmentIssueListNewBinding.recyclerView.setAdapter(mAdapter);
        }
    }

    @Override
    public void handleErrorDriverListing(Throwable throwable) {
        mAdapter.setEmptyItem(CommonUtils.getEmptyItem(getActivity(), DataManager.ErrorState.API_FAIL));
        mFragmentIssueListNewBinding.recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onItemClick(int driverId) {
        getViewModel().updateDriverId(driverId);
        ((DashboardActivity) getActivity()).showLocationTypes();
    }

    @Override
    public void handleError(Throwable throwable) {

    }

    @Override
    public void setSystemList(List<ServiceCode> systemList) {

    }

    @Override
    public void onNext() {

    }

    public void searchQuery(String query) {
        List<DriverListResponse.Driver> temp = new ArrayList();
        for (DriverListResponse.Driver driver : getViewModel().getDriverList()) {
            //or use .equal(text) with you want equal match
            //use .toLowerCase() for better matches
            if (driver.getDisplaytitle().toLowerCase().contains(query.toLowerCase())) {
                temp.add(driver);
            }
        }
        //update recyclerview
        mAdapter.addItems(temp);
    }
}

