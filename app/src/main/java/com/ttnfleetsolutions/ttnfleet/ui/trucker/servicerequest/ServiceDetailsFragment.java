package com.ttnfleetsolutions.ttnfleet.ui.trucker.servicerequest;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.databinding.ObservableField;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.ttnfleetsolutions.ttnfleet.BuildConfig;
import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.BR;
import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.AdditionalJobResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.listing.Event;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.requirements.RequireImagesResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.AcceptWorkOrderResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.CompleteJobResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.EnrouteResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.survey.SubmitSurveyResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.survey.SurveyQuestionsResponse;
import com.ttnfleetsolutions.ttnfleet.databinding.FragmentServiceDetailsNewBinding;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseActivity;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseFragment;
import com.ttnfleetsolutions.ttnfleet.ui.empty.EmptyItemViewModel;
import com.ttnfleetsolutions.ttnfleet.ui.fleet.DashboardActivity;
import com.ttnfleetsolutions.ttnfleet.ui.serviceprovider.RejectWithReasonDialogFragment;
import com.ttnfleetsolutions.ttnfleet.ui.serviceprovider.ServiceProviderDashboardActivity;
import com.ttnfleetsolutions.ttnfleet.ui.technician.TechnicianDashboardActivity;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.MainActivity;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.MainActivityNew;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.form.FeedbackDialogFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.model.OrderStatus;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.model.TimeLineModel;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.servicerequest.jobs.JobsRecyclerViewAdapter;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.servicerequest.requirements.RequirementImageItemViewModel;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.servicerequest.requirements.RequirementImageRecyclerViewAdapter;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.servicerequest.timeline.TimelineAdapter;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.servicerequest.timeline.TimelineItemViewModel;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.servicerequest.workorder.WorkOrderRecyclerViewAdapter;
import com.ttnfleetsolutions.ttnfleet.utils.AppConstants;
import com.ttnfleetsolutions.ttnfleet.utils.CommonUtils;
import com.ttnfleetsolutions.ttnfleet.utils.geofence.GeofenceErrorMessages;
import com.ttnfleetsolutions.ttnfleet.utils.geofence.GeofenceTransitionsIntentService;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

/**
 * Created by Sandip Chaulagain on 12/7/2017.
 *
 * @version 1.0
 */

public class ServiceDetailsFragment extends BaseFragment<FragmentServiceDetailsNewBinding, ServiceDetailsViewModel>
        implements OnCompleteListener<Void>, ServiceDetailsNavigator, EmptyItemViewModel.EmptyItemViewModelListener {

    @Inject
    ServiceDetailsViewModel mServiceDetailsViewModel;

    FragmentServiceDetailsNewBinding mFragmentServiceDetailsNewBinding;

    @Inject
    WorkOrderRecyclerViewAdapter mWorkOrderRecyclerViewAdapter;

    @Inject
    JobsRecyclerViewAdapter mJobsRecyclerViewAdapter;

    @Inject
    RequirementImageRecyclerViewAdapter mRequirementImageRecyclerViewAdapter;

    private int mEventId;

    @Override
    protected ServiceDetailsViewModel getViewModel() {
        return mServiceDetailsViewModel;
    }

    @Override
    protected int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_service_details_new;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mServiceDetailsViewModel.setNavigator(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFragmentServiceDetailsNewBinding = getViewDataBinding();
        setUp();
    }

    private void setFabMargin() {
        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) mFragmentServiceDetailsNewBinding.chatFloatingActionButton.getLayoutParams();
        layoutParams.setMargins(0, 0, (int) getResources().getDimension(R.dimen.fab_margin), (int) getResources().getDimension(R.dimen.fab_margin_bottom));
        mFragmentServiceDetailsNewBinding.chatFloatingActionButton.setLayoutParams(layoutParams);
    }

    private void setUp() {
        if (getArguments() != null) {
            mEventId = getArguments().getInt("eventId");

            if(mEventId == 0) {
                mFragmentServiceDetailsNewBinding.nestedScrollView.setVisibility(View.GONE);
                mFragmentServiceDetailsNewBinding.status.setVisibility(View.GONE);
                getViewModel().setEmptyItemViewModel(new ObservableField<>(new EmptyItemViewModel(CommonUtils.getEmptyItem(getActivity(), DataManager.ErrorState.NO_DATA), this)));
                ((TextView) getActivity().findViewById(R.id.subTitle)).setText("Event Details");
                mFragmentServiceDetailsNewBinding.chatFloatingActionButton.setVisibility(View.GONE);
                return;
            }

            if (!((BaseActivity) getActivity()).isLandscape()) {
                if (getActivity() instanceof MainActivityNew)
                    ((MainActivityNew) getActivity()).mToolbar.setTitle("Event #" + mEventId);
                else if (getActivity() instanceof DashboardActivity)
                    ((DashboardActivity) getActivity()).mToolbar.setTitle("Event #" + mEventId);
                else if (getActivity() instanceof ServiceProviderDashboardActivity)
                    ((ServiceProviderDashboardActivity) getActivity()).mToolbar.setTitle("Event #" + mEventId);
                else if (getActivity() instanceof TechnicianDashboardActivity)
                    ((TechnicianDashboardActivity) getActivity()).mToolbar.setTitle("Event #" + mEventId);
            } else {
                ((TextView) getActivity().findViewById(R.id.subTitle)).setText("Event #" + mEventId);
            }

            /*if (getActivity() instanceof MainActivityNew) {
                if (!((BaseActivity) getActivity()).isLandscape())
                    ((BaseActivity) getActivity()).mToolbar.setTitle("Event #" + mEventId);
                else
                    ((TextView) getActivity().findViewById(R.id.subTitle)).setText("Event #" + mEventId);
            } else if (getActivity() instanceof DashboardActivity) {
                if (!((BaseActivity) getActivity()).isLandscape())
                    ((BaseActivity) getActivity()).mToolbar.setTitle("Event #" + mEventId);
                else
                    ((TextView) getActivity().findViewById(R.id.subTitle)).setText("Event #" + mEventId);
            } else if (getActivity() instanceof ServiceProviderDashboardActivity) {
                if (!((ServiceProviderDashboardActivity) getActivity()).isLandscape())
                    ((ServiceProviderDashboardActivity) getActivity()).mToolbar.setTitle("Event #" + mEventId);
                else
                    ((TextView) getActivity().findViewById(R.id.subTitle)).setText("Event #" + mEventId);
            } else if (getActivity() instanceof TechnicianDashboardActivity) {
                if (!((TechnicianDashboardActivity) getActivity()).isLandscape())
                    ((TechnicianDashboardActivity) getActivity()).mToolbar.setTitle("Event #" + mEventId);
                else
                    ((TextView) getActivity().findViewById(R.id.subTitle)).setText("Event #" + mEventId);
            }*/
        }
        getViewModel().setListener(mItemViewModelListener);
        mFragmentServiceDetailsNewBinding.workOrderRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mFragmentServiceDetailsNewBinding.workOrderRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));

        mFragmentServiceDetailsNewBinding.imagesRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        mRequirementImageRecyclerViewAdapter.setListener(mImagesItemViewModelListener);

        if (getViewModel().getEventDetails() == null)
            getDetails();
        else {
            getViewModel().setEventDetails(getViewModel().getEventDetails());
            loadJobRequirementImages(getViewModel().getRequirementImageList(), mEventId, getViewModel().getEventDetails().getWorkOrders().get(0).getJobs().get(0).getJobid());
        }

        // Empty list for storing geofences.
        mGeofenceList = new ArrayList<>();

        // Initially set the PendingIntent used in addGeofences() and removeGeofences() to null.
        mGeofencePendingIntent = null;

        setButtonsEnabledState();

        // Get the geofences used. Geofence data is hard coded in this sample.
        populateGeofenceList();

        mGeofencingClient = LocationServices.getGeofencingClient(getActivity());
        mFragmentServiceDetailsNewBinding.chatFloatingActionButton.setVisibility(View.GONE);


        //click listeners
        mFragmentServiceDetailsNewBinding.chatFloatingActionButton.setOnClickListener(view -> {
            if (getActivity() instanceof MainActivityNew)
                ((MainActivityNew) getActivity()).showServiceChatThreadDetails(0, "Service Request 1");
            else if (getActivity() instanceof ServiceProviderDashboardActivity)
                ((ServiceProviderDashboardActivity) getActivity()).showServiceChatThreadDetails(0, "Service Request 1");
            else if (getActivity() instanceof TechnicianDashboardActivity)
                ((TechnicianDashboardActivity) getActivity()).showServiceChatThreadDetails(0, "Service Request 1");
        });

        mFragmentServiceDetailsNewBinding.uploadImageLayout.setOnClickListener(v -> {
            if (getActivity() instanceof TechnicianDashboardActivity)
                ((TechnicianDashboardActivity) getActivity()).showJobRequirements(mEventId, getViewModel().getEventDetails().getWorkOrders().get(0).getJobs().get(0).getJobid());
        });

        mFragmentServiceDetailsNewBinding.addGeofence.setOnClickListener(view -> accessLocationServices());

        if (from == 1) {
            //drop off and pick up - new
            mFragmentServiceDetailsNewBinding.breakdownReportLabel.setText(getString(R.string.service_required));
            mFragmentServiceDetailsNewBinding.description.setText("Service Type: Drop off & Pick up");
            mFragmentServiceDetailsNewBinding.issueDescription.setText("Drop off: 8th Dec 12:45  |  Pick up: 9th Dec 08:00");
            mFragmentServiceDetailsNewBinding.datetime.setText("10 mins ago");
            mFragmentServiceDetailsNewBinding.serviceIssue.setText(getString(R.string.dummy_services));

            mFragmentServiceDetailsNewBinding.additionalJobLabel.setVisibility(View.GONE);
            mFragmentServiceDetailsNewBinding.additionalJobRecyclerView.setVisibility(View.GONE);
            mFragmentServiceDetailsNewBinding.serviceProviderDetailsLabel.setVisibility(View.GONE);
            mFragmentServiceDetailsNewBinding.serviceDispatcherName.setVisibility(View.GONE);
            mFragmentServiceDetailsNewBinding.technicianDetails.setVisibility(View.GONE);

            mFragmentServiceDetailsNewBinding.location.setText("3884 Eu Road, Boston, MA, 43448");
            mFragmentServiceDetailsNewBinding.vehicleName.setText("Maan Truck 8.136 FAE 4X4");
            mFragmentServiceDetailsNewBinding.vinNo.setText("WVM56502016034590");
        } else {
            //emergency roadside - ongoing
        }

        switch (index) {
            case 1:
                mFragmentServiceDetailsNewBinding.status.setText("NEW");
                mFragmentServiceDetailsNewBinding.status.setBackgroundTintList(getResources().getColorStateList(android.R.color.holo_orange_dark));
                break;
            case 2:
                mFragmentServiceDetailsNewBinding.status.setText("ONGOING");
                mFragmentServiceDetailsNewBinding.status.setBackgroundTintList(getResources().getColorStateList(R.color.colorPrimary));
                break;
            case 3:
                mFragmentServiceDetailsNewBinding.status.setText("COMPLETED");
                mFragmentServiceDetailsNewBinding.status.setBackgroundTintList(getResources().getColorStateList(android.R.color.holo_blue_dark));
                break;
        }

        /*mFragmentServiceDetailsNewBinding.nestedScrollView.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
            if (scrollY > oldScrollY) {
                mFragmentServiceDetailsNewBinding.chatFloatingActionButton.hide();
            } else {
                mFragmentServiceDetailsNewBinding.chatFloatingActionButton.show();
            }
        });*/

        setDataListItems();
    }

    public void updateEventDetails(int eventId) {
        this.mEventId = eventId;
        getDetails();
    }

    private void getDetails() {
        if (!isNetworkConnected()) {
            mFragmentServiceDetailsNewBinding.nestedScrollView.setVisibility(View.GONE);
            mFragmentServiceDetailsNewBinding.status.setVisibility(View.GONE);
            getViewModel().setEmptyItemViewModel(new ObservableField<>(new EmptyItemViewModel(CommonUtils.getEmptyItem(getActivity(), DataManager.ErrorState.NETWORK_ERROR), this)));
        } else {
            mFragmentServiceDetailsNewBinding.includedLayout.linearLayoutView.setVisibility(View.GONE);
            getViewModel().getEventDetailsAPICall(mEventId);
        }
    }


    @Override
    public void onRetryClick() {
        getDetails();
    }

    @Override
    public void loadEventDetails(Event event) {
        mFragmentServiceDetailsNewBinding.status.setVisibility(View.VISIBLE);
        mFragmentServiceDetailsNewBinding.nestedScrollView.setVisibility(View.VISIBLE);

        mWorkOrderRecyclerViewAdapter.addItems(event.getWorkOrders());

        if (event.getWorkOrders() != null && event.getWorkOrders().size() > 0)
            mJobsRecyclerViewAdapter.addItems(event.getWorkOrders().get(0).getJobs());

        mFragmentServiceDetailsNewBinding.acceptJob.setBackgroundColor(getViewModel().getDataManager().getThemeTitleBarColor());
        mFragmentServiceDetailsNewBinding.additionalJobButton.setBackgroundColor(getViewModel().getDataManager().getThemeTitleBarColor());
        mFragmentServiceDetailsNewBinding.surveyButton.setBackgroundColor(getViewModel().getDataManager().getThemeTitleBarColor());
        mFragmentServiceDetailsNewBinding.dispatchEventButton.setBackgroundColor(getViewModel().getDataManager().getThemeTitleBarColor());
        mFragmentServiceDetailsNewBinding.workOrderRecyclerView.setAdapter(mJobsRecyclerViewAdapter);
        if (event.getWorkOrders() != null && event.getWorkOrders().size() > 0) {
            if (getActivity() instanceof DashboardActivity) {
                if (event.getStatusid() != DataManager.EventStatus.CLOSED.getStatus()
                        && event.getStatusid() != DataManager.EventStatus.CANCELED.getStatus()
                        && event.getStatusid() != DataManager.EventStatus.COMPLETE.getStatus())
                    mFragmentServiceDetailsNewBinding.bottomLayoutFD.setVisibility(View.VISIBLE);
                int workOrderStatusId = event.getWorkOrders().get(0).getStatusid();
                if (workOrderStatusId == DataManager.Status.DISPATCH.getStatus()) {
                    setFabMargin();
                    mFragmentServiceDetailsNewBinding.dispatchEventButton.setVisibility(View.VISIBLE);
                    mFragmentServiceDetailsNewBinding.bottomLayoutDividerFD.setVisibility(View.VISIBLE);
                } else {
                    mFragmentServiceDetailsNewBinding.dispatchEventButton.setVisibility(View.GONE);
                    mFragmentServiceDetailsNewBinding.bottomLayoutDividerFD.setVisibility(View.GONE);
                }
            } else if (getActivity() instanceof ServiceProviderDashboardActivity) {
                int workOrderStatusId = event.getWorkOrders().get(0).getStatusid();
                if (workOrderStatusId == DataManager.Status.ACCEPT_REJECT_SP.getStatus()) {
                    setFabMargin();
                    mFragmentServiceDetailsNewBinding.bottomLayout.setVisibility(View.VISIBLE);
                }
            } else if (getActivity() instanceof TechnicianDashboardActivity) {
                if (event.getWorkOrders().get(0).getJobs() != null && event.getWorkOrders().get(0).getJobs().size() > 0) {
                    int jobStatusId = event.getWorkOrders().get(0).getJobs().get(0).getStatusid();
                    if (jobStatusId == DataManager.Status.ACCEPT_REJECT_TECH.getStatus()) {
                        setFabMargin();
                        mFragmentServiceDetailsNewBinding.bottomLayout.setVisibility(View.VISIBLE);
                    } else if (jobStatusId == DataManager.Status.ACCEPTED_SHOW_ENROUTE.getStatus()) {
                        setFabMargin();
                        mFragmentServiceDetailsNewBinding.enrouteButton.setVisibility(View.VISIBLE);
                    } else if (jobStatusId == DataManager.Status.ENROUTED.getStatus()) {
                        setFabMargin();
                        mFragmentServiceDetailsNewBinding.confirmBreakdownButton.setVisibility(View.VISIBLE);
                        mFragmentServiceDetailsNewBinding.uploadImageLayout.setVisibility(View.VISIBLE);
                    } else if (jobStatusId == DataManager.Status.REACHED.getStatus()) {
                        setFabMargin();
                        mFragmentServiceDetailsNewBinding.confirmBreakdownButton.setVisibility(View.VISIBLE);
                        mFragmentServiceDetailsNewBinding.uploadImageLayout.setVisibility(View.VISIBLE);
                    } else if (jobStatusId == DataManager.Status.CONFIRMED_BREAKDOWN.getStatus()) {
                        setFabMargin();
                        mFragmentServiceDetailsNewBinding.confirmBreakdownButton.setVisibility(View.GONE);
                        mFragmentServiceDetailsNewBinding.bottomLayout2.setVisibility(View.VISIBLE);
                        mFragmentServiceDetailsNewBinding.uploadImageLayout.setVisibility(View.VISIBLE);
                    } else {
                        mFragmentServiceDetailsNewBinding.bottomLayout2.setVisibility(View.GONE);
                        mFragmentServiceDetailsNewBinding.uploadImageLayout.setVisibility(View.GONE);
                    }
                }
            }

            /*for (int i = 0; i < event.getWorkOrders().size(); i++) {
                int statusId = event.getWorkOrders().get(i).getStatusid();
                if (getActivity() instanceof ServiceProviderDashboardActivity) {
                    //service provider
                    if (statusId == DataManager.Status.REQUESTED.getStatus()) {
                        setFabMargin();
                        mFragmentServiceDetailsNewBinding.bottomLayout.setVisibility(View.VISIBLE);
                    }
                } else if (getActivity() instanceof TechnicianDashboardActivity) {
                    //technician
                    for (int j = 0; j < event.getWorkOrders().get(i).getJobs().size(); j++) {
                        if (event.getWorkOrders().get(i).getJobs().get(j).getStatusid() == DataManager.Status.ACCEPT_REJECT.getStatus()) {
                            setFabMargin();
                            mFragmentServiceDetailsNewBinding.bottomLayout.setVisibility(View.VISIBLE);
                        }
                    }

                }
            }
            if (getActivity() instanceof TechnicianDashboardActivity && mFragmentServiceDetailsNewBinding.bottomLayout.getVisibility() == View.GONE) {
                setFabMargin();
                mFragmentServiceDetailsNewBinding.bottomLayout2.setVisibility(View.VISIBLE);
            }*/
        }

        if (getActivity() instanceof MainActivityNew) {
            if (event.getStatusid() == 3 || event.getStatusid() == 4) {
                mFragmentServiceDetailsNewBinding.surveyButton.setVisibility(View.VISIBLE);
            }
        }
    }

    private ServiceDetailsViewModel.ItemViewModelListener mItemViewModelListener = new ServiceDetailsViewModel.ItemViewModelListener() {
        @Override
        public void onAcceptButtonClick(int eventId, int workOrderId, int workOrderStatus, int jobId, int jobStatusId) {
            if (getActivity() instanceof ServiceProviderDashboardActivity) {
                ((ServiceProviderDashboardActivity) getActivity()).allocateJobToTechnician(eventId, workOrderId, workOrderStatus);
            } else if (getActivity() instanceof TechnicianDashboardActivity) {
                ((TechnicianDashboardActivity) getActivity()).getViewModel().acceptRejectJobAPICall(eventId, jobId, jobStatusId, "", 0, false);
            }
        }

        @Override
        public void onRejectButtonClick(int eventId, int workOrderId, int workOrderStatus, int jobId, int jobStatusId) {
            if (getActivity() instanceof ServiceProviderDashboardActivity) {
                ((ServiceProviderDashboardActivity) getActivity()).getViewModel().getRejectionTypesSP(eventId, workOrderId, workOrderStatus);
            } else if (getActivity() instanceof TechnicianDashboardActivity) {
                ((TechnicianDashboardActivity) getActivity()).getViewModel().getRejectionTypesTech(eventId, jobId, jobStatusId);
            }
        }

        @Override
        public void onEstimateButtonClick(int eventId, int workOrderId, int workOrderStatus, int jobId, int jobStatusId) {

        }

        @Override
        public void onEnrouteButtonClick(int eventId, int workOrderId, int workOrderStatus, int jobId, int jobStatusId) {
            if (getActivity() instanceof TechnicianDashboardActivity)
                ((TechnicianDashboardActivity) getActivity()).getViewModel().enrouteAPICall(eventId, jobStatusId);
        }

        @Override
        public void onConfirmBreakReportButtonClick(int eventId, int workOrderId, int workOrderStatus, int jobId, int jobStatusId, String eventType, String breakdown) {
            if (getActivity() instanceof TechnicianDashboardActivity)
                ((TechnicianDashboardActivity) getActivity()).jobInspections(eventId, eventType, breakdown);
        }

        @Override
        public void onAdditionalJobButtonClick(int eventId, int workOrderId, int workOrderStatus, int jobId, int jobStatusId) {
            if (getActivity() instanceof TechnicianDashboardActivity) {
                ((TechnicianDashboardActivity) getActivity()).getViewModel().initAdditionalJob(eventId, workOrderId, jobStatusId);
                ((TechnicianDashboardActivity) getActivity()).showSystemListing();
            }
        }

        @Override
        public void onCompleteJobButtonClick(int eventId, int workOrderId, int workOrderStatus, int jobId, int jobStatusId) {
            if (getActivity() instanceof TechnicianDashboardActivity) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("Are you sure you want to complete Event #" + eventId + "?");
                builder.setPositiveButton("YES", (dialogInterface, i) -> ((TechnicianDashboardActivity) getActivity()).getViewModel().completeJobAPICall(eventId, jobStatusId));
                builder.setNegativeButton("NO", (dialogInterface, i) -> dialogInterface.dismiss());
                builder.show();
            }
        }

        @Override
        public void onFeedbackButtonClick(int eventId, String vehicleName, String VIN, String serviceIssue, String datetime) {
            getViewModel().getSurveyQuestionsAPICall(eventId, vehicleName, VIN, serviceIssue, datetime);
        }

        @Override
        public void onDispatchEventButtonClick(int eventId) {
            ((DashboardActivity) getActivity()).dispatchService(eventId, true);
        }

        @Override
        public void onAddNoteFDButtonClick(int eventId, String note) {
            ((DashboardActivity) getActivity()).addNoteToEvent(eventId, note);
        }
    };

    @Override
    public void loadSurveyQuestions(SurveyQuestionsResponse surveyQuestionsResponse, int eventId, String vehicleName, String VIN, String serviceIssue, String datetime) {
        FeedbackDialogFragment fragment = new FeedbackDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("eventId", eventId);
        bundle.putInt("colorPrimary", getViewModel().getDataManager().getThemeTitleBarColor());
        bundle.putString("vehicleName", vehicleName);
        bundle.putString("VIN", VIN);
        bundle.putString("serviceIssue", serviceIssue);
        bundle.putString("datetime", datetime);
        bundle.putSerializable("survey", (Serializable) surveyQuestionsResponse.getResult());
        fragment.setArguments(bundle);
        fragment.setSubmitListener(surveySubmitListener);
        fragment.show(getActivity().getSupportFragmentManager(), "feedback");
    }

    public FeedbackDialogFragment.SubmitListener surveySubmitListener = surveyRequest -> getViewModel().submitSurveyAPICall(surveyRequest);

    @Override
    public void onSubmitSurveyResponse(SubmitSurveyResponse submitSurveyResponse) {
        Toast.makeText(getActivity(), "Successfully submitted!", Toast.LENGTH_LONG).show();
    }

    public RejectWithReasonDialogFragment.SubmitListener submitListener = (eventId, workOrderId, workOrderStatusId, jobId, jobStatusId, rejectionDescription, rejectionTypeId) -> {
        if (getActivity() instanceof ServiceProviderDashboardActivity)
            ((ServiceProviderDashboardActivity) getActivity()).getViewModel().rejectWorkOrdersAPICall(eventId, workOrderId, workOrderStatusId, rejectionDescription, rejectionTypeId);
        if (getActivity() instanceof TechnicianDashboardActivity)
            ((TechnicianDashboardActivity) getActivity()).getViewModel().acceptRejectJobAPICall(eventId, jobId, jobStatusId, rejectionDescription, rejectionTypeId, true);
    };

    @Override
    public void onRejectWorkOrders(AcceptWorkOrderResponse acceptWorkOrderResponse) {
        mFragmentServiceDetailsNewBinding.bottomLayout.setVisibility(View.GONE);
    }

    public void onAcceptWorkOrders() {
        mFragmentServiceDetailsNewBinding.bottomLayout.setVisibility(View.GONE);
    }

    public void onAcceptRejectJob(AcceptWorkOrderResponse acceptWorkOrderResponse) {
        mFragmentServiceDetailsNewBinding.bottomLayout.setVisibility(View.GONE);
    }

    public void onAdditionalJobCreation(AdditionalJobResponse additionalJobResponse) {
        getDetails();
    }

    public void onJobCompletion(CompleteJobResponse completeJobResponse) {
        getDetails();
    }

    public void onEnroute(EnrouteResponse enrouteResponse) {
        getDetails();
    }

    @Override
    public void handleErrorDetails(Throwable throwable) {
        mFragmentServiceDetailsNewBinding.nestedScrollView.setVisibility(View.GONE);
        mFragmentServiceDetailsNewBinding.status.setVisibility(View.GONE);
        getViewModel().setEmptyItemViewModel(new ObservableField<>(new EmptyItemViewModel(CommonUtils.getEmptyItem(getActivity(), DataManager.ErrorState.API_FAIL), this)));
    }

    @Override
    public void handleErrorSubmitSurvey(Throwable throwable) {
        Toast.makeText(getActivity(), getString(R.string.failed_submitting_survey), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void handleErrorSurveyQuestions(Throwable throwable) {
        Toast.makeText(getActivity(), getString(R.string.failed_loading_survey), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void handleErrorRequirementImages(Throwable throwable) {
        Toast.makeText(getActivity(), getString(R.string.failed_loading_requirements), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void loadJobRequirementImages(List<RequireImagesResponse.Image> imagesList, Integer eventId, int jobId) {
        if (imagesList != null && imagesList.size() > 0) {
            mFragmentServiceDetailsNewBinding.imagesRecyclerView.setVisibility(View.VISIBLE);
            mFragmentServiceDetailsNewBinding.requiredImages.setVisibility(View.VISIBLE);
            mRequirementImageRecyclerViewAdapter.addItems(imagesList);
            mFragmentServiceDetailsNewBinding.imagesRecyclerView.setAdapter(mRequirementImageRecyclerViewAdapter);
        }
    }

    private RequirementImageItemViewModel.ItemViewModelListener mImagesItemViewModelListener = (thumbView, url) -> zoomImageFromThumb(thumbView, url);

    public void reloadRequirementImages() {
        getViewModel().getRequirementImagesAPICall(getViewModel().getEventDetails().getWorkOrders().get(0).getJobs().get(0).getJobid());
    }

    public void onConfirmedBreakdown() {
        getDetails();
    }

    public void onDispatchedEvent() {
        getDetails();
    }

    // Hold a reference to the current animator,
    // so that it can be canceled mid-way.
    private Animator mCurrentAnimator;

    // The system "short" animation time duration, in milliseconds. This
    // duration is ideal for subtle animations or animations that occur
    // very frequently.
    private int mShortAnimationDuration;

    private void zoomImageFromThumb(final View thumbView, String url) {
        // Retrieve and cache the system's default "short" animation time.
        mShortAnimationDuration = getResources().getInteger(
                android.R.integer.config_shortAnimTime);

        // If there's an animation in progress, cancel it
        // immediately and proceed with this one.
        if (mCurrentAnimator != null) {
            mCurrentAnimator.cancel();
        }

        // Load the high-resolution "zoomed-in" image.
        Glide.with(getActivity())
                .load(url).into(mFragmentServiceDetailsNewBinding.expandedImage);

        // Calculate the starting and ending bounds for the zoomed-in image.
        // This step involves lots of math. Yay, math.
        final Rect startBounds = new Rect();
        final Rect finalBounds = new Rect();
        final Point globalOffset = new Point();

        // The start bounds are the global visible rectangle of the thumbnail,
        // and the final bounds are the global visible rectangle of the container
        // view. Also set the container view's offset as the origin for the
        // bounds, since that's the origin for the positioning animation
        // properties (X, Y).
        thumbView.getGlobalVisibleRect(startBounds);
        getActivity().findViewById(R.id.drawerLayout).getGlobalVisibleRect(finalBounds, globalOffset);
        startBounds.offset(-globalOffset.x, -globalOffset.y);
        finalBounds.offset(-globalOffset.x, -globalOffset.y);

        // Adjust the start bounds to be the same aspect ratio as the final
        // bounds using the "center crop" technique. This prevents undesirable
        // stretching during the animation. Also calculate the start scaling
        // factor (the end scaling factor is always 1.0).
        float startScale;
        if ((float) finalBounds.width() / finalBounds.height()
                > (float) startBounds.width() / startBounds.height()) {
            // Extend start bounds horizontally
            startScale = (float) startBounds.height() / finalBounds.height();
            float startWidth = startScale * finalBounds.width();
            float deltaWidth = (startWidth - startBounds.width()) / 2;
            startBounds.left -= deltaWidth;
            startBounds.right += deltaWidth;
        } else {
            // Extend start bounds vertically
            startScale = (float) startBounds.width() / finalBounds.width();
            float startHeight = startScale * finalBounds.height();
            float deltaHeight = (startHeight - startBounds.height()) / 2;
            startBounds.top -= deltaHeight;
            startBounds.bottom += deltaHeight;
        }

        // Hide the thumbnail and show the zoomed-in view. When the animation
        // begins, it will position the zoomed-in view in the place of the
        // thumbnail.
        thumbView.setAlpha(0f);
        mFragmentServiceDetailsNewBinding.expandedImage.setVisibility(View.VISIBLE);

        // Set the pivot point for SCALE_X and SCALE_Y transformations
        // to the top-left corner of the zoomed-in view (the default
        // is the center of the view).
        mFragmentServiceDetailsNewBinding.expandedImage.setPivotX(0f);
        mFragmentServiceDetailsNewBinding.expandedImage.setPivotY(0f);

        // Construct and run the parallel animation of the four translation and
        // scale properties (X, Y, SCALE_X, and SCALE_Y).
        AnimatorSet set = new AnimatorSet();
        set
                .play(ObjectAnimator.ofFloat(mFragmentServiceDetailsNewBinding.expandedImage, View.X,
                        startBounds.left, finalBounds.left))
                .with(ObjectAnimator.ofFloat(mFragmentServiceDetailsNewBinding.expandedImage, View.Y,
                        startBounds.top, finalBounds.top))
                .with(ObjectAnimator.ofFloat(mFragmentServiceDetailsNewBinding.expandedImage, View.SCALE_X,
                        startScale, 1f))
                .with(ObjectAnimator.ofFloat(mFragmentServiceDetailsNewBinding.expandedImage,
                        View.SCALE_Y, startScale, 1f));
        set.setDuration(mShortAnimationDuration);
        set.setInterpolator(new DecelerateInterpolator());
        set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mCurrentAnimator = null;
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                mCurrentAnimator = null;
            }
        });
        set.start();
        mCurrentAnimator = set;

        // Upon clicking the zoomed-in image, it should zoom back down
        // to the original bounds and show the thumbnail instead of
        // the expanded image.
        final float startScaleFinal = startScale;
        mFragmentServiceDetailsNewBinding.expandedImage.setOnClickListener(view -> {
            if (mCurrentAnimator != null) {
                mCurrentAnimator.cancel();
            }

            // Animate the four positioning/sizing properties in parallel,
            // back to their original values.
            AnimatorSet set1 = new AnimatorSet();
            set1.play(ObjectAnimator
                    .ofFloat(mFragmentServiceDetailsNewBinding.expandedImage, View.X, startBounds.left))
                    .with(ObjectAnimator
                            .ofFloat(mFragmentServiceDetailsNewBinding.expandedImage,
                                    View.Y, startBounds.top))
                    .with(ObjectAnimator
                            .ofFloat(mFragmentServiceDetailsNewBinding.expandedImage,
                                    View.SCALE_X, startScaleFinal))
                    .with(ObjectAnimator
                            .ofFloat(mFragmentServiceDetailsNewBinding.expandedImage,
                                    View.SCALE_Y, startScaleFinal));
            set1.setDuration(mShortAnimationDuration);
            set1.setInterpolator(new DecelerateInterpolator());
            set1.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    thumbView.setAlpha(1f);
                    mFragmentServiceDetailsNewBinding.expandedImage.setVisibility(View.GONE);
                    mCurrentAnimator = null;
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                    thumbView.setAlpha(1f);
                    mFragmentServiceDetailsNewBinding.expandedImage.setVisibility(View.GONE);
                    mCurrentAnimator = null;
                }
            });
            set1.start();
            mCurrentAnimator = set1;
        });
    }

    private int index = 0, from = 0;
    private List<TimeLineModel> mDataList = new ArrayList<>();
    private NotificationManager notificationManager;
    private NotificationCompat.Builder notificationBuilder;
    private static final String TAG = ServiceDetailsFragment.class.getSimpleName();
    int REQUEST_CHECK_SETTINGS = 2;

    /*@Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_service_details_new, container, false);

        *//*Toolbar toolbar = findViewById(R.id.baseToolbar);
        toolbar.setTitle("Service Request 1");
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }*//*

     *//*if (getArguments() != null) {
            index = getArguments().getInt("index", 0);
        }*//*


        mRecyclerView = mRootView.findViewById(R.id.timelineRecyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setNestedScrollingEnabled(false);

        RecyclerView additionalJobRecyclerView = mRootView.findViewById(R.id.additionalJobRecyclerView);
        additionalJobRecyclerView.setVisibility(View.GONE);
        additionalJobRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        additionalJobRecyclerView.setHasFixedSize(true);
        additionalJobRecyclerView.setNestedScrollingEnabled(false);
        additionalJobRecyclerView.setAdapter(new AdditionalJobsRecyclerViewAdapter(getActivity()));
        mRootView.findViewById(R.id.additionalJobLabel).setVisibility(View.GONE);
        initView();

        NestedScrollView nsv = mRootView.findViewById(R.id.nestedScrollView);
        nsv.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
            if (scrollY > oldScrollY) {
                ((FloatingActionButton) mRootView.findViewById(R.id.chatFloatingActionButton)).hide();
            } else {
                ((FloatingActionButton) mRootView.findViewById(R.id.chatFloatingActionButton)).show();
            }
        });
        mTimeLineAdapter.notifyDataSetChanged();
        return mRootView;
    }
*/
    public void updateIndex(int from, int index) {
        if (from == 1) {
            this.index = 1;
            this.from = 1;
        } else if (from == 2) {
            this.index = 2;
            this.from = 0;
        } else {
            this.index = 3;
            this.from = 0;
        }
       /* this.index = index;
        this.from = from;*/
    }

    @SuppressWarnings("deprecation")
    private void generateLocalNotification() {
        notificationManager = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationBuilder = new NotificationCompat.Builder(getActivity())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(this.getResources(),
                        R.mipmap.ic_launcher))
                .setContentTitle("Fleet Dispatcher")
                .setPriority(Notification.PRIORITY_MAX)
                .setStyle(new NotificationCompat.BigTextStyle().bigText("Our technician arrived at your location"))
                .setContentText("Our technician arrived at your location");

        Intent answerIntent = new Intent(getActivity(), MainActivity.class);
        answerIntent.setAction("Yes");
        PendingIntent pendingIntentYes = PendingIntent.getActivity(getActivity(), 1, answerIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        notificationBuilder.addAction(R.drawable.common_google_signin_btn_icon_dark, "Confirm", pendingIntentYes);

//        answerIntent.setAction("No");
//        PendingIntent pendingIntentNo = PendingIntent.getActivity(this, 1, answerIntent, PendingIntent.FLAG_UPDATE_CURRENT);
//        notificationBuilder.addAction(R.drawable.thumbs_down, "No", pendingIntentNo);


        sendNotification();
    }

    private void sendNotification() {
        Intent notificationIntent = new Intent(getActivity(), MainActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(getActivity(), 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        notificationBuilder.setContentIntent(contentIntent);
        Notification notification = notificationBuilder.build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notification.defaults |= Notification.DEFAULT_SOUND;
        int notificationId = 11;

        notificationManager.notify(notificationId, notification);
    }

    private void initView() {
//     mFragmentServiceDetailsNewBinding.chatFloatingActionButton).setOnClickListener(view -> ((MainActivity) getActivity()).showIssueList(0));
//        mRootView.findViewById(R.id.chatFloatingActionButton).setOnClickListener(view -> ((MainActivity) getActivity()).showServiceChatThreadDetails(0, "Service Request 1"));
//        mRootView.findViewById(R.id.addGeofence).setOnClickListener(view -> accessLocationServices());

        setDataListItems();
        /*mTimeLineAdapter = new TimeLineAdapter(mDataList, mOrientation);
        mRecyclerView.setAdapter(mTimeLineAdapter);*/


    }

    private void setDataListItems() {
        if (mDataList != null) {
            mDataList.clear();
        }
        if (index == 3) {
            mFragmentServiceDetailsNewBinding.status.setBackgroundTintList(getResources().getColorStateList(android.R.color.holo_blue_dark));
            mFragmentServiceDetailsNewBinding.status.setText("COMPLETED");
            mDataList.add(new TimeLineModel("Job completed", "2017-12-06 17:50", OrderStatus.ACTIVE, "Feedback"));
//        mDataList.add(new TimeLineModel("Service request has been successfully completed", "2017-12-06 20:55", OrderStatus.ACTIVE));
//        mDataList.add(new TimeLineModel("Additional tasks are completed", "2017-12-06 20:50", OrderStatus.INACTIVE));
//        mDataList.add(new TimeLineModel("There is some additional task to perform. The breaks are not working well.", "2017-12-06 19:00", OrderStatus.INACTIVE));
            mDataList.add(new TimeLineModel("Work in progress", "2017-12-06 17:20", OrderStatus.COMPLETED));
            mDataList.add(new TimeLineModel("ATA: 17:10", "2017-12-06 17:10", OrderStatus.COMPLETED));
        }

//        mDataList.add(new TimeLineModel("Technician supposed to arrive at location on  05.10, but will be late by 30 mins.", "2017-12-06 17:00", OrderStatus.ACTIVE));
        mDataList.add(new TimeLineModel("Technician is on his way.\nETA: 17:10", "2017-12-06 16:40", OrderStatus.ACTIVE, "Track Location"));
        mDataList.add(new TimeLineModel("Technician has been allocated by Fleet Dispatcher.\nETC: 1 hour", "2017-12-06 16:40", OrderStatus.COMPLETED));
        mDataList.add(new TimeLineModel("Fleet Dispatcher confirmed your service request. Technician will be allocated soon.", "2017-12-06 16:40", OrderStatus.COMPLETED));
        mDataList.add(new TimeLineModel("Service requested", "2017-12-06 16:35", OrderStatus.COMPLETED));


        if (index == 2) {
            mDataList.get(0).setStatus(OrderStatus.ACTIVE);
            mDataList.get(1).setStatus(OrderStatus.COMPLETED);
            mDataList.get(2).setStatus(OrderStatus.COMPLETED);
            mDataList.get(3).setStatus(OrderStatus.COMPLETED);
        }
        if (from == 1 && index == 1) {
            mDataList.clear();
            mDataList.add(new TimeLineModel("Search in progress...", "2017-12-06 16:40", OrderStatus.ACTIVE));
            mDataList.add(new TimeLineModel("Service requested", "2017-12-06 16:35", OrderStatus.COMPLETED));
        }
        if (index == 3) {
            mDataList.get(3).setStatus(OrderStatus.COMPLETED);
            mDataList.get(3).setAction("");
        }
        Collections.reverse(mDataList);

        mFragmentServiceDetailsNewBinding.timelineRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mFragmentServiceDetailsNewBinding.timelineRecyclerView.setHasFixedSize(true);
        mFragmentServiceDetailsNewBinding.timelineRecyclerView.setNestedScrollingEnabled(false);
        TimelineAdapter mTimeLineAdapter = new TimelineAdapter(mDataList);
        mTimeLineAdapter.setListener(mTimelineItemViewModelListener);
        mFragmentServiceDetailsNewBinding.timelineRecyclerView.setAdapter(mTimeLineAdapter);

        mFragmentServiceDetailsNewBinding.additionalJobRecyclerView.setVisibility(View.GONE);
        mFragmentServiceDetailsNewBinding.additionalJobRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mFragmentServiceDetailsNewBinding.additionalJobRecyclerView.setHasFixedSize(true);
        mFragmentServiceDetailsNewBinding.additionalJobRecyclerView.setNestedScrollingEnabled(false);
//        mFragmentServiceDetailsNewBinding.additionalJobRecyclerView.setAdapter(new AdditionalJobsRecyclerViewAdapter(getActivity()));
        mFragmentServiceDetailsNewBinding.additionalJobLabel.setVisibility(View.GONE);

    }


    private TimelineItemViewModel.TimelineItemViewModelListener mTimelineItemViewModelListener = action -> {
        if (!TextUtils.isEmpty(action)) {
            if (action.equalsIgnoreCase("Track Location")) {
                ((MainActivityNew) getActivity()).trackLocation(0);
            } else if (action.equalsIgnoreCase("Feedback")) {
                ((MainActivityNew) getActivity()).showFeedbackDialog();
            }
        }
    };



    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.service, menu);
        switch (index) {
            case 0:
                menu.getItem(0).setTitle("COMPLETED");
                break;
            default:
                menu.getItem(0).setTitle("ONGOING");
        }
//        menu.getItem(0).setVisible(false);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //Menu
        switch (item.getItemId()) {
            //When home is clicked
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }*/

//Geo Fencing Code

    /**
     * Tracks whether the user requested to add or remove geofences, or to do neither.
     */
    private enum PendingGeofenceTask {
        ADD, REMOVE, NONE
    }

    /**
     * Provides access to the Geofencing API.
     */
    private GeofencingClient mGeofencingClient;

    /**
     * The list of geofences used in this sample.
     */
    private ArrayList<Geofence> mGeofenceList;

    /**
     * Used when requesting to add or remove geofences.
     */
    private PendingIntent mGeofencePendingIntent;

    private PendingGeofenceTask mPendingGeofenceTask = PendingGeofenceTask.NONE;

    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;

    @Override
    public void onStart() {
        super.onStart();
        if (!checkPermissions()) {
        } else {
            performPendingGeofenceTask();
        }
    }

    /**
     * Builds and returns a GeofencingRequest. Specifies the list of geofences to be monitored.
     * Also specifies how the geofence notifications are initially triggered.
     */
    private GeofencingRequest getGeofencingRequest() {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();

        // The INITIAL_TRIGGER_ENTER flag indicates that geofencing service should trigger a
        // GEOFENCE_TRANSITION_ENTER notification when the geofence is added and if the device
        // is already inside that geofence.
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);

        // Add the geofences to be monitored by geofencing service.
        builder.addGeofences(mGeofenceList);

        // Return a GeofencingRequest.
        return builder.build();
    }

    /**
     * Adds geofences, which sets alerts to be notified when the device enters or exits one of the
     * specified geofences. Handles the success or failure results returned by addGeofences().
     */
    public void addGeofencesButtonHandler(View view) {
        if (!checkPermissions()) {
            mPendingGeofenceTask = PendingGeofenceTask.ADD;
            requestPermissions();
            return;
        }
        addGeofences();
    }

    /**
     * Adds geofences. This method should be called after the user has granted the location
     * permission.
     */
    @SuppressWarnings("MissingPermission")
    private void addGeofences() {
        if (!checkPermissions()) {
            showSnackbar(getString(R.string.insufficient_permissions));
            return;
        }

        mGeofencingClient.addGeofences(getGeofencingRequest(), getGeofencePendingIntent())
                .addOnCompleteListener(this);
    }

    /**
     * Removes geofences, which stops further notifications when the device enters or exits
     * previously registered geofences.
     */
    public void removeGeofencesButtonHandler(View view) {
        if (!checkPermissions()) {
            mPendingGeofenceTask = PendingGeofenceTask.REMOVE;
            requestPermissions();
            return;
        }
        removeGeofences();
    }

    /**
     * Removes geofences. This method should be called after the user has granted the location
     * permission.
     */
    @SuppressWarnings("MissingPermission")
    private void removeGeofences() {
        if (!checkPermissions()) {
            showSnackbar(getString(R.string.insufficient_permissions));
            return;
        }

        mGeofencingClient.removeGeofences(getGeofencePendingIntent()).addOnCompleteListener(this);
    }

    /**
     * Runs when the result of calling {@link #addGeofences()} and/or {@link #removeGeofences()}
     * is available.
     *
     * @param task the resulting Task, containing either a result or error.
     */
    @Override
    public void onComplete(@NonNull Task<Void> task) {
        mPendingGeofenceTask = PendingGeofenceTask.NONE;
        if (task.isSuccessful()) {
            updateGeofencesAdded(!getGeofencesAdded());
            setButtonsEnabledState();

            int messageId = getGeofencesAdded() ? R.string.geofences_added :
                    R.string.geofences_removed;
            Toast.makeText(getActivity(), getString(messageId), Toast.LENGTH_SHORT).show();
        } else {
            // Get the status code for the error and log it using a user-friendly message.
            String errorMessage = GeofenceErrorMessages.getErrorString(getActivity(), task.getException());
            Log.e(TAG, errorMessage);
        }
    }

    /**
     * Gets a PendingIntent to send with the request to add or remove Geofences. Location Services
     * issues the Intent inside this PendingIntent whenever a geofence transition occurs for the
     * current list of geofences.
     *
     * @return A PendingIntent for the IntentService that handles geofence transitions.
     */
    private PendingIntent getGeofencePendingIntent() {
        // Reuse the PendingIntent if we already have it.
        if (mGeofencePendingIntent != null) {
            return mGeofencePendingIntent;
        }
        Intent intent = new Intent(getActivity(), GeofenceTransitionsIntentService.class);
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when calling
        // addGeofences() and removeGeofences().
        return PendingIntent.getService(getActivity(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    /**
     * This sample hard codes geofence data. A real app might dynamically create geofences based on
     * the user's location.
     */
    private void populateGeofenceList() {
        for (Map.Entry<String, LatLng> entry : AppConstants.BAY_AREA_LANDMARKS.entrySet()) {

            mGeofenceList.add(new Geofence.Builder()
                    // Set the request ID of the geofence. This is a string to identify this
                    // geofence.
                    .setRequestId(entry.getKey())

                    // Set the circular region of this geofence.
                    .setCircularRegion(
                            entry.getValue().latitude,
                            entry.getValue().longitude,
                            AppConstants.GEOFENCE_RADIUS_IN_METERS
                    )

                    // Set the expiration duration of the geofence. This geofence gets automatically
                    // removed after this period of time.
                    .setExpirationDuration(AppConstants.GEOFENCE_EXPIRATION_IN_MILLISECONDS)

                    // Set the transition types of interest. Alerts are only generated for these
                    // transition. We track entry and exit transitions in this sample.
                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER |
                            Geofence.GEOFENCE_TRANSITION_EXIT)

                    // Create the geofence.
                    .build());
        }
    }

    /**
     * Ensures that only one button is enabled at any time. The Add Geofences button is enabled
     * if the user hasn't yet added geofences. The Remove Geofences button is enabled if the
     * user has added geofences.
     */
    private void setButtonsEnabledState() {
        if (getGeofencesAdded()) {
//            mAddGeofencesButton.setEnabled(false);
//            mRemoveGeofencesButton.setEnabled(true);
        } else {
//            mAddGeofencesButton.setEnabled(true);
//            mRemoveGeofencesButton.setEnabled(false);
        }
    }

    /**
     * Shows a {@link Snackbar} using {@code text}.
     *
     * @param text The Snackbar text.
     */
    private void showSnackbar(final String text) {
        View container = mFragmentServiceDetailsNewBinding.getRoot().findViewById(android.R.id.content);
        if (container != null) {
            Snackbar.make(container, text, Snackbar.LENGTH_LONG).show();
        }
    }

    /**
     * Shows a {@link Snackbar}.
     *
     * @param mainTextStringId The id for the string resource for the Snackbar text.
     * @param actionStringId   The text of the action item.
     * @param listener         The listener associated with the Snackbar action.
     */
    private void showSnackbar(final int mainTextStringId, final int actionStringId,
                              View.OnClickListener listener) {
        Snackbar.make(mFragmentServiceDetailsNewBinding.getRoot().findViewById(android.R.id.content),
                getString(mainTextStringId),
                Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(actionStringId), listener).show();
    }

    /**
     * Returns true if geofences were added, otherwise false.
     */
    private boolean getGeofencesAdded() {
        return PreferenceManager.getDefaultSharedPreferences(getActivity()).getBoolean(
                AppConstants.GEOFENCES_ADDED_KEY, false);
    }

    /**
     * Stores whether geofences were added ore removed in {@link};
     *
     * @param added Whether geofences were added or removed.
     */
    private void updateGeofencesAdded(boolean added) {
        PreferenceManager.getDefaultSharedPreferences(getActivity())
                .edit()
                .putBoolean(AppConstants.GEOFENCES_ADDED_KEY, added)
                .apply();
    }

    /**
     * Performs the geofencing task that was pending until location permission was granted.
     */
    private void performPendingGeofenceTask() {
        if (mPendingGeofenceTask == PendingGeofenceTask.ADD) {
            addGeofences();
        } else if (mPendingGeofenceTask == PendingGeofenceTask.REMOVE) {
            removeGeofences();
        }
    }

    /**
     * Return the current state of the permissions needed.
     */
    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                        Manifest.permission.ACCESS_FINE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.e(TAG, "Displaying permission rationale to provide additional context.");
            showSnackbar(R.string.permission_rationale, android.R.string.ok,
                    view -> {
                        // Request permission
                        ActivityCompat.requestPermissions(getActivity(),
                                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                REQUEST_PERMISSIONS_REQUEST_CODE);
                    });
        } else {
            Log.e(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    /*   */

    /**
     * Callback received when a permissions request has been completed.
     *//*
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Log.e(TAG, "onRequestPermissionResult");
        switch (requestCode) {

        }
    }*/
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_PERMISSIONS_REQUEST_CODE:
                if (grantResults.length <= 0) {
                    // If user interaction was interrupted, the permission request is cancelled and you
                    // receive empty arrays.
                    Log.e(TAG, "User interaction was cancelled.");
                } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e(TAG, "Permission granted.");
                    performPendingGeofenceTask();
                } else {
                    // Permission denied.

                    // Notify the user via a SnackBar that they have rejected a core permission for the
                    // app, which makes the Activity useless. In a real app, core permissions would
                    // typically be best requested during a welcome-screen flow.

                    // Additionally, it is important to remember that a permission might have been
                    // rejected without asking the user for permission (device policy or "Never ask
                    // again" prompts). Therefore, a user interface affordance is typically implemented
                    // when permissions are denied. Otherwise, your app could appear unresponsive to
                    // touches or interactions which have required permissions.
                    showSnackbar(R.string.permission_denied_explanation, R.string.settings,
                            view -> {
                                // Build intent that displays the App settings screen.
                                Intent intent = new Intent();
                                intent.setAction(
                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package",
                                        BuildConfig.APPLICATION_ID, null);
                                intent.setData(uri);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            });
                    mPendingGeofenceTask = PendingGeofenceTask.NONE;
                }
                break;
        }
    }

    private void accessLocationServices() {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setNeedBle(true);

        Task<LocationSettingsResponse> task =
                LocationServices.getSettingsClient(getActivity()).checkLocationSettings(builder.build());
        task.addOnCompleteListener(task1 -> {
            try {
                LocationSettingsResponse response = task1.getResult(ApiException.class);
                // All location settings are satisfied. The client can initialize location
                // requests here.
                addGeofencesButtonHandler(null);
            } catch (ApiException exception) {
                switch (exception.getStatusCode()) {
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the
                        // user a dialog.
                        try {
                            // Cast to a resolvable exception.
                            ResolvableApiException resolvable = (ResolvableApiException) exception;
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            resolvable.startResolutionForResult(
                                    getActivity(),
                                    REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException | ClassCastException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CHECK_SETTINGS) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    // All required changes were successfully made
                    showToast("RESULT_OK");
                    addGeofencesButtonHandler(null);
                    break;
                case Activity.RESULT_CANCELED:
                    showToast("You must turn on location service in order to add geofencing.");
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * Shows a toast with the given text.
     */
    private void showToast(String text) {
//        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }
}
