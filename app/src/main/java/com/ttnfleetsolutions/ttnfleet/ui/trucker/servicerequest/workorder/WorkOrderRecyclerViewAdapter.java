package com.ttnfleetsolutions.ttnfleet.ui.trucker.servicerequest.workorder;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.github.vipulasri.timelineview.TimelineView;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.listing.WorkOrder;
import com.ttnfleetsolutions.ttnfleet.databinding.RowWorkOrderBinding;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseViewHolder;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.servicerequest.timeline.TimelineItemViewModel;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */

public class WorkOrderRecyclerViewAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private List<WorkOrder> mList;

    private TimelineItemViewModel.TimelineItemViewModelListener mListener;

    public void setListener(TimelineItemViewModel.TimelineItemViewModelListener listener) {
        this.mListener = listener;
    }

    public WorkOrderRecyclerViewAdapter(List<WorkOrder> list) {
        this.mList = list;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RowWorkOrderBinding rowTimelineBinding = RowWorkOrderBinding.inflate(LayoutInflater.from(parent.getContext()),
                parent, false);
        return new MyViewHolder(rowTimelineBinding);
    }
    @Override
    public int getItemViewType(int position) {
        return TimelineView.getTimeLineViewType(position, getItemCount());
    }

    /*@Override
    public int getItemViewType(int position) {
        if (mList != null && mList.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }*/

    @Override
    public int getItemCount() {
        if (mList != null && mList.size() > 0) {
            return mList.size();
        } else {
            return 0;
        }
    }

    public void addItems(List<WorkOrder> list) {
        mList = list;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends BaseViewHolder implements WorkOrderItemViewModel.ItemViewModelListener {

        private RowWorkOrderBinding mBinding;

        private WorkOrderItemViewModel mItemViewModel;

        public MyViewHolder(RowWorkOrderBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {

            final WorkOrder workOrder = mList.get(position);

            mItemViewModel = new WorkOrderItemViewModel(workOrder, this);

            mBinding.setViewModel(mItemViewModel);

            // Immediate Binding
            // When a variable or observable changes, the binding will be scheduled to change before
            // the next frame. There are times, however, when binding must be executed immediately.
            // To force execution, use the executePendingBindings() method.
            mBinding.executePendingBindings();

        }

        @Override
        public void onItemClick(int id) {

        }
    }
}
