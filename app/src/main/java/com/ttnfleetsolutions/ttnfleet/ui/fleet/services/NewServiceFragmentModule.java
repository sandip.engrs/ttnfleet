package com.ttnfleetsolutions.ttnfleet.ui.fleet.services;

import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.dashboard.OngoingServiceAdapter;
import com.ttnfleetsolutions.ttnfleet.utils.rx.SchedulerProvider;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Sandip Chaulagain on 4/4/2018.
 *
 * @version 1.0
 */
@Module
public class NewServiceFragmentModule {

    @Provides
    NewServiceFragmentViewModel provideNewServiceFragmentViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        return new NewServiceFragmentViewModel(dataManager, schedulerProvider);
    }

    @Provides
    OngoingServiceAdapter provideNewServiceFragmentAdapter() {
        return new OngoingServiceAdapter(new ArrayList<>());
    }
}
