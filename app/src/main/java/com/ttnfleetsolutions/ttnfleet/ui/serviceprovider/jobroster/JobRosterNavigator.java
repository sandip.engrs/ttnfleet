package com.ttnfleetsolutions.ttnfleet.ui.serviceprovider.jobroster;

import com.ttnfleetsolutions.ttnfleet.data.model.api.event.listing.Event;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 1/10/2018.
 *
 * @version 1.0
 */

public interface JobRosterNavigator {

    void getEvents(List<Event> eventList);

    void handleError(Throwable throwable);
}
