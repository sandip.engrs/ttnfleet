package com.ttnfleetsolutions.ttnfleet.ui.fleet.provider;

import android.databinding.BindingAdapter;
import android.databinding.ObservableField;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.request.RequestOptions;
import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.data.model.api.serviceprovider.ServiceProvider;
import com.ttnfleetsolutions.ttnfleet.data.remote.ApiEndPoint;
import com.ttnfleetsolutions.ttnfleet.utils.GlideApp;

/**
 * Created by Sandip Chaulagain on 4/4/2018.
 *
 * @version 1.0
 */
public class ServiceProviderFragmentItemViewModel {

    private ServiceProvider mServiceProvider;
    public ObservableField<Integer> id;
    public ObservableField<String> name, location, icon;
    public ObservableField<Boolean> isAvailable;
    private ItemViewModelListener mListener;

    ServiceProviderFragmentItemViewModel(ServiceProvider serviceProvider, ItemViewModelListener listener) {
        this.mServiceProvider = serviceProvider;
        this.mListener = listener;
        id = new ObservableField<>(serviceProvider.getServiceproviderid());

        if (serviceProvider.getLocation() != null)
            location = new ObservableField<>(serviceProvider.getLocation().getAddress());

        isAvailable = new ObservableField<>(false);

        if (serviceProvider.getUser() != null) {
            name = new ObservableField<>(serviceProvider.getUser().getFirstname() + " " + serviceProvider.getUser().getLastname());
            icon = new ObservableField<>(ApiEndPoint.DIR_IMAGE_PROFILE + serviceProvider.getUserid() + "/" + serviceProvider.getUser().getImage());
        }

    }

    public void onItemClick() {
        mListener.onItemClick(mServiceProvider.getServiceproviderid(), name.get());
    }

    public interface ItemViewModelListener {
        void onItemClick(int id, String name);
    }

    @BindingAdapter({"profileImage"})
    public static void setImageViewResource(View view, String url) {
        GlideApp.with(view.getContext())
                .load(url)
                .apply(RequestOptions.circleCropTransform())
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .into((ImageView) view);
    }
}
