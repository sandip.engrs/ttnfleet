package com.ttnfleetsolutions.ttnfleet.ui.fleet;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseActivity;
import com.ttnfleetsolutions.ttnfleet.ui.fleet.services.NewServiceFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sandip Chaulagain on 12/8/2017.
 *
 * @version 1.0
 */

public class JobDashboardFragment extends Fragment {

    private TabLayout mTabLayout;
    private int mColorPrimary, mColorPrimaryDark;
    private ViewPager mViewPager;
    private ViewPagerAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_job_dashboard_fd, container, false);

        if (getArguments() != null) {
            mColorPrimary = getArguments().getInt("colorPrimary");
            mColorPrimaryDark = getArguments().getInt("colorPrimaryDark");
        }
        mViewPager = rootView.findViewById(R.id.viewpager);
        mTabLayout = rootView.findViewById(R.id.tabs);

        mTabLayout.setBackgroundColor(mColorPrimary);
        setupViewPager(mViewPager);
        return rootView;
    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getChildFragmentManager());

        Fragment newFragment = new NewServiceFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("status", DataManager.FilterDashboardHistory.NEW.getType());
        newFragment.setArguments(bundle);
        adapter.addFragment(newFragment, "REQUESTED");

        Fragment ongoingFragment = new NewServiceFragment();
        bundle = new Bundle();
        bundle.putInt("status", DataManager.FilterDashboardHistory.ACTIVE.getType());
        ongoingFragment.setArguments(bundle);
        adapter.addFragment(ongoingFragment, "ACTIVE");

        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (((BaseActivity) getActivity()).isLandscape())
                    ((NewServiceFragment) adapter.getItem(position)).loadFirstItemId();
               /* switch (position) {
                    case 0:
                        ((NewServiceFragment) adapter.getItem(0)).loadFirstItemId();
                        break;
                    case 1:
                        ((DashboardActivity) getActivity()).goToOrderDetailsFromSwipe(((NewServiceFragment) adapter.getItem(1)).getFirstItemId());
                        break;
                }*/
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mTabLayout.setupWithViewPager(viewPager);
    }

    public void onDispatchedEvent() {
        setupViewPager(mViewPager);
    }

    public void onDispatchedEventFromDetails() {
        ((DashboardActivity) getActivity()).getViewModel().isReloadNeeded = true;
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        /*@Override
        public Parcelable saveState() {
            return null;
        }*/

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

    }
}
