package com.ttnfleetsolutions.ttnfleet.ui.trucker.dashboard;

import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.listing.Event;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseViewModel;
import com.ttnfleetsolutions.ttnfleet.utils.rx.SchedulerProvider;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 11/10/2017.
 *
 * @version 1.0
 */

public class OngoingServicesViewModel extends BaseViewModel<OngoingServicesNavigator> {

    OngoingServicesViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    private List<Event> mEventList;

    public List<Event> getEventList() {
        return mEventList;
    }

    public void setEventList(List<Event> mEventList) {
        this.mEventList = mEventList;
    }

    void getActiveEvents() {
//        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .doServerGetEventsApiCall(getDataManager().getCurrentUserID())
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    setEventList(response.getResult());
                    getNavigator().getActiveEvents(response.getResult());
//                    setIsLoading(false);
                }, throwable -> {
//                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                }));
    }
}
