package com.ttnfleetsolutions.ttnfleet.ui.technician.additional;

import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseViewModel;
import com.ttnfleetsolutions.ttnfleet.utils.rx.SchedulerProvider;

/**
 * Created by Sandip Chaulagain on 11/10/2017.
 *
 * @version 1.0
 */

public class CreateAdditionalJobViewModel extends BaseViewModel<CreateAdditionalJobNavigator> {

    CreateAdditionalJobViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

}
