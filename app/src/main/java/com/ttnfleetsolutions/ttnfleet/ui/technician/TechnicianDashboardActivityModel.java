package com.ttnfleetsolutions.ttnfleet.ui.technician;

import android.util.Log;

import com.google.gson.Gson;
import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.AdditionalJobRequest;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.AcceptRejectJobRequest;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.CompleteJobRequest;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.EnrouteRequest;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseViewModel;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.details.CreateEventFragment;
import com.ttnfleetsolutions.ttnfleet.utils.rx.SchedulerProvider;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Sandip Chaulagain on 12/22/2017.
 *
 * @version 1.0
 */

public class TechnicianDashboardActivityModel extends BaseViewModel {

    public TechnicianDashboardActivityModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    public boolean isReloadNeeded = false;

    //Technician
    public void getRejectionTypesTech(int eventId, int jobId, int jobStatusId) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .doServerGetRejectTypesTechApiCall()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    ((TechnicianDashboardNavigator) getNavigator()).getRejectionTypesTech(eventId, jobId, jobStatusId, response.getResult());
//                    getNavigator().getRejectionTypesTech(eventId, jobId, jobStatusId, response.getResult());
                    setIsLoading(false);
                }, throwable -> {
                    setIsLoading(false);
                    ((TechnicianDashboardNavigator) getNavigator()).handleError(throwable);
//                    getNavigator().handleError(throwable);
                }));
    }

    private JSONObject getAcceptRejectJobRequest(int eventId, int jobId, int jobStatusId, String rejectionDescription, int rejectionTypeId, boolean isRejected) {
        AcceptRejectJobRequest acceptRejectJobRequest = new AcceptRejectJobRequest(jobId, jobStatusId, isRejected,
                getDataManager().getCurrentUserID(), eventId, rejectionDescription, rejectionTypeId);
        try {
            JSONObject jsonObject = new JSONObject(new Gson().toJson(acceptRejectJobRequest, AcceptRejectJobRequest.class));
            JSONObject dataObject = new JSONObject();
            dataObject.put("data", jsonObject);
            Log.e(CreateEventFragment.class.getSimpleName(), "Json -- " + dataObject.toString());
            return dataObject;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void acceptRejectJobAPICall(int eventId, int jobId, int jobStatusId, String rejectionDescription, int rejectionTypeId, boolean isRejected) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .doServerAcceptRejectJobApiCall(getAcceptRejectJobRequest(eventId, jobId, jobStatusId, rejectionDescription, rejectionTypeId, isRejected))
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    ((TechnicianDashboardNavigator) getNavigator()).onAcceptRejectJob(response);
//                    getNavigator().onAcceptRejectJob(response);
                    setIsLoading(false);
                }, throwable -> {
                    setIsLoading(false);
                    ((TechnicianDashboardNavigator) getNavigator()).handleError(throwable);
//                    getNavigator().handleError(throwable);
                }));
    }

    public void initAdditionalJob(int eventId, int workOrderId, int jobStatusId) {
        Gson gson = new Gson();
        AdditionalJobRequest additionalJobRequest = gson.fromJson(getDataManager().getAdditionalJobRequest(), AdditionalJobRequest.class);
        if (additionalJobRequest == null) additionalJobRequest = new AdditionalJobRequest();
        additionalJobRequest.setEventId(eventId);
        additionalJobRequest.setWorkorderId(workOrderId);
        additionalJobRequest.setJobStatusId(jobStatusId);
        additionalJobRequest.setTechnicianId(getDataManager().getCurrentUserID());

        getDataManager().setAdditionalJobRequest(gson.toJson(additionalJobRequest, AdditionalJobRequest.class));
    }

    private JSONObject getAdditionalJobRequest(String note, String mSystemCode, String mAssemblyCode, String componentCodeArray) {
        Gson gson = new Gson();
        AdditionalJobRequest additionalJobRequest = gson.fromJson(getDataManager().getAdditionalJobRequest(), AdditionalJobRequest.class);
        AdditionalJobRequest.SystemCode systemCode = additionalJobRequest.getSystemCode();
        if (systemCode == null) systemCode = additionalJobRequest.new SystemCode();
        additionalJobRequest.setAdditionalJobNote(note);
        systemCode.setCode1(mSystemCode);

        AdditionalJobRequest.SystemCode.AssemblyCode assemblyCode = systemCode.getAssemblyCode();

        if (assemblyCode == null) assemblyCode = systemCode.new AssemblyCode();
        assemblyCode.setCode2(mAssemblyCode);
        systemCode.setAssemblyCode(assemblyCode);
        additionalJobRequest.setSystemCode(systemCode);
        getDataManager().setAdditionalJobRequest(gson.toJson(additionalJobRequest, AdditionalJobRequest.class));

        try {
            JSONObject jsonObject = new JSONObject(new Gson().toJson(additionalJobRequest, AdditionalJobRequest.class));
            JSONObject dataObject = new JSONObject();
            dataObject.put("data", jsonObject);
            Log.e(CreateEventFragment.class.getSimpleName(), "Json -- " + dataObject.toString());
            return dataObject;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    void additionalJobAPICall(String note, String mSystemCode, String mAssemblyCode, String componentCodeArray) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .doServerCreateAdditionalJobApiCall(getAdditionalJobRequest(note, mSystemCode, mAssemblyCode, componentCodeArray))
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    ((TechnicianDashboardNavigator) getNavigator()).onAdditionalJob(response);
                    setIsLoading(false);
                }, throwable -> {
                    setIsLoading(false);
                    ((TechnicianDashboardNavigator) getNavigator()).handleError(throwable);
                }));
    }

    private JSONObject getCompleteJobRequest(int jobId, int jobStatusId) {
        CompleteJobRequest completeJobRequest = new CompleteJobRequest();
        completeJobRequest.setJobId(jobId);
        completeJobRequest.setJobStatusId(jobStatusId);
        try {
            JSONObject jsonObject = new JSONObject(new Gson().toJson(completeJobRequest, CompleteJobRequest.class));
            JSONObject dataObject = new JSONObject();
            dataObject.put("data", jsonObject);
            Log.e(CreateEventFragment.class.getSimpleName(), "Json -- " + dataObject.toString());
            return dataObject;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void completeJobAPICall(int jobId, int jobStatusId) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .doServerCompleteJobApiCall(getCompleteJobRequest(jobId, jobStatusId))
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    ((TechnicianDashboardNavigator) getNavigator()).onCompleteJob(response);
                    setIsLoading(false);
                }, throwable -> {
                    setIsLoading(false);
                    ((TechnicianDashboardNavigator) getNavigator()).handleError(throwable);
                }));
    }

    private JSONObject getEnrouteRequest(int jobId, int jobStatusId) {
        EnrouteRequest enrouteRequest = new EnrouteRequest();
        enrouteRequest.setJobId(jobId);
        enrouteRequest.setJobStatusId(jobStatusId);
        try {
            JSONObject jsonObject = new JSONObject(new Gson().toJson(enrouteRequest, EnrouteRequest.class));
            JSONObject dataObject = new JSONObject();
            dataObject.put("data", jsonObject);
            Log.e(CreateEventFragment.class.getSimpleName(), "Json -- " + dataObject.toString());
            return dataObject;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void enrouteAPICall(int jobId, int jobStatusId) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .doServerEnrouteApiCall(getEnrouteRequest(jobId, jobStatusId))
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    ((TechnicianDashboardNavigator) getNavigator()).onEnroute(response);
                    setIsLoading(false);
                }, throwable -> {
                    setIsLoading(false);
                    ((TechnicianDashboardNavigator) getNavigator()).handleError(throwable);
                }));
    }
}

