package com.ttnfleetsolutions.ttnfleet.ui.login;

import android.content.Context;
import android.content.Intent;
import android.databinding.ObservableField;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Toast;

import com.ttnfleetsolutions.ttnfleet.BR;
import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.databinding.ActivityLoginBinding;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseActivity;
import com.ttnfleetsolutions.ttnfleet.ui.empty.EmptyItemViewModel;
import com.ttnfleetsolutions.ttnfleet.ui.fleet.DashboardActivity;
import com.ttnfleetsolutions.ttnfleet.ui.serviceprovider.ServiceProviderDashboardActivity;
import com.ttnfleetsolutions.ttnfleet.ui.technician.TechnicianDashboardActivity;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.MainActivityNew;
import com.ttnfleetsolutions.ttnfleet.utils.CommonUtils;

import javax.inject.Inject;

/**
 * Created by Sandip Chaulagain on 10/4/2017.
 *
 * @version 1.0
 */

public class LoginActivity extends BaseActivity<ActivityLoginBinding, LoginViewModel> implements LoginNavigator, EmptyItemViewModel.EmptyItemViewModelListener  {

    @Inject
    LoginViewModel mLoginViewModel;

    private ActivityLoginBinding mActivityLoginBinding;

    private int selectedRole = 2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityLoginBinding = getViewDataBinding();
        mLoginViewModel.setNavigator(this);
        setUp();
    }

    public static Intent getStartIntent(Context context) {
        return new Intent(context, LoginActivity.class);
    }

    private void setUp() {
        mActivityLoginBinding.driver.setOnClickListener(view -> {
            selectedRole = 1;
            getViewModel().getDataManager().setGUID("596QE9U7-E3D9-45YO-A525-G9WEGUKIB5XA");
            mActivityLoginBinding.driver.setTextColor(Color.WHITE);
            mActivityLoginBinding.driver.setBackgroundResource(R.drawable.bg_round_corner_filled);

            mActivityLoginBinding.technician.setTextColor(ContextCompat.getColor(LoginActivity.this, R.color.colorPrimary));
            mActivityLoginBinding.technician.setBackgroundResource(R.drawable.bg_round_corner_outline);

            mActivityLoginBinding.fleetDispatcher.setTextColor(ContextCompat.getColor(LoginActivity.this, R.color.colorPrimary));
            mActivityLoginBinding.fleetDispatcher.setBackgroundResource(R.drawable.bg_round_corner_outline);

            mActivityLoginBinding.serviceDispatcher.setTextColor(ContextCompat.getColor(LoginActivity.this, R.color.colorPrimary));
            mActivityLoginBinding.serviceDispatcher.setBackgroundResource(R.drawable.bg_round_corner_outline);

        });
        mActivityLoginBinding.technician.setOnClickListener(view -> {
            getViewModel().getDataManager().setGUID("596QE9U7-FZMD-40AP-E865-2RG1RWB2Q2XA");
            selectedRole = 4;
            mActivityLoginBinding.technician.setTextColor(Color.WHITE);
            mActivityLoginBinding.technician.setBackgroundResource(R.drawable.bg_round_corner_filled);

            mActivityLoginBinding.driver.setTextColor(ContextCompat.getColor(LoginActivity.this, R.color.colorPrimary));
            mActivityLoginBinding.driver.setBackgroundResource(R.drawable.bg_round_corner_outline);

            mActivityLoginBinding.fleetDispatcher.setTextColor(ContextCompat.getColor(LoginActivity.this, R.color.colorPrimary));
            mActivityLoginBinding.fleetDispatcher.setBackgroundResource(R.drawable.bg_round_corner_outline);

            mActivityLoginBinding.serviceDispatcher.setTextColor(ContextCompat.getColor(LoginActivity.this, R.color.colorPrimary));
            mActivityLoginBinding.serviceDispatcher.setBackgroundResource(R.drawable.bg_round_corner_outline);
        });
        mActivityLoginBinding.fleetDispatcher.setOnClickListener(view -> {
            getViewModel().getDataManager().setGUID("596QE9U7-H5VS-94WF-N456-ZVEF4OA4ZEXA");
            selectedRole = 2;
            mActivityLoginBinding.fleetDispatcher.setTextColor(Color.WHITE);
            mActivityLoginBinding.fleetDispatcher.setBackgroundResource(R.drawable.bg_round_corner_filled);

            mActivityLoginBinding.technician.setTextColor(ContextCompat.getColor(LoginActivity.this, R.color.colorPrimary));
            mActivityLoginBinding.technician.setBackgroundResource(R.drawable.bg_round_corner_outline);

            mActivityLoginBinding.driver.setTextColor(ContextCompat.getColor(LoginActivity.this, R.color.colorPrimary));
            mActivityLoginBinding.driver.setBackgroundResource(R.drawable.bg_round_corner_outline);

            mActivityLoginBinding.serviceDispatcher.setTextColor(ContextCompat.getColor(LoginActivity.this, R.color.colorPrimary));
            mActivityLoginBinding.serviceDispatcher.setBackgroundResource(R.drawable.bg_round_corner_outline);
        });
        mActivityLoginBinding.serviceDispatcher.setOnClickListener(view -> {
            getViewModel().getDataManager().setGUID("596QE9U7-3X5K-14ZI-A985-64V2TZ3IUQXA");
            selectedRole = 3;
            mActivityLoginBinding.serviceDispatcher.setTextColor(Color.WHITE);
            mActivityLoginBinding.serviceDispatcher.setBackgroundResource(R.drawable.bg_round_corner_filled);

            mActivityLoginBinding.technician.setTextColor(ContextCompat.getColor(LoginActivity.this, R.color.colorPrimary));
            mActivityLoginBinding.technician.setBackgroundResource(R.drawable.bg_round_corner_outline);

            mActivityLoginBinding.fleetDispatcher.setTextColor(ContextCompat.getColor(LoginActivity.this, R.color.colorPrimary));
            mActivityLoginBinding.fleetDispatcher.setBackgroundResource(R.drawable.bg_round_corner_outline);

            mActivityLoginBinding.driver.setTextColor(ContextCompat.getColor(LoginActivity.this, R.color.colorPrimary));
            mActivityLoginBinding.driver.setBackgroundResource(R.drawable.bg_round_corner_outline);
        });

        mActivityLoginBinding.password.setOnEditorActionListener((textView, actionId, keyEvent) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                mActivityLoginBinding.signIn.performClick();
                return true;
            }
            return false;
        });

    }

    private void loginIn() {
        mActivityLoginBinding.includedLayout.linearLayoutView.setVisibility(View.GONE);
        if (!isNetworkConnected()) {
            mActivityLoginBinding.includedLayout.linearLayoutView.setVisibility(View.VISIBLE);
            getViewModel().setEmptyItemViewModel(new ObservableField<>(new EmptyItemViewModel(CommonUtils.getEmptyItem(this, DataManager.ErrorState.NETWORK_ERROR), this)));

        } else if (!TextUtils.isEmpty(getViewModel().getDataManager().getGUID()))

            mLoginViewModel.getAuthentication(mActivityLoginBinding.email.getText().toString(), mActivityLoginBinding.password.getText().toString());

        else
            mActivityLoginBinding.loginWithoutLink.setVisibility(View.GONE);
    }

    @Override
    public void onRetryClick() {

        mActivityLoginBinding.loginWithoutLink.setVisibility(View.GONE);
        mActivityLoginBinding.emailTextInputLayout.setVisibility(View.VISIBLE);
        mActivityLoginBinding.passwordTextInputLayout.setVisibility(View.VISIBLE);
        mActivityLoginBinding.selectRoleLayout.setVisibility(View.VISIBLE);
        mActivityLoginBinding.signIn.setVisibility(View.VISIBLE);
        mActivityLoginBinding.includedLayout.linearLayoutView.setVisibility(View.GONE);

    }

    @Override
    public LoginViewModel getViewModel() {
        return mLoginViewModel;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    public void openTruckerActivity() {
        Intent intent = MainActivityNew.getStartIntent(LoginActivity.this);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    public void openFleetDispatcherActivity() {
        Intent intent = DashboardActivity.getStartIntent(LoginActivity.this);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    public void openServiceProviderActivity() {
        Intent intent = ServiceProviderDashboardActivity.getStartIntent(LoginActivity.this);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    public void openServiceRepActivity() {
        Intent intent = TechnicianDashboardActivity.getStartIntent(LoginActivity.this);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    public void handleError(Throwable throwable) {
        mActivityLoginBinding.includedLayout.linearLayoutView.setVisibility(View.VISIBLE);
        mActivityLoginBinding.loginWithoutLink.setVisibility(View.GONE);
        mActivityLoginBinding.emailTextInputLayout.setVisibility(View.GONE);
        mActivityLoginBinding.passwordTextInputLayout.setVisibility(View.GONE);
        mActivityLoginBinding.selectRoleLayout.setVisibility(View.GONE);
        mActivityLoginBinding.signIn.setVisibility(View.GONE);
        getViewModel().setEmptyItemViewModel(new ObservableField<>(new EmptyItemViewModel(CommonUtils.getEmptyItem(this, DataManager.ErrorState.API_FAIL), this)));
    }

    @Override
    public void login() {
        String email = mActivityLoginBinding.email.getText().toString();
        String password = mActivityLoginBinding.password.getText().toString();
//        int selectedRole = mActivityLoginBinding.selectRoles.getSelectedItemPosition();
        if (/*mLoginViewModel.isEmailAndPasswordValid(email, password)*/true) {
            hideKeyboard();
            if (!isNetworkConnected())
                showInternetConnectionErrorDialog();
            else if (!TextUtils.isEmpty(getViewModel().getDataManager().getGUID()))
                mLoginViewModel.getAuthentication(email, password);
            else
                mLoginViewModel.getAccessToken(email, password, selectedRole);
        } else {
            Toast.makeText(this, getString(R.string.invalid_email_password), Toast.LENGTH_SHORT).show();
        }
    }
}
