package com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices;

import com.google.gson.Gson;
import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.EventRequest;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseViewModel;
import com.ttnfleetsolutions.ttnfleet.utils.rx.SchedulerProvider;

/**
 * Created by Sandip Chaulagain on 11/10/2017.
 *
 * @version 1.0
 */

public class MainServicesViewModel extends BaseViewModel<MainServicesNavigator> {

    public MainServicesViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    public void getLocationTypes(int eventType) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager().doServerLocationTypeApiCall(String.valueOf(eventType), getDataManager().getCurrentUserID())
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    setIsLoading(false);
                    updateEventType(eventType);
                    getNavigator().setLocationTypeList(eventType, response.getResult());
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                }));
    }

    private void updateEventType(int eventType) {
        Gson gson = new Gson();
        EventRequest eventRequest = gson.fromJson(getDataManager().getEventRequest(), EventRequest.class);
        eventRequest.setEventType(eventType);
        getDataManager().setEventRequest(gson.toJson(eventRequest, EventRequest.class));
    }
}
