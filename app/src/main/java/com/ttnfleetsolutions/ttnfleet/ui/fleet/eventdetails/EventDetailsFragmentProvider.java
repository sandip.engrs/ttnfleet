package com.ttnfleetsolutions.ttnfleet.ui.fleet.eventdetails;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Sandip Chaulagain on 2/2/2018.
 *
 * @version 1.0
 */
@Module
public abstract class EventDetailsFragmentProvider {

    @ContributesAndroidInjector(modules = EventDetailsFragmentModule.class)
    abstract EventDetailsFragment provideEventDetailsFragmentFactory();
}
