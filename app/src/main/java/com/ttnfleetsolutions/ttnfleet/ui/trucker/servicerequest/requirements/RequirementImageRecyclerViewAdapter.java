package com.ttnfleetsolutions.ttnfleet.ui.trucker.servicerequest.requirements;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.vipulasri.timelineview.TimelineView;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.requirements.RequireImagesResponse;
import com.ttnfleetsolutions.ttnfleet.databinding.RowImageBinding;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseViewHolder;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */

public class RequirementImageRecyclerViewAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private List<RequireImagesResponse.Image> mList;

    private RequirementImageItemViewModel.ItemViewModelListener mListener;

    public void setListener(RequirementImageItemViewModel.ItemViewModelListener listener) {
        this.mListener = listener;
    }

    public RequirementImageRecyclerViewAdapter(List<RequireImagesResponse.Image> list) {
        this.mList = list;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RowImageBinding rowImageBinding = RowImageBinding.inflate(LayoutInflater.from(parent.getContext()),
                parent, false);
        return new MyViewHolder(rowImageBinding);
    }

    @Override
    public int getItemViewType(int position) {
        return TimelineView.getTimeLineViewType(position, getItemCount());
    }

    /*@Override
    public int getItemViewType(int position) {
        if (mList != null && mList.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }*/

    @Override
    public int getItemCount() {
        if (mList != null && mList.size() > 0) {
            return mList.size();
        } else {
            return 0;
        }
    }

    public void addItems(List<RequireImagesResponse.Image> list) {
        mList = list;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends BaseViewHolder implements RequirementImageItemViewModel.ItemViewModelListener {

        private RowImageBinding mBinding;

        private RequirementImageItemViewModel mItemViewModel;

        public MyViewHolder(RowImageBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {

            final RequireImagesResponse.Image result = mList.get(position);

            mItemViewModel = new RequirementImageItemViewModel(result, this);

            mBinding.setViewModel(mItemViewModel);

            // Immediate Binding
            // When a variable or observable changes, the binding will be scheduled to change before
            // the next frame. There are times, however, when binding must be executed immediately.
            // To force execution, use the executePendingBindings() method.
            mBinding.executePendingBindings();

        }

        @Override
        public void onItemClick(View thumbView, String url) {
            mListener.onItemClick(thumbView, url);
        }
    }
}
