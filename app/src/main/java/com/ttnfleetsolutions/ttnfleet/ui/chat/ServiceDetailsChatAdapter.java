package com.ttnfleetsolutions.ttnfleet.ui.chat;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.ui.fleet.DashboardActivity;
import com.ttnfleetsolutions.ttnfleet.ui.serviceprovider.ServiceProviderDashboardActivity;
import com.ttnfleetsolutions.ttnfleet.ui.technician.TechnicianDashboardActivity;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.MainActivityNew;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */

public class ServiceDetailsChatAdapter extends RecyclerView.Adapter<ServiceDetailsChatAdapter.MyViewHolder> {

    private Context mContext;
    private String[] names, dates, counts, texts;
    private int mFrom = 0;

    public ServiceDetailsChatAdapter(Context context, int from) {
        this.mContext = context;
        this.mFrom = from;

        switch (mFrom) {
            case 0:
                names = new String[] {"Technician", "Fleet Dispatcher", "Service Provider"};
                break;
            case 1:
                names = new String[] {"Technician", "Driver", "Service Provider"};
                break;
            case 2:
                names = new String[] {"Technician", "Fleet Dispatcher", "Driver"};
                break;
            case 3:
                names = new String[] {"Driver", "Fleet Dispatcher", "Service Provider"};
                break;
        }
        texts = mContext.getResources().getStringArray(R.array.services_chat_text1);

        dates = mContext.getResources().getStringArray(R.array.times);
        counts = mContext.getResources().getStringArray(R.array.services_chat_count);

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_services_chat, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.names.setText(names[position]);
        holder.dates.setText(dates[position]);
        holder.counts.setText(counts[position]);
        holder.texts.setText(texts[position]);
        if (position > 1) {
            holder.counts.setVisibility(View.GONE);
        } else {
            holder.counts.setVisibility(View.VISIBLE);
        }
//        holder.chatRowLayout.setOnClickListener(view -> mContext.startActivity(new Intent(mContext, ChatActivity.class).putExtra("from", names[position])));
        holder.chatRowLayout.setOnClickListener(view -> {
            if (mContext instanceof DashboardActivity)
                ((DashboardActivity) mContext).showChatDetails(position, names[position]);
            else if (mContext instanceof ServiceProviderDashboardActivity)
                ((ServiceProviderDashboardActivity) mContext).showChatDetails(position,names[position]);
            else if (mContext instanceof TechnicianDashboardActivity)
                ((TechnicianDashboardActivity) mContext).showChatDetails(position, names[position]);
            else if (mContext instanceof MainActivityNew)
                ((MainActivityNew) mContext).showChatDetails(position, names[position]);
        });
    }

    @Override
    public int getItemCount() {
        return 3;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView names, dates, counts, texts;
        ConstraintLayout chatRowLayout;

        public MyViewHolder(View view) {
            super(view);
            chatRowLayout = view.findViewById(R.id.chatRowLayout);
            names = view.findViewById(R.id.name);
            dates = view.findViewById(R.id.date);
            counts = view.findViewById(R.id.count);
            texts = view.findViewById(R.id.chatText);
        }
    }
}
