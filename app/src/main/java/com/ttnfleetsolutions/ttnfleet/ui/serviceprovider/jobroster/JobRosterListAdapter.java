package com.ttnfleetsolutions.ttnfleet.ui.serviceprovider.jobroster;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseActivity;
import com.ttnfleetsolutions.ttnfleet.ui.serviceprovider.RejectWithReasonDialogFragment;
import com.ttnfleetsolutions.ttnfleet.ui.serviceprovider.ServiceProviderDashboardActivity;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */

public class JobRosterListAdapter extends RecyclerView.Adapter<JobRosterListAdapter.MyViewHolder> {

    private Context mContext;
    private String[] dates, numbers, issues, locations, models, vin;
    Integer selectedItemPosition = null;

    public JobRosterListAdapter(Context context) {
        this.mContext = context;
        dates = context.getResources().getStringArray(R.array.times);
        numbers = context.getResources().getStringArray(R.array.jobs);
        issues = context.getResources().getStringArray(R.array.services_issue_details);
        locations = mContext.getResources().getStringArray(R.array.locations);
        models = mContext.getResources().getStringArray(R.array.truck_array);
        vin = mContext.getResources().getStringArray(R.array.truck_number);
        if (((ServiceProviderDashboardActivity) (mContext)).isLandscape()) {
            selectedItemPosition = 0;
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_service_request_sp_new, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        if (selectedItemPosition != null && selectedItemPosition == position) {
            // Here I am just highlighting the background
            holder.rowLayout.setBackgroundColor(mContext.getResources().getColor(R.color.tr_gray));
        } else {
            holder.rowLayout.setBackgroundColor(Color.TRANSPARENT);
        }
        holder.serviceIssue.setText(Html.fromHtml(issues[position]));
        holder.jobCardNo.setText(numbers[position]);
        if (position > 1) {
            holder.coloredLine.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
            Drawable img = mContext.getResources().getDrawable(R.drawable.circle);
            img.setTint(ContextCompat.getColor(mContext, R.color.colorPrimary));
            holder.status.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
            holder.status.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
//            holder.framelayout.setOnClickListener(view -> ((ServiceProviderDashboardActivity) mContext).showOrderDetails());
            holder.status.setText(R.string.ongoing);
            holder.jobCardNo.setBackgroundTintList(mContext.getResources().getColorStateList(R.color.colorPrimary));
            holder.datetime.setText("40 mins ago");

            holder.serviceType.setVisibility(View.GONE);
            holder.serviceTypeDetails.setVisibility(View.GONE);
            holder.arrivalRepairTimingInfo.setVisibility(View.VISIBLE);

            holder.mEstimateButton.setVisibility(View.GONE);
            holder.mAcceptButton.setVisibility(View.GONE);
            holder.mRejectButton.setVisibility(View.GONE);

            holder.rowLayout.setOnClickListener(view -> {
                if (selectedItemPosition != null) {
                    notifyItemChanged(selectedItemPosition);
                }
                selectedItemPosition = position;
                notifyItemChanged(selectedItemPosition);

                ((ServiceProviderDashboardActivity) mContext).goToServiceDetails(1);
            });
        } else {
            holder.coloredLine.setBackgroundColor(ContextCompat.getColor(mContext, android.R.color.holo_orange_dark));
            Drawable img = mContext.getResources().getDrawable(R.drawable.circle2);
            img.setTint(ContextCompat.getColor(mContext, android.R.color.holo_orange_dark));
            holder.status.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
            holder.status.setTextColor(ContextCompat.getColor(mContext, android.R.color.holo_orange_dark));
//            holder.framelayout.setOnClickListener(view -> ((ServiceProviderDashboardActivity) mContext).showOrderDetails());
            holder.status.setText(R.string.new_job);
            holder.arrivalRepairTimingInfo.setVisibility(View.GONE);
            holder.jobCardNo.setBackgroundTintList(mContext.getResources().getColorStateList(android.R.color.holo_orange_dark));

            if(position == 0) {
                holder.serviceType.setVisibility(View.VISIBLE);
                holder.serviceTypeDetails.setVisibility(View.VISIBLE);
                holder.mEstimateButton.setVisibility(View.VISIBLE);
                holder.mAcceptButton.setVisibility(View.GONE);
                holder.mRejectButton.setVisibility(View.GONE);

//                holder.serviceIssue.setText(Html.fromHtml(mContext.getString(R.string.dummy_service)));
                holder.datetime.setText("Just now");
                holder.rowLayout.setOnClickListener(view -> {
                    if (selectedItemPosition != null) {
                        notifyItemChanged(selectedItemPosition);
                    }
                    selectedItemPosition = position;
                    notifyItemChanged(selectedItemPosition);

                    ((ServiceProviderDashboardActivity) mContext).goToServiceDetails(2);

                });
            } else {
                holder.serviceType.setVisibility(View.GONE);
                holder.serviceTypeDetails.setVisibility(View.GONE);
                holder.datetime.setText("5 mins ago");
                holder.mAcceptButton.setVisibility(View.VISIBLE);
                holder.mRejectButton.setVisibility(View.VISIBLE);
                holder.mEstimateButton.setVisibility(View.GONE);
                holder.rowLayout.setOnClickListener(view -> {
                    if (selectedItemPosition != null) {
                        notifyItemChanged(selectedItemPosition);
                    }
                    selectedItemPosition = position;
                    notifyItemChanged(selectedItemPosition);

                    ((ServiceProviderDashboardActivity) mContext).goToServiceDetails(1);

                });
            }
        }
        holder.mRejectButton.setOnClickListener(view -> {
            RejectWithReasonDialogFragment fragment = new RejectWithReasonDialogFragment();
            Bundle bundle = new Bundle();
            bundle.putBoolean("from-sd", true);
            fragment.setArguments(bundle);
            fragment.show(((ServiceProviderDashboardActivity) mContext).getSupportFragmentManager(), "RejectWithReason");
        });

        holder.mEstimateButton.setOnClickListener(view -> ((ServiceProviderDashboardActivity) mContext).serviceEstimation(0));
        holder.mAcceptButton.setOnClickListener(view -> ((ServiceProviderDashboardActivity) mContext).allocateJobToTechnician(0,0, 0));


        if(((BaseActivity) mContext).isLandscape()) {
            holder.bottomLayout.setVisibility(View.GONE);
        }
//        holder.date.setText(dates[position]);

        /*if(position > 0) {
            holder.status.setText("COMPLETED");
            holder.status.setTextColor(ContextCompat.getColor(mContext, android.R.color.holo_blue_dark));
            holder.count.setVisibility(View.GONE);
            holder.View.setBackgroundColor(ContextCompat.getColor(mContext, android.R.color.holo_blue_dark));
            Drawable img = mContext.getResources().getDrawable( R.drawable.ic_done_all );
            img.setTint(ContextCompat.getColor(mContext, android.R.color.holo_blue_dark));
            holder.status.setCompoundDrawablesWithIntrinsicBounds( img, null, null, null );
            holder.framelayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mContext.startActivity(new Intent(mContext, ServiceDetailsActivity.class).putExtra("index", 0));
                }
            });
        } else {
            holder.status.setText("ONGOING");
            holder.count.setVisibility(View.VISIBLE);
            holder.status.setTextColor(ContextCompat.getColor(mContext, android.R.color.holo_green_light));
            holder.View.setBackgroundColor(ContextCompat.getColor(mContext, android.R.color.holo_green_light));
            Drawable img = mContext.getResources().getDrawable( R.drawable.circle );
            holder.status.setCompoundDrawablesWithIntrinsicBounds( img, null, null, null );

            holder.framelayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mContext.startActivity(new Intent(mContext, ServiceDetailsActivity.class).putExtra("index", 0));
                }
            });
        }*/

        holder.location.setText(locations[position]);
        holder.vehicleName.setText(models[position]);
        holder.vinNo.setText(vin[position]);
    }

    @Override
    public int getItemCount() {
        return 3;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView status, datetime, serviceIssue, location, vehicleName, vinNo, arrivalRepairTimingInfo, jobCardNo, serviceType, serviceTypeDetails;
        public View coloredLine;
        public ConstraintLayout rowLayout;
        public RelativeLayout bottomLayout;
        public Button mAcceptButton, mRejectButton, mEstimateButton;

        public MyViewHolder(View view) {
            super(view);
            rowLayout = view.findViewById(R.id.rowLayout);
            bottomLayout = view.findViewById(R.id.bottomLayout);
            coloredLine = view.findViewById(R.id.coloredLine);

            jobCardNo = view.findViewById(R.id.jobCardNo);
            status = view.findViewById(R.id.status);
            datetime = view.findViewById(R.id.datetime);
            serviceIssue = view.findViewById(R.id.serviceIssue);
            location = view.findViewById(R.id.location);
            vehicleName = view.findViewById(R.id.vehicleName);
            vinNo = view.findViewById(R.id.vinNo);
            arrivalRepairTimingInfo = view.findViewById(R.id.arrivalRepairTimingInfo);
            mAcceptButton = view.findViewById(R.id.acceptButton);
            mRejectButton = view.findViewById(R.id.rejectButton);
            mEstimateButton = view.findViewById(R.id.estimateButton);
            serviceType = view.findViewById(R.id.serviceType);
            serviceTypeDetails = view.findViewById(R.id.serviceTypeDetails);
        }
    }
}
