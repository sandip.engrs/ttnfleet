package com.ttnfleetsolutions.ttnfleet.ui.fleet.order.status;

import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseViewModel;
import com.ttnfleetsolutions.ttnfleet.utils.rx.SchedulerProvider;

/**
 * Created by Sandip Chaulagain on 11/10/2017.
 *
 * @version 1.0
 */

public class StatusViewModel extends BaseViewModel<StatusNavigator> {

    public StatusViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }
}
