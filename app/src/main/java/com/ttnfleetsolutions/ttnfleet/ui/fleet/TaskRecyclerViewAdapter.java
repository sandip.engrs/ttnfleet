package com.ttnfleetsolutions.ttnfleet.ui.fleet;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ttnfleetsolutions.ttnfleet.R;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */

public class TaskRecyclerViewAdapter extends RecyclerView.Adapter<TaskRecyclerViewAdapter.MyViewHolder> {

    private Context mContext;
    private String[] dates, numbers, issues, locations, models, vin;

    public TaskRecyclerViewAdapter(Context context) {
        this.mContext = context;
        dates = context.getResources().getStringArray(R.array.times);
        numbers = context.getResources().getStringArray(R.array.services);
        issues = context.getResources().getStringArray(R.array.services_issue);
        locations = mContext.getResources().getStringArray(R.array.locations);
        models = mContext.getResources().getStringArray(R.array.truck_array);
        vin = mContext.getResources().getStringArray(R.array.truck_number);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_task_approval, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        /*if (mStatus.equalsIgnoreCase("new")) {
            holder.View.setBackgroundColor(ContextCompat.getColor(mContext, android.R.color.holo_orange_dark));
            Drawable img = mContext.getResources().getDrawable(R.drawable.circle);
            img.setTint(ContextCompat.getColor(mContext, android.R.color.holo_orange_dark));
            holder.status.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
            holder.status.setTextColor(ContextCompat.getColor(mContext, android.R.color.holo_orange_dark));
            holder.framelayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((ServiceProviderDashboardActivity) mContext).showOrderDetails(0);
                }
            });
        } else {
            holder.View.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
            Drawable img = mContext.getResources().getDrawable(R.drawable.circle);
            img.setTint(ContextCompat.getColor(mContext, R.color.colorPrimary));
            holder.status.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
            holder.status.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
            holder.framelayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((ServiceProviderDashboardActivity) mContext).showOrderDetails(1);
                }
            });
        }*/
//        holder.date.setText(dates[position]);

        /*if(position > 0) {
            holder.status.setText("COMPLETED");
            holder.status.setTextColor(ContextCompat.getColor(mContext, android.R.color.holo_blue_dark));
            holder.count.setVisibility(View.GONE);
            holder.View.setBackgroundColor(ContextCompat.getColor(mContext, android.R.color.holo_blue_dark));
            Drawable img = mContext.getResources().getDrawable( R.drawable.ic_done_all );
            img.setTint(ContextCompat.getColor(mContext, android.R.color.holo_blue_dark));
            holder.status.setCompoundDrawablesWithIntrinsicBounds( img, null, null, null );
            holder.framelayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mContext.startActivity(new Intent(mContext, ServiceDetailsActivity.class).putExtra("index", 0));
                }
            });
        } else {
            holder.status.setText("ONGOING");
            holder.count.setVisibility(View.VISIBLE);
            holder.status.setTextColor(ContextCompat.getColor(mContext, android.R.color.holo_green_light));
            holder.View.setBackgroundColor(ContextCompat.getColor(mContext, android.R.color.holo_green_light));
            Drawable img = mContext.getResources().getDrawable( R.drawable.circle );
            holder.status.setCompoundDrawablesWithIntrinsicBounds( img, null, null, null );

            holder.framelayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mContext.startActivity(new Intent(mContext, ServiceDetailsActivity.class).putExtra("index", 0));
                }
            });
        }*/
//        holder.issue.setText(issues[position]);
//        holder.count.setVisibility(View.GONE);
//        holder.status.setText(mStatus);
        holder.requestNo.setText(String.valueOf(position + 1) + ". ");
//        holder.locations.setText("Address: " + locations[position]);
//        holder.model.setText(models[position]);
//        holder.number.setText(vin[position]);
        switch (position) {
            case 0:
                holder.status.setText("Approved by you");
                holder.View.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorPrimaryDark));
                holder.date.setText("40 min ago");
                holder.title.setText("Renew the brake pads (first axle)");
                holder.duration.setText("Total Time (H) : 1.40");
                holder.cost.setText("Cost : $500");
                holder.threshold.setText("");
                holder.parts.setText("Parts required : 2 (Brake pad set, disc brake)");
                break;
            case 1:
                holder.status.setText("Need your approval");
                holder.status.setTextColor(ContextCompat.getColor(mContext, android.R.color.holo_orange_dark));
                holder.View.setBackgroundColor(ContextCompat.getColor(mContext, android.R.color.holo_orange_dark));
                holder.date.setText("15 min ago");

                holder.title.setText("Renew front axle left wheel tyre");
                holder.duration.setText("Total Time (H) : 2.00");
                holder.cost.setText("Cost : $1000");
                holder.threshold.setText("(Threshold exceeded)");
                holder.parts.setText("Parts required : 1 (Spare Tyre)");
                break;
            case 2:
                holder.status.setText("Fix this later");
                holder.status.setTextColor(ContextCompat.getColor(mContext, android.R.color.darker_gray));
                holder.View.setBackgroundColor(ContextCompat.getColor(mContext, android.R.color.darker_gray));
                holder.date.setText("5 min ago");

                holder.title.setText("Remove/refit the parking brake valve");
                holder.duration.setText("Total Time (H) : 0.50");
                holder.cost.setText("Cost : $100");
                holder.threshold.setText("");
                holder.parts.setText("Parts required : 0");

                break;
        }
    }

    @Override
    public int getItemCount() {
        return 3;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView status, requestNo, title, cost, duration, parts, threshold, date;
        public View View;

        public MyViewHolder(View view) {
            super(view);
            View = view.findViewById(R.id.coloredLine);
            status = view.findViewById(R.id.approvalStatus);
            requestNo = view.findViewById(R.id.index);
            title = view.findViewById(R.id.serviceIssue);
            cost = view.findViewById(R.id.cost);
            duration = view.findViewById(R.id.duration);
            parts = view.findViewById(R.id.parts);
            threshold = view.findViewById(R.id.threshold);
            date = view.findViewById(R.id.datetime);

            view.setOnClickListener(view1 -> {
                TaskApprovalDialogFragment fragment = new TaskApprovalDialogFragment();
                fragment.show(((AppCompatActivity) mContext).getSupportFragmentManager(), "Task Approval");
            });
        }
    }
}
