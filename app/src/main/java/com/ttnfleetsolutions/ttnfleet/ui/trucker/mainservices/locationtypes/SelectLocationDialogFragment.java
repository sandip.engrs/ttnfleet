package com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.locationtypes;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.data.model.api.location.Location;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseActivity;
import com.ttnfleetsolutions.ttnfleet.ui.fleet.DashboardActivity;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.MainActivityNew;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.SelectLocationActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */

public class SelectLocationDialogFragment extends DialogFragment implements View.OnClickListener {

    private View rootView;
    private String mType;
    private int mColorPrimary, mColorPrimaryDark, mLocationTypeId, mAlwaysLive;
    private List<Location> mLocationTypeList = new ArrayList<>();
    private TextView mName;
    private ImageView mIcon;
    private RecyclerView mRecyclerView;
    private static int REQUEST_LOCATION = 1001;
    private int mEventType;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.dialog_fragment_location_types, container, false);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        //to hide keyboard when showing dialog fragment
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        mName = rootView.findViewById(R.id.mainServiceName);
        mIcon = rootView.findViewById(R.id.mainServiceIcon);
        mRecyclerView = rootView.findViewById(R.id.locationsTypeRecyclerView);
        rootView.findViewById(R.id.closeBtn).setOnClickListener(v -> dismiss());

        if (getArguments() != null) {
            mColorPrimary = getArguments().getInt("colorPrimary");
            mColorPrimaryDark = getArguments().getInt("colorPrimaryDark");
            rootView.findViewById(R.id.topBgImg1).setBackgroundTintList(ColorStateList.valueOf(mColorPrimary));
            rootView.findViewById(R.id.mainServiceIconLayout1).setBackgroundTintList(ColorStateList.valueOf(mColorPrimary));
            rootView.findViewById(R.id.bottomBgImg1).setBackgroundTintList(ColorStateList.valueOf(mColorPrimary));

            mName.setTextColor(mColorPrimary);

            mLocationTypeList.add(new Location("Search location"));
            mLocationTypeList.addAll((List<Location>) getArguments().get("locationList"));

            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            mRecyclerView.setAdapter(new LocationAdapter(mLocationTypeList, mOnClickListener, mColorPrimary));

            mType = getArguments().getString("locationType");
            mLocationTypeId = getArguments().getInt("locationTypeId");
            mAlwaysLive = getArguments().getInt("alwaysLive");
            mName.setText(mType);

            mEventType = (int) getArguments().get("eventType");
            switch (mEventType) {
                case 1:
                    mIcon.setImageResource(R.mipmap.popup_icon_scheduled);
                    break;
                case 2:
                    mIcon.setImageResource(R.mipmap.popup_icon_road_service);
                    break;
                case 3:
                    mIcon.setImageResource(R.mipmap.popup_icon_acident);
                    break;
                case 4:
                    mIcon.setImageResource(R.mipmap.popup_icon_towing);
                    break;
            }
        }
        return rootView;
    }

    private View.OnClickListener mOnClickListener = v -> {
        int position = (int) v.getTag();
        if (position == 0) {
            Intent intent = new Intent(getActivity(), SelectLocationActivity.class);
            intent.putExtra("colorPrimary", mColorPrimary);
            intent.putExtra("colorPrimaryDark", mColorPrimaryDark);
            intent.putExtra("locationTypeId", mLocationTypeId);
            intent.putExtra("alwaysLive", mAlwaysLive);
            startActivityForResult(intent, REQUEST_LOCATION);
        } else {
            dismiss();
            mLocationTypeList.get(position).setLocationType(mLocationTypeList.get(position).new LocationType());
            mLocationTypeList.get(position).getLocationType().setAlwayslive(mAlwaysLive);
            if (getActivity() instanceof MainActivityNew) {
                ((MainActivityNew) getActivity()).showForm(mLocationTypeList.get(position));
            } else {
                ((DashboardActivity) getActivity()).showForm(mLocationTypeList.get(position));
            }
        }
    };


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 1001:
                if (data != null && data.getExtras() != null) {
                   /* // Get the location passed to this service through an extra.
                    Location mDefaultLocation = data.getParcelableExtra(AppConstants.LOCATION_DATA_EXTRA);
                    // Display the address string or an error message sent from the intent service.
                    String mAddressOutput = data.getStringExtra(AppConstants.RESULT_DATA_KEY);*/
                    Location selectedLocation = (Location) data.getExtras().get("location");
                    dismiss();
                    ((MainActivityNew) getActivity()).showForm(selectedLocation);
                } else {
                    dismiss();
                }
                break;
        }
    }

    @Override
    public void onClick(View view) {
        dismiss();
    }

    @Override
    public void onResume() {
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        if (((BaseActivity) getActivity()).isLandscape())
            params.width = (int) getResources().getDimension(R.dimen.dialog_min_width);
        else
            params.width = ViewGroup.LayoutParams.MATCH_PARENT;

        params.height = ViewGroup.LayoutParams.WRAP_CONTENT;

        getDialog().getWindow().setAttributes((WindowManager.LayoutParams) params);
        super.onResume();
    }
}
