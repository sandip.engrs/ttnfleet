package com.ttnfleetsolutions.ttnfleet.ui.technician;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ttnfleetsolutions.ttnfleet.R;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */

public class EditReportDialogFragment extends DialogFragment {

    private View rootView;
    private TextView stepCount;
    private TextView submitButton;
    private LinearLayout layout1;
    private LinearLayout layout2;
    private int step = 1;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.dialog_edit_breakdown_report, container, false);
        //to hide keyboard when showing dialog fragment
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        /*TextView textView = rootView.findViewById(R.id.textView);
        Drawable img = getResources().getDrawable(R.drawable.ic_done_all);
        img.setTint(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));
        textView.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);

        getDialog().setTitle("FEEDBACK");*/
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        stepCount = rootView.findViewById(R.id.step_count);
        submitButton = rootView.findViewById(R.id.submit);
        submitButton.setText(R.string.next);

        layout1 = rootView.findViewById(R.id.editDetailsLayout);
        layout2 = rootView.findViewById(R.id.editDetailsToolsLayout);

        rootView.findViewById(R.id.submit).setOnClickListener(view -> {
            if (step == 1) {
                step++;
                submitButton.setText(R.string.save);
                layout1.setVisibility(View.INVISIBLE);
                layout2.setVisibility(View.VISIBLE);
                stepCount.setText(R.string.step_2);
            } else {
                dismiss();
            }

        });

        rootView.findViewById(R.id.cancel).setOnClickListener(view -> dismiss());
        return rootView;
    }


}
