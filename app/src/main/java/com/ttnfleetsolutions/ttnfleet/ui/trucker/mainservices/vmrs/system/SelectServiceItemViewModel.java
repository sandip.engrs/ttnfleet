package com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.vmrs.system;

import android.databinding.BindingAdapter;
import android.databinding.ObservableField;
import android.view.View;
import android.widget.ImageView;

import com.ttnfleetsolutions.ttnfleet.BuildConfig;
import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.ServiceCode;
import com.ttnfleetsolutions.ttnfleet.data.remote.ApiEndPoint;
import com.ttnfleetsolutions.ttnfleet.utils.GlideApp;

/**
 * Created by Sandip Chaulagain on 1/19/2018.
 *
 * @version 1.0
 */

public class SelectServiceItemViewModel {

    private ServiceCode mServiceCode;
    public ObservableField<String> name, icon;
    private ServiceItemViewModelListener mListener;
    public boolean mIsMultipleSelectionEnabled;
    public ObservableField<Boolean> isSelected;
    public int codeStep = 0;

    SelectServiceItemViewModel(ServiceCode serviceCode, boolean isMultipleSelectionEnabled, int codeStep, ServiceItemViewModelListener listener) {
        this.mServiceCode = serviceCode;
        this.mListener = listener;
        this.mIsMultipleSelectionEnabled = isMultipleSelectionEnabled;
        this.codeStep = codeStep;
        name = new ObservableField<>(serviceCode.getDisplaytitle());
        isSelected = new ObservableField<>(serviceCode.isSelected());
        icon = new ObservableField<>(BuildConfig.BASE_URL + ApiEndPoint.DIR_IMAGE_VMRS_PARENT + serviceCode.getImagename());
    }

    public void onItemClick() {
        if (mIsMultipleSelectionEnabled) {
            mListener.onItemSelection(mServiceCode.getPosition(), mServiceCode.isSelected());
        } else if (codeStep == 1) {
            mListener.onItemClick(mServiceCode.getCode1(), mServiceCode.getCode2(), mServiceCode.getCode3(), mServiceCode.getDisplaytitle(), "");
        } else if (codeStep == 2) {
            mListener.onItemClick(mServiceCode.getCode1(), mServiceCode.getCode2(), mServiceCode.getCode3(), "", mServiceCode.getDisplaytitle());
        } else if (codeStep == 3) {
            mListener.onItemClick(mServiceCode.getCode1(), mServiceCode.getCode2(), mServiceCode.getCode3(), "", mServiceCode.getDisplaytitle());
        }
    }

    public interface ServiceItemViewModelListener {
        void onItemClick(String systemCode, String assemblyCode, String componentCode, String systemCodeName, String assemblyCodeName);

        void onItemSelection(int position, boolean isSelected);
    }

    @BindingAdapter({"srcIcon"})
    public static void setImageViewResource(View view, String url) {
        GlideApp.with(view.getContext())
                .load(url)
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .fitCenter()
                .into((ImageView) view);
    }

    @BindingAdapter({"selectionSrc"})
    public static void setSelectionSrc(View view, boolean selected) {
//        ((TextView) view).setTypeface(((TextView) view).getTypeface(), selected ? Typeface.BOLD : Typeface.NORMAL);
    }
}
