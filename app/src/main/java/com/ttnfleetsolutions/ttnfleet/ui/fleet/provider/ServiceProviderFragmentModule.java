package com.ttnfleetsolutions.ttnfleet.ui.fleet.provider;

import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.utils.rx.SchedulerProvider;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Sandip Chaulagain on 4/4/2018.
 *
 * @version 1.0
 */
@Module
public class ServiceProviderFragmentModule {

    @Provides
    ServiceProviderFragmentViewModel provideServiceProviderFragmentViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        return new ServiceProviderFragmentViewModel(dataManager, schedulerProvider);
    }

    @Provides
    ServicesProviderListAdapter provideServicesProviderListAdapter() {
        return new ServicesProviderListAdapter(new ArrayList<>());
    }
}
