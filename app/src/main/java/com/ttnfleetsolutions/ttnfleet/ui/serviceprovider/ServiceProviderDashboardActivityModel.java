package com.ttnfleetsolutions.ttnfleet.ui.serviceprovider;

import android.util.Log;

import com.google.gson.Gson;
import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.RejectWorkOrderRequest;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseViewModel;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.details.CreateEventFragment;
import com.ttnfleetsolutions.ttnfleet.utils.rx.SchedulerProvider;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Sandip Chaulagain on 12/22/2017.
 *
 * @version 1.0
 */

public class ServiceProviderDashboardActivityModel extends BaseViewModel {

    public ServiceProviderDashboardActivityModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    public boolean isReloadNeeded = false;

    public void getRejectionTypesSP(int eventId, int workOrderId, int workOrderStatusId) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .doServerGetRejectTypesApiCall()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    ((ServiceProviderDashboardNavigator)getNavigator()).getRejectionTypes(eventId, workOrderId, workOrderStatusId, response.getResult());
//                    getNavigator().getRejectionTypes(eventId, workOrderId, workOrderStatusId, response.getResult());
                    setIsLoading(false);
                }, throwable -> {
                    setIsLoading(false);
                    ((ServiceProviderDashboardNavigator)getNavigator()).handleError(throwable);
//                    getNavigator().handleError(throwable);
                }));
    }

    private JSONObject getRejectWorkOrderRequest(int eventId, int workOrderId, int workOrderStatusId, String rejectionDescription, int rejectionTypeId) {
        RejectWorkOrderRequest rejectWorkOrderRequest = new RejectWorkOrderRequest(getDataManager().getCurrentUserID(), eventId, workOrderId, rejectionDescription,
                rejectionTypeId, workOrderStatusId);
        try {
            JSONObject jsonObject = new JSONObject(new Gson().toJson(rejectWorkOrderRequest, RejectWorkOrderRequest.class));
            JSONObject dataObject = new JSONObject();
            dataObject.put("data", jsonObject);
            Log.e(CreateEventFragment.class.getSimpleName(), "Json -- " + dataObject.toString());
            return dataObject;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void rejectWorkOrdersAPICall(int eventId, int workOrderId, int workOrderStatusId, String rejectionDescription, int rejectionTypeId) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .doServerRejectWorkOrderApiCall(getRejectWorkOrderRequest(eventId, workOrderId, workOrderStatusId, rejectionDescription, rejectionTypeId))
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    ((ServiceProviderDashboardNavigator)getNavigator()).onRejectWorkOrders(response);
//                    getNavigator().onRejectWorkOrders(response);
                    setIsLoading(false);
                }, throwable -> {
                    setIsLoading(false);
                    ((ServiceProviderDashboardNavigator)getNavigator()).handleError(throwable);
//                    getNavigator().handleError(throwable);
                }));
    }
}

