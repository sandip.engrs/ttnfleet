package com.ttnfleetsolutions.ttnfleet.ui.serviceprovider.jobroster.details;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.ui.serviceprovider.AdditionalJobsRecyclerViewAdapter;
import com.ttnfleetsolutions.ttnfleet.ui.serviceprovider.RejectWithReasonDialogFragment;
import com.ttnfleetsolutions.ttnfleet.ui.serviceprovider.ServiceProviderDashboardActivity;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.model.OrderStatus;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.model.Orientation;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.model.TimeLineModel;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.servicerequest.timeline.TimelineAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Sandip Chaulagain on 10/10/2017.
 *
 * @version 1.0
 */

public class JobCardDetailsFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private List<TimeLineModel> mDataList = new ArrayList<>();
    private Orientation mOrientation = Orientation.VERTICAL;
    private int index = 0, from = 0;
    private View mRootView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_job_card_details_sp_new, container, false);
        initComponents();
        return mRootView;
    }

    public void updateIndex(int from, int index) {
        this.from = from;
        this.index = index;
    }

    private void initComponents() {
        NestedScrollView nsv = mRootView.findViewById(R.id.nestedScrollView);
        nsv.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
            if (scrollY > oldScrollY) {
                ((FloatingActionButton) mRootView.findViewById(R.id.chatFloatingActionButton)).hide();
            } else {
                ((FloatingActionButton) mRootView.findViewById(R.id.chatFloatingActionButton)).show();
            }
        });

        mRootView.findViewById(R.id.chatFloatingActionButton).setOnClickListener(view -> ((ServiceProviderDashboardActivity) getActivity()).showServiceChatThreadDetails(0, "Service Request 1"));

        mRootView.findViewById(R.id.estimateButton).setOnClickListener(view -> ((ServiceProviderDashboardActivity) getActivity()).serviceEstimation(0));

        mRootView.findViewById(R.id.acceptJob).setOnClickListener(view -> ((ServiceProviderDashboardActivity) getActivity()).allocateJobToTechnician(0,0, 0));

        mRootView.findViewById(R.id.rejectJob).setOnClickListener(view -> {
            RejectWithReasonDialogFragment fragment = new RejectWithReasonDialogFragment();
            Bundle bundle = new Bundle();
            bundle.putBoolean("from-sd", true);
            fragment.setArguments(bundle);
            fragment.show(getActivity().getSupportFragmentManager(), "RejectWithReason");
        });

        switch (index) {
            case 1:
                ((TextView) mRootView.findViewById(R.id.status)).setText("NEW");
                mRootView.findViewById(R.id.status).setBackgroundTintList(getResources().getColorStateList(android.R.color.holo_orange_dark));

                mRootView.findViewById(R.id.technicianDetailsLabel).setVisibility(View.GONE);
                mRootView.findViewById(R.id.technicianName).setVisibility(View.GONE);
                mRootView.findViewById(R.id.technicianContact).setVisibility(View.GONE);

                CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) mRootView.findViewById(R.id.chatFloatingActionButton).getLayoutParams();
                layoutParams.setMargins(0, 0, (int) getResources().getDimension(R.dimen.fab_margin), (int) getResources().getDimension(R.dimen.fab_margin_bottom));
                mRootView.findViewById(R.id.chatFloatingActionButton).setLayoutParams(layoutParams);

                switch (from) {
                    case 2:
                        //Drop off & Pick Up (NEW)
                        mRootView.findViewById(R.id.estimateButton).setVisibility(View.VISIBLE);

                        ((TextView) mRootView.findViewById(R.id.breakdownReportLabel)).setText(getString(R.string.service_required));
                        ((TextView) mRootView.findViewById(R.id.description)).setText("Service Type: Drop off & Pick up");
                        ((TextView) mRootView.findViewById(R.id.issueDescription)).setText("Drop off: 8th Dec 12:45  |  Pick up: 19th Dec 08:00");
                        ((TextView) mRootView.findViewById(R.id.datetime)).setText("Just now");
                        ((TextView) mRootView.findViewById(R.id.serviceIssue)).setText(getString(R.string.dummy_services));

                        ((TextView) mRootView.findViewById(R.id.location)).setText("3884 Eu Road, Boston, MA, 43448");
                        ((TextView) mRootView.findViewById(R.id.vehicleName)).setText("Maan Truck 8.136 FAE 4X4");
                        ((TextView) mRootView.findViewById(R.id.vinNo)).setText("WVM56502016034590");
                        break;
                    case 1:
                        //Emergency RoadSide (NEW)
                        mRootView.findViewById(R.id.bottomLayout).setVisibility(View.VISIBLE);
                        mRootView.findViewById(R.id.estimateButton).setVisibility(View.GONE);

                        ((TextView) mRootView.findViewById(R.id.datetime)).setText("5 mins ago");
                        break;
                }
                break;
            case 2:
                ((TextView) mRootView.findViewById(R.id.status)).setText("ONGOING");
                mRootView.findViewById(R.id.status).setBackgroundTintList(getResources().getColorStateList(R.color.colorPrimary));

                ((TextView) mRootView.findViewById(R.id.datetime)).setText("40 mins ago");

                layoutParams = (CoordinatorLayout.LayoutParams) mRootView.findViewById(R.id.chatFloatingActionButton).getLayoutParams();
                layoutParams.setMargins(0, 0, (int) getResources().getDimension(R.dimen.fab_margin), (int) getResources().getDimension(R.dimen.fab_margin));
                mRootView.findViewById(R.id.chatFloatingActionButton).setLayoutParams(layoutParams);

                mRootView.findViewById(R.id.additionalJobLabel).setVisibility(View.VISIBLE);
                mRootView.findViewById(R.id.additionalJobRecyclerView).setVisibility(View.VISIBLE);
                RecyclerView additionalJobRecyclerView = mRootView.findViewById(R.id.additionalJobRecyclerView);
                additionalJobRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                additionalJobRecyclerView.setHasFixedSize(true);
                additionalJobRecyclerView.setNestedScrollingEnabled(false);
                additionalJobRecyclerView.setAdapter(new AdditionalJobsRecyclerViewAdapter(getActivity()));

                ((TextView) mRootView.findViewById(R.id.location)).setText("State Highway 580, Tampa, FL, USA");
                ((TextView) mRootView.findViewById(R.id.vehicleName)).setText("Mercedes-Benz Unimog");
                ((TextView) mRootView.findViewById(R.id.vinNo)).setText("WVM58956444545545");
                ((TextView) mRootView.findViewById(R.id.serviceIssue)).setText("Front axle brake jam issue");
                ((TextView) mRootView.findViewById(R.id.issueDescription)).setText("Brakes are squealing, caused by a worn down brake pad, which comes into direct contact with the rotor.");
                break;
            case 3:
                ((TextView) mRootView.findViewById(R.id.status)).setText("COMPLETED");
                mRootView.findViewById(R.id.status).setBackgroundTintList(getResources().getColorStateList(android.R.color.holo_blue_dark));

                ((TextView) mRootView.findViewById(R.id.datetime)).setText("Just now");

                layoutParams = (CoordinatorLayout.LayoutParams) mRootView.findViewById(R.id.chatFloatingActionButton).getLayoutParams();
                layoutParams.setMargins(0, 0, (int) getResources().getDimension(R.dimen.fab_margin), (int) getResources().getDimension(R.dimen.fab_margin));
                mRootView.findViewById(R.id.chatFloatingActionButton).setLayoutParams(layoutParams);
                break;
        }

        mRecyclerView = mRootView.findViewById(R.id.timelineRecyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setNestedScrollingEnabled(false);
        setDataListItems();

        TimelineAdapter mTimeLineAdapter = new TimelineAdapter(mDataList);
        mRecyclerView.setAdapter(mTimeLineAdapter);
    }

    private void setDataListItems() {
        mDataList.clear();
        switch (index) {
            case 1:
                mDataList.add(new TimeLineModel("Received job request", "2017-12-06 16:35", OrderStatus.ACTIVE));
                break;
            case 2:
                mDataList.add(new TimeLineModel("Work in progress", "2017-12-06 17:10", OrderStatus.ACTIVE));
                mDataList.add(new TimeLineModel("ATA: 05:10", "2017-12-06 17:10", OrderStatus.COMPLETED));
                mDataList.add(new TimeLineModel("Technician is on his way.\nETA: 17:10", "2017-12-06 16:40", OrderStatus.COMPLETED));
                mDataList.add(new TimeLineModel("Technician assigned.\nETC: 1 Hour", "2017-12-06 16:40", OrderStatus.COMPLETED));
                mDataList.add(new TimeLineModel("Received job request", "2017-12-06 16:35", OrderStatus.COMPLETED));
                break;
            case 3:
                mDataList.add(new TimeLineModel("Job completed", "2017-12-06 18:20", OrderStatus.ACTIVE));
                mDataList.add(new TimeLineModel("Work in progress", "2017-12-06 17:10", OrderStatus.COMPLETED));
                mDataList.add(new TimeLineModel("ATA: 05:10", "2017-12-06 17:10", OrderStatus.COMPLETED));
                mDataList.add(new TimeLineModel("Technician is on his way.\nETA: 17:10", "2017-12-06 16:40", OrderStatus.COMPLETED));
                mDataList.add(new TimeLineModel("Technician assigned.\nETC: 1 hour", "2017-12-06 16:40", OrderStatus.COMPLETED));
                mDataList.add(new TimeLineModel("Received job request", "2017-12-06 16:35", OrderStatus.COMPLETED));
                break;
        }
        Collections.reverse(mDataList);
    }
}
