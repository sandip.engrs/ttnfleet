package com.ttnfleetsolutions.ttnfleet.ui.fleet.order.repairs;

import android.arch.lifecycle.LiveData;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.Toast;

import com.ttnfleetsolutions.ttnfleet.BR;
import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.Repair;
import com.ttnfleetsolutions.ttnfleet.databinding.FragmentFdOrderRepairDetailsBinding;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseFragment;
import com.ttnfleetsolutions.ttnfleet.ui.fleet.TaskApprovalDialogFragment;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Sandip Chaulagain on 11/10/2017.
 *
 * @version 1.0
 */
public class RepairsFragment extends BaseFragment<FragmentFdOrderRepairDetailsBinding, RepairsViewModel> implements RepairsNavigator, RepairItemViewModel.RepairItemViewModelListener {

    @Inject
    RepairsViewModel mRepairsViewModel;

    FragmentFdOrderRepairDetailsBinding mFragmentRepairsBinding;

    @Inject
    LinearLayoutManager mLayoutManager;

    @Inject
    RepairAdapter mRepairAdapter;

    public static RepairsFragment newInstance() {
        Bundle args = new Bundle();
        RepairsFragment fragment = new RepairsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRepairsViewModel.setNavigator(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFragmentRepairsBinding = getViewDataBinding();
        mRepairAdapter.setListener(this);
        setUp();
    }

    @Override
    public void updateRepairs(LiveData<List<Repair>> repairs) {
        if (!mRepairsViewModel.getRepairListLiveData().hasActiveObservers())
            mRepairsViewModel.getRepairListLiveData().observe(getActivity(), repairs1 -> mRepairAdapter.addItems(repairs1));
        else
            mRepairAdapter.addItems(repairs.getValue());

    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(getActivity(), getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
    }

    @Override
    public RepairsViewModel getViewModel() {
        return mRepairsViewModel;
    }


    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_fd_order_repair_details;
    }

   /* @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser && mFragmentRepairsBinding != null && mFragmentRepairsBinding.tasksRecyclerView != null && mFragmentRepairsBinding.tasksRecyclerView.getAdapter() == null) {
            setUp();
        }
    }*/

    private void setUp() {
        /*mFragmentRepairsBinding.addNew.setOnClickListener((View.OnClickListener) view -> new Handler().post(() -> {
            mRepairsViewModel.insertData(new Repair(3, "Renew the brake shoes (third axle)", "", "just now", "Total Time (H) : 2.40", "Cost : $200", "", 2, "Parts required :1 (disc brake)"));
            mRepairsViewModel.insertData(new Repair(0, "Renew the brake pads (first axle)", "", "40 min ago", "Total Time (H) : 1.40", "Cost : $1500", "", 0, "Parts required : 2 (Brake pad set, disc brake)"));
        }));*/
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mFragmentRepairsBinding.tasksRecyclerView.setLayoutManager(mLayoutManager);
        mFragmentRepairsBinding.tasksRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        mFragmentRepairsBinding.tasksRecyclerView.setAdapter(mRepairAdapter);

        //One method one responsibility.May be we can move getAllRepairs() outside this function.
        if (mRepairsViewModel.getRepairListLiveData() == null)
            mRepairsViewModel.getAllRepairs();
    }

    @Override
    public void onItemClick(long id) {
        TaskApprovalDialogFragment fragment = new TaskApprovalDialogFragment();
        fragment.show(getActivity().getSupportFragmentManager(), "Task Approval");
    }
}
