package com.ttnfleetsolutions.ttnfleet.ui.trucker.vehicle;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Sandip Chaulagain on 1/22/2018.
 *
 * @version 1.0
 */
@Module
public abstract class VehicleFragmentProvider {

    @ContributesAndroidInjector(modules = VehicleFragmentModule.class)
    abstract VehicleFragment provideVehicleFragmentFactory();
}
