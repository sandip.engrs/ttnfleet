package com.ttnfleetsolutions.ttnfleet.ui.fleet.eventdetails;

import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.servicerequest.workorder.WorkOrderRecyclerViewAdapter;
import com.ttnfleetsolutions.ttnfleet.utils.rx.SchedulerProvider;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Sandip Chaulagain on 5/28/2018.
 *
 * @version 1.0
 */
@Module
public class EventDetailsFragmentModule {

    @Provides
    EventDetailsViewModel provideEventDetailsViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        return new EventDetailsViewModel(dataManager, schedulerProvider);
    }

    @Provides
    WorkOrderRecyclerViewAdapter provideWorkOrderRecyclerViewAdapter() {
        return new WorkOrderRecyclerViewAdapter(new ArrayList<>());
    }
}
