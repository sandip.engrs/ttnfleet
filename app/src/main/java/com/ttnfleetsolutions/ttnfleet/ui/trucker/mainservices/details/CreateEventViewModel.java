package com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.details;

import android.util.Log;

import com.google.gson.Gson;
import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.data.model.api.asset.Vehicle;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.EventRequest;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.SystemCode;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.TruckLoadResponse;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseViewModel;
import com.ttnfleetsolutions.ttnfleet.utils.rx.SchedulerProvider;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sandip Chaulagain on 11/10/2017.
 *
 * @version 1.0
 */

public class CreateEventViewModel extends BaseViewModel<CreateEventNavigator> {


    CreateEventViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    private List<SystemCode> mCodeList = new ArrayList<>();

    List<SystemCode> getCodeList() {
        return mCodeList;
    }

    private List<TruckLoadResponse.TruckLoad> mTruckLoadList;

    List<TruckLoadResponse.TruckLoad> getTruckLoadList() {
        return mTruckLoadList;
    }

    public void setTruckLoadList(List<TruckLoadResponse.TruckLoad> mTruckLoadList) {
        this.mTruckLoadList = mTruckLoadList;
    }

    private List<Vehicle> mVehicleList;

    List<Vehicle> getVehicleList() {
        return mVehicleList;
    }

    public void setVehicleList(List<Vehicle> mVehicleList) {
        this.mVehicleList = mVehicleList;
    }

    void setCodeList() {
        mCodeList = new Gson().fromJson(getDataManager().getEventRequest(), EventRequest.class).getSystemCodeList();
        getNavigator().setCodeList(mCodeList);
    }

    void removeCode(int position) {
        Gson gson = new Gson();
        EventRequest eventRequest = gson.fromJson(getDataManager().getEventRequest(), EventRequest.class);
        eventRequest.getSystemCodeList().remove(position);
        getCodeList().remove(position);
        getDataManager().setEventRequest(gson.toJson(eventRequest, EventRequest.class));
        getNavigator().notifyAdapter();
    }

    void loadDetails() {
        Gson gson = new Gson();
        EventRequest eventRequest = gson.fromJson(getDataManager().getEventRequest(), EventRequest.class);
        getNavigator().loadDetails(eventRequest.getAssetId(), eventRequest.getAddress(), eventRequest.getRTA(), eventRequest.getRTC(), eventRequest.getLoadId(), eventRequest.getRemarks());
    }

    void loadLocationDetails() {
        Gson gson = new Gson();
        EventRequest eventRequest = gson.fromJson(getDataManager().getEventRequest(), EventRequest.class);
        getNavigator().loadLocationDetails(eventRequest.getAddress());
    }

    int getDriverId() {
        Gson gson = new Gson();
        EventRequest eventRequest = gson.fromJson(getDataManager().getEventRequest(), EventRequest.class);
       return eventRequest.getUserId();
    }

    void saveDetails(int assetId, String RTA, String RTC, int loadId, String remarks, boolean isCreatedByDriver) {
        Gson gson = new Gson();
        EventRequest eventRequest = gson.fromJson(getDataManager().getEventRequest(), EventRequest.class);
        eventRequest.setAssetId(assetId);
        eventRequest.setEventId(0);
        if (isCreatedByDriver)
            eventRequest.setUserId(getDataManager().getCurrentUserID());
        eventRequest.setStatusId(DataManager.EventStatus.REQUESTED.getStatus());
        eventRequest.setRTA(RTA);
        eventRequest.setRTC(RTC);
        eventRequest.setLoadId(loadId);
        eventRequest.setRemarks(remarks);
        getDataManager().setEventRequest(gson.toJson(eventRequest, EventRequest.class));
    }

    /*void saveDetails(String RTA, String RTC, String remarks) {
        Gson gson = new Gson();
        EventRequest eventRequest = gson.fromJson(getDataManager().getEventRequest(), EventRequest.class);
        eventRequest.setEventId(0);
        eventRequest.setUserId(getDataManager().getCurrentUserID());
        eventRequest.setStatusId(DataManager.EventStatus.REQUESTED.getStatus());
        eventRequest.setRTA(RTA);
        eventRequest.setRTC(RTC);
        eventRequest.setRemarks(remarks);
        getDataManager().setEventRequest(gson.toJson(eventRequest, EventRequest.class));
    }*/

    void submitEventAPICall() {
        setIsLoading(true);
        Gson gson = new Gson();
        EventRequest eventRequest = gson.fromJson(getDataManager().getEventRequest(), EventRequest.class);

        getDataManager().setEventRequest(gson.toJson(eventRequest, EventRequest.class));

        Log.i("create event param",getRequestJson().toString());

        getCompositeDisposable().add(getDataManager().doServerCreateEventsApiCall(getRequestJson())
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    setIsLoading(false);
                    getNavigator().onEventCreation(response);
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                }));
    }

    private JSONObject getRequestJson() {
        try {
            JSONObject jsonObject = new JSONObject(getDataManager().getEventRequest());
            JSONArray jsonArray = jsonObject.getJSONArray("systemcode");

            Log.i("system code array",jsonArray.toString());

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject codeObj = jsonArray.getJSONObject(i);
                codeObj.remove("systemCodeName");

                JSONArray jsonArray1 = codeObj.getJSONArray("assemblycode");

                for (int j = 0; j < jsonArray1.length(); j++) {

                    JSONObject codeObj1 = jsonArray1.getJSONObject(j);

                    codeObj1.remove("assemblyCodeName");
                    codeObj1.remove("componentCodeName");
                    if(codeObj1.getInt("componentcode") ==0){
                        codeObj1.remove("componentcode");
                    }
                }
            }
            JSONObject dataObject = new JSONObject();
            dataObject.put("data", jsonObject);
            Log.e(CreateEventFragment.class.getSimpleName(), "Json -- " + dataObject.toString());
            return dataObject;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return new JSONObject();
    }

    void getTruckLoadTypes() {
        setIsLoading(true);

        getCompositeDisposable().add(getDataManager().doServerGetTruckLLoadTypesApiCall()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    setIsLoading(false);
                    response.addTruckLoad(0, "Select truck load");
                    setTruckLoadList(response.getResult());
                    getNavigator().setTruckLoadList(response.getResult());
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleErrorTruckLoadTypes(throwable);
                }));
    }

    void getAssetsApiCall(int driverId) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager().doServerGetAssetsApiCall(driverId)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    setIsLoading(false);
                    response.addAsset(0, "Select Vehicle");
                    setVehicleList(response.getResult());
                    getNavigator().setAssetList(response.getResult());
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleErrorAssetList(throwable);
                }));
    }

}
