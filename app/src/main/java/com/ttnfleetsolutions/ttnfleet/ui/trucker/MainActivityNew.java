package com.ttnfleetsolutions.ttnfleet.ui.trucker;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.request.RequestOptions;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.ttnfleetsolutions.ttnfleet.BR;
import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.Location;
import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.ServiceRequest;
import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.Vehicle;
import com.ttnfleetsolutions.ttnfleet.databinding.ActivityMainTruckerBinding;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseActivity;
import com.ttnfleetsolutions.ttnfleet.ui.chat.ChatFragment;
import com.ttnfleetsolutions.ttnfleet.ui.chat.MainChatFragment;
import com.ttnfleetsolutions.ttnfleet.ui.chat.ServiceDetailsChatFragment;
import com.ttnfleetsolutions.ttnfleet.ui.login.LoginActivity;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.dashboard.OngoingServiceFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.form.FeedbackDialogFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.history.ServicesHistoryFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.MainServicesFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.details.CreateEventFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.vmrs.assembly.SelectAssemblyFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.vmrs.component.SelectComponentFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.vmrs.system.SelectServiceFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.notification.NotificationsFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.servicerequest.ServiceDetailsFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.vehicle.VehicleFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.vehicle.details.VehicleDetailsFragment;
import com.ttnfleetsolutions.ttnfleet.utils.CommonUtils;
import com.ttnfleetsolutions.ttnfleet.utils.GlideApp;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;
import pl.com.salsoft.sqlitestudioremote.SQLiteStudioService;

import static com.ttnfleetsolutions.ttnfleet.data.remote.ApiEndPoint.DIR_IMAGE_PROFILE;
import static com.ttnfleetsolutions.ttnfleet.data.remote.ApiEndPoint.DIR_IMAGE_THEME_LOGO;
import static com.ttnfleetsolutions.ttnfleet.utils.AppConstants.PERMISSION_REQUEST_CALL_PHONE;

/**
 * Created by Sandip Chaulagain on 10/12/2017.
 *
 * @version 1.0
 */

public class MainActivityNew extends BaseActivity<ActivityMainTruckerBinding, MainViewModel> implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener, HasSupportFragmentInjector {

    public Toolbar mToolbar;
    private static final String TAG = MainActivityNew.class.getSimpleName();
    private DrawerLayout mDrawerLayout;
    private NavigationView mNavigationView;
    private MenuItem menuItemDone, menuItemNotification, menuItemSearch;
    private SearchView mSearchView;


    @Inject
    MainViewModel mainViewModel;

    private ActivityMainTruckerBinding mActivityMainTruckerBinding;

    @Inject
    DispatchingAndroidInjector<Fragment> fragmentDispatchingAndroidInjector;

    @Override
    protected MainViewModel getViewModel() {
        return mainViewModel;
    }

    @Override
    protected int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main_trucker;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.primary_dark));*/

        mActivityMainTruckerBinding = getViewDataBinding();
        mainViewModel.setNavigator(this);

        setUp();
        /* setContentView(R.layout.activity_main_trucker);

         */
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        String appLinkAction = appLinkIntent.getAction();
        Uri appLinkData = appLinkIntent.getData();
    }

    private void setUp() {
        SQLiteStudioService.instance().start(this);
        SQLiteStudioService.instance().setPort(9999);
        mToolbar = findViewById(R.id.baseToolbar);
        mToolbar.setTitle("Dashboard");
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayUseLogoEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(false);
        }
        setThemeColors();
        initComponents();
        addDataToDB();
    }

    public void setThemeColors() {
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(getViewModel().getDataManager().getThemeStatusBarColor());

        mToolbar.setBackgroundColor(getViewModel().getDataManager().getThemeTitleBarColor());
    }

    public static void setOverflowButtonColor(final Toolbar toolbar, final int color) {
        Drawable drawable = toolbar.getOverflowIcon();
        if (drawable != null) {
            drawable = DrawableCompat.wrap(drawable);
            DrawableCompat.setTint(drawable.mutate(), color);
            toolbar.setOverflowIcon(drawable);
        }
    }


    void addDataToDB() {
        mainViewModel.insertLocation(new Location(1, 0.0, 0.0, "3884 Eu Road, Boston, MA, 43448"));
        mainViewModel.insertLocation(new Location(2, 0.0, 0.0, "I-81, Blountville, TN 37617, USA"));
        mainViewModel.insertLocation(new Location(3, 0.0, 0.0, "State Highway 580, Tampa, FL, USA"));
        mainViewModel.insertLocation(new Location(4, 0.0, 0.0, "484–2500 Ullamcorper. Road, Houston, Texas, 31515"));
        mainViewModel.insertLocation(new Location(5, 0.0, 0.0, "8555 Semper Road, Independence, Missouri"));

        mainViewModel.insertVehicle(new Vehicle(1, "Maan Truck 8.136 FAE 4X4", "WVM56502016034590", "TX 01 FJ0122", "MAAN", "2013", "Biofuels"));
        mainViewModel.insertVehicle(new Vehicle(2, "IVECO Stralis 440S48 2002 – 2013", "WVM24389042384849", "WR 22 RK5685", "IVECO", "2012", "Diesel"));
        mainViewModel.insertVehicle(new Vehicle(3, "Mercedes-Benz Unimog", "WVM58956444545545", "", "", "", ""));
        mainViewModel.insertVehicle(new Vehicle(4, "Ford F-650 Supertruck", "WVM25454545454525", "", "", "", ""));
        mainViewModel.insertVehicle(new Vehicle(5, "Cenntro Citelec", "WVM56487897878787", "", "", "", ""));

        mainViewModel.insertServiceRequest(new ServiceRequest(1, "Drop off & Pick up", "SERVICE REQUEST 10", 1, 1,
                "", "17:10", "", "", "Unknown", "10 mins ago", 0, "8th Dec 12:45", "9th Dec 08:00"));
        mainViewModel.insertServiceRequest(new ServiceRequest(2, "", "SERVICE REQUEST 9", 2, 2,
                "Front axle left wheel tyre damage", "17:10", "", "", "1 Hour", "40 mins ago", 1, "", ""));
        mainViewModel.insertServiceRequest(new ServiceRequest(3, "", "SERVICE REQUEST 10", 3, 3,
                "Front axle left wheel tyre damage", "", "17:10", "3 Hours", "", "40 mins ago", 2, "", ""));
    }

    public static Intent getStartIntent(Context context) {
        return new Intent(context, MainActivityNew.class);
    }

    private void replaceFragment(Fragment fragment) {
        /*if (fragment instanceof IssueListFragment) {
            if (menuItemDone != null)
                menuItemDone.setVisible(true);
        } else {
            if (menuItemDone != null)
                menuItemDone.setVisible(false);
            invalidateOptionsMenu();
        }*/
        invalidateOptionsMenu();
        String backStateName = fragment.getClass().getName();
        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.container, fragment, backStateName);
            ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
        if (isLandscape()) {
            if (fragment instanceof OngoingServiceFragment) {
                /*ServiceDetailsFragment serviceDetailsFragment = new ServiceDetailsFragment();
                 *//*Bundle bundle = new Bundle();
                bundle.putInt("index", 5);
                serviceDetailsFragment.setArguments(bundle);*//*
                serviceDetailsFragment.updateIndex(1, 1);
                replaceFragmentDetails(serviceDetailsFragment, false);
                findViewById(R.id.details_container).setVisibility(View.VISIBLE);*/
            } else if (fragment instanceof VehicleFragment) {
                VehicleDetailsFragment fragmentAddVehicle = new VehicleDetailsFragment();
                fragmentAddVehicle.updateId(1);
                replaceFragmentDetails(fragmentAddVehicle, false);
                findViewById(R.id.details_container).setVisibility(View.VISIBLE);
            } else if (fragment instanceof ServicesHistoryFragment) {
                /*ServiceDetailsFragment serviceDetailsFragment = new ServiceDetailsFragment();
                 *//*Bundle bundle = new Bundle();
                bundle.putInt("in         dex", 5);
                serviceDetailsFragment.setArguments(bundle);*//*
                serviceDetailsFragment.updateIndex(-1, 3);
                replaceFragmentDetails(serviceDetailsFragment, false);
                findViewById(R.id.details_container).setVisibility(View.VISIBLE);*/
            } else if (fragment instanceof MainChatFragment) {
                ServiceDetailsChatFragment serviceDetailsChatFragment = new ServiceDetailsChatFragment();
                /*Bundle bundle = new Bundle();
                bundle.putInt("index", 5);
                serviceDetailsFragment.setArguments(bundle);*/
//                serviceDetailsFragment.updateIndex(0);
                replaceFragmentDetails(serviceDetailsChatFragment, false);
                findViewById(R.id.details_container).setVisibility(View.VISIBLE);
            } else {
                findViewById(R.id.details_container).setVisibility(View.GONE);
            }
        }

    }

    private void replaceFragmentDetails(Fragment fragment, boolean fragmentPopped) {
        String backStateName = fragment.getClass().getName();
        FragmentManager manager = getSupportFragmentManager();
//        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);
//        boolean fragmentPopped = false;

        if (!fragmentPopped) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.details_container, fragment, backStateName);
            ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
        updateSubTitle(fragment);
    }

    private void updateTitleAndDrawer(Fragment fragment) {
        /*if (fragment instanceof IssueListFragment) {
            if (menuItemDone != null)
                menuItemDone.setVisible(true);
        } else {
            if (menuItemDone != null)
                menuItemDone.setVisible(false);
            invalidateOptionsMenu();
        }*/
        invalidateOptionsMenu();
        CommonUtils.hideKeyboard(this);
        if (findViewById(R.id.details_container) != null)
            findViewById(R.id.details_container).setVisibility(View.GONE);

//        findViewById(R.id.floatingActionsMenu).setVisibility(View.GONE);
        String fragClassName = fragment.getClass().getName();
        if (fragClassName.equals(MainServicesFragment.class.getName())) {
            /*if (fragment.getTargetFragment() != null)
                fragment.getTargetFragment().setMenuVisibility(false);*/
            mToolbar.setTitle("Dashboard");
            mNavigationView.setCheckedItem(R.id.navItemFillForm);
        } else if (fragClassName.equals(SelectServiceFragment.class.getName())) {
            mToolbar.setTitle("Select System Code");
            mNavigationView.setCheckedItem(R.id.navItemFillForm);
        } else if (fragClassName.equals(SelectAssemblyFragment.class.getName())) {
            mToolbar.setTitle("Select Assembly Code");
            mNavigationView.setCheckedItem(R.id.navItemFillForm);
        } else if (fragClassName.equals(SelectComponentFragment.class.getName())) {
            mToolbar.setTitle("Select Component Code");
            mNavigationView.setCheckedItem(R.id.navItemFillForm);
        } else if (fragClassName.equals(OngoingServiceFragment.class.getName())) {
            mToolbar.setTitle("Ongoing Services");
            mNavigationView.setCheckedItem(R.id.navItemOngoing);
            if (findViewById(R.id.details_container) != null)
                findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else if (fragClassName.equals(NotificationsFragment.class.getName())) {
            mToolbar.setTitle("Notifications");
            mNavigationView.setCheckedItem(R.id.navItemNotifications);
        } else if (fragClassName.equals(MainChatFragment.class.getName())) {
            mToolbar.setTitle("Chat");
            mNavigationView.setCheckedItem(R.id.navItemChat);
            if (findViewById(R.id.details_container) != null)
                findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else if (fragClassName.equals(ServicesHistoryFragment.class.getName())) {
            mToolbar.setTitle("Event History");
            mNavigationView.setCheckedItem(R.id.navItemHistory);
            if (findViewById(R.id.details_container) != null)
                findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else if (fragClassName.equals(CreateEventFragment.class.getName())) {
            mToolbar.setTitle("Create Event");
            mNavigationView.setCheckedItem(R.id.navItemFillForm);
        } else if (fragClassName.equals(VehicleFragment.class.getName())) {
            mToolbar.setTitle("My Vehicles");
            mNavigationView.setCheckedItem(R.id.navItemVehicle);
//            findViewById(R.id.floatingActionsMenu).setVisibility(View.VISIBLE);
            if (findViewById(R.id.details_container) != null)
                findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else if (fragClassName.equals(VehicleDetailsFragment.class.getName())) {
//            mToolbar.setTitle("Add Vehicle");
            mNavigationView.setCheckedItem(R.id.navItemVehicle);
        } /*else if (fragClassName.equals(ServiceDetailsFragment.class.getName())) {
            mToolbar.setTitle("Event Details");
            mNavigationView.setCheckedItem(R.id.navItemOngoing);
        }*/ else if (fragClassName.equals(ServiceDetailsChatFragment.class.getName())) {
//            mToolbar.setTitle("Service Details Chat");
            mNavigationView.setCheckedItem(R.id.navItemChat);
        } else if (fragClassName.equals(ChatFragment.class.getName())) {
//            mToolbar.setTitle("Service Details 1");
            mNavigationView.setCheckedItem(R.id.navItemChat);
        } else if (fragClassName.equals(TrackLocationFragment.class.getName())) {
            mToolbar.setTitle("Track Location");
            mNavigationView.setCheckedItem(R.id.navItemOngoing);
        }
    }

    public void setTitle(String title) {
        if (!TextUtils.isEmpty(title)) {
            mToolbar.setTitle(title);
        }
    }

    private void updateSubTitle(Fragment fragment) {
        String fragClassName = fragment.getClass().getName();
        if (fragClassName.equals(VehicleDetailsFragment.class.getName())) {
//            setSubTitle("Add Vehicle");
        } /*else if (fragClassName.equals(ServiceDetailsFragment.class.getName())) {
            setSubTitle("Service Details");
        } */ else if (fragClassName.equals(ServiceDetailsChatFragment.class.getName())) {
//            setSubTitle("Chat");
        } else if (fragClassName.equals(ChatFragment.class.getName())) {
//            setSubTitle("Chat");
        } else if (fragClassName.equals(TrackLocationFragment.class.getName())) {
            setSubTitle("Track Location");
        }
    }

    public void setSubTitle(String subTitle) {
        if (!TextUtils.isEmpty(subTitle)) {
            ((TextView) findViewById(R.id.subTitle)).setText(subTitle);
            findViewById(R.id.subTitle).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.subTitle).setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);

        if (fragment != null) {
            if (isLandscape()) {
                Fragment subFragment = getSupportFragmentManager().findFragmentById(R.id.details_container);

                if (fragment instanceof SelectServiceFragment || fragment instanceof SelectAssemblyFragment || fragment instanceof SelectComponentFragment) {
                    if (menuItemSearch != null)
                        menuItemSearch.setVisible(true);
                    if (menuItemDone != null)
                        menuItemDone.setVisible(false);
                    if (menuItemNotification != null)
                        menuItemNotification.setVisible(false);
                } else if (fragment instanceof CreateEventFragment) {
                    if (menuItemSearch != null)
                        menuItemSearch.setVisible(false);
                    if (menuItemDone != null)
                        menuItemDone.setVisible(true);
                    if (menuItemNotification != null)
                        menuItemNotification.setVisible(false);
                } else {
                    if (menuItemSearch != null)
                        menuItemSearch.setVisible(false);
                    if (menuItemDone != null)
                        menuItemDone.setVisible(false);
                    if (menuItemNotification != null)
                        menuItemNotification.setVisible(true);
                }
            } else {
                if (fragment instanceof SelectServiceFragment || fragment instanceof SelectAssemblyFragment || fragment instanceof SelectComponentFragment) {
                    if (menuItemSearch != null)
                        menuItemSearch.setVisible(true);
                    if (menuItemDone != null)
                        menuItemDone.setVisible(false);
                    if (menuItemNotification != null)
                        menuItemNotification.setVisible(false);
                } else if (fragment instanceof CreateEventFragment) {
                    if (menuItemSearch != null)
                        menuItemSearch.setVisible(false);
                    if (menuItemDone != null)
                        menuItemDone.setVisible(true);
                    if (menuItemNotification != null)
                        menuItemNotification.setVisible(false);
                } else {
                    if (menuItemSearch != null)
                        menuItemSearch.setVisible(false);
                    if (menuItemDone != null)
                        menuItemDone.setVisible(false);
                    if (menuItemNotification != null)
                        menuItemNotification.setVisible(true);
                }
            }
            /*if (fragment instanceof OngoingServiceFragment) {
                findViewById(R.id.floatingActionsMenu).setVisibility(View.GONE);
            }*/
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.container);
        if (isLandscape()) {
            Fragment f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
            if (f != null && f1 != null) {
                if (f instanceof OngoingServiceFragment && f1 instanceof ServiceDetailsFragment) {
                    getSupportFragmentManager().popBackStack();
                } else if (f instanceof VehicleFragment && f1 instanceof VehicleDetailsFragment) {
                    getSupportFragmentManager().popBackStack();
//                    findViewById(R.id.floatingActionsMenu).setVisibility(View.GONE);
                } else if (f instanceof ServicesHistoryFragment && f1 instanceof ServiceDetailsFragment) {
                    getSupportFragmentManager().popBackStack();
                } else if (f instanceof MainChatFragment && f1 instanceof ServiceDetailsChatFragment) {
                    getSupportFragmentManager().popBackStack();
                }
                updateSubTitle(f1);
            }
        }
        if (f instanceof MainServicesFragment || f instanceof OngoingServiceFragment) {
            finish();
        } else {
            super.onBackPressed();
            f = getSupportFragmentManager().findFragmentById(R.id.container);
            if (f != null) {
                updateTitleAndDrawer(f);
            }
        }
    }

    private void initComponents() {
        mDrawerLayout = findViewById(R.id.drawerLayout);
        mNavigationView = findViewById(R.id.navigationView);


        int[][] state = new int[][]{
                new int[]{android.R.attr.state_checked},
                new int[]{android.R.attr.state_enabled},
                new int[]{android.R.attr.state_pressed},
                new int[]{android.R.attr.state_focused},
                new int[]{android.R.attr.state_pressed}
        };

        int[] color = new int[]{
                getViewModel().getDataManager().getThemeStatusBarColor(),
                Color.DKGRAY,
                Color.DKGRAY,
                Color.DKGRAY,
                Color.DKGRAY
        };

        //Defining ColorStateList for menu item Icon
        ColorStateList navMenuIconList = new ColorStateList(
                new int[][]{
                        new int[]{android.R.attr.state_checked},
                        new int[]{android.R.attr.state_enabled},
                        new int[]{android.R.attr.state_pressed},
                        new int[]{android.R.attr.state_focused},
                        new int[]{android.R.attr.state_pressed}
                },
                new int[]{
                        getViewModel().getDataManager().getThemeStatusBarColor(),
                        getViewModel().getDataManager().getThemeTitleBarColor(),
                        getViewModel().getDataManager().getThemeTitleBarColor(),
                        getViewModel().getDataManager().getThemeTitleBarColor(),
                        getViewModel().getDataManager().getThemeTitleBarColor()
                }
        );
        ColorStateList ColorStateList1 = new ColorStateList(state, color);
        mNavigationView.setItemTextColor(ColorStateList1);
//        mNavigationView.setItemIconTintList(ColorStateList.valueOf(getViewModel().getDataManager().getThemeStatusBarColor()));
        mNavigationView.setItemIconTintList(navMenuIconList);

//        mActivityMainTruckerBinding.navigationViewContent.setLayoutManager(new LinearLayoutManager(this));
//        mActivityMainTruckerBinding.navigationViewContent.setAdapter(new NavigationDrawerListAdapter(this, getViewModel().getDataManager().getThemeTitleBarColor());
        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawerLayout,
                mToolbar,
                R.string.open_drawer,
                R.string.close_drawer) {
        };
        mDrawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
        mNavigationView.setNavigationItemSelectedListener(this);

        View.OnClickListener onClickListener = mDrawerToggle.getToolbarNavigationClickListener();
        getSupportFragmentManager().addOnBackStackChangedListener(() -> {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
            if (fragment != null)
                if (fragment instanceof SelectServiceFragment || fragment instanceof SelectAssemblyFragment
                        || fragment instanceof SelectComponentFragment || fragment instanceof CreateEventFragment
                        || fragment instanceof VehicleDetailsFragment || fragment instanceof ServiceDetailsFragment
                        || fragment instanceof ChatFragment || fragment instanceof ServiceDetailsChatFragment
                        || fragment instanceof TrackLocationFragment) {
                    mDrawerToggle.setDrawerIndicatorEnabled(false);
                    mDrawerToggle.setToolbarNavigationClickListener(v -> onBackPressed());
                } else {
                    mDrawerToggle.setDrawerIndicatorEnabled(true);
                    mDrawerToggle.setToolbarNavigationClickListener(onClickListener);
                }

            Fragment f = getSupportFragmentManager().findFragmentById(R.id.container);
            if (f != null) {
                updateTitleAndDrawer(f);
            }
            if (isLandscape()) {
                Fragment f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
                if (f1 != null) {
                    updateSubTitle(f1);
                }
            }
        });
        mNavigationView.getMenu().findItem(R.id.navItemPlaceCall).setTitle(getViewModel().getDataManager().getCallForHelpLabel());
        View header = mNavigationView.getHeaderView(0);
        ImageView imageViewLogo = header.findViewById(R.id.logo);
        ImageView imageViewProfile = header.findViewById(R.id.profileImageView);
        header.findViewById(R.id.drawerHeaderBg).setBackgroundColor(getViewModel().getDataManager().getThemeTitleBarColor());

        ((TextView) header.findViewById(R.id.name)).setText(getViewModel().getDataManager().getCurrentUserFullName());
        header.findViewById(R.id.number).setVisibility(TextUtils.isEmpty(getViewModel().getDataManager().getCurrentUserRoleName()) ? View.GONE : View.VISIBLE);
        ((TextView) header.findViewById(R.id.number)).setText(getViewModel().getDataManager().getCurrentUserRoleName());

        GlideApp.with(this)
                .load(DIR_IMAGE_PROFILE + getViewModel().getDataManager().getCurrentUserID() + "/" + getViewModel().getDataManager().getCurrentUserProfileImage())
                .apply(RequestOptions.circleCropTransform())
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .into(imageViewProfile);

        GlideApp.with(this)
                .load(DIR_IMAGE_THEME_LOGO + getViewModel().getDataManager().getThemeLogo())
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .fitCenter()
                .into(imageViewLogo);

        getViewModel().createNewEventRequest();
        replaceFragment(new MainServicesFragment());
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        mDrawerLayout.closeDrawer(GravityCompat.START);

        switch (item.getItemId()) {
            case R.id.navItemPlaceCall:
                showCallDialog();
                return true;
            case R.id.navItemFillForm:
                getViewModel().createNewEventRequest();
                replaceFragment(new MainServicesFragment());
                return true;
            case R.id.navItemChat:
                replaceFragment(new MainChatFragment());
                return true;
            case R.id.navItemHistory:
                Fragment fragment = new ServicesHistoryFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("from", 2);
                fragment.setArguments(bundle);
                replaceFragment(fragment);
                return true;
            case R.id.navItemNotifications:
                replaceFragment(new NotificationsFragment());
                return true;
            case R.id.navItemOngoing:
                replaceFragment(new OngoingServiceFragment());
                return true;
            case R.id.navItemVehicle:
                replaceFragment(new VehicleFragment());
                return true;
            case R.id.navItemLogout:
                setDefaultTheme();
                Intent intent = LoginActivity.getStartIntent(MainActivityNew.this);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                return true;
            default:
                return false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.main, menu);
        menuItemDone = menu.findItem(R.id.action_done);
        menuItemNotification = menu.findItem(R.id.navItemNotifications);

        View count = menu.findItem(R.id.navItemNotifications).getActionView();
        count.setOnClickListener(view -> replaceFragment(new NotificationsFragment()));

        menuItemSearch = menu.findItem(R.id.action_search);
        mSearchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        mSearchView.setMaxWidth(Integer.MAX_VALUE);
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
                if (fragment != null)
                    if (fragment instanceof SelectServiceFragment)
                        ((SelectServiceFragment) fragment).searchQuery(newText);
                    else if (fragment instanceof SelectAssemblyFragment)
                        ((SelectAssemblyFragment) fragment).searchQuery(newText);
                    else if (fragment instanceof SelectComponentFragment)
                        ((SelectComponentFragment) fragment).searchQuery(newText);
                return true;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Drawable drawable = item.getIcon();
        if (drawable instanceof Animatable) {
            ((Animatable) drawable).start();
        }
        switch (item.getItemId()) {
            case R.id.navItemNotifications:
                replaceFragment(new NotificationsFragment());
                return true;
            case R.id.action_done:
                // handle confirmation button click here
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
                if (fragment != null)
                    if (fragment instanceof SelectServiceFragment)
                        showForm(null);
                    else if (fragment instanceof CreateEventFragment)
                        ((CreateEventFragment) fragment).onSubmitEventRequest();
                    else if (fragment instanceof VehicleDetailsFragment)
                        ((VehicleDetailsFragment) fragment).updateVehicle();
                    else if (isLandscape() && fragment instanceof VehicleFragment) {
                        Fragment f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
                        if (f1 instanceof VehicleDetailsFragment) {
                            ((VehicleDetailsFragment) f1).updateVehicle();
                        }
                    }

//                        replaceFragment(new VehicleFragment());
                /*if(menuItem != null) {
                    menuItem.setVisible(false);
                }*/
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void showChatDetails(int from, String title) {
        if (isLandscape()) {
            Fragment f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
            if (f1 instanceof ChatFragment) {
                final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ((ChatFragment) f1).setReceiver(title);
                ft.detach(f1);
                ft.attach(f1);
                ft.commit();
            } else {
                ChatFragment chatFragment = new ChatFragment();
                chatFragment.setReceiver(title);
                replaceFragmentDetails(chatFragment, false);
            }
            setSubTitle(title);
            //update details only
            findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else {
            mToolbar.setTitle(title);
            ChatFragment chatFragment = new ChatFragment();
            chatFragment.setReceiver(title);
            replaceFragment(chatFragment);
        }
        mNavigationView.setCheckedItem(R.id.navItemChat);
    }

    public void showServiceChatThreadDetails(int from, String title) {
        if (isLandscape()) {
            Fragment f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
            if (f1 instanceof ServiceDetailsChatFragment) {
                final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//                ((ServiceDetailsChatFragment) f1).updateIndex(index);
                ft.detach(f1);
                ft.attach(f1);
                ft.commit();
            } else {
                ServiceDetailsChatFragment serviceDetailsChatFragment = new ServiceDetailsChatFragment();
                String backStateName = serviceDetailsChatFragment.getClass().getName();
                FragmentManager manager = getSupportFragmentManager();
                boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);
                replaceFragmentDetails(serviceDetailsChatFragment, fragmentPopped);
            }
            setSubTitle(title);
            //update details only
            findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else {
            mToolbar.setTitle(title);
            ServiceDetailsChatFragment serviceDetailsChatFragment = new ServiceDetailsChatFragment();
            replaceFragment(serviceDetailsChatFragment);
        }
        mNavigationView.setCheckedItem(R.id.navItemChat);
    }

    public void trackLocation(int from) {
        if (isLandscape()) {
            Fragment f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
            if (f1 instanceof TrackLocationFragment) {
                final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//                ((ServiceDetailsChatFragment) f1).updateIndex(index);
                ft.detach(f1);
                ft.attach(f1);
                ft.commit();
            } else {
                TrackLocationFragment trackLocationFragment = new TrackLocationFragment();
                replaceFragmentDetails(trackLocationFragment, false);
            }

            //update details only
            findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else {
            TrackLocationFragment trackLocationFragment = new TrackLocationFragment();
            replaceFragment(trackLocationFragment);
        }
        mNavigationView.setCheckedItem(R.id.navItemChat);
    }

    /* public void goToHome() {
         replaceFragment(new MainServicesFragment());
         mNavigationView.setCheckedItem(R.id.navItemFillForm);
     }
 */
    public void showFeedbackDialog() {
        DialogFragment fragment = new FeedbackDialogFragment();
        fragment.show(getSupportFragmentManager(), "feedback");
    }

    public void showForm(com.ttnfleetsolutions.ttnfleet.data.model.api.location.Location location) {
        /*Fragment newFragment = new CreateServiceRequestFormFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("type", type);
        bundle.putString("service", serviceType);
        newFragment.setArguments(bundle);
        replaceFragment(newFragment);*/
        if (location != null)
            getViewModel().updateLocation(location);
        Fragment newFragment = new CreateEventFragment();
        replaceFragment(newFragment);
    }

    public void showSystemListing() {
        Fragment newFragment = new SelectServiceFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("from", 0);
        newFragment.setArguments(bundle);
        replaceFragment(newFragment);
    }

    public void showAssemblyListing(String systemCode, String systemCodeName) {
        Fragment newFragment = new SelectAssemblyFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("from", 0);
        bundle.putString("system", systemCode);
        bundle.putString("systemName", systemCodeName);
        newFragment.setArguments(bundle);
        replaceFragment(newFragment);
    }

    public void showComponentListing(String systemCode, String assemblyCode, String systemCodeName, String assemblyCodeName) {
        Fragment newFragment = new SelectComponentFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("from", 0);
        bundle.putString("system", systemCode);
        bundle.putString("assembly", assemblyCode);
        bundle.putString("systemName", systemCodeName);
        bundle.putString("assemblyName", assemblyCodeName);
        newFragment.setArguments(bundle);
        replaceFragment(newFragment);
    }

    public void showOngoingServices() {
        FragmentManager fm = getSupportFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
        getSupportFragmentManager().beginTransaction().remove(getSupportFragmentManager().findFragmentById(R.id.container)).commit();
        replaceFragment(new OngoingServiceFragment());
        mNavigationView.setCheckedItem(R.id.navItemOngoing);
    }

    public void goToVehicleDetails(int from, int index) {
        if (isLandscape()) {
            /*ServiceDetailsFragment serviceDetailsFragment = new ServiceDetailsFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("index", index);
            serviceDetailsFragment.setArguments(bundle);
            replaceFragmentDetails(serviceDetailsFragment);*/

            Fragment f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
            if (f1 instanceof VehicleDetailsFragment) {
                final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//                ((VehicleDetailsFragment) f1).updateIndex(index);
                ft.detach(f1);
                ft.attach(f1);

                ft.commit();
            } else {
               /*ServiceDetailsFragment serviceDetailsFragment = new ServiceDetailsFragment();
               Bundle bundle = new Bundle();
               bundle.putInt("index", index);
               serviceDetailsFragment.setArguments(bundle);
               replaceFragment(serviceDetailsFragment);*/
            }

            //update details only
            findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else {
            VehicleDetailsFragment fragmentAddVehicle = new VehicleDetailsFragment();
            /*Bundle bundle = new Bundle();
            bundle.putInt("index", index);
            serviceDetailsFragment.setArguments(bundle);*/
//            fragmentAddVehicle.updateIndex(index);
            replaceFragment(fragmentAddVehicle);
        }
        mNavigationView.setCheckedItem(R.id.navItemVehicle);
    }

    public void goToVehicleDetails(long _id) {
        if (isLandscape()) {
            /*ServiceDetailsFragment serviceDetailsFragment = new ServiceDetailsFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("index", index);
            serviceDetailsFragment.setArguments(bundle);
            replaceFragmentDetails(serviceDetailsFragment);*/

            Fragment f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
            if (f1 instanceof VehicleDetailsFragment) {
                final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ((VehicleDetailsFragment) f1).updateId(_id);
                ft.detach(f1);
                ft.attach(f1);

                ft.commit();
            } else {
               /*ServiceDetailsFragment serviceDetailsFragment = new ServiceDetailsFragment();
               Bundle bundle = new Bundle();
               bundle.putInt("index", index);
               serviceDetailsFragment.setArguments(bundle);
               replaceFragment(serviceDetailsFragment);*/
            }

            //update details only
            findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else {
            VehicleDetailsFragment fragmentAddVehicle = new VehicleDetailsFragment();
            /*Bundle bundle = new Bundle();
            bundle.putInt("index", index);
            serviceDetailsFragment.setArguments(bundle);*/
            fragmentAddVehicle.updateId(_id);
            replaceFragment(fragmentAddVehicle);
        }
        mNavigationView.setCheckedItem(R.id.navItemVehicle);
    }

    public void goToServiceDetails(int eventId) {
        if (isLandscape()) {
            loadSplitViewIfLandscape(eventId);
            /*Fragment f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
            if (f1 instanceof ServiceDetailsFragment) {
                final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                Bundle bundle = new Bundle();
                bundle.putInt("eventId", eventId);
                f1.setArguments(bundle);

                String backStateName = f1.getClass().getName();
                FragmentManager manager = getSupportFragmentManager();
                boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);
                replaceFragmentDetails(f1, fragmentPopped);
//                ((ServiceDetailsFragment) f1).updateEventDetails(eventId);
//                ft.detach(f1);
//                ft.attach(f1);
//
//                ft.commit();
            } else {
                ServiceDetailsFragment serviceDetailsFragment = new ServiceDetailsFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("eventId", eventId);
                serviceDetailsFragment.setArguments(bundle);

                String backStateName = serviceDetailsFragment.getClass().getName();
                FragmentManager manager = getSupportFragmentManager();
                boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);
                replaceFragmentDetails(serviceDetailsFragment, fragmentPopped);

                *//*if (fragmentPopped) {
                    f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
                    if (f1 instanceof ServiceDetailsFragment) {
                        final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();


                        replaceFragmentDetails(f1, fragmentPopped);
//                        ((ServiceDetailsFragment) f1).updateEventDetails(eventId);
//                        ft.detach(f1);
//                        ft.attach(f1);
//                        ft.commit();
                    }
                }*//*
            }*/

            //update details only
            findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else {
            ServiceDetailsFragment serviceDetailsFragment = new ServiceDetailsFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("eventId", eventId);
            serviceDetailsFragment.setArguments(bundle);
            replaceFragment(serviceDetailsFragment);
        }
        mNavigationView.setCheckedItem(R.id.navItemOngoing);
    }

    public void goToServiceDetailsFromNotifiction(int from, int index) {
        if (isLandscape()) {
            replaceFragment(new OngoingServiceFragment());
        } else {
            ServiceDetailsFragment serviceDetailsFragment = new ServiceDetailsFragment();
            serviceDetailsFragment.updateIndex(from, index);
            replaceFragment(serviceDetailsFragment);
        }
    }


    public void showCallDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.call_desc);
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.call, (dialogInterface, i) -> {
            if (ActivityCompat.checkSelfPermission(MainActivityNew.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(MainActivityNew.this, new String[]{Manifest.permission.CALL_PHONE}, PERMISSION_REQUEST_CALL_PHONE);
                return;
            }
            actionCallIntent();
        });
        builder.setNegativeButton(R.string.cancel, null);
        builder.show();
    }

    private void actionCallIntent() {
        if (CommonUtils.isSimAvailable(MainActivityNew.this)) {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + getString(R.string.call_desc)));
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                startActivity(callIntent);
            }
        } else {
            Toast.makeText(this, "Sim card not available", Toast.LENGTH_LONG).show();
        }

    }

    private void actionDialIntent() {
        if (CommonUtils.isSimAvailable(MainActivityNew.this)) {
            Intent callIntent = new Intent(Intent.ACTION_DIAL);
            callIntent.setData(Uri.parse("tel:" + getString(R.string.call_desc)));
            startActivity(callIntent);
        } else {
            Toast.makeText(this, "Sim card not available", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST_CALL_PHONE:
                if (grantResults.length <= 0) {
                    actionDialIntent();
                } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    actionCallIntent();
                } else {
                    actionDialIntent();
                }
                break;
        }
    }

    public void addVehicle(int id) {
        switch (id) {
            case R.id.actionEnterVIN:
//                ((FloatingActionsMenu) findViewById(R.id.floatingActionsMenu)).collapse();
                goToVehicleDetails(-1);
                break;
            case R.id.actionScanBarcode:
//                ((FloatingActionsMenu) findViewById(R.id.floatingActionsMenu)).collapse();
                new IntentIntegrator(MainActivityNew.this).initiateScan(); // `this` is the current Activity
                break;
            case R.id.actionAddVehicle:
//                ((FloatingActionsMenu) findViewById(R.id.floatingActionsMenu)).collapse();
                goToVehicleDetails(-1);
                break;
        }
    }

    // Get the results:
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {
                Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "Scanned: " + result.getContents(), Toast.LENGTH_LONG).show();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onClick(View view) {
        /*switch (view.getId()) {
            case R.id.actionEnterVIN:
            case R.id.actionScanBarcode:
            case R.id.actionAddVehicle:
                addVehicle(view.getId());
                break;

        }*/
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentDispatchingAndroidInjector;
    }

    public void loadSplitViewIfLandscape(int eventId) {
        ServiceDetailsFragment serviceDetailsFragment = new ServiceDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("eventId", eventId);
        serviceDetailsFragment.setArguments(bundle);
        replaceFragmentDetails(serviceDetailsFragment, false);
        findViewById(R.id.details_container).setVisibility(View.VISIBLE);
    }
}
