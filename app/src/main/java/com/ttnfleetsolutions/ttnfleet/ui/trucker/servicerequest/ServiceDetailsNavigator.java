package com.ttnfleetsolutions.ttnfleet.ui.trucker.servicerequest;

import com.ttnfleetsolutions.ttnfleet.data.model.api.event.listing.Event;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.requirements.RequireImagesResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.AcceptWorkOrderResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.survey.SubmitSurveyResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.survey.SurveyQuestionsResponse;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 2/2/2018.
 *
 * @version 1.0
 */

interface ServiceDetailsNavigator {

    void loadEventDetails(Event event);

    void loadJobRequirementImages(List<RequireImagesResponse.Image> imagesList, Integer eventId, int jobId);

    void handleErrorDetails(Throwable throwable);

    void onRejectWorkOrders(AcceptWorkOrderResponse acceptWorkOrderResponse);

    void loadSurveyQuestions(SurveyQuestionsResponse surveyQuestionsResponse, int eventId, String vehicleName, String VIN, String serviceIssue, String datetime);

    void onSubmitSurveyResponse(SubmitSurveyResponse submitSurveyResponse);

    void handleErrorSubmitSurvey(Throwable throwable);

    void handleErrorSurveyQuestions(Throwable throwable);

    void handleErrorRequirementImages(Throwable throwable);
}
