package com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.details;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.SystemCode;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseViewHolder;
import com.ttnfleetsolutions.ttnfleet.databinding.RowSelectedCodeBinding;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */

public class SelectedCodeRecyclerViewAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private List<SystemCode> mList;

    private SelectedCodeItemViewModel.ItemViewModelListener mListener;

    public void setListener(SelectedCodeItemViewModel.ItemViewModelListener listener) {
        this.mListener = listener;
    }

    public SelectedCodeRecyclerViewAdapter(List<SystemCode> list) {
        this.mList = list;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RowSelectedCodeBinding rowBinding = RowSelectedCodeBinding.inflate(LayoutInflater.from(parent.getContext()),
                parent, false);
        return new MyViewHolder(rowBinding);
    }

    @Override
    public int getItemViewType(int position) {
        if (mList != null && mList.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public int getItemCount() {
        if (mList != null && mList.size() > 0) {
            return mList.size();
        } else {
            return 0;
        }
    }

    public SystemCode getItem(int position) {
        return mList.get(position);
    }

    public void addItems(List<SystemCode> list) {
        mList = list;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends BaseViewHolder implements SelectedCodeItemViewModel.ItemViewModelListener {

        private RowSelectedCodeBinding mBinding;

        private SelectedCodeItemViewModel mSelectServiceItemViewModel;

        public MyViewHolder(RowSelectedCodeBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {

            final SystemCode code = mList.get(position);
//            code.setPosition(position);
            mSelectServiceItemViewModel = new SelectedCodeItemViewModel(code, this);

            mBinding.setViewModel(mSelectServiceItemViewModel);

            // Immediate Binding
            // When a variable or observable changes, the binding will be scheduled to change before
            // the next frame. There are times, however, when binding must be executed immediately.
            // To force execution, use the executePendingBindings() method.
            mBinding.executePendingBindings();
        }

        @Override
        public void onRemoveItemClick(String systemCode) {
            mListener.onRemoveItemClick(systemCode);
        }
    }
}

