package com.ttnfleetsolutions.ttnfleet.ui.fleet;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ttnfleetsolutions.ttnfleet.R;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */

public class NotificationRecyclerViewAdapter extends RecyclerView.Adapter<NotificationRecyclerViewAdapter.MyViewHolder> {

    private Context mContext;

    private String[] times, texts;

    public NotificationRecyclerViewAdapter(Context context) {
        this.mContext = context;
        times = mContext.getResources().getStringArray(R.array.times2);
        texts = mContext.getResources().getStringArray(R.array.fd_notifications);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_notification_fd, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        if (position > 0) {
            holder.rowLayout.setBackgroundColor(ContextCompat.getColor(mContext, android.R.color.transparent));
        } else {
            holder.rowLayout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.tr_gray));
        }
//        holder.serviceRequestName.setVisibility(View.GONE);
        switch (position) {
            case 7:
//                holder.serviceRequestName.setText("Service Request 1");
                holder.acceptButton.setVisibility(View.GONE);
                break;
            case 6:
//                holder.serviceRequestName.setText("Service Request 1");
                holder.acceptButton.setVisibility(View.VISIBLE);
                holder.acceptButton.setText(R.string.track_location);
                break;
            case 5:
//                holder.serviceRequestName.setText("Service Request 1");
                holder.acceptButton.setVisibility(View.GONE);
                break;
            case 4:
//                holder.serviceRequestName.setText("Service Request 2");
                holder.acceptButton.setVisibility(View.GONE);
                break;
            case 3:
//                holder.serviceRequestName.setText("Service Request 1");
                holder.acceptButton.setVisibility(View.GONE);
                break;
            case 2:
//                holder.serviceRequestName.setText("Service Request 1");
                holder.acceptButton.setVisibility(View.GONE);
                holder.acceptButton.setText(R.string.complete_order);
                break;
            case 1:
//                holder.serviceRequestName.setText("Service Request 2");
                holder.acceptButton.setVisibility(View.GONE);
                holder.acceptButton.setText(R.string.approve);
                break;
            case 0:
                holder.acceptButton.setVisibility(View.VISIBLE);
                holder.acceptButton.setText(R.string.complete_job);
                break;
        }
        /*if(holder.serviceRequestName.getText().toString().contains("1")) {
            holder.serviceRequestName.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(mContext, R.color.colorPrimary)));
        } else {
            holder.serviceRequestName.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(mContext, android.R.color.holo_orange_dark)));
        }*/
        holder.serviceRequestName.setText("Service Request 1");
        holder.rejectButton.setVisibility(View.GONE);
        holder.acceptButton.setTag(holder.acceptButton.getText().toString());
        holder.acceptButton.setOnClickListener(view -> {
            String tag = view.getTag().toString();
            if(tag.equalsIgnoreCase(mContext.getResources().getString(R.string.dispatch))) {

            } else if(tag.equalsIgnoreCase(mContext.getResources().getString(R.string.allocate_technician))) {

            }
           /* switch ((int) view.getTag()) {
                case 4:
//                        mContext.startActivity(new Intent(mContext, TrackLocationActivity.class));
                    break;
                case 3:
                   *//* AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    builder.setMessage("Please acceptButton technician arrival");
                    builder.setCancelable(false);
                    builder.setPositiveButton("acceptButton", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    builder.setNegativeButton("CANCEL", null);
                    builder.show();*//*
                    break;
                case 1:
                    break;
                case 0:
//                    mContext.startActivity(new Intent(mContext, TrackLocationActivity.class));
//                        FeedbackDialogFragment fragment = new FeedbackDialogFragment();
//                        fragment.show(((AppCompatActivity) mContext).getSupportFragmentManager()(), "feedback");
                   *//* FragmentManager fragmentManager = ((AppCompatActivity) mContext).getSupportFragmentManager()();
                    Fragment newFragment = new FeedbackFragment();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
                    transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                    transaction.replace(R.id.container, newFragment).addToBackStack("FEEDBACK").commit();*//*
                    break;
            }*/
        });
        holder.time.setText(times[position]);
        holder.message.setText(Html.fromHtml(texts[position]));
        holder.rowLayout.setTag(position);
//        holder.layout1.setOnClickListener(view -> mContext.startActivity(new Intent(mContext, OrderDetailsActivity.class).putExtra("index", 5)));
        holder.rowLayout.setOnClickListener(view -> ((DashboardActivity) mContext).goToOrderDetailsFromNotifiction(-1, 3));
    }

    @Override
    public int getItemCount() {
        return texts.length;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView message, time, acceptButton, rejectButton, serviceRequestName;
        public ConstraintLayout rowLayout;

        public MyViewHolder(View view) {
            super(view);
            serviceRequestName = view.findViewById(R.id.serviceRequestName);
            rowLayout = view.findViewById(R.id.rowLayout);
            message = view.findViewById(R.id.notificationMessage);
            time = view.findViewById(R.id.datetime);
            acceptButton = view.findViewById(R.id.acceptButton);
            rejectButton = view.findViewById(R.id.rejectButton);
        }
    }
}
