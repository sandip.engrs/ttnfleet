package com.ttnfleetsolutions.ttnfleet.ui.technician;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ttnfleetsolutions.ttnfleet.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class JobCheckListFragment extends Fragment {


    View jobCheckListViewTech;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        jobCheckListViewTech = inflater.inflate(R.layout.fragment_tech_job_checklist, container, false);
        jobCheckListViewTech.findViewById(R.id.confirm).setOnClickListener(view -> getActivity().onBackPressed());
        return jobCheckListViewTech;
    }

}
