package com.ttnfleetsolutions.ttnfleet.ui.fleet.provider;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Sandip Chaulagain on 4/4/2018.
 *
 * @version 1.0
 */
@Module
public abstract class ServiceProviderFragmentProvider {

    @ContributesAndroidInjector(modules = ServiceProviderFragmentModule.class)
    abstract ServiceProviderFragment provideServiceProviderFragmentFactory();
}
