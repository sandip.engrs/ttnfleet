package com.ttnfleetsolutions.ttnfleet.ui.serviceprovider.technician;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.ttnfleetsolutions.ttnfleet.data.model.api.technician.Technician;
import com.ttnfleetsolutions.ttnfleet.databinding.RowEmptyViewBinding;
import com.ttnfleetsolutions.ttnfleet.databinding.RowTechnicianBinding;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseViewHolder;
import com.ttnfleetsolutions.ttnfleet.ui.empty.EmptyItem;
import com.ttnfleetsolutions.ttnfleet.ui.empty.EmptyItemViewModel;
import com.ttnfleetsolutions.ttnfleet.ui.empty.EmptyViewHolder;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */

public class AllocateTechnicianAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private List<Technician> mList;

    private AllocateTechnicianItemViewModel.ViewModelListener mListener;
    private EmptyItemViewModel.EmptyItemViewModelListener mEmptyItemViewModelListener;
    private EmptyItem mEmptyItem;

    public void setEmptyItem(EmptyItem mEmptyItem) {
        this.mEmptyItem = mEmptyItem;
    }

    public void setEmptyItemViewModelListener(EmptyItemViewModel.EmptyItemViewModelListener mEmptyItemViewModelListener) {
        this.mEmptyItemViewModelListener = mEmptyItemViewModelListener;
    }

    public void setListener(AllocateTechnicianItemViewModel.ViewModelListener listener) {
        this.mListener = listener;
    }

    public AllocateTechnicianAdapter(List<Technician> list) {
        this.mList = list;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                RowTechnicianBinding rowBinding = RowTechnicianBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
                return new MyViewHolder(rowBinding);
            case VIEW_TYPE_EMPTY:
            default:
                RowEmptyViewBinding emptyViewBinding = RowEmptyViewBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
                return new EmptyViewHolder(emptyViewBinding, mEmptyItem, mEmptyItemViewModelListener);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mList != null && mList.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public int getItemCount() {
        if (mList != null && mList.size() > 0) {
            return mList.size();
        } else {
            return 1;
        }
    }

    public void addItems(List<Technician> list) {
        mList = list;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends BaseViewHolder implements AllocateTechnicianItemViewModel.ViewModelListener {

        private RowTechnicianBinding mBinding;

        private AllocateTechnicianItemViewModel mItemViewModel;

        public MyViewHolder(RowTechnicianBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {

            final Technician technician = mList.get(position);

            mItemViewModel = new AllocateTechnicianItemViewModel(technician, this);

            mBinding.setViewModel(mItemViewModel);

            // Immediate Binding
            // When a variable or observable changes, the binding will be scheduled to change before
            // the next frame. There are times, however, when binding must be executed immediately.
            // To force execution, use the executePendingBindings() method.
            mBinding.executePendingBindings();

        }

        @Override
        public void onItemClick(int technicianId, String name) {
            mListener.onItemClick(technicianId, name);
        }
    }
}
