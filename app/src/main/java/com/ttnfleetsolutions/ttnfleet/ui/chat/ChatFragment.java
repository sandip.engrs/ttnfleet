package com.ttnfleetsolutions.ttnfleet.ui.chat;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.ttnfleetsolutions.ttnfleet.R;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */

public class ChatFragment extends Fragment {

    private String mReceiver;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_chat, container, false);
        ListView chatListView = rootView.findViewById(R.id.chatListView);
        chatListView.setAdapter(new ChatAdapter(getActivity(), mReceiver));
        return rootView;
    }

    public void setReceiver(String mReceiver) {
        this.mReceiver = mReceiver;
    }
}
