package com.ttnfleetsolutions.ttnfleet.ui.technician.additional;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Sandip Chaulagain on 1/22/2018.
 *
 * @version 1.0
 */
@Module
public abstract class CreateAdditionalJobFragmentProvider {

    @ContributesAndroidInjector(modules = CreateAdditionalJobFragmentModule.class)
    abstract CreateAdditionalJobFragment provideCreateAdditionalJobFragmentFactory();
}
