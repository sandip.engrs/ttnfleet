package com.ttnfleetsolutions.ttnfleet.ui.trucker.notification;

import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.utils.rx.SchedulerProvider;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Sandip Chaulagain on 2/2/2018.
 *
 * @version 1.0
 */
@Module
public class NotificationsFragmentModule {

    @Provides
    NotificationsViewModel provideNotificationsViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        return new NotificationsViewModel(dataManager, schedulerProvider);
    }

    @Provides
    NotificationRecyclerViewAdapter provideNotificationRecyclerViewAdapter() {
        return new NotificationRecyclerViewAdapter(new ArrayList<>());
    }


}
