package com.ttnfleetsolutions.ttnfleet.ui.serviceprovider;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.ttnfleetsolutions.ttnfleet.R;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */

public class AdditionalJobsListAdapter extends RecyclerView.Adapter<AdditionalJobsListAdapter.MyViewHolder> {

    private final String[] times;
    private final String[] desc;

    public AdditionalJobsListAdapter(Context context) {
        Context mContext = context;
        String[] tasks = mContext.getResources().getStringArray(R.array.additional_job_task);
        times = mContext.getResources().getStringArray(R.array.additional_job_task_duration);
        desc = mContext.getResources().getStringArray(R.array.additional_job_task_desc);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_additional_job_new_sp, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
//        holder.task.setText(tasks[position]);
        holder.duration.setText(times[position]);
        holder.desc.setText(desc[position]);

        holder.rowLayout.setOnClickListener(view -> holder.task.setChecked(!holder.task.isChecked()));
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public final CheckBox task;
        public final TextView duration;
        public final TextView desc;
        public final ConstraintLayout rowLayout;

        public MyViewHolder(View view) {
            super(view);
            rowLayout = view.findViewById(R.id.rowLayout);
            task = view.findViewById(R.id.checkBox);
            duration = view.findViewById(R.id.datetime);
            desc = view.findViewById(R.id.issueDescription);

        }
    }
}
