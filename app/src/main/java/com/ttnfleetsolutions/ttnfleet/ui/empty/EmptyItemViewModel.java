package com.ttnfleetsolutions.ttnfleet.ui.empty;

import android.databinding.ObservableField;

/**
 * Created by Sandip Chaulagain on 11/10/2017.
 *
 * @version 1.0
 */
public class EmptyItemViewModel {

    private EmptyItemViewModelListener mListener;
    public ObservableField<String> mMessage;
    public ObservableField<Boolean> mIsRetryVisible;
    public ObservableField<Boolean> mIsLogoVisible;

    public EmptyItemViewModel(EmptyItem emptyItem, EmptyItemViewModelListener listener) {
        this.mListener = listener;
        if(emptyItem == null) {
            emptyItem = new EmptyItem("No items available.", false, false);
        }
        this.mMessage = new ObservableField<>(emptyItem.getMessage());
        this.mIsRetryVisible = new ObservableField<>(emptyItem.isRetryVisible());
        this.mIsLogoVisible = new ObservableField<>(emptyItem.isLogoVisible());
    }

    public void onRetryClick() {
        mListener.onRetryClick();
    }

    public interface EmptyItemViewModelListener {
        void onRetryClick();
    }

}
