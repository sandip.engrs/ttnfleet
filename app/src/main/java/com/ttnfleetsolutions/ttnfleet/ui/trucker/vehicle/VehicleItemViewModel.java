package com.ttnfleetsolutions.ttnfleet.ui.trucker.vehicle;

import android.databinding.ObservableField;

import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.Vehicle;

/**
 * Created by Sandip Chaulagain on 1/19/2018.
 *
 * @version 1.0
 */

public class VehicleItemViewModel {

    private Vehicle mVehicle;
    public ObservableField<Long> id;
    public ObservableField<String> vehicleName;
    public ObservableField<String> VIN;

    private VehicleItemViewModelListener mListener;

    VehicleItemViewModel(Vehicle vehicle, VehicleItemViewModelListener listener) {
        this.mVehicle = vehicle;
        this.mListener = listener;
        id = new ObservableField<>(vehicle.id);
        vehicleName = new ObservableField<>(vehicle.vehicle_name);
        VIN = new ObservableField<>(vehicle.vin);
    }

    public void onItemClick() {
        mListener.onItemClick(mVehicle.id);
    }

    public interface VehicleItemViewModelListener {
        void onItemClick(long id);
    }
}
