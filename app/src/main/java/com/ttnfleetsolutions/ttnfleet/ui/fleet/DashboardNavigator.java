package com.ttnfleetsolutions.ttnfleet.ui.fleet;

import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.AddNoteResponse;

/**
 * Created by Sandip Chaulagain on 1/10/2018.
 *
 * @version 1.0
 */

public interface DashboardNavigator {

    void handleError(Throwable throwable);

    void onAddedNote(AddNoteResponse addNoteResponse);

}
