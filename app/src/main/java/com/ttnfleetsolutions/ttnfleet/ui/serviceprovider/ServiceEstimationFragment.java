package com.ttnfleetsolutions.ttnfleet.ui.serviceprovider;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ttnfleetsolutions.ttnfleet.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ServiceEstimationFragment extends Fragment {


    View serviceEstimationView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        serviceEstimationView = inflater.inflate(R.layout.fragment_service_estimation, container, false);


        RecyclerView mRecyclerView = serviceEstimationView.findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        mRecyclerView.setAdapter(new ServiceEstimationAdapter(getActivity()));

        return serviceEstimationView;
    }
}
