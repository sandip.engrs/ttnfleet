package com.ttnfleetsolutions.ttnfleet.ui.technician.requirements;

/**
 * Created by Sandip Chaulagain on 6/22/2018.
 *
 * @version 1.0
 */
interface RequirementsNavigator {

    void handleError(Throwable throwable);

    void onUploadRequirementsResponseReceived(String response);
}
