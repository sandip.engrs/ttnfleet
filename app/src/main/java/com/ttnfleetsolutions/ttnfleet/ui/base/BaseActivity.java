/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.ttnfleetsolutions.ttnfleet.ui.base;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;


import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.utils.CommonUtils;
import com.ttnfleetsolutions.ttnfleet.utils.NetworkUtils;

import dagger.android.AndroidInjection;

import static android.content.res.Configuration.ORIENTATION_LANDSCAPE;


public abstract class BaseActivity<T extends ViewDataBinding, V extends BaseViewModel> extends AppCompatActivity implements BaseFragment.Callback {

    // TODO
    // this can probably depend on isLoading variable of BaseViewModel,
    // since its going to be common for all the activities
    private ProgressDialog mProgressDialog;

    private T mViewDataBinding;
    private V mViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        if (getResources().getBoolean(R.bool.portrait_mode)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
        performDependencyInjection();
        super.onCreate(savedInstanceState);
        performDataBinding();
        mViewModel.onViewCreated();
    }

    private void performDataBinding() {
        mViewDataBinding = DataBindingUtil.setContentView(this, getLayoutId());
        this.mViewModel = mViewModel == null ? getViewModel() : mViewModel;
        mViewDataBinding.setVariable(getBindingVariable(), mViewModel);
        mViewDataBinding.executePendingBindings();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void requestPermissionsSafely(String[] permissions, int requestCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissions, requestCode);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean hasPermission(String permission) {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
                checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
    }


    @Override
    protected void onDestroy() {
        mViewModel.onDestroyView();
        super.onDestroy();
    }

    @Override
    public void onFragmentAttached() {

    }

    @Override
    public void onFragmentDetached(String tag) {

    }

    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    public void openActivityOnTokenExpire() {
//        startActivity(LoginActivity.getStartIntent(this));
        finish();
    }

    public boolean isNetworkConnected() {
        return NetworkUtils.isNetworkConnected(getApplicationContext());
    }

    public void showLoading() {
        hideLoading();
        mProgressDialog = CommonUtils.showLoadingDialog(this);
    }

    public void hideLoading() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.cancel();
        }
    }

    public void showInternetConnectionErrorDialog() {
        Toast.makeText(this, getString(R.string.network_error), Toast.LENGTH_LONG).show();
    }

    protected T getViewDataBinding() {
        return mViewDataBinding;
    }

    /**
     * Override for set view model
     *
     * @return view model instance
     */
    protected abstract V getViewModel();

    /**
     * Override for set binding variable
     *
     * @return variable id
     */
    protected abstract int getBindingVariable();

    /**
     * @return layout resource id
     */
    protected abstract @LayoutRes
    int getLayoutId();

    private void performDependencyInjection() {
        AndroidInjection.inject(this);
    }

    protected void setDefaultTheme() {
        getViewModel().getDataManager().setGUID("");
        getViewModel().getDataManager().setCurrentUserFullName("");
        getViewModel().getDataManager().setCurrentUserRoleName("");
        getViewModel().getDataManager().setCurrentUserProfileImage("");
        getViewModel().getDataManager().setThemeTextColor(Color.parseColor("#212121"));
        getViewModel().getDataManager().setThemeButtonColor(0);
        getViewModel().getDataManager().setThemeTransparentColor(Color.parseColor("#4D34a975"));
        getViewModel().getDataManager().setThemeBgImage("");
        getViewModel().getDataManager().setThemeTitleBarColor(Color.parseColor("#34a975"));
        getViewModel().getDataManager().setThemeStatusBarColor(Color.parseColor("#2a865f"));
        getViewModel().getDataManager().setThemeLogo("");
        getViewModel().getDataManager().setThemeCompanyID(0);
        getViewModel().getDataManager().setThemeID(0);
        getViewModel().getDataManager().setCurrentUserLoggedInMode(DataManager.LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT);
        getViewModel().getDataManager().setCallForHelpLabel(getString(R.string.call_for_help));
        getViewModel().getDataManager().setThemeFooterDisplayFlag(1);
        getViewModel().getDataManager().setThemeFooterTextColor(Color.WHITE);
    }

    public boolean isLandscape() {
        return getResources().getConfiguration().orientation == ORIENTATION_LANDSCAPE;
    }
}

