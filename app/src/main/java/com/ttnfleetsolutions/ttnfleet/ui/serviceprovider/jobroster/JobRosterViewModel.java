package com.ttnfleetsolutions.ttnfleet.ui.serviceprovider.jobroster;

import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.listing.Event;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseViewModel;
import com.ttnfleetsolutions.ttnfleet.utils.rx.SchedulerProvider;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 11/10/2017.
 *
 * @version 1.0
 */

public class JobRosterViewModel extends BaseViewModel<JobRosterNavigator> {

    public JobRosterViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    private List<Event> mEventList;

    public List<Event> getEventList() {
        return mEventList;
    }

    public void setEventList(List<Event> mEventList) {
        this.mEventList = mEventList;
    }

    void getEventsSP(int statusType) {
//        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .doServerEventListingServiceProviderApiCall(getDataManager().getCurrentUserID(), statusType)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    setEventList(response.getResult());
                    getNavigator().getEvents(response.getResult());
//                    setIsLoading(false);
                }, throwable -> {
//                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                }));
    }

    public void getEventsTech(int statusType) {
//        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .doServerEventListingTechnicianApiCall(getDataManager().getCurrentUserID(), statusType)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    setEventList(response.getResult());
                    getNavigator().getEvents(response.getResult());
//                    setIsLoading(false);
                }, throwable -> {
//                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                }));
    }
}
