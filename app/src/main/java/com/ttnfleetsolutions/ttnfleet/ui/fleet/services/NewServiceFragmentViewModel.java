package com.ttnfleetsolutions.ttnfleet.ui.fleet.services;

import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.listing.Event;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseViewModel;
import com.ttnfleetsolutions.ttnfleet.utils.rx.SchedulerProvider;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 4/4/2018.
 *
 * @version 1.0
 */
public class NewServiceFragmentViewModel extends BaseViewModel<NewServiceFragmentNavigator> {

    NewServiceFragmentViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    private List<Event> mEventList;

    public List<Event> getEventList() {
        return mEventList;
    }

    public void setEventList(List<Event> mEventList) {
        this.mEventList = mEventList;
    }

    void getEvents(int status) {
//        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .doServerEventListingFleetDispatcherApiCall(getDataManager().getCurrentUserID(), status)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    setEventList(response.getResult());
                    getNavigator().getEvents(response.getResult());
//                    setIsLoading(false);
                }, throwable -> {
//                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                }));
    }
}
