package com.ttnfleetsolutions.ttnfleet.ui.trucker;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.ttnfleetsolutions.ttnfleet.R;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */

public class ServiceTypeAdapter extends RecyclerView.Adapter<ServiceTypeAdapter.MyViewHolder> {

    private Context mContext;
    private String[] services;
    private TypedArray icons;

    public ServiceTypeAdapter(Context context) {
        this.mContext = context;
        services = mContext.getResources().getStringArray(R.array.services_issue);
        icons = mContext.getResources().obtainTypedArray(R.array.services_icon);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_service, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.service.setText(services[position]);
        holder.icon.setImageDrawable(icons.getDrawable(position));
        holder.rowLayout.setOnClickListener(view -> holder.checkBox.setChecked(!holder.checkBox.isChecked()));
    }

    @Override
    public int getItemCount() {
        return services.length;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView service;
        public ImageView icon;
        public ConstraintLayout rowLayout;
        public CheckBox checkBox;

        public MyViewHolder(View view) {
            super(view);
            service = view.findViewById(R.id.serviceName);
            icon = view.findViewById(R.id.serviceIcon);
            checkBox = view.findViewById(R.id.checkbox);
            rowLayout = view.findViewById(R.id.rowLayout);
        }
    }
}
