package com.ttnfleetsolutions.ttnfleet.ui.fleet.provider;

import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.DispatchEventResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.serviceprovider.ServiceProvider;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 4/4/2018.
 *
 * @version 1.0
 */
interface ServiceProviderFragmentNavigator {

    void loadServiceProviderList(List<ServiceProvider> serviceProviderList);

    void onDispatchedResponseReceived(DispatchEventResponse dispatchEventResponse);

    void handleError(Throwable throwable);

    void handleErrorDispatch(Throwable throwable);
}
