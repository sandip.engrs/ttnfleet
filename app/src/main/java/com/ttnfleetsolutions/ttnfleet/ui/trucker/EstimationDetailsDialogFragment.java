package com.ttnfleetsolutions.ttnfleet.ui.trucker;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.ui.fleet.DashboardActivity;
import com.ttnfleetsolutions.ttnfleet.ui.fleet.EstimationDetailsRecyclerViewAdapter;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */

public class EstimationDetailsDialogFragment extends DialogFragment implements View.OnClickListener {

    private View rootView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.dialog_fragment_confirm_esitmation, container, false);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);

        //to hide keyboard when showing dialog fragment
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        rootView.findViewById(R.id.confirm).setOnClickListener(this);
        RecyclerView mEstimationDetailsRecyclerView = rootView.findViewById(R.id.detailedEstimationRecyclerView);
        mEstimationDetailsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mEstimationDetailsRecyclerView.setHasFixedSize(true);
        mEstimationDetailsRecyclerView.setNestedScrollingEnabled(false);
        mEstimationDetailsRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        mEstimationDetailsRecyclerView.setAdapter(new EstimationDetailsRecyclerViewAdapter(getActivity()));
        return rootView;
    }

    @Override
    public void onClick(View view) {
        dismiss();
    }

    @Override
    public void onResume() {
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        if (((DashboardActivity) getActivity()).isLandscape())
            params.width = (int) getResources().getDimension(R.dimen.dialog_min_width);
        else
            params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);
        super.onResume();
    }
}
