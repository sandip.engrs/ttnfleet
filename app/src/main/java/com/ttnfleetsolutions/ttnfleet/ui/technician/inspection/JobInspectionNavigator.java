package com.ttnfleetsolutions.ttnfleet.ui.technician.inspection;

import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.ConfirmBreakdownResponse;

/**
 * Created by Sandip Chaulagain on 6/22/2018.
 *
 * @version 1.0
 */
interface JobInspectionNavigator {

    void handleError(Throwable throwable);

    void onConfirmBreakdownResponseReceived(ConfirmBreakdownResponse confirmBreakdownResponse);
}
