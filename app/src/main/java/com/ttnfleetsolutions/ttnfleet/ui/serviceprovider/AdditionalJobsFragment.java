package com.ttnfleetsolutions.ttnfleet.ui.serviceprovider;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ttnfleetsolutions.ttnfleet.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AdditionalJobsFragment extends Fragment {


    View additionalJobView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        additionalJobView = inflater.inflate(R.layout.fragment_additional_jobs, container, false);

        RecyclerView mRecyclerView = additionalJobView.findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(new AdditionalJobsListAdapter(getActivity()));

        additionalJobView.findViewById(R.id.approve).setOnClickListener(view -> getActivity().onBackPressed());

        return additionalJobView;

    }

}
