package com.ttnfleetsolutions.ttnfleet.ui.fleet.order.technician;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.ttnfleetsolutions.ttnfleet.BR;
import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.databinding.FragmentFdOrderTechnicianDetailsBinding;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseFragment;

import javax.inject.Inject;

/**
 * Created by Sandip Chaulagain on 11/10/2017.
 *
 * @version 1.0
 */
public class TechnicianFragment extends BaseFragment<FragmentFdOrderTechnicianDetailsBinding, TechnicianViewModel> implements TechnicianNavigator {

    @Inject
    TechnicianViewModel mTechnicianViewModel;

    private FragmentFdOrderTechnicianDetailsBinding mFragmentTechnicianBinding;

    public static TechnicianFragment newInstance() {
        Bundle args = new Bundle();
        TechnicianFragment fragment = new TechnicianFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTechnicianViewModel.setNavigator(this);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFragmentTechnicianBinding = getViewDataBinding();
    }

    @Override
    public TechnicianViewModel getViewModel() {
        return mTechnicianViewModel;
    }


    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_fd_order_technician_details;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
