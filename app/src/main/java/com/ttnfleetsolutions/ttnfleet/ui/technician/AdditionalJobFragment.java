package com.ttnfleetsolutions.ttnfleet.ui.technician;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ttnfleetsolutions.ttnfleet.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AdditionalJobFragment extends Fragment {

    View additionalJobView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        additionalJobView = inflater.inflate(R.layout.fragment_additional_job_tech, container, false);

        return additionalJobView;
    }

}
