package com.ttnfleetsolutions.ttnfleet.ui.technician.requirements;

import android.databinding.Observable;
import android.util.Log;

import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.requirements.ImageRequirement;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseViewModel;
import com.ttnfleetsolutions.ttnfleet.utils.rx.SchedulerProvider;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 6/22/2018.
 *
 * @version 1.0
 */
public class RequirementsViewModel extends BaseViewModel<RequirementsNavigator> implements Observable {

    public RequirementsViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    @Override
    public void addOnPropertyChangedCallback(OnPropertyChangedCallback callback) {

    }

    @Override
    public void removeOnPropertyChangedCallback(OnPropertyChangedCallback callback) {

    }

    void uploadRequirementsApiCall(List<ImageRequirement> imageRequirementList, int eventId, int jobId, String OdometerReading) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .doServerUploadRequirementsApiCall(imageRequirementList, eventId, jobId, getDataManager().getCurrentUserID(), OdometerReading)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    Log.e("TAG", "Response:: " +response);
                    getNavigator().onUploadRequirementsResponseReceived(response);
                    setIsLoading(false);
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                }));
    }
}
