package com.ttnfleetsolutions.ttnfleet.ui.technician.requirements;


import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.ttnfleetsolutions.ttnfleet.BR;
import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.requirements.ImageRequirement;
import com.ttnfleetsolutions.ttnfleet.databinding.FragmentRequiredActionsBinding;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseActivity;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.servicerequest.callback.UpdateEventDetailsCallbackTech;
import com.ttnfleetsolutions.ttnfleet.utils.CommonUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * A simple {@link Fragment} subclass.
 */
public class RequirementsFragment extends BaseFragment<FragmentRequiredActionsBinding, RequirementsViewModel> implements RequirementsNavigator, View.OnClickListener {

    @Inject
    RequirementsViewModel mViewModel;

    @Inject
    ImageRequirementRecyclerViewAdapter mAdapter;

    FragmentRequiredActionsBinding mFragmentBinding;

    private List<ImageRequirement> mImageRequirementList = new ArrayList<>();

    private int mEventId, mJobId;

    @Override
    protected RequirementsViewModel getViewModel() {
        return mViewModel;
    }

    @Override
    protected int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_required_actions;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModel.setNavigator(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mFragmentBinding = getViewDataBinding();
        setUp();
    }

    private void setUp() {
        if(getArguments() != null) {
            mEventId = getArguments().getInt("eventId");
            mJobId = getArguments().getInt("jobId");
        }

        mFragmentBinding.imageRequirementRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        if (!((BaseActivity) getActivity()).isLandscape())
            mAdapter.setRootViewWidth(CommonUtils.getScreenWidth()/3 - 60);
        else
            mAdapter.setRootViewWidth(2*CommonUtils.getScreenWidth()/9 - 30);
        mAdapter.setListener(mItemViewModelListener);
        String[] array = getResources().getStringArray(R.array.predefined_image_requirements);
        for (int i = 0; i < array.length; i++) {
            mImageRequirementList.add(new ImageRequirement(i, array[i], null));
        }
        mAdapter.addItems(mImageRequirementList);
        mFragmentBinding.imageRequirementRecyclerView.setAdapter(mAdapter);


//        mFragmentBinding.vinImage.setOnClickListener(this);
//        mFragmentBinding.damageImage.setOnClickListener(this);
//        mFragmentBinding.replacedPartImage.setOnClickListener(this);
//        mFragmentBinding.frontDamageImage.setOnClickListener(this);
//        mFragmentBinding.middleDamageImage.setOnClickListener(this);
//        mFragmentBinding.backDamageImage.setOnClickListener(this);
//        mFragmentBinding.dotFormImage.setOnClickListener(this);
//        mFragmentBinding.SRFormImage.setOnClickListener(this);
    }

    private ImageRequirementItemViewModel.ItemViewModelListener mItemViewModelListener = new ImageRequirementItemViewModel.ItemViewModelListener() {
        @Override
        public void onItemClick(int index) {
            selectedImageViewIndex = index;
            selectImage();
        }
    };

    public void actionDone() {
        getViewModel().uploadRequirementsApiCall(mImageRequirementList, mEventId, mJobId, mFragmentBinding.currentOdometerReadingEditText.getText().toString());
    }

    @Override
    public void onUploadRequirementsResponseReceived(String response) {
        Toast.makeText(getActivity(), response, Toast.LENGTH_SHORT).show();
        getActivity().onBackPressed();
        mUpdateEventDetailsCallbackTech.onNewRequirementImageUpload();
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(getActivity(), getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
    }

    private UpdateEventDetailsCallbackTech mUpdateEventDetailsCallbackTech;

    public void setUpdateEventDetailsCallback(UpdateEventDetailsCallbackTech mUpdateEventDetailsCallbackTech) {
        this.mUpdateEventDetailsCallbackTech = mUpdateEventDetailsCallbackTech;
    }

    //    View requirementsView;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private int selectedImageViewIndex;
    private String userChoosenTask;

    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 124;
    private static final int REQUEST_PERMISSION_SETTING = 101;

    private boolean sentToSettings;

    /*@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        requirementsView = inflater.inflate(R.layout.fragment_required_actions, container, false);

        requirementsView.findViewById(R.id.vinImage).setOnClickListener(this);
        requirementsView.findViewById(R.id.damageImage).setOnClickListener(this);
        requirementsView.findViewById(R.id.replacedPartImage).setOnClickListener(this);
        requirementsView.findViewById(R.id.frontDamageImage).setOnClickListener(this);
        requirementsView.findViewById(R.id.middleDamageImage).setOnClickListener(this);
        requirementsView.findViewById(R.id.backDamageImage).setOnClickListener(this);
        requirementsView.findViewById(R.id.dotFormImage).setOnClickListener(this);
        requirementsView.findViewById(R.id.SRFormImage).setOnClickListener(this);

        return requirementsView;
    }*/

    @Override
    public void onClick(View view) {


    }

    private void selectImage() {
        CharSequence[] items;
        if (mImageRequirementList.get(selectedImageViewIndex).getFilePath() == null)
            items = new CharSequence[]{"Take Photo", "Choose from Library"};
        else items = new CharSequence[]{"Take Photo", "Choose from Library", "Remove"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setItems(items, (dialog, item) -> {

            if (items[item].equals("Take Photo")) {
                userChoosenTask = "Take Photo";
                boolean result = checkPermission(getActivity());
                if (result)
                    cameraIntent();
            } else if (items[item].equals("Choose from Library")) {
                userChoosenTask = "Choose from Library";
                boolean result = checkPermissionForGallery(getActivity());
                if (result)
                    galleryIntent();
            } else if (items[item].equals("Remove")) {
//                mImageRequirementList.get(selectedImageViewIndex).setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
                mImageRequirementList.get(selectedImageViewIndex).setFilePath(null);
                mAdapter.notifyDataSetChanged();
            }
        });
        builder.show();
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    galleryIntent();
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Need Storage Permission");
                    builder.setMessage("This app needs storage permission.");
                    builder.setPositiveButton("Grant", (dialog, which) -> {
                        dialog.cancel();
                        sentToSettings = true;
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                        Toast.makeText(getActivity(), "Go to Permissions to Grant Storage Permission", Toast.LENGTH_LONG).show();
                    });
                    builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());
                    builder.show();
                }
                break;
            case MY_PERMISSIONS_REQUEST_CAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    cameraIntent();
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Need Camera Permission");
                    builder.setMessage("This app needs Camera permission.");
                    builder.setPositiveButton("Grant", (dialog, which) -> {
                        dialog.cancel();
                        sentToSettings = true;
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                        Toast.makeText(getActivity(), "Go to Permissions to Grant Camera Permission", Toast.LENGTH_LONG).show();
                    });
                    builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());
                    builder.show();
                }
                break;
        }
    }


//    @Override
//    protected void onPostResume() {
//        super.onPostResume();
//        if (sentToSettings) {
//            sentToSettings = false;
//            if(!TextUtils.isEmpty(userChoosenTask)) return;
//            if (userChoosenTask.equals("Take Photo") && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
//                cameraIntent();
//            } else if (userChoosenTask.equals("Choose from Library") && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
//                galleryIntent();
//            }
//        }
//    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        } else if (requestCode == REQUEST_PERMISSION_SETTING) {
            sentToSettings = false;
            if (!TextUtils.isEmpty(userChoosenTask)) return;
            if (userChoosenTask.equals("Take Photo") && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                cameraIntent();
            } else if (userChoosenTask.equals("Choose from Library") && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                galleryIntent();
            }
        }
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        onImageResult(thumbnail);
        /*ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File destination = new File(getActivity().getFilesDir(),
                System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {

            mImageRequirementList.get(selectedImageViewIndex).setFilePath(Uri.parse(destination.getCanonicalPath()));
            mAdapter.notifyDataSetChanged();
//            ((ImageView) mFragmentBinding.getRoot().findViewById(selectedImageViewId)).setImageBitmap(thumbnail);
//            mFragmentBinding.getRoot().findViewById(selectedImageViewId).setBackground(null);
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    private void onImageResult(Bitmap thumbnail) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File destination = new File(getActivity().getFilesDir(),
                System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {

            mImageRequirementList.get(selectedImageViewIndex).setFilePath(Uri.parse(destination.getCanonicalPath()));
            mAdapter.notifyDataSetChanged();
//            ((ImageView) mFragmentBinding.getRoot().findViewById(selectedImageViewId)).setImageBitmap(thumbnail);
//            mFragmentBinding.getRoot().findViewById(selectedImageViewId).setBackground(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private boolean checkPermission(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                /*if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.CAMERA)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("Camera permission is necessary");
                    alertBuilder.setPositiveButton(android.R.string.yes, (dialog, which) -> ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_CAMERA));
                    AlertDialog alert = alertBuilder.create();
                    alert.show();
                } else {*/
                requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_CAMERA);
//                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private boolean checkPermissionForGallery(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                /*if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("External storage permission is necessary");
                    alertBuilder.setPositiveButton(android.R.string.yes, (dialog, which) -> ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE));
                    AlertDialog alert = alertBuilder.create();
                    alert.show();
                } else {*/
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
//                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        onImageResult(bm);
       /* try {
//            Uri selectedImage = data.getData();
//            String path = ImageFilePath.getPath(getActivity().getApplicationContext(), selectedImage);
            mImageRequirementList.get(selectedImageViewIndex).setFilePath(data.getData());
            mAdapter.notifyDataSetChanged();
//            ((ImageView) mFragmentBinding.getRoot().findViewById(selectedImageViewId)).setImageBitmap(bm);
        } catch (Exception e) {
            e.printStackTrace();
        }
//        mFragmentBinding.getRoot().findViewById(selectedImageViewId).setBackground(null);*/
    }
}
