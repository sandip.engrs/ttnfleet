package com.ttnfleetsolutions.ttnfleet.ui.fleet.services.note;

/**
 * Created by Sandip Chaulagain on 1/10/2018.
 *
 * @version 1.0
 */

public interface AddNoteNavigator {

    void handleError(Throwable throwable);

}
