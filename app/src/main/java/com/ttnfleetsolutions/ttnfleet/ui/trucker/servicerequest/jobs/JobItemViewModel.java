package com.ttnfleetsolutions.ttnfleet.ui.trucker.servicerequest.jobs;

import android.databinding.ObservableField;

import com.ttnfleetsolutions.ttnfleet.data.model.api.event.listing.Job;
import com.ttnfleetsolutions.ttnfleet.utils.CommonUtils;

/**
 * Created by Sandip Chaulagain on 1/19/2018.
 *
 * @version 1.0
 */

public class JobItemViewModel {

    private Job mJob;
    public ObservableField<String> title, status, datetime;
    private ItemViewModelListener mListener;

    JobItemViewModel(Job job, ItemViewModelListener listener) {
        this.mJob = job;
        if (job.getServiceCode() != null)
            title = new ObservableField<>(job.getServiceCode().getDisplaytitle());
        if (job.getStatus() != null)
            status = new ObservableField<>(job.getStatus().getTitle());
        datetime = new ObservableField<>(CommonUtils.getPrettyTime(job.getUpdatedtime()));
        this.mListener = listener;
    }

    public void onItemClick() {
        mListener.onItemClick(mJob.getJobid());
    }

    public interface ItemViewModelListener {
        void onItemClick(int id);
    }
}
