package com.ttnfleetsolutions.ttnfleet.ui.technician.inspection;

import android.databinding.Bindable;
import android.databinding.Observable;
import android.databinding.ObservableField;
import android.databinding.PropertyChangeRegistry;

import com.ttnfleetsolutions.ttnfleet.BR;
import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseViewModel;
import com.ttnfleetsolutions.ttnfleet.utils.rx.SchedulerProvider;

/**
 * Created by Sandip Chaulagain on 6/22/2018.
 *
 * @version 1.0
 */
public class JobInspectionViewModel extends BaseViewModel<JobInspectionNavigator> implements Observable {

    private PropertyChangeRegistry registry = new PropertyChangeRegistry();

    public ObservableField<String> eventType = new ObservableField<>();
    public ObservableField<String> breakdownType = new ObservableField<>();

    JobInspectionViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }
    @Bindable
    public ObservableField<String> getEventType() {
        return eventType;
    }

    public void setEventType(ObservableField<String> eventType) {
        this.eventType = eventType;
        registry.notifyChange(this, BR.eventType);
    }

    @Bindable
    public ObservableField<String> getBreakdownType() {
        return breakdownType;
    }

    public void setBreakdownType(ObservableField<String> breakdownType) {
        this.breakdownType = breakdownType;
        registry.notifyChange(this, BR.breakdownType);
    }

    void confirmBreakdown(int jobId) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .doServerConfirmJobApiCall(jobId)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    getNavigator().onConfirmBreakdownResponseReceived(response);
                    setIsLoading(false);
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                }));
    }


    @Override
    public void addOnPropertyChangedCallback(OnPropertyChangedCallback callback) {
        registry.add(callback);
    }

    @Override
    public void removeOnPropertyChangedCallback(OnPropertyChangedCallback callback) {
        registry.remove(callback);
    }
}
