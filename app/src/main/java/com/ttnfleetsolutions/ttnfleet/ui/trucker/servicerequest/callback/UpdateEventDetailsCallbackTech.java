package com.ttnfleetsolutions.ttnfleet.ui.trucker.servicerequest.callback;

/**
 * Created by Sandip Chaulagain on 7/3/2018.
 *
 * @version 1.0
 */
public interface UpdateEventDetailsCallbackTech {

    void onNewRequirementImageUpload();

    void onConfirmedBreakdown();
}
