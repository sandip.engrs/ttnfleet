package com.ttnfleetsolutions.ttnfleet.ui.serviceprovider;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ttnfleetsolutions.ttnfleet.R;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */

public class TechnicianListAdapter extends RecyclerView.Adapter<TechnicianListAdapter.MyViewHolder> {

    private Context mContext;
    private String[] names, locations, distances, status;

    public TechnicianListAdapter(Context context) {
        this.mContext = context;
        names = mContext.getResources().getStringArray(R.array.technician_list_name);
        locations = mContext.getResources().getStringArray(R.array.locations);
        distances = mContext.getResources().getStringArray(R.array.technician_list_distance);
        status = mContext.getResources().getStringArray(R.array.technician_list_status);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_technician_profile, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.names.setText(names[position]);
        holder.location.setText(locations[position]);
        holder.status.setText(status[position]);
//        holder.distance.setText(distances[position]);
        /*if(position == 0) {
            holder.imageView.setImageResource(R.drawable.ic_action_check_box);
        } else {
            holder.imageView.setVisibility(View.INVISIBLE);
        }*/
        if(position == 0) {
            holder.location.setText("3500 TN-126, Blountville, TN 37617, USA");
//            holder.frameLayout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.tr_gray));
        } else if(position == 1) {
            holder.location.setText("659 TN-75, Blountville, TN 37617, USA");
        } else if(position == 2) {
            holder.location.setText("1348 TN-394, Blountville, TN 37617, USA");
            holder.status.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
            holder.jobcard.setVisibility(View.VISIBLE);
        }
        /*holder.frameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ServiceProviderDashboardActivity) mContext).showChatDetails();
            }
        });*/

    }

    @Override
    public int getItemCount() {
        return 3;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView names, location, status, jobcard;
        public ImageView imageView;

        public MyViewHolder(View view) {
            super(view);
            names = view.findViewById(R.id.name);
            imageView = view.findViewById(R.id.checkbox);
            location = view.findViewById(R.id.location);
            status = view.findViewById(R.id.status);
            jobcard = view.findViewById(R.id.jobcard);
        }
    }
}
