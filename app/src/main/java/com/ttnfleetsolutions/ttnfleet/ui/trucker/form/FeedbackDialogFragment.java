package com.ttnfleetsolutions.ttnfleet.ui.trucker.form;

import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.survey.SurveyQuestionsResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.survey.SurveyRequest;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseActivity;
import com.ttnfleetsolutions.ttnfleet.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */

public class FeedbackDialogFragment extends DialogFragment {

    private View rootView;
    private int eventId, colorPrimary;
    private String vehicleName, VIN, serviceIssue, datetime;
    private List<SurveyQuestionsResponse.Result> mResultList;
    private SubmitListener mSubmitListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.dialog_fragment_feedback, container, false);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        //to hide keyboard when showing dialog fragment
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        /*TextView textView = rootView.findViewById(R.id.status);
        Drawable img = getResources().getDrawable(R.drawable.ic_done_all);
        img.setTint(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));
        textView.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);*/


        if (getArguments() != null) {
            colorPrimary = getArguments().getInt("colorPrimary");
            eventId = getArguments().getInt("eventId");
            vehicleName = getArguments().getString("vehicleName");
            VIN = getArguments().getString("VIN");
            serviceIssue = getArguments().getString("serviceIssue");
            datetime = getArguments().getString("datetime");
            mResultList = (List<SurveyQuestionsResponse.Result>) getArguments().getSerializable("survey");
        }

        ((TextView) rootView.findViewById(R.id.serviceRequestName)).setText("Event #" + eventId);
        ((TextView) rootView.findViewById(R.id.serviceRequestName)).setTextColor(colorPrimary);
        ((TextView) rootView.findViewById(R.id.vehicleName)).setText(vehicleName);
        ((TextView) rootView.findViewById(R.id.vinNo)).setText(VIN);
        ((TextView) rootView.findViewById(R.id.datetime)).setText(CommonUtils.getFromattedDate(datetime));
        ((TextView) rootView.findViewById(R.id.serviceIssue)).setText(serviceIssue);

        if (mResultList != null) {
            for (int i = 0; i < mResultList.size(); i++) {
                View view = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.row_survey_rating, null);
                RatingBar ratingBar = view.findViewById(R.id.ratingBar);
                ratingBar.setTag(i);
                ratingBar.setOnRatingBarChangeListener((ratingBar1, rating, fromUser) -> {
                    int index = (int) ratingBar1.getTag();
                    mResultList.get(index).setRating(rating);
                });
                ratingBar.setSecondaryProgressTintList(ColorStateList.valueOf(colorPrimary));
                ratingBar.setProgressTintList(ColorStateList.valueOf(colorPrimary));
                TextView ratingLabel = view.findViewById(R.id.ratingLabel);
                ratingLabel.setText(mResultList.get(i).getQuestion());
                ((LinearLayout) rootView.findViewById(R.id.surveyQuestionsLayout)).addView(view);
            }
        }

        rootView.findViewById(R.id.cancel).setOnClickListener(view -> dismiss());
        rootView.findViewById(R.id.submitFeedback).setOnClickListener(view -> {
            SurveyRequest surveyRequest = new SurveyRequest();
            surveyRequest.setComments(((EditText) rootView.findViewById(R.id.additionalRemarksEditText)).getText().toString());
            surveyRequest.setEventId(eventId);
            List<SurveyRequest.SurveyQuestion> surveyQuestionList = new ArrayList<>();
            for (int i = 0; i < mResultList.size(); i++) {
                SurveyRequest.SurveyQuestion surveyQuestion = surveyRequest.new SurveyQuestion();
                surveyQuestion.setSurveyQuestionId(mResultList.get(i).getSurveyquestionid());
                surveyQuestion.setRating(mResultList.get(i).getRating());
                surveyQuestionList.add(surveyQuestion);
            }
            surveyRequest.setSurveyQuestion(surveyQuestionList);
            mSubmitListener.onSubmit(surveyRequest);
            dismiss();
        });
//        getDialog().setTitle("FEEDBACK");
        return rootView;
    }

    @Override
    public void onResume() {
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        if (((BaseActivity) getActivity()).isLandscape())
            params.width = (int) getResources().getDimension(R.dimen.dialog_min_width);
        else
            params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);
        super.onResume();
    }

    public void setSubmitListener(SubmitListener mSubmitListener) {
        this.mSubmitListener = mSubmitListener;
    }

    public interface SubmitListener {

        void onSubmit(SurveyRequest surveyRequest);

    }

}
