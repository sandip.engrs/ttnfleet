package com.ttnfleetsolutions.ttnfleet.ui.trucker.vehicle;

import android.arch.lifecycle.LiveData;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.Toast;

import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.BR;
import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.Vehicle;
import com.ttnfleetsolutions.ttnfleet.databinding.FragmentVehiclesBinding;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseActivity;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.MainActivityNew;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */

public class VehicleFragment extends BaseFragment<FragmentVehiclesBinding, VehicleViewModel> implements View.OnClickListener, VehicleNavigator, VehicleItemViewModel.VehicleItemViewModelListener {

    @Inject
    VehicleViewModel mVehicleViewModel;

    FragmentVehiclesBinding mFragmentVehiclesBinding;

    /*@Inject
    LinearLayoutManager mLayoutManager;*/

    @Inject
    VehiclesAdapter mAdapter;

    @Override
    protected VehicleViewModel getViewModel() {
        return mVehicleViewModel;
    }

    @Override
    protected int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_vehicles;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mVehicleViewModel.setNavigator(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFragmentVehiclesBinding = getViewDataBinding();
        setUp();
    }

    private void setUp() {
        mFragmentVehiclesBinding.actionEnterVIN.setOnClickListener(this);
        mFragmentVehiclesBinding.actionScanBarcode.setOnClickListener(this);
        mFragmentVehiclesBinding.actionAddVehicle.setOnClickListener(this);

        mFragmentVehiclesBinding.vehicleRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mFragmentVehiclesBinding.vehicleRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mFragmentVehiclesBinding.vehicleRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        mFragmentVehiclesBinding.vehicleRecyclerView.setAdapter(mAdapter);
        mAdapter.setListener(this);
        //One method one responsibility.May be we can move getAllRepairs() outside this function.
        if (mVehicleViewModel.getVehicleLiveData() == null)
            mVehicleViewModel.getVehicles();

        if (!((BaseActivity) getActivity()).isLandscape()) {
            mFragmentVehiclesBinding.floatingActionsMenu.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.actionEnterVIN:
            case R.id.actionScanBarcode:
            case R.id.actionAddVehicle:
                mFragmentVehiclesBinding.floatingActionsMenu.collapse();
                ((MainActivityNew) getActivity()).addVehicle(view.getId());
                break;
        }
    }

    @Override
    public void updateVehicles(LiveData<List<Vehicle>> listLiveData) {
        if (!mVehicleViewModel.getVehicleLiveData().hasActiveObservers())
            mVehicleViewModel.getVehicleLiveData().observe(getActivity(), list -> mAdapter.addItems(list));
        else
            mAdapter.addItems(listLiveData.getValue());
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(getActivity(), getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemClick(long id) {
        ((MainActivityNew) getActivity()).goToVehicleDetails(id);
    }
}