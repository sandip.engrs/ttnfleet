/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.ttnfleetsolutions.ttnfleet.ui.fleet.order.vehicle;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.ttnfleetsolutions.ttnfleet.BR;
import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.databinding.FragmentFdOrderVehicleDetailsBinding;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseFragment;

import javax.inject.Inject;

/**
 * Created by Sandip Chaulagain on 11/10/2017.
 *
 * @version 1.0
 */
public class VehicleFragment extends BaseFragment<FragmentFdOrderVehicleDetailsBinding, VehicleViewModel> implements VehicleNavigator {

    @Inject
    VehicleViewModel mVehicleViewModel;

    private FragmentFdOrderVehicleDetailsBinding mFragmentVehicleBinding;

    public static VehicleFragment newInstance() {
        Bundle args = new Bundle();
        VehicleFragment fragment = new VehicleFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mVehicleViewModel.setNavigator(this);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFragmentVehicleBinding = getViewDataBinding();
    }

    @Override
    public VehicleViewModel getViewModel() {
        return mVehicleViewModel;
    }


    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_fd_order_vehicle_details;
    }

}
