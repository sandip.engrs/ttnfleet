package com.ttnfleetsolutions.ttnfleet.ui.fleet.services;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Sandip Chaulagain on 4/4/2018.
 *
 * @version 1.0
 */
@Module
public abstract class NewServiceFragmentProvider {

    @ContributesAndroidInjector(modules = NewServiceFragmentModule.class)
    abstract NewServiceFragment provideNewServiceFragmentFactory();
}
