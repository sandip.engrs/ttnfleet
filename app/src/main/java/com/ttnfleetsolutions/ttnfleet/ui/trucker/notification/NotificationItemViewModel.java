package com.ttnfleetsolutions.ttnfleet.ui.trucker.notification;

import android.databinding.BindingAdapter;
import android.databinding.ObservableField;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.TextView;

import com.github.vipulasri.timelineview.TimelineView;
import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.data.model.NotificationModel;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.model.OrderStatus;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.model.VectorDrawableUtils;

/**
 * Created by Sandip Chaulagain on 1/19/2018.
 *
 * @version 1.0
 */

public class NotificationItemViewModel {

    private NotificationModel mNotificationModel;
    public ObservableField<Long> id;
    public ObservableField<String> date, message, actionPositive, actionNegative;
    public ObservableField<OrderStatus> status;
    public ObservableField<Boolean> isRead;
    private NotificationItemViewModelListener mListener;


    NotificationItemViewModel(NotificationModel notificationModel, NotificationItemViewModelListener listener) {
        this.mNotificationModel = notificationModel;
        this.mListener = listener;
        id = new ObservableField<>(notificationModel.getId());
        date = new ObservableField<>(notificationModel.getDate());
        message = new ObservableField<>(notificationModel.getMessage());
        actionPositive = new ObservableField<>(notificationModel.getActionPositive());
        actionNegative = new ObservableField<>(notificationModel.getActionNegative());
        isRead = new ObservableField<>(notificationModel.isRead());
    }

    public void onItemClick() {
        mListener.onItemClick(mNotificationModel.getId());
    }

    public void onItemClickActionPositive() {
        mListener.onItemClickActionPositive(mNotificationModel.getActionPositive());
    }

    public void onItemClickActionNegative() {
        mListener.onItemClickActionNegative(mNotificationModel.getActionPositive());
    }

    public interface NotificationItemViewModelListener {

        void onItemClick(long id);

        void onItemClickActionPositive(String action);

        void onItemClickActionNegative(String action);
    }

    @BindingAdapter({"background"})
    public static void setBackground(View view, OrderStatus status) {
        switch (status) {
            case INACTIVE:
                ((TimelineView) view).setMarker(VectorDrawableUtils.getDrawable(view.getContext(), R.drawable.ic_marker_inactive, android.R.color.darker_gray));
                break;
            case ACTIVE:
                ((TimelineView) view).setMarker(VectorDrawableUtils.getDrawable(view.getContext(), R.drawable.ic_marker_active, R.color.colorPrimary));
                break;
            case COMPLETED:
                ((TimelineView) view).setMarker(VectorDrawableUtils.getDrawable(view.getContext(), R.drawable.ic_marker, R.color.colorPrimary));
                break;
        }
    }

    @BindingAdapter({"textColor"})
    public static void setTextColor(View view, OrderStatus status) {
        switch (status) {
            case INACTIVE:
                ((TextView) view).setTextColor(ContextCompat.getColor(view.getContext(), android.R.color.darker_gray));
                break;
            case ACTIVE:
                ((TextView) view).setTextColor(ContextCompat.getColor(view.getContext(), R.color.colorPrimary));
                break;
            case COMPLETED:
                ((TextView) view).setTextColor(ContextCompat.getColor(view.getContext(), R.color.gray_darker));
                break;
            case DELAYED:
                ((TextView) view).setTextColor(ContextCompat.getColor(view.getContext(), android.R.color.holo_orange_dark));
                break;
        }
    }
}
