package com.ttnfleetsolutions.ttnfleet.ui.fleet.order.chat;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.ttnfleetsolutions.ttnfleet.BR;
import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.databinding.FragmentFdOrderChatBinding;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseFragment;
import com.ttnfleetsolutions.ttnfleet.ui.chat.MainChatAdapter;

import javax.inject.Inject;

/**
 * Created by Sandip Chaulagain on 11/10/2017.
 *
 * @version 1.0
 */
public class ChatFragment extends BaseFragment<FragmentFdOrderChatBinding, ChatViewModel> implements ChatNavigator {

    @Inject
    ChatViewModel mChatViewModel;

    private FragmentFdOrderChatBinding mFragmentChatBinding;

    @Inject
    MainChatAdapter mServiceChatAdapter;

    @Inject
    LinearLayoutManager mLayoutManager;

    public static ChatFragment newInstance() {
        Bundle args = new Bundle();
        ChatFragment fragment = new ChatFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mChatViewModel.setNavigator(this);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFragmentChatBinding = getViewDataBinding();
        setUp();
    }

    /*@Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && mFragmentChatBinding != null && mFragmentChatBinding.chatRecyclerView != null && mFragmentChatBinding.chatRecyclerView.getAdapter() == null) {
            setUp();
        }
    }*/

    @Override
    public ChatViewModel getViewModel() {
        return mChatViewModel;
    }


    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_fd_order_chat;
    }

    private void setUp() {
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mFragmentChatBinding.chatRecyclerView.setLayoutManager(mLayoutManager);
        mFragmentChatBinding.chatRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        mFragmentChatBinding.chatRecyclerView.setAdapter(mServiceChatAdapter);
    }
}
