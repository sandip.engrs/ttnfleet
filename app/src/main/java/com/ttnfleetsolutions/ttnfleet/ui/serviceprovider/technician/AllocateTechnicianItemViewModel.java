package com.ttnfleetsolutions.ttnfleet.ui.serviceprovider.technician;

import android.databinding.BindingAdapter;
import android.databinding.ObservableField;
import android.view.View;
import android.widget.ImageView;

import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.data.model.api.technician.Technician;
import com.ttnfleetsolutions.ttnfleet.utils.GlideApp;

import static com.ttnfleetsolutions.ttnfleet.data.remote.ApiEndPoint.DIR_IMAGE_PROFILE;

/**
 * Created by Sandip Chaulagain on 1/19/2018.
 *
 * @version 1.0
 */

public class AllocateTechnicianItemViewModel {

    private Technician mTechnician;
    public ObservableField<Integer> id;
    public ObservableField<String> technicianName;
    public ObservableField<String> icon;
    public ObservableField<String> address;
    public ObservableField<String> availability;

    private ViewModelListener mListener;

    AllocateTechnicianItemViewModel(Technician technician, ViewModelListener listener) {
        this.mTechnician = technician;
        this.mListener = listener;
        id = new ObservableField<>(technician.getTechnicianid());
        if (technician.getUser() != null) {
            technicianName = new ObservableField<>(String.format("%s %s", technician.getUser().getFirstname(), technician.getUser().getLastname()));
            icon = new ObservableField<>(DIR_IMAGE_PROFILE + technician.getTechnicianid() + "/" + technician.getUser().getImage());
        } else technicianName = new ObservableField<>("");

        if (technician.getLocation() != null)
            address = new ObservableField<>(technician.getLocation().getAddress());

        if (technician.getAvailability() != null)
            availability = new ObservableField<>(technician.getAvailability().getTitle());
    }

    public void onItemClick() {
        mListener.onItemClick(mTechnician.getTechnicianid(), technicianName.get());
    }

    public interface ViewModelListener {
        void onItemClick(int technicianId, String name);
    }

    @BindingAdapter({"profileImage"})
    public static void setImageViewResource(View view, String url) {
        GlideApp.with(view.getContext())
                .load(url)
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .into((ImageView) view);
    }
}
