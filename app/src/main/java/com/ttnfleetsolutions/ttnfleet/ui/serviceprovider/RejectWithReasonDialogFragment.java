package com.ttnfleetsolutions.ttnfleet.ui.serviceprovider;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.RejectionTypesResponse;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */

public class RejectWithReasonDialogFragment extends DialogFragment implements View.OnClickListener {

    private View rootView;
    private List<RejectionTypesResponse.RejectionType> mRejectionTypeList;
    private RadioGroup mRadioGroup;
    private TextView mTextView;
    private SubmitListener mSubmitListener;
    private int from, eventId, workOrderId, workOrderStatusId, jobId, jobStatusId;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.dialog_fragment_reject_with_reason, container, false);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);

        //to hide keyboard when showing dialog fragment
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        rootView.findViewById(R.id.cancel).setOnClickListener(this);
        rootView.findViewById(R.id.submit).setOnClickListener(this);

        mRadioGroup = rootView.findViewById(R.id.radioGroup);
        mTextView = rootView.findViewById(R.id.otherReason);

        if (getArguments() != null) {
            boolean flag = getArguments().getBoolean("from-sd");
            mRejectionTypeList = (List<RejectionTypesResponse.RejectionType>) getArguments().get("rejections");
            from = getArguments().getInt("from");
            eventId = getArguments().getInt("eventId");
            workOrderId = getArguments().getInt("workOrderId");
            workOrderStatusId = getArguments().getInt("workOrderStatusId");
            jobId = getArguments().getInt("jobId");
            jobStatusId = getArguments().getInt("jobStatusId");
           /* if (flag) {
                rootView.findViewById(R.id.radioButton3).setVisibility(View.GONE);
                ((RadioButton) rootView.findViewById(R.id.radioButton1)).setText(getString(R.string.tech_not_available));
            }*/

            if (mRejectionTypeList != null) {
                for (int i = 0; i < mRejectionTypeList.size(); i++) {
                    View view = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.row_reject_reason, null);
                    RadioButton radioButton = view.findViewById(R.id.radioButton);
                    radioButton.setText(mRejectionTypeList.get(i).getTitle());
                    radioButton.setId(mRejectionTypeList.get(i).getReqrejectiontypeid());
                    if (mRejectionTypeList.get(i).getTitle().equalsIgnoreCase("Other")) {
                        radioButton.setOnCheckedChangeListener((buttonView, isChecked) -> {
                            if (isChecked) {
                                mTextView.setEnabled(true);
                                mTextView.requestFocus();
                            } else {
                                mTextView.setEnabled(false);
                                mTextView.setText("");
                            }
                        });
                    }
                    mRadioGroup.addView(radioButton);
                }
            }
        }
        /*mRadioGroup.setOnCheckedChangeListener((group, checkedId) -> {
           if(((RadioButton) mRadioGroup.findViewById(checkedId)).getText().toString().equalsIgnoreCase("Other")) {
               rootView.findViewById(R.id.otherReason).findViewById(View.VISIBLE);
           } else {
               rootView.findViewById(R.id.otherReason).findViewById(View.GONE);
           }
        });*/
        return rootView;
    }

    public SubmitListener getSubmitListener() {
        return mSubmitListener;
    }

    public void setSubmitListener(SubmitListener mSubmitListener) {
        this.mSubmitListener = mSubmitListener;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cancel:
                dismiss();
                break;
            case R.id.submit:
                mSubmitListener.onSubmit(eventId, workOrderId, workOrderStatusId, jobId, jobStatusId, mTextView.getText().toString(), mRadioGroup.getCheckedRadioButtonId());
                dismiss();
                break;
        }
    }

    public interface SubmitListener {

        void onSubmit(int eventId, int workOrderId, int workOrderStatusId, int jobId, int jobStatusId, String rejectionDescription, int rejectionTypeId);

    }
}
