package com.ttnfleetsolutions.ttnfleet.ui.trucker.form;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.SelectLocationActivity;
import com.ttnfleetsolutions.ttnfleet.utils.AppConstants;

import java.util.Calendar;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */

public class CreateServiceRequestFormFragment extends Fragment implements View.OnClickListener {

    private View rootView;
    private String mServiceType;
    private int mType;
    private String mStartDateStr, mEndDateStr;
    private EditText mStartDateTextView, mEndDateTextView;
    /**
     * The formatted location address.
     */
    private String mAddressOutput;

    private LatLng mDefaultLocation;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_create_service_request_form, container, false);

        if (getArguments() != null) {
            mServiceType = getArguments().getString("service");
            mType = getArguments().getInt("type");
        }

        if(!TextUtils.isEmpty(mServiceType) && mServiceType.equalsIgnoreCase(getString(R.string.drop_off_amp_pick_up))) {
            rootView.findViewById(R.id.serviceRequiredLabel).setVisibility(View.VISIBLE);
            rootView.findViewById(R.id.service1).setVisibility(View.VISIBLE);
            rootView.findViewById(R.id.service2).setVisibility(View.VISIBLE);
            rootView.findViewById(R.id.service3).setVisibility(View.VISIBLE);
            rootView.findViewById(R.id.timeDeliverAsset).setVisibility(View.VISIBLE);
            rootView.findViewById(R.id.timePickupAsset).setVisibility(View.VISIBLE);
            rootView.findViewById(R.id.startDate).setVisibility(View.VISIBLE);
            rootView.findViewById(R.id.endDate).setVisibility(View.VISIBLE);
        }

//        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        /*if (getActivity() instanceof ServiceProviderDashboardActivity) {
            rootView.findViewById(R.id.spinnerMainServices).setVisibility(View.VISIBLE);
            rootView.findViewById(R.id.spinnerSubservices).setVisibility(View.VISIBLE);
        }*/

        /*ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_action_cancel);
        }
        setHasOptionsMenu(true);*/

        /*if (getArguments() != null) {
            Spinner spinner = rootView.findViewById(R.id.spinnerSubservices);
            Spinner mainServicesSpinner = rootView.findViewById(R.id.spinnerMainServices);

            mainServicesSpinner.setSelection(mType);
            switch (mType) {
                case 0:
                    ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>
                            (getActivity(), android.R.layout.simple_spinner_item,
                                    getResources().getStringArray(R.array.subservices_emergency)); //selected item will look like a spinner set from XML
                    spinnerArrayAdapter.setDropDownViewResource(android.R.layout
                            .simple_spinner_dropdown_item);
                    spinner.setAdapter(spinnerArrayAdapter);
                    break;
                case 1:
                    spinnerArrayAdapter = new ArrayAdapter<>
                            (getActivity(), android.R.layout.simple_spinner_item,
                                    getResources().getStringArray(R.array.subservices_maintainance)); //selected item will look like a spinner set from XML
                    spinnerArrayAdapter.setDropDownViewResource(android.R.layout
                            .simple_spinner_dropdown_item);
                    spinner.setAdapter(spinnerArrayAdapter);
                    break;
                case 2:
                    spinnerArrayAdapter = new ArrayAdapter<>
                            (getActivity(), android.R.layout.simple_spinner_item,
                                    getResources().getStringArray(R.array.subservices_emergency)); //selected item will look like a spinner set from XML
                    spinnerArrayAdapter.setDropDownViewResource(android.R.layout
                            .simple_spinner_dropdown_item);
                    spinner.setAdapter(spinnerArrayAdapter);
                    break;
            }
        }

        ((Spinner) rootView.findViewById(R.id.spinnerMainServices)).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Spinner spinner = rootView.findViewById(R.id.spinnerSubservices);
                switch (i) {
                    case 0:
                        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>
                                (getActivity(), android.R.layout.simple_spinner_item,
                                        getResources().getStringArray(R.array.subservices_emergency)); //selected item will look like a spinner set from XML
                        spinnerArrayAdapter.setDropDownViewResource(android.R.layout
                                .simple_spinner_dropdown_item);
                        spinner.setAdapter(spinnerArrayAdapter);
                        break;
                    case 1:
                        spinnerArrayAdapter = new ArrayAdapter<>
                                (getActivity(), android.R.layout.simple_spinner_item,
                                        getResources().getStringArray(R.array.subservices_maintainance)); //selected item will look like a spinner set from XML
                        spinnerArrayAdapter.setDropDownViewResource(android.R.layout
                                .simple_spinner_dropdown_item);
                        spinner.setAdapter(spinnerArrayAdapter);
                        break;
                    case 2:
                        spinnerArrayAdapter = new ArrayAdapter<>
                                (getActivity(), android.R.layout.simple_spinner_item,
                                        getResources().getStringArray(R.array.subservices_emergency)); //selected item will look like a spinner set from XML
                        spinnerArrayAdapter.setDropDownViewResource(android.R.layout
                                .simple_spinner_dropdown_item);
                        spinner.setAdapter(spinnerArrayAdapter);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/

        rootView.findViewById(R.id.addLocation).setOnClickListener(view -> {
            Intent intent = new Intent(getActivity(), SelectLocationActivity.class);
            if (mDefaultLocation != null && !TextUtils.isEmpty(mAddressOutput)) {
                intent.putExtra(AppConstants.RESULT_DATA_KEY, mAddressOutput);
                intent.putExtra(AppConstants.LOCATION_DATA_EXTRA, mDefaultLocation);
            }
            startActivityForResult(intent, 1001);
        });
        /*Spinner mIssueSpinner = rootView.findViewById(R.id.spinner);
        mIssueSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 12) {
                    rootView.findViewById(R.id.enterIssue).setVisibility(View.VISIBLE);
                } else {
                    rootView.findViewById(R.id.enterIssue).setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/

        mStartDateTextView = rootView.findViewById(R.id.startDate);
        mEndDateTextView = rootView.findViewById(R.id.endDate);
        mStartDateTextView.setOnClickListener(this);
        mEndDateTextView.setOnClickListener(this);
        return rootView;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.startDate:
                Calendar now = Calendar.getInstance();
                new DatePickerDialog(getActivity(), (datePicker, i, i1, i2) -> {
                    mStartDateStr = datePicker.getYear() + "-" + datePicker.getMonth() + "-" + datePicker.getDayOfMonth();
                    new TimePickerDialog(getActivity(), (timePicker, i32, i112) -> {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            mStartDateStr = mStartDateStr + " " +  timePicker.getHour() + ":" +timePicker.getMinute();
                        } else {
                            mStartDateStr = mStartDateStr + " " +  timePicker.getCurrentHour() + ":" +timePicker.getCurrentMinute();
                        }
                        mStartDateTextView.setText(mStartDateStr);
                    }, now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE), true).show();
                }, now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)).show();
                break;
            case R.id.endDate:
                now = Calendar.getInstance();
                new DatePickerDialog(getActivity(), (datePicker, i, i1, i2) -> {
                    mEndDateStr = datePicker.getYear() + "-" + datePicker.getMonth() + "-" + datePicker.getDayOfMonth();
                    new TimePickerDialog(getActivity(), (timePicker, i3, i11) -> {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            mEndDateStr = mEndDateStr + " " +  timePicker.getHour() + ":" +timePicker.getMinute();
                        } else {
                            mEndDateStr = mEndDateStr + " " +  timePicker.getCurrentHour() + ":" +timePicker.getCurrentMinute();
                        }
                        mEndDateTextView.setText(mEndDateStr);
                    }, now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE), true).show();
                }, now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)).show();
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 1001:
                if (data != null && data.getExtras() != null) {
                    // Get the location passed to this service through an extra.
                    mDefaultLocation = data.getParcelableExtra(AppConstants.LOCATION_DATA_EXTRA);
                    // Display the address string or an error message sent from the intent service.
                    mAddressOutput = data.getStringExtra(AppConstants.RESULT_DATA_KEY);

                    ((TextView) rootView.findViewById(R.id.addLocation)).setText(mAddressOutput);
                }

                break;
        }
    }

    /*@Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        getActivity().getMenuInflater().inflate(R.menu.fill_form, menu);
        if (getActivity() instanceof MainActivity) {
            menu.getItem(0).setTitle("SEND");
        } else {
            menu.getItem(0).setTitle("CREATE");
        }
    }*/

    /*@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setStyle(STYLE_NORMAL, R.style.AppTheme);
    }*/

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                // handle confirmation button click here
                if (getActivity() instanceof MainActivity)
                    ((MainActivity) getActivity()).showOngoingServices();
                else
                    ((ServiceProviderDashboardActivity) getActivity()).showOngoingServices();
//                dismiss();
                return true;
            case android.R.id.home:
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("Are you sure you want to discard request?");
                builder.setCancelable(false);
                builder.setPositiveButton("KEEP EDITING", (dialogInterface, i) -> dialogInterface.dismiss());
                builder.setNegativeButton("DISCARD", (dialogInterface, i) -> {
                    dialogInterface.dismiss();
//                    dismiss();
                });
                builder.show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }*/
}
