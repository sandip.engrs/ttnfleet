package com.ttnfleetsolutions.ttnfleet.ui.serviceprovider;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.request.RequestOptions;
import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.BR;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.AcceptWorkOrderResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.RejectionTypesResponse;
import com.ttnfleetsolutions.ttnfleet.databinding.ActivityServiceProviderDashboardBinding;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseActivity;
import com.ttnfleetsolutions.ttnfleet.ui.chat.ChatFragment;
import com.ttnfleetsolutions.ttnfleet.ui.chat.MainChatFragment;
import com.ttnfleetsolutions.ttnfleet.ui.chat.ServiceDetailsChatFragment;
import com.ttnfleetsolutions.ttnfleet.ui.login.LoginActivity;
import com.ttnfleetsolutions.ttnfleet.ui.serviceprovider.history.ServiceHistoryFragment;
import com.ttnfleetsolutions.ttnfleet.ui.serviceprovider.jobroster.JobRosterFragment;
import com.ttnfleetsolutions.ttnfleet.ui.serviceprovider.technician.AllocateTechnicianFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.TrackLocationFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.servicerequest.ServiceDetailsFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.servicerequest.callback.UpdateEventDetailsCallbackSP;
import com.ttnfleetsolutions.ttnfleet.utils.CommonUtils;
import com.ttnfleetsolutions.ttnfleet.utils.GlideApp;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

import static com.ttnfleetsolutions.ttnfleet.data.remote.ApiEndPoint.DIR_IMAGE_PROFILE;
import static com.ttnfleetsolutions.ttnfleet.data.remote.ApiEndPoint.DIR_IMAGE_THEME_LOGO;

/**
 * Created by Sandip Chaulagain on 10/10/2017.
 *
 * @version 1.0
 */

public class ServiceProviderDashboardActivity extends BaseActivity<ActivityServiceProviderDashboardBinding, ServiceProviderDashboardActivityModel>
        implements NavigationView.OnNavigationItemSelectedListener, HasSupportFragmentInjector, ServiceProviderDashboardNavigator, UpdateEventDetailsCallbackSP {

    public Toolbar mToolbar;
    private NavigationView mNavigationView;
    private DrawerLayout mDrawerLayout;
    private MenuItem menuItemDone, menuItemNotification;

    @Inject
    ServiceProviderDashboardActivityModel mDashboardViewModel;

    private ActivityServiceProviderDashboardBinding mActivityServiceProviderDashboardBinding;

    @Inject
    DispatchingAndroidInjector<Fragment> fragmentDispatchingAndroidInjector;

    @Override
    public ServiceProviderDashboardActivityModel getViewModel() {
        return mDashboardViewModel;
    }

    @Override
    protected int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_service_provider_dashboard;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityServiceProviderDashboardBinding = getViewDataBinding();
        mDashboardViewModel.setNavigator(this);
        setUp();
    }

    private void setUp() {
        mToolbar = findViewById(R.id.baseToolbar);
        mToolbar.setTitle("Job Roster");
        setSupportActionBar(mToolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayUseLogoEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(false);
        }
        setThemeColors();
        initComponents();
    }

    public void setThemeColors() {
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(getViewModel().getDataManager().getThemeStatusBarColor());

        mToolbar.setBackgroundColor(getViewModel().getDataManager().getThemeTitleBarColor());
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        mDrawerLayout.closeDrawer(GravityCompat.START);

        int size = mNavigationView.getMenu().size();
        for (int i = 0; i < size; i++) {
            mNavigationView.getMenu().getItem(i).setChecked(false);
        }
        switch (item.getItemId()) {
            case R.id.navItemJobRoster:
                replaceFragment(new JobRosterFragment());
                return true;
            case R.id.navItemHistory:
                replaceFragment(new ServiceHistoryFragment());
                return true;
            case R.id.navItemNotifications:
                replaceFragment(new NotificationsFragment());
                return true;
            case R.id.navItemChat:
                replaceFragment(new MainChatFragment());
                return true;
            case R.id.navItemTechnicians:
                replaceFragment(new AllocateTechnicianFragment());
                return true;
            case R.id.navItemLogout:
                setDefaultTheme();
                Intent intent = LoginActivity.getStartIntent(ServiceProviderDashboardActivity.this);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                return true;
            default:
                return false;
        }
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentDispatchingAndroidInjector;
    }

    public static Intent getStartIntent(Context context) {
        return new Intent(context, ServiceProviderDashboardActivity.class);
    }

    private void replaceFragment(Fragment fragment) {
        invalidateOptionsMenu();
        String backStateName = fragment.getClass().getName();

        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.container, fragment, backStateName);
            ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            ft.addToBackStack(backStateName);
            ft.commit();
        }

        if (isLandscape()) {
            if (fragment instanceof JobRosterFragment) {
                /*ServiceDetailsFragment serviceDetailsFragment = new ServiceDetailsFragment();
                serviceDetailsFragment.updateIndex(2, 1);
                replaceFragmentDetails(serviceDetailsFragment, false);
                findViewById(R.id.details_container).setVisibility(View.VISIBLE);*/
            } else if (fragment instanceof ServiceHistoryFragment) {
                /*ServiceDetailsFragment serviceDetailsFragment = new ServiceDetailsFragment();
                serviceDetailsFragment.updateIndex(-1, 3);
                replaceFragmentDetails(serviceDetailsFragment, false);
                findViewById(R.id.details_container).setVisibility(View.VISIBLE);*/
            } else if (fragment instanceof MainChatFragment) {
                ServiceDetailsChatFragment serviceDetailsChatFragment = new ServiceDetailsChatFragment();
                replaceFragmentDetails(serviceDetailsChatFragment, false);
                findViewById(R.id.details_container).setVisibility(View.VISIBLE);
            } else {
                findViewById(R.id.details_container).setVisibility(View.GONE);
            }
        }
    }

    private void replaceFragmentDetails(Fragment fragment, boolean fragmentPopped) {
        String backStateName = fragment.getClass().getName();
        FragmentManager manager = getSupportFragmentManager();
//        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);
//        boolean fragmentPopped = false;

        if (!fragmentPopped) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.details_container, fragment, backStateName);
            ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
        updateSubTitle(fragment);
    }

    @Override
    public void onBackPressed() {
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.container);
        if (isLandscape()) {
            Fragment f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
            if (f != null && f1 != null) {
                if (f instanceof JobRosterFragment && f1 instanceof ServiceDetailsFragment) {
                    finish();
                } else if (f instanceof ServiceHistoryFragment && f1 instanceof ServiceDetailsFragment) {
                    getSupportFragmentManager().popBackStack();
                } else if (f instanceof MainChatFragment && f1 instanceof ServiceDetailsChatFragment) {
                    getSupportFragmentManager().popBackStack();
                }
                updateSubTitle(f1);
            }
        }
        if (f instanceof JobRosterFragment && !isLandscape()) {
            finish();
        } else {
            super.onBackPressed();
            f = getSupportFragmentManager().findFragmentById(R.id.container);
            if (f != null) {
                updateTitleAndDrawer(f);
            }
        }
    }

    private void updateTitleAndDrawer(Fragment fragment) {
        invalidateOptionsMenu();
        CommonUtils.hideKeyboard(this);
        String fragClassName = fragment.getClass().getName();
        if (findViewById(R.id.details_container) != null)
            findViewById(R.id.details_container).setVisibility(View.GONE);

        if (fragClassName.equals(JobRosterFragment.class.getName())) {
            mToolbar.setTitle("Job Roster");
            mNavigationView.setCheckedItem(R.id.navItemJobRoster);
            if (findViewById(R.id.details_container) != null)
                findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else if (fragClassName.equals(NotificationsFragment.class.getName())) {
            mToolbar.setTitle("Notifications");
            mNavigationView.setCheckedItem(R.id.navItemNotifications);
        } else if (fragClassName.equals(MainChatFragment.class.getName())) {
            mToolbar.setTitle("Chat");
            mNavigationView.setCheckedItem(R.id.navItemChat);
            if (findViewById(R.id.details_container) != null)
                findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else if (fragClassName.equals(ServiceHistoryFragment.class.getName())) {
            mToolbar.setTitle("Job History");
            mNavigationView.setCheckedItem(R.id.navItemHistory);
            if (findViewById(R.id.details_container) != null)
                findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else if (fragClassName.equals(TechnicianListFragment.class.getName())) {
            mToolbar.setTitle("Technicians");
            mNavigationView.setCheckedItem(R.id.navItemTechnicians);
        } else if (fragClassName.equals(ServiceDetailsChatFragment.class.getName())) {
//            mToolbar.setTitle("Service Details Chat");
            mNavigationView.setCheckedItem(R.id.navItemChat);
        } else if (fragClassName.equals(ChatFragment.class.getName())) {
//            mToolbar.setTitle("Service Details 1");
            mNavigationView.setCheckedItem(R.id.navItemChat);
        } else if (fragClassName.equals(TrackLocationFragment.class.getName())) {
            mToolbar.setTitle("Track Location");
            mNavigationView.setCheckedItem(R.id.navItemJobRoster);
        } /*else if (fragClassName.equals(ServiceDetailsFragment.class.getName())) {
            mToolbar.setTitle("Job Details");
            mNavigationView.setCheckedItem(R.id.navItemJobRoster);
        }*/ else if (fragClassName.equals(AllocateTechnicianFragment.class.getName()) && ((AllocateTechnicianFragment) fragment).isFlag()) {
            mToolbar.setTitle("Allocate Job");
            mNavigationView.setCheckedItem(R.id.navItemJobRoster);
        } else if (fragClassName.equals(AllocateTechnicianFragment.class.getName())) {
            mToolbar.setTitle("Technicians");
            mNavigationView.setCheckedItem(R.id.navItemTechnicians);
        } else if (fragClassName.equals(ServiceEstimationFragment.class.getName())) {
            mToolbar.setTitle("Service Estimation");
            mNavigationView.setCheckedItem(R.id.navItemJobRoster);
        } else if (fragClassName.equals(AdditionalJobsFragment.class.getName())) {
            mToolbar.setTitle("Additional Jobs");
            mNavigationView.setCheckedItem(R.id.navItemJobRoster);
        }
    }

    private void updateSubTitle(Fragment fragment) {
        String fragClassName = fragment.getClass().getName();
        if (fragClassName.equals(ServiceEstimationFragment.class.getName())) {
            setSubTitle("Service Estimation");
        } /*else if (fragClassName.equals(ServiceDetailsFragment.class.getName())) {
            setSubTitle("Job Details");
        } */ else if (fragClassName.equals(ServiceDetailsChatFragment.class.getName())) {
//            setSubTitle("Chat");
        } else if (fragClassName.equals(ChatFragment.class.getName())) {
//            setSubTitle("Chat");
        } else if (fragClassName.equals(TrackLocationFragment.class.getName())) {
            setSubTitle("Track Location");
        } else if (fragClassName.equals(AdditionalJobsFragment.class.getName())) {
            setSubTitle("Additional Jobs");
        } else if (fragClassName.equals(TrackLocationFragment.class.getName())) {
            setSubTitle("Allocate Job");
        } else if (fragClassName.equals(AllocateTechnicianFragment.class.getName()) && ((AllocateTechnicianFragment) fragment).isFlag()) {
            setSubTitle("Allocate Job");
        } else if(fragClassName.equals(AllocateTechnicianFragment.class.getName())) {
            setSubTitle("Technicians");
        }
    }

    public void setTitle(String title) {
        if (!TextUtils.isEmpty(title)) {
            mToolbar.setTitle(title);
        }
    }

    public void setSubTitle(String subTitle) {
        if (!TextUtils.isEmpty(subTitle)) {
            ((TextView) findViewById(R.id.subTitle)).setText(subTitle);
            findViewById(R.id.subTitle).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.subTitle).setVisibility(View.GONE);
        }
    }

    private void initComponents() {
        mDrawerLayout = findViewById(R.id.drawerLayout);
        mNavigationView = findViewById(R.id.navigationView);

        int[][] state = new int[][]{
                new int[]{android.R.attr.state_checked},
                new int[]{android.R.attr.state_enabled},
                new int[]{android.R.attr.state_pressed},
                new int[]{android.R.attr.state_focused},
                new int[]{android.R.attr.state_pressed}
        };

        int[] color = new int[]{
                getViewModel().getDataManager().getThemeStatusBarColor(),
                Color.DKGRAY,
                Color.DKGRAY,
                Color.DKGRAY,
                Color.DKGRAY
        };

        //Defining ColorStateList for menu item Icon
        ColorStateList navMenuIconList = new ColorStateList(
                new int[][]{
                        new int[]{android.R.attr.state_checked},
                        new int[]{android.R.attr.state_enabled},
                        new int[]{android.R.attr.state_pressed},
                        new int[]{android.R.attr.state_focused},
                        new int[]{android.R.attr.state_pressed}
                },
                new int[]{
                        getViewModel().getDataManager().getThemeStatusBarColor(),
                        getViewModel().getDataManager().getThemeTitleBarColor(),
                        getViewModel().getDataManager().getThemeTitleBarColor(),
                        getViewModel().getDataManager().getThemeTitleBarColor(),
                        getViewModel().getDataManager().getThemeTitleBarColor()
                }
        );
        ColorStateList ColorStateList1 = new ColorStateList(state, color);
        mNavigationView.setItemTextColor(ColorStateList1);
//        mNavigationView.setItemIconTintList(ColorStateList.valueOf(getViewModel().getDataManager().getThemeStatusBarColor()));
        mNavigationView.setItemIconTintList(navMenuIconList);

        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawerLayout,
                mToolbar,
                R.string.open_drawer,
                R.string.close_drawer) {
        };
        mDrawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        mNavigationView.setNavigationItemSelectedListener(this);
        mNavigationView.setCheckedItem(R.id.navItemJobRoster);
        replaceFragment(new JobRosterFragment());

        View.OnClickListener onClickListener = mDrawerToggle.getToolbarNavigationClickListener();
        getSupportFragmentManager().addOnBackStackChangedListener(() -> {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
            if (fragment != null)
                if (fragment instanceof ServiceDetailsFragment || fragment instanceof TrackLocationFragment
                        || fragment instanceof ChatFragment || fragment instanceof ServiceDetailsChatFragment
                        || (fragment instanceof AllocateTechnicianFragment && ((AllocateTechnicianFragment) fragment).isFlag())
                        || fragment instanceof AdditionalJobsFragment
                        || fragment instanceof ServiceEstimationFragment) {
                    mDrawerToggle.setDrawerIndicatorEnabled(false);
                    mDrawerToggle.setToolbarNavigationClickListener(v -> onBackPressed());
                } else {
                    mDrawerToggle.setDrawerIndicatorEnabled(true);
                    mDrawerToggle.setToolbarNavigationClickListener(onClickListener);
                }

            Fragment f = getSupportFragmentManager().findFragmentById(R.id.container);
            if (f != null) {
                updateTitleAndDrawer(f);
            }
            if (isLandscape()) {
                Fragment f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
                if (f1 != null) {
                    updateSubTitle(f1);
                }
            }
        });

        View header = mNavigationView.getHeaderView(0);
        ImageView imageViewLogo = header.findViewById(R.id.logo);
        ImageView imageViewProfile = header.findViewById(R.id.profileImageView);

        header.findViewById(R.id.drawerHeaderBg).setBackgroundColor(getViewModel().getDataManager().getThemeTitleBarColor());

        ((TextView) header.findViewById(R.id.name)).setText(getViewModel().getDataManager().getCurrentUserFullName());
        header.findViewById(R.id.number).setVisibility(TextUtils.isEmpty(getViewModel().getDataManager().getCurrentUserRoleName()) ? View.GONE : View.VISIBLE);
        ((TextView) header.findViewById(R.id.number)).setText(getViewModel().getDataManager().getCurrentUserRoleName());

        GlideApp.with(this)
                .load(DIR_IMAGE_PROFILE + getViewModel().getDataManager().getCurrentUserID() + "/" + getViewModel().getDataManager().getCurrentUserProfileImage())
                .apply(RequestOptions.circleCropTransform())
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .into(imageViewProfile);

        GlideApp.with(this)
                .load(DIR_IMAGE_THEME_LOGO + getViewModel().getDataManager().getThemeLogo())
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .fitCenter()
                .into(imageViewLogo);
    }

    public void showChatDetails(int from, String title) {
        if (isLandscape()) {
            Fragment f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
            if (f1 instanceof ChatFragment) {
                final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ((ChatFragment) f1).setReceiver(title);
                ft.detach(f1);
                ft.attach(f1);
                ft.commit();
            } else {
                ChatFragment chatFragment = new ChatFragment();
                chatFragment.setReceiver(title);
                replaceFragmentDetails(chatFragment, false);
            }
            setSubTitle(title);
            //update details only
            findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else {
            mToolbar.setTitle(title);
            ChatFragment chatFragment = new ChatFragment();
            chatFragment.setReceiver(title);
            replaceFragment(chatFragment);
        }
        mNavigationView.setCheckedItem(R.id.navItemChat);
    }

    public void showServiceChatThreadDetails(int from, String title) {
        if (isLandscape()) {
            Fragment f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
            if (f1 instanceof ServiceDetailsChatFragment) {
                final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//                ((ServiceDetailsChatFragment) f1).updateIndex(index);
                ft.detach(f1);
                ft.attach(f1);
                ft.commit();
            } else {
                ServiceDetailsChatFragment serviceDetailsChatFragment = new ServiceDetailsChatFragment();
                String backStateName = serviceDetailsChatFragment.getClass().getName();
                FragmentManager manager = getSupportFragmentManager();
                boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);
                replaceFragmentDetails(serviceDetailsChatFragment, fragmentPopped);
            }
            setSubTitle(title);
            //update details only
            findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else {
            mToolbar.setTitle(title);
            ServiceDetailsChatFragment serviceDetailsChatFragment = new ServiceDetailsChatFragment();
            replaceFragment(serviceDetailsChatFragment);
        }
        mNavigationView.setCheckedItem(R.id.navItemChat);
    }

    public void trackLocation(int from) {
        if (isLandscape()) {
            Fragment f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
            if (f1 instanceof TrackLocationFragment) {
                final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//                ((ServiceDetailsChatFragment) f1).updateIndex(index);
                ft.detach(f1);
                ft.attach(f1);
                ft.commit();
            } else {
                TrackLocationFragment trackLocationFragment = new TrackLocationFragment();
                replaceFragmentDetails(trackLocationFragment, false);
            }

            //update details only
            findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else {
            TrackLocationFragment trackLocationFragment = new TrackLocationFragment();
            replaceFragment(trackLocationFragment);
        }
        mNavigationView.setCheckedItem(R.id.navItemChat);
    }

    public void serviceEstimation(int from) {
        if (isLandscape()) {
            Fragment f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
            if (f1 instanceof ServiceEstimationFragment) {
                final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//                ((ServiceDetailsChatFragment) f1).updateIndex(index);
                ft.detach(f1);
                ft.attach(f1);
                ft.commit();
            } else {
                ServiceEstimationFragment serviceEstimationFragment = new ServiceEstimationFragment();
                replaceFragmentDetails(serviceEstimationFragment, false);
            }

            //update details only
            findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else {
            ServiceEstimationFragment serviceEstimationFragment = new ServiceEstimationFragment();
            replaceFragment(serviceEstimationFragment);
        }
        mNavigationView.setCheckedItem(R.id.navItemJobRoster);
    }

    public void additionalJobs(int from) {
        if (isLandscape()) {
            Fragment f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
            if (f1 instanceof AdditionalJobsFragment) {
                final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//                ((ServiceDetailsChatFragment) f1).updateIndex(index);
                ft.detach(f1);
                ft.attach(f1);
                ft.commit();
            } else {
                AdditionalJobsFragment additionalJobsFragment = new AdditionalJobsFragment();
                replaceFragmentDetails(additionalJobsFragment, false);
            }

            //update details only
            findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else {
            AdditionalJobsFragment additionalJobsFragment = new AdditionalJobsFragment();
            replaceFragment(additionalJobsFragment);
        }
        mNavigationView.setCheckedItem(R.id.navItemJobRoster);
    }

    public void allocateJobToTechnician(int eventId, int workOrderId, int serviceProviderId) {
        if (isLandscape()) {
            Fragment f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
            if (f1 instanceof AllocateTechnicianFragment) {
                final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ((AllocateTechnicianFragment) f1).setUpdateEventDetailsCallbackSP(this);
//                ((ServiceDetailsChatFragment) f1).updateIndex(index);
                ft.detach(f1);
                ft.attach(f1);
                ft.commit();
            } else {
                AllocateTechnicianFragment allocateTechnicianFragment = new AllocateTechnicianFragment();
                allocateTechnicianFragment.setUpdateEventDetailsCallbackSP(this);
                Bundle bundle = new Bundle();
                bundle.putBoolean("flag", true);
                bundle.putInt("eventId", eventId);
                bundle.putInt("workOrderId", workOrderId);
                bundle.putInt("serviceProviderId", serviceProviderId);
                allocateTechnicianFragment.setArguments(bundle);
                replaceFragmentDetails(allocateTechnicianFragment, false);
            }

            //update details only
            findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else {
            AllocateTechnicianFragment allocateTechnicianFragment = new AllocateTechnicianFragment();
            allocateTechnicianFragment.setUpdateEventDetailsCallbackSP(this);
            Bundle bundle = new Bundle();
            bundle.putBoolean("flag", true);
            bundle.putInt("eventId", eventId);
            bundle.putInt("workOrderId", workOrderId);
            bundle.putInt("serviceProviderId", serviceProviderId);
            allocateTechnicianFragment.setArguments(bundle);
            replaceFragment(allocateTechnicianFragment);
        }
        mNavigationView.setCheckedItem(R.id.navItemJobRoster);
    }

    public void goToServiceDetails(int eventId) {
        if (isLandscape()) {
            loadSplitViewIfLandscape(eventId);
            /*Fragment f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
            if (f1 instanceof ServiceDetailsFragment) {
                final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ((ServiceDetailsFragment) f1).updateIndex(eventId, index);
                ft.detach(f1);
                ft.attach(f1);
                ft.commit();
            } else {
                ServiceDetailsFragment serviceDetailsFragment = new ServiceDetailsFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("eventId", eventId);
                serviceDetailsFragment.setArguments(bundle);
//                serviceDetailsFragment.updateIndex(from, index);
                String backStateName = serviceDetailsFragment.getClass().getName();
                FragmentManager manager = getSupportFragmentManager();
                boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);
                replaceFragmentDetails(serviceDetailsFragment, fragmentPopped);
                if (fragmentPopped) {
                    f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
                    if (f1 instanceof ServiceDetailsFragment) {
                        final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                        ((ServiceDetailsFragment) f1).updateIndex(eventId, index);
                        ft.detach(f1);
                        ft.attach(f1);
                        ft.commit();
                    }
                }
            }*/

            //update details only
            findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else {
            ServiceDetailsFragment serviceDetailsFragment = new ServiceDetailsFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("eventId", eventId);
            serviceDetailsFragment.setArguments(bundle);
//            serviceDetailsFragment.updateIndex(from, index);
            replaceFragment(serviceDetailsFragment);
        }
        mNavigationView.setCheckedItem(R.id.navItemJobRoster);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);

        if (fragment != null) {
            if (isLandscape()) {
                Fragment subFragment = getSupportFragmentManager().findFragmentById(R.id.details_container);
                if (fragment instanceof JobRosterFragment && subFragment instanceof AllocateTechnicianFragment) {
                    if (menuItemDone != null)
                        menuItemDone.setVisible(false);
                    if (menuItemNotification != null)
                        menuItemNotification.setVisible(false);
                } else if ((fragment instanceof JobRosterFragment && subFragment instanceof ServiceEstimationFragment)) {
                    if (menuItemDone != null)
                        menuItemDone.setVisible(true);
                    if (menuItemNotification != null)
                        menuItemNotification.setVisible(false);
                } else {
                    if (menuItemDone != null)
                        menuItemDone.setVisible(false);
                    if (menuItemNotification != null)
                        menuItemNotification.setVisible(true);
                }
            } else {
                if (fragment instanceof AllocateTechnicianFragment) {
                    if (menuItemDone != null)
                        menuItemDone.setVisible(false);
                    if (menuItemNotification != null)
                        menuItemNotification.setVisible(false);
                } else if (fragment instanceof ServiceEstimationFragment) {
                    if (menuItemDone != null)
                        menuItemDone.setVisible(true);
                    if (menuItemNotification != null)
                        menuItemNotification.setVisible(false);
                } else {
                    if (menuItemDone != null)
                        menuItemDone.setVisible(false);
                    if (menuItemNotification != null)
                        menuItemNotification.setVisible(true);
                }
            }
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.sp_dashboard, menu);
        menuItemDone = menu.findItem(R.id.action_done);
        menuItemNotification = menu.findItem(R.id.action_notifications);
        View count = menu.findItem(R.id.action_notifications).getActionView();
        count.setOnClickListener(view -> replaceFragment(new NotificationsFragment()));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Drawable drawable = item.getIcon();
        if (drawable instanceof Animatable) {
            ((Animatable) drawable).start();
        }
        switch (item.getItemId()) {
            /*case R.id.action_logout:
                getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE).edit().putInt("PREF_KEY_USER_LOGGED_IN_MODE", 0).apply();
                Intent intent = LoginActivity.getStartIntent(ServiceProviderDashboardActivity.this);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                return true;
            case R.id.action_new:
                return true;*/
            case R.id.action_notifications:
                return true;
            case R.id.action_done:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //NEW - 0
    //ONGOING - 1
    //COMPLETED - 2
    //ARCHIVE - 3
    public void showOrderDetailsFromNotifications(int from, int index) {
        if (isLandscape()) {
            replaceFragment(new JobRosterFragment());
        } else {
            ServiceDetailsFragment serviceDetailsFragment = new ServiceDetailsFragment();
            serviceDetailsFragment.updateIndex(from, index);
            replaceFragment(serviceDetailsFragment);
        }
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getRejectionTypes(int eventId, int workOrderId, int workOrderStatusId, List<RejectionTypesResponse.RejectionType> rejectionTypeList) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        if (fragment == null) return;
        /*if (isLandscape()) {
            Fragment subFragment = getSupportFragmentManager().findFragmentById(R.id.details_container);
        } else {*/
        RejectWithReasonDialogFragment rejectWithReasonDialogFragment = new RejectWithReasonDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("from", 0);
        bundle.putInt("eventId", eventId);
        bundle.putInt("workOrderId", workOrderId);
        bundle.putInt("workOrderStatusId", workOrderStatusId);
        bundle.putSerializable("rejections", (Serializable) rejectionTypeList);
        rejectWithReasonDialogFragment.setArguments(bundle);
        if (fragment instanceof JobRosterFragment) {
            rejectWithReasonDialogFragment.setSubmitListener(((JobRosterFragment) fragment).submitListener);
            if (isLandscape()) {
                Fragment subFragment = getSupportFragmentManager().findFragmentById(R.id.details_container);
                rejectWithReasonDialogFragment.setSubmitListener(((ServiceDetailsFragment) subFragment).submitListener);
            }
        } else if (fragment instanceof ServiceDetailsFragment) {
            rejectWithReasonDialogFragment.setSubmitListener(((ServiceDetailsFragment) fragment).submitListener);
        }
        rejectWithReasonDialogFragment.show(this.getSupportFragmentManager(), "RejectWithReason");
//        }
    }

    @Override
    public void onRejectWorkOrders(AcceptWorkOrderResponse acceptWorkOrderResponse) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        if (fragment == null) return;
        if (isLandscape()) {
            Fragment subFragment = getSupportFragmentManager().findFragmentById(R.id.details_container);
            if (fragment instanceof JobRosterFragment) {
                ((JobRosterFragment) fragment).onRejectWorkOrders(acceptWorkOrderResponse);
            }
            if (subFragment instanceof ServiceDetailsFragment) {
                ((ServiceDetailsFragment) subFragment).onRejectWorkOrders(acceptWorkOrderResponse);
            }
        } else {
            if (fragment instanceof JobRosterFragment) {
                ((JobRosterFragment) fragment).onRejectWorkOrders(acceptWorkOrderResponse);
            } else if (fragment instanceof ServiceDetailsFragment) {
                ((ServiceDetailsFragment) fragment).onRejectWorkOrders(acceptWorkOrderResponse);
                FragmentManager fragmentManager = getSupportFragmentManager();
                Fragment fragment1 = fragmentManager.findFragmentByTag(JobRosterFragment.class.getName());
                if (fragment1 instanceof JobRosterFragment) {
                    getViewModel().isReloadNeeded = true;
                }
            }
        }
    }

    @Override
    public void onAcceptEvent() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        if (fragment == null) return;
        if (isLandscape()) {
            Fragment subFragment = getSupportFragmentManager().findFragmentById(R.id.details_container);
            /*if (fragment instanceof JobRosterFragment) {
                ((JobRosterFragment) fragment).onAcceptWorkOrders();
            }*/
            Toast.makeText(this, "Successfully accepted", Toast.LENGTH_LONG).show();
            if (subFragment instanceof ServiceDetailsFragment) {
                ((ServiceDetailsFragment) subFragment).onAcceptWorkOrders();
            }
        } else {
            if (fragment instanceof JobRosterFragment) {
                ((JobRosterFragment) fragment).onAcceptWorkOrders();
            } else if (fragment instanceof ServiceDetailsFragment) {
                ((ServiceDetailsFragment) fragment).onAcceptWorkOrders();
                FragmentManager fragmentManager = getSupportFragmentManager();
                Fragment fragment1 = fragmentManager.findFragmentByTag(JobRosterFragment.class.getName());
                if (fragment1 instanceof JobRosterFragment) {
                    getViewModel().isReloadNeeded = true;
                }
            }
        }
    }

    public void loadSplitViewIfLandscape(int eventId) {
        ServiceDetailsFragment serviceDetailsFragment = new ServiceDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("eventId", eventId);
        serviceDetailsFragment.setArguments(bundle);
        replaceFragmentDetails(serviceDetailsFragment, false);
        findViewById(R.id.details_container).setVisibility(View.VISIBLE);
    }
}
