package com.ttnfleetsolutions.ttnfleet.ui.chat;

import android.content.Context;
import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseActivity;
import com.ttnfleetsolutions.ttnfleet.ui.fleet.DashboardActivity;
import com.ttnfleetsolutions.ttnfleet.ui.serviceprovider.ServiceProviderDashboardActivity;
import com.ttnfleetsolutions.ttnfleet.ui.technician.TechnicianDashboardActivity;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.MainActivityNew;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */

public class MainChatAdapter extends RecyclerView.Adapter<MainChatAdapter.MyViewHolder> {

    private Context mContext;
    private String[] names, dates, counts, texts;
    Integer selectedItemPosition = null;

    public MainChatAdapter(Context context) {
        this.mContext = context;
        if (mContext instanceof DashboardActivity
                || mContext instanceof MainActivityNew) {
            names = mContext.getResources().getStringArray(R.array.services_chat);
            texts = mContext.getResources().getStringArray(R.array.services_chat_text);
        } else if (mContext instanceof ServiceProviderDashboardActivity
                || mContext instanceof TechnicianDashboardActivity) {
            names = mContext.getResources().getStringArray(R.array.jobs_chat);
            texts = mContext.getResources().getStringArray(R.array.services_chat_text);
        } else {
            names = mContext.getResources().getStringArray(R.array.services_chat1);
            texts = mContext.getResources().getStringArray(R.array.services_chat_text1);
        }

        dates = mContext.getResources().getStringArray(R.array.times);
        counts = mContext.getResources().getStringArray(R.array.services_chat_count);

        /*if (mContext instanceof DashboardActivity) {
            if (((DashboardActivity) mContext).isLandscape()) {
                selectedItemPosition = 0;
            }
        } else if (mContext instanceof ServiceProviderDashboardActivity) {
            if (((ServiceProviderDashboardActivity) mContext).isLandscape()) {
                selectedItemPosition = 0;
            }
        } else if (mContext instanceof TechnicianDashboardActivity) {
            if (((TechnicianDashboardActivity) mContext).isLandscape()) {
                selectedItemPosition = 0;
            }
        } else if (mContext instanceof MainActivityNew) {
            if (((MainActivityNew) mContext).isLandscape()) {
                selectedItemPosition = 0;
            }
        }*/
        if (((BaseActivity) mContext).isLandscape()) {
            selectedItemPosition = 0;
        }

//        else if (mContext instanceof com.ttnfleetsolutions.ttnfleet.ui.trucker.TruckerHomeActivity) {
//            if (((com.ttnfleetsolutions.ttnfleet.ui.trucker.TruckerHomeActivity) mContext).isLandscape()) {
//                selectedItemPosition = 0;
//            }
//        }

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_services_chat, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        if (selectedItemPosition != null && selectedItemPosition == position) {
            // Here I am just highlighting the background
            holder.chatRowLayout.setBackgroundColor(mContext.getResources().getColor(R.color.tr_gray));
        } else {
            holder.chatRowLayout.setBackgroundColor(Color.TRANSPARENT);
        }

        holder.names.setText(names[position]);
        holder.dates.setText(dates[position]);
        holder.counts.setText(counts[position]);
        holder.texts.setText(texts[position]);
        if (position > 1) {
            holder.counts.setVisibility(View.GONE);
        } else {
            holder.counts.setVisibility(View.VISIBLE);
        }
        holder.chatRowLayout.setOnClickListener(view -> {

            if (selectedItemPosition != null) {
                notifyItemChanged(selectedItemPosition);
            }
            selectedItemPosition = position;
            notifyItemChanged(selectedItemPosition);

            if (mContext instanceof DashboardActivity)
                ((DashboardActivity) mContext).showServiceChatThreadDetails(position, names[position]);
            else if (mContext instanceof ServiceProviderDashboardActivity)
                ((ServiceProviderDashboardActivity) mContext).showServiceChatThreadDetails(position, names[position]);
            else if (mContext instanceof TechnicianDashboardActivity)
                ((TechnicianDashboardActivity) mContext).showServiceChatThreadDetails(position, names[position]);
            else if (mContext instanceof MainActivityNew)
                ((MainActivityNew) mContext).showServiceChatThreadDetails(position, names[position]);
        });

    }

    @Override
    public int getItemCount() {
        if (mContext instanceof DashboardActivity) return 5;
        else return 3;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView names, dates, counts, texts;
        ConstraintLayout chatRowLayout;

        public MyViewHolder(View view) {
            super(view);
            chatRowLayout = view.findViewById(R.id.chatRowLayout);
            names = view.findViewById(R.id.name);
            dates = view.findViewById(R.id.date);
            counts = view.findViewById(R.id.count);
            texts = view.findViewById(R.id.chatText);
        }
    }
}
