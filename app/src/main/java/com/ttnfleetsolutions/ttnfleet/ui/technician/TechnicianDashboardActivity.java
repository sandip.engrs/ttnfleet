package com.ttnfleetsolutions.ttnfleet.ui.technician;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.request.RequestOptions;
import com.ttnfleetsolutions.ttnfleet.BR;
import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.AdditionalJobResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.AcceptWorkOrderResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.CompleteJobResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.EnrouteResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.RejectionTypesResponse;
import com.ttnfleetsolutions.ttnfleet.databinding.ActivityTechnicianDashboardBinding;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseActivity;
import com.ttnfleetsolutions.ttnfleet.ui.chat.ChatFragment;
import com.ttnfleetsolutions.ttnfleet.ui.chat.MainChatFragment;
import com.ttnfleetsolutions.ttnfleet.ui.chat.ServiceDetailsChatFragment;
import com.ttnfleetsolutions.ttnfleet.ui.login.LoginActivity;
import com.ttnfleetsolutions.ttnfleet.ui.serviceprovider.RejectWithReasonDialogFragment;
import com.ttnfleetsolutions.ttnfleet.ui.technician.additional.CreateAdditionalJobFragment;
import com.ttnfleetsolutions.ttnfleet.ui.technician.history.ServiceHistoryFragment;
import com.ttnfleetsolutions.ttnfleet.ui.technician.inspection.JobInspectionTechFragment;
import com.ttnfleetsolutions.ttnfleet.ui.technician.jobroster.JobRosterFragment;
import com.ttnfleetsolutions.ttnfleet.ui.technician.requirements.RequirementsFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.TrackLocationFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.vmrs.assembly.SelectAssemblyFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.vmrs.component.SelectComponentFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.vmrs.system.SelectServiceFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.servicerequest.ServiceDetailsFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.servicerequest.callback.UpdateEventDetailsCallbackTech;
import com.ttnfleetsolutions.ttnfleet.utils.CommonUtils;
import com.ttnfleetsolutions.ttnfleet.utils.GlideApp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

import static com.ttnfleetsolutions.ttnfleet.data.remote.ApiEndPoint.DIR_IMAGE_PROFILE;
import static com.ttnfleetsolutions.ttnfleet.data.remote.ApiEndPoint.DIR_IMAGE_THEME_LOGO;

/**
 * Created by Sandip Chaulagain on 10/10/2017.
 *
 * @version 1.0
 */

public class TechnicianDashboardActivity extends BaseActivity<ActivityTechnicianDashboardBinding, TechnicianDashboardActivityModel>
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener, HasSupportFragmentInjector, TechnicianDashboardNavigator, UpdateEventDetailsCallbackTech {

    public Toolbar mToolbar;
    private NavigationView mNavigationView;
    private DrawerLayout mDrawerLayout;
    private MenuItem menuItemDone, menuItemNotification, menuItemSearch;
    private SearchView mSearchView;

    @Inject
    TechnicianDashboardActivityModel mDashboardViewModel;

    private ActivityTechnicianDashboardBinding mActivityTechnicianDashboardBinding;

    @Inject
    DispatchingAndroidInjector<Fragment> fragmentDispatchingAndroidInjector;

    @Override
    public TechnicianDashboardActivityModel getViewModel() {
        return mDashboardViewModel;
    }

    @Override
    protected int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_technician_dashboard;
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentDispatchingAndroidInjector;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityTechnicianDashboardBinding = getViewDataBinding();
        mDashboardViewModel.setNavigator(this);

        setUp();
    }

    private void setUp() {
        mToolbar = findViewById(R.id.baseToolbar);
        mToolbar.setTitle("Jobs");
        setSupportActionBar(mToolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayUseLogoEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(false);
        }

        setThemeColors();
        initComponents();
    }

    public void setThemeColors() {
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(getViewModel().getDataManager().getThemeStatusBarColor());

        mToolbar.setBackgroundColor(getViewModel().getDataManager().getThemeTitleBarColor());
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        mDrawerLayout.closeDrawer(GravityCompat.START);

        int size = mNavigationView.getMenu().size();
        for (int i = 0; i < size; i++) {
            mNavigationView.getMenu().getItem(i).setChecked(false);
        }
        switch (item.getItemId()) {
            case R.id.navItemJobs:
                replaceFragment(new JobRosterFragment());
                return true;
            case R.id.navItemHistory:
                replaceFragment(new ServiceHistoryFragment());
                return true;
            case R.id.navItemNotifications:
                replaceFragment(new NotificationsFragment());
                return true;
            case R.id.navItemChat:
                replaceFragment(new MainChatFragment());
                return true;
            case R.id.navItemLogout:
                setDefaultTheme();
                Intent intent = LoginActivity.getStartIntent(TechnicianDashboardActivity.this);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                return true;
            default:
                return false;
        }
    }

    @Override
    public void onClick(View v) {

    }

    /*@Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentDispatchingAndroidInjector;
    }*/

    public static Intent getStartIntent(Context context) {
        return new Intent(context, TechnicianDashboardActivity.class);
    }

    private void replaceFragment(Fragment fragment) {
        invalidateOptionsMenu();
        String backStateName = fragment.getClass().getName();

        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.container, fragment, backStateName);
            ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            ft.addToBackStack(backStateName);
            ft.commit();
        }

        if (isLandscape()) {
            if (fragment instanceof JobRosterFragment) {
                /*ServiceDetailsFragment serviceDetailsFragment = new ServiceDetailsFragment();
                serviceDetailsFragment.updateIndex(2, 1);
                replaceFragmentDetails(serviceDetailsFragment, false);
                findViewById(R.id.details_container).setVisibility(View.VISIBLE);*/
            } else if (fragment instanceof ServiceHistoryFragment) {
                /*ServiceDetailsFragment serviceDetailsFragment = new ServiceDetailsFragment();
                serviceDetailsFragment.updateIndex(-1, 3);
                replaceFragmentDetails(serviceDetailsFragment, false);
                findViewById(R.id.details_container).setVisibility(View.VISIBLE);*/
            } else if (fragment instanceof MainChatFragment) {
                ServiceDetailsChatFragment serviceDetailsChatFragment = new ServiceDetailsChatFragment();
                replaceFragmentDetails(serviceDetailsChatFragment, false);
                findViewById(R.id.details_container).setVisibility(View.VISIBLE);
            } else {
                findViewById(R.id.details_container).setVisibility(View.GONE);
            }
        }
    }

    private void replaceFragmentDetails(Fragment fragment, boolean fragmentPopped) {
        String backStateName = fragment.getClass().getName();
        FragmentManager manager = getSupportFragmentManager();
//        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);
//        boolean fragmentPopped = false;

        if (!fragmentPopped) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.details_container, fragment, backStateName);
            ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
        updateSubTitle(fragment);
    }

    @Override
    public void onBackPressed() {
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.container);
        if (isLandscape()) {
            Fragment f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
            if (f != null && f1 != null) {
                if (f instanceof JobRosterFragment && f1 instanceof ServiceDetailsFragment) {
                    finish();
                } else if (f instanceof ServiceHistoryFragment && f1 instanceof ServiceDetailsFragment) {
                    getSupportFragmentManager().popBackStack();
                } else if (f instanceof MainChatFragment && f1 instanceof ServiceDetailsChatFragment) {
                    getSupportFragmentManager().popBackStack();
                }
                updateSubTitle(f1);
            }
        }
        if (f instanceof JobRosterFragment && !isLandscape()) {
            finish();
        } else {
            super.onBackPressed();
            f = getSupportFragmentManager().findFragmentById(R.id.container);
            if (f != null) {
                updateTitleAndDrawer(f);
            }
        }
    }

    private void updateTitleAndDrawer(Fragment fragment) {
        invalidateOptionsMenu();
        CommonUtils.hideKeyboard(this);
        String fragClassName = fragment.getClass().getName();
        if (findViewById(R.id.details_container) != null)
            findViewById(R.id.details_container).setVisibility(View.GONE);

        if (fragClassName.equals(JobRosterFragment.class.getName())) {
            mToolbar.setTitle("Jobs");
            mNavigationView.setCheckedItem(R.id.navItemJobs);
            if (findViewById(R.id.details_container) != null)
                findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else if (fragClassName.equals(SelectServiceFragment.class.getName())) {
            mToolbar.setTitle("Select System Code");
            mNavigationView.setCheckedItem(R.id.navItemFillForm);
        } else if (fragClassName.equals(SelectAssemblyFragment.class.getName())) {
            mToolbar.setTitle("Select Assembly Code");
            mNavigationView.setCheckedItem(R.id.navItemFillForm);
        } else if (fragClassName.equals(SelectComponentFragment.class.getName())) {
            mToolbar.setTitle("Select Component Code");
            mNavigationView.setCheckedItem(R.id.navItemFillForm);
        } else if (fragClassName.equals(NotificationsFragment.class.getName())) {
            mToolbar.setTitle("Notifications");
            mNavigationView.setCheckedItem(R.id.navItemNotifications);
        } else if (fragClassName.equals(MainChatFragment.class.getName())) {
            mToolbar.setTitle("Chat");
            mNavigationView.setCheckedItem(R.id.navItemChat);
            if (findViewById(R.id.details_container) != null)
                findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else if (fragClassName.equals(ServiceHistoryFragment.class.getName())) {
            mToolbar.setTitle("Job History");
            mNavigationView.setCheckedItem(R.id.navItemHistory);
            if (findViewById(R.id.details_container) != null)
                findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else if (fragClassName.equals(JobInspectionTechFragment.class.getName())) {
            mToolbar.setTitle("Job Inspection");
            mNavigationView.setCheckedItem(R.id.navItemJobs);
        } else if (fragClassName.equals(AdditionalJobFragment.class.getName())) {
            mToolbar.setTitle("Additional Job");
            mNavigationView.setCheckedItem(R.id.navItemJobs);
        } else if (fragClassName.equals(CreateAdditionalJobFragment.class.getName())) {
            mToolbar.setTitle("Add Note");
            mNavigationView.setCheckedItem(R.id.navItemJobs);
        } else if (fragClassName.equals(RequirementsFragment.class.getName())) {
            mToolbar.setTitle("Requirements");
            mNavigationView.setCheckedItem(R.id.navItemJobs);
        } else if (fragClassName.equals(JobCheckListFragment.class.getName())) {
            mToolbar.setTitle("Job Checklist");
            mNavigationView.setCheckedItem(R.id.navItemJobs);
        } else if (fragClassName.equals(ServiceDetailsChatFragment.class.getName())) {
//            mToolbar.setTitle("Service Details Chat");
            mNavigationView.setCheckedItem(R.id.navItemChat);
        } else if (fragClassName.equals(ChatFragment.class.getName())) {
//            mToolbar.setTitle("Service Details 1");
            mNavigationView.setCheckedItem(R.id.navItemChat);
        } /*else if (fragClassName.equals(ServiceDetailsFragment.class.getName())) {
            mToolbar.setTitle("Job Details");
            mNavigationView.setCheckedItem(R.id.navItemJobs);
        }*/
    }

    private void updateSubTitle(Fragment fragment) {
        String fragClassName = fragment.getClass().getName();
        if (fragClassName.equals(RequirementsFragment.class.getName())) {
            setSubTitle("Requirements");
        } else if (fragClassName.equals(JobInspectionTechFragment.class.getName())) {
            setSubTitle("Job Inspection");
        } else if (fragClassName.equals(JobCheckListFragment.class.getName())) {
            setSubTitle("Job Checklist");
        } /*else if (fragClassName.equals(ServiceDetailsFragment.class.getName())) {
            setSubTitle("Job Details");
        }*/ else if (fragClassName.equals(ServiceDetailsChatFragment.class.getName())) {
//            setSubTitle("Chat");
        } else if (fragClassName.equals(ChatFragment.class.getName())) {
//            setSubTitle("Chat");
        } else if (fragClassName.equals(TrackLocationFragment.class.getName())) {
            setSubTitle("Track Location");
        } else if (fragClassName.equals(AdditionalJobFragment.class.getName())) {
            setSubTitle("Additional Job");
        } else if (fragClassName.equals(CreateAdditionalJobFragment.class.getName())) {
            setSubTitle("Add Note");
        }
    }

    public void setSubTitle(String subTitle) {
        if (!TextUtils.isEmpty(subTitle)) {
            ((TextView) findViewById(R.id.subTitle)).setText(subTitle);
            findViewById(R.id.subTitle).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.subTitle).setVisibility(View.GONE);
        }
    }

    public void setTitle(String title) {
        if (!TextUtils.isEmpty(title)) {
            mToolbar.setTitle(title);
        }
    }

    private void initComponents() {
        mDrawerLayout = findViewById(R.id.drawerLayout);
        mNavigationView = findViewById(R.id.navigationView);
        int[][] state = new int[][]{
                new int[]{android.R.attr.state_checked},
                new int[]{android.R.attr.state_enabled},
                new int[]{android.R.attr.state_pressed},
                new int[]{android.R.attr.state_focused},
                new int[]{android.R.attr.state_pressed}
        };

        int[] color = new int[]{
                getViewModel().getDataManager().getThemeStatusBarColor(),
                Color.DKGRAY,
                Color.DKGRAY,
                Color.DKGRAY,
                Color.DKGRAY
        };

        //Defining ColorStateList for menu item Icon
        ColorStateList navMenuIconList = new ColorStateList(
                new int[][]{
                        new int[]{android.R.attr.state_checked},
                        new int[]{android.R.attr.state_enabled},
                        new int[]{android.R.attr.state_pressed},
                        new int[]{android.R.attr.state_focused},
                        new int[]{android.R.attr.state_pressed}
                },
                new int[]{
                        getViewModel().getDataManager().getThemeStatusBarColor(),
                        getViewModel().getDataManager().getThemeTitleBarColor(),
                        getViewModel().getDataManager().getThemeTitleBarColor(),
                        getViewModel().getDataManager().getThemeTitleBarColor(),
                        getViewModel().getDataManager().getThemeTitleBarColor()
                }
        );
        ColorStateList ColorStateList1 = new ColorStateList(state, color);
        mNavigationView.setItemTextColor(ColorStateList1);
//        mNavigationView.setItemIconTintList(ColorStateList.valueOf(getViewModel().getDataManager().getThemeStatusBarColor()));
        mNavigationView.setItemIconTintList(navMenuIconList);
        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawerLayout,
                mToolbar,
                R.string.open_drawer,
                R.string.close_drawer) {
        };
        mDrawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        mNavigationView.setNavigationItemSelectedListener(this);
        replaceFragment(new JobRosterFragment());
        mNavigationView.setCheckedItem(R.id.navItemJobs);

        View.OnClickListener onClickListener = mDrawerToggle.getToolbarNavigationClickListener();
        getSupportFragmentManager().addOnBackStackChangedListener(() -> {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
            if (fragment != null)
                if (fragment instanceof ServiceDetailsFragment || fragment instanceof TrackLocationFragment
                        || fragment instanceof ChatFragment || fragment instanceof ServiceDetailsChatFragment
                        || fragment instanceof AdditionalJobFragment || fragment instanceof JobCheckListFragment
                        || fragment instanceof JobInspectionTechFragment || fragment instanceof RequirementsFragment
                        || fragment instanceof SelectServiceFragment || fragment instanceof SelectAssemblyFragment
                        || fragment instanceof SelectComponentFragment || fragment instanceof CreateAdditionalJobFragment) {
                    mDrawerToggle.setDrawerIndicatorEnabled(false);
                    mDrawerToggle.setToolbarNavigationClickListener(v -> onBackPressed());
                } else {
                    mDrawerToggle.setDrawerIndicatorEnabled(true);
                    mDrawerToggle.setToolbarNavigationClickListener(onClickListener);
                }

            Fragment f = getSupportFragmentManager().findFragmentById(R.id.container);
            if (f != null) {
                updateTitleAndDrawer(f);
            }
            if (isLandscape()) {
                Fragment f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
                if (f1 != null) {
                    updateSubTitle(f1);
                }
            }
        });

        View header = mNavigationView.getHeaderView(0);
        ImageView imageViewLogo = header.findViewById(R.id.logo);
        ImageView imageViewProfile = header.findViewById(R.id.profileImageView);
        header.findViewById(R.id.drawerHeaderBg).setBackgroundColor(getViewModel().getDataManager().getThemeTitleBarColor());

        ((TextView) header.findViewById(R.id.name)).setText(getViewModel().getDataManager().getCurrentUserFullName());
        header.findViewById(R.id.number).setVisibility(TextUtils.isEmpty(getViewModel().getDataManager().getCurrentUserRoleName()) ? View.GONE : View.VISIBLE);
        ((TextView) header.findViewById(R.id.number)).setText(getViewModel().getDataManager().getCurrentUserRoleName());

        GlideApp.with(this)
                .load(DIR_IMAGE_PROFILE + getViewModel().getDataManager().getCurrentUserID() + "/" + getViewModel().getDataManager().getCurrentUserProfileImage())
                .apply(RequestOptions.circleCropTransform())
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .into(imageViewProfile);

        GlideApp.with(this)
                .load(DIR_IMAGE_THEME_LOGO + getViewModel().getDataManager().getThemeLogo())
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .fitCenter()
                .into(imageViewLogo);
    }

    public void showSystemListing() {
        Fragment newFragment = new SelectServiceFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("from", 2);
        newFragment.setArguments(bundle);

        replaceFragment(newFragment);
    }

    public void showAssemblyListing(String systemCode, String systemCodeName) {
        Fragment newFragment = new SelectAssemblyFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("from", 2);
        bundle.putString("system", systemCode);
        bundle.putString("systemName", systemCodeName);
        newFragment.setArguments(bundle);
        replaceFragment(newFragment);
    }

    public void showComponentListing(String systemCode, String assemblyCode, String systemCodeName, String assemblyCodeName) {
        Fragment newFragment = new SelectComponentFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("from", 2);
        bundle.putString("system", systemCode);
        bundle.putString("assembly", assemblyCode);
        bundle.putString("systemName", systemCodeName);
        bundle.putString("assemblyName", assemblyCodeName);
        newFragment.setArguments(bundle);
        replaceFragment(newFragment);
    }

    public void addNoteForAdditionalJob(String systemCode, String systemCodeName, String assemblyCode, String assemblyCodeName, String componentCodeStr, List<String> componentCodeNameArray) {
        Fragment newFragment = new CreateAdditionalJobFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("from", 2);
        bundle.putString("system", systemCode);
        bundle.putString("assembly", assemblyCode);
        bundle.putString("systemName", systemCodeName);
        bundle.putString("assemblyName", assemblyCodeName);
        bundle.putString("componentCodeStr", componentCodeStr);
        bundle.putStringArrayList("componentCodeNameArray", (ArrayList<String>) componentCodeNameArray);
        newFragment.setArguments(bundle);
        replaceFragment(newFragment);
    }

    public void createAdditionalJobAPI(String note, String systemCode, String assemblyCode, String componentCodeStr) {
        getViewModel().additionalJobAPICall(note, systemCode, assemblyCode, componentCodeStr);
    }

    public void showChatDetails(int from, String title) {
        if (isLandscape()) {
            Fragment f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
            if (f1 instanceof ChatFragment) {
                final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ((ChatFragment) f1).setReceiver(title);
                ft.detach(f1);
                ft.attach(f1);
                ft.commit();
            } else {
                ChatFragment chatFragment = new ChatFragment();
                chatFragment.setReceiver(title);
                replaceFragmentDetails(chatFragment, false);
            }
            setSubTitle(title);
            //update details only
            findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else {
            mToolbar.setTitle(title);
            ChatFragment chatFragment = new ChatFragment();
            chatFragment.setReceiver(title);
            replaceFragment(chatFragment);
        }
        mNavigationView.setCheckedItem(R.id.navItemChat);
    }

    public void showServiceChatThreadDetails(int from, String title) {
        if (isLandscape()) {
            Fragment f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
            if (f1 instanceof ServiceDetailsChatFragment) {
                final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//                ((ServiceDetailsChatFragment) f1).updateIndex(index);
                ft.detach(f1);
                ft.attach(f1);
                ft.commit();
            } else {
                ServiceDetailsChatFragment serviceDetailsChatFragment = new ServiceDetailsChatFragment();
                String backStateName = serviceDetailsChatFragment.getClass().getName();
                FragmentManager manager = getSupportFragmentManager();
                boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);
                replaceFragmentDetails(serviceDetailsChatFragment, fragmentPopped);
            }
            setSubTitle(title);
            //update details only
            findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else {
            mToolbar.setTitle(title);
            ServiceDetailsChatFragment serviceDetailsChatFragment = new ServiceDetailsChatFragment();
            replaceFragment(serviceDetailsChatFragment);
        }
        mNavigationView.setCheckedItem(R.id.navItemChat);
    }

    public void trackLocation(int from) {
        if (isLandscape()) {
            Fragment f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
            if (f1 instanceof TrackLocationFragment) {
                final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//                ((ServiceDetailsChatFragment) f1).updateIndex(index);
                ft.detach(f1);
                ft.attach(f1);
                ft.commit();
            } else {
                TrackLocationFragment trackLocationFragment = new TrackLocationFragment();
                replaceFragmentDetails(trackLocationFragment, false);
            }

            //update details only
            findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else {
            TrackLocationFragment trackLocationFragment = new TrackLocationFragment();
            replaceFragment(trackLocationFragment);
        }
        mNavigationView.setCheckedItem(R.id.navItemChat);
    }

    public void showJobRequirements(int eventId, int jobId) {
        if (isLandscape()) {
            Fragment f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
            if (f1 instanceof RequirementsFragment) {
                final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ((RequirementsFragment) f1).setUpdateEventDetailsCallback(this);
                ft.detach(f1);
                ft.attach(f1);
                ft.commit();
            } else {
                RequirementsFragment requirementsFragment = new RequirementsFragment();
                requirementsFragment.setUpdateEventDetailsCallback(this);
                Bundle bundle = new Bundle();
                bundle.putInt("eventId", eventId);
                bundle.putInt("jobId", jobId);
                requirementsFragment.setArguments(bundle);
                replaceFragmentDetails(requirementsFragment, false);
            }

            //update details only
            findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else {
            RequirementsFragment requirementsFragment = new RequirementsFragment();
            requirementsFragment.setUpdateEventDetailsCallback(this);
            Bundle bundle = new Bundle();
            bundle.putInt("eventId", eventId);
            bundle.putInt("jobId", jobId);
            requirementsFragment.setArguments(bundle);
            replaceFragment(requirementsFragment);
        }
        mNavigationView.setCheckedItem(R.id.navItemChat);
    }

    public void showJobCheckList(int from) {
        if (isLandscape()) {
            Fragment f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
            if (f1 instanceof JobCheckListFragment) {
                final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//                ((ServiceDetailsChatFragment) f1).updateIndex(index);
                ft.detach(f1);
                ft.attach(f1);
                ft.commit();
            } else {
                JobCheckListFragment jobCheckListFragment = new JobCheckListFragment();
                replaceFragmentDetails(jobCheckListFragment, false);
            }

            //update details only
            findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else {
            JobCheckListFragment jobCheckListFragment = new JobCheckListFragment();
            replaceFragment(jobCheckListFragment);
        }
        mNavigationView.setCheckedItem(R.id.navItemChat);
    }

    public void additionalJobs(int from) {
        if (isLandscape()) {
            Fragment f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
            if (f1 instanceof AdditionalJobFragment) {
                final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//                ((ServiceDetailsChatFragment) f1).updateIndex(index);
                ft.detach(f1);
                ft.attach(f1);
                ft.commit();
            } else {
                AdditionalJobFragment additionalJobFragment = new AdditionalJobFragment();
                replaceFragmentDetails(additionalJobFragment, false);
            }

            //update details only
            findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else {
            AdditionalJobFragment additionalJobFragment = new AdditionalJobFragment();
            replaceFragment(additionalJobFragment);
        }
        mNavigationView.setCheckedItem(R.id.navItemChat);
    }


    public void jobInspections(int jobId, String eventType, String breakdownNote) {
        if (isLandscape()) {
            Fragment f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
            if (f1 instanceof JobInspectionTechFragment) {
                final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ((JobInspectionTechFragment) f1).setUpdateEventDetailsCallback(this);
                ft.detach(f1);
                ft.attach(f1);
                ft.commit();
            } else {
                JobInspectionTechFragment jobInspectionTechFragment = new JobInspectionTechFragment();
                jobInspectionTechFragment.setUpdateEventDetailsCallback(this);
                Bundle bundle = new Bundle();
                bundle.putInt("jobId", jobId);
                bundle.putString("eventType", eventType);
                bundle.putString("breakdownNote", breakdownNote);
                jobInspectionTechFragment.setArguments(bundle);
                replaceFragmentDetails(jobInspectionTechFragment, false);
            }

            //update details only
            findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else {
            JobInspectionTechFragment jobInspectionTechFragment = new JobInspectionTechFragment();
            jobInspectionTechFragment.setUpdateEventDetailsCallback(this);
            Bundle bundle = new Bundle();
            bundle.putInt("jobId", jobId);
            bundle.putString("eventType", eventType);
            bundle.putString("breakdownNote", breakdownNote);
            jobInspectionTechFragment.setArguments(bundle);
            replaceFragment(jobInspectionTechFragment);
        }
        mNavigationView.setCheckedItem(R.id.navItemChat);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);

        if (fragment != null) {
            if (isLandscape()) {
                Fragment subFragment = getSupportFragmentManager().findFragmentById(R.id.details_container);
                if ((fragment instanceof JobRosterFragment && subFragment instanceof RequirementsFragment)
                        || (fragment instanceof JobRosterFragment && subFragment instanceof AdditionalJobFragment)
                        || (fragment instanceof JobRosterFragment && subFragment instanceof JobInspectionTechFragment)
                        || fragment instanceof CreateAdditionalJobFragment) {
                    if (menuItemDone != null)
                        menuItemDone.setVisible(true);
                    if (menuItemNotification != null)
                        menuItemNotification.setVisible(false);
                } else if (fragment instanceof SelectServiceFragment || fragment instanceof SelectAssemblyFragment || fragment instanceof SelectComponentFragment) {
                    if (menuItemSearch != null)
                        menuItemSearch.setVisible(true);
                    if (menuItemDone != null)
                        menuItemDone.setVisible(false);
                    if (menuItemNotification != null)
                        menuItemNotification.setVisible(false);
                } else {
                    if (menuItemSearch != null)
                        menuItemSearch.setVisible(false);
                    if (menuItemDone != null)
                        menuItemDone.setVisible(false);
                    if (menuItemNotification != null)
                        menuItemNotification.setVisible(true);
                }
            } else {
                if (fragment instanceof SelectServiceFragment || fragment instanceof SelectAssemblyFragment || fragment instanceof SelectComponentFragment
                        || fragment instanceof CreateAdditionalJobFragment) {

                    if (fragment instanceof CreateAdditionalJobFragment) {
                        if (menuItemSearch != null)
                            menuItemSearch.setVisible(false);

                        if (menuItemDone != null)
                            menuItemDone.setVisible(true);
                    } else {
                        if (menuItemSearch != null)
                            menuItemSearch.setVisible(true);

                        if (menuItemDone != null)
                            menuItemDone.setVisible(false);
                    }

                    if (menuItemNotification != null)
                        menuItemNotification.setVisible(false);

                } else if (fragment instanceof RequirementsFragment || fragment instanceof AdditionalJobFragment
                        || fragment instanceof JobInspectionTechFragment) {
                    if (menuItemDone != null)
                        menuItemDone.setVisible(true);
                    if (menuItemNotification != null)
                        menuItemNotification.setVisible(false);
                } else {
                    if (menuItemDone != null)
                        menuItemDone.setVisible(false);
                    if (menuItemNotification != null)
                        menuItemNotification.setVisible(true);
                }
            }
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.sp_dashboard, menu);
        menuItemDone = menu.findItem(R.id.action_done);
        menuItemNotification = menu.findItem(R.id.action_notifications);
        View count = menu.findItem(R.id.action_notifications).getActionView();
        count.setOnClickListener(view -> replaceFragment(new NotificationsFragment()));

        menuItemSearch = menu.findItem(R.id.action_search);
        mSearchView = (SearchView) menu.findItem(R.id.action_search).getActionView();

        mSearchView.setMaxWidth(Integer.MAX_VALUE);
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
                if (fragment != null)
                    if (fragment instanceof SelectServiceFragment)
                        ((SelectServiceFragment) fragment).searchQuery(newText);
                    else if (fragment instanceof SelectAssemblyFragment)
                        ((SelectAssemblyFragment) fragment).searchQuery(newText);
                    else if (fragment instanceof SelectComponentFragment)
                        ((SelectComponentFragment) fragment).searchQuery(newText);
                return true;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Drawable drawable = item.getIcon();
        if (drawable instanceof Animatable) {
            ((Animatable) drawable).start();
        }
        switch (item.getItemId()) {
            case R.id.action_notifications:
                replaceFragment(new NotificationsFragment());
                return true;
            case R.id.action_done:
                Fragment f = getSupportFragmentManager().findFragmentById(R.id.container);
                if (isLandscape()) {
                    Fragment subFragment = getSupportFragmentManager().findFragmentById(R.id.details_container);
                    if (subFragment instanceof JobInspectionTechFragment)
                        ((JobInspectionTechFragment) subFragment).actionDone();
                    else if (subFragment instanceof RequirementsFragment)
                        ((RequirementsFragment) subFragment).actionDone();
                    else if (f instanceof CreateAdditionalJobFragment)
                        ((CreateAdditionalJobFragment) f).actionDone();
                } else {
                    if (f instanceof JobInspectionTechFragment)
                        ((JobInspectionTechFragment) f).actionDone();
                    else if (f instanceof RequirementsFragment)
                        ((RequirementsFragment) f).actionDone();
                    else if (f instanceof CreateAdditionalJobFragment)
                        ((CreateAdditionalJobFragment) f).actionDone();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //NEW - 0
    //ONGOING - 1
    //COMPLETED - 2
    //ARCHIVE - 3
    public void showOrderDetails(int eventId) {
        if (isLandscape()) {
            loadSplitViewIfLandscape(eventId);
            /*Fragment f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
            if (f1 instanceof ServiceDetailsFragment) {
                final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ((ServiceDetailsFragment) f1).updateIndex(eventId, index);
                ft.detach(f1);
                ft.attach(f1);
                ft.commit();
            } else {
                ServiceDetailsFragment serviceDetailsFragment = new ServiceDetailsFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("eventId", eventId);
                serviceDetailsFragment.setArguments(bundle);
//                serviceDetailsFragment.updateIndex(from, index);
                String backStateName = serviceDetailsFragment.getClass().getName();
                FragmentManager manager = getSupportFragmentManager();
                boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);
                replaceFragmentDetails(serviceDetailsFragment, fragmentPopped);
                if (fragmentPopped) {
                    f1 = getSupportFragmentManager().findFragmentById(R.id.details_container);
                    if (f1 instanceof ServiceDetailsFragment) {
                        final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                        ((ServiceDetailsFragment) f1).updateIndex(eventId, index);
                        ft.detach(f1);
                        ft.attach(f1);
                        ft.commit();
                    }
                }
            }*/

            //update details only
            findViewById(R.id.details_container).setVisibility(View.VISIBLE);
        } else {
            ServiceDetailsFragment serviceDetailsFragment = new ServiceDetailsFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("eventId", eventId);
            serviceDetailsFragment.setArguments(bundle);
//            serviceDetailsFragment.updateIndex(from, index);
            replaceFragment(serviceDetailsFragment);
        }
        mNavigationView.setCheckedItem(R.id.navItemOngoing);
    }

    public void showOrderDetailsFromNotifications(int from, int index) {
        if (isLandscape()) {
            replaceFragment(new JobRosterFragment());
        } else {
            ServiceDetailsFragment serviceDetailsFragment = new ServiceDetailsFragment();
            serviceDetailsFragment.updateIndex(from, index);
            replaceFragment(serviceDetailsFragment);
        }
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getRejectionTypesTech(int eventId, int jobId, int jobStatusId, List<RejectionTypesResponse.RejectionType> rejectionTypeList) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        if (fragment == null) return;
        /*if (isLandscape()) {
            Fragment subFragment = getSupportFragmentManager().findFragmentById(R.id.details_container);
        } else {*/
        RejectWithReasonDialogFragment rejectWithReasonDialogFragment = new RejectWithReasonDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("from", 1);
        bundle.putInt("eventId", eventId);
        bundle.putInt("jobId", jobId);
        bundle.putInt("jobStatusId", jobStatusId);
        bundle.putSerializable("rejections", (Serializable) rejectionTypeList);
        rejectWithReasonDialogFragment.setArguments(bundle);
        if (fragment instanceof JobRosterFragment) {
            rejectWithReasonDialogFragment.setSubmitListener(((JobRosterFragment) fragment).submitListener);
        } else if (fragment instanceof ServiceDetailsFragment) {
            rejectWithReasonDialogFragment.setSubmitListener(((ServiceDetailsFragment) fragment).submitListener);
        }
        rejectWithReasonDialogFragment.show(this.getSupportFragmentManager(), "RejectWithReason");
//        }
    }

    @Override
    public void onAcceptRejectJob(AcceptWorkOrderResponse acceptWorkOrderResponse) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        if (fragment == null) return;
        if (isLandscape()) {
            Fragment subFragment = getSupportFragmentManager().findFragmentById(R.id.details_container);
            /*if (fragment instanceof JobRosterFragment) {
                ((JobRosterFragment) fragment).onAcceptRejectJob(acceptWorkOrderResponse);
            }*/
            if (subFragment instanceof ServiceDetailsFragment) {
                ((ServiceDetailsFragment) subFragment).onAcceptRejectJob(acceptWorkOrderResponse);
            }
        } else {
            if (fragment instanceof JobRosterFragment) {
                ((JobRosterFragment) fragment).onAcceptRejectJob(acceptWorkOrderResponse);
            } else if (fragment instanceof ServiceDetailsFragment) {
                ((ServiceDetailsFragment) fragment).onAcceptRejectJob(acceptWorkOrderResponse);
                Fragment fragment1 = getSupportFragmentManager().findFragmentByTag(JobRosterFragment.class.getName());
                if (fragment1 instanceof JobRosterFragment) {
                    getViewModel().isReloadNeeded = true;
                }
            }
        }
    }

    @Override
    public void onAdditionalJob(AdditionalJobResponse additionalJobResponse) {
        if (isLandscape()) {
            replaceFragment(new JobRosterFragment());
//            loadSplitViewIfLandscape(additionalJobResponse.getResult().getEventid());
        } else {
            Fragment fragment1 = getSupportFragmentManager().findFragmentByTag(ServiceDetailsFragment.class.getName());
            if (fragment1 != null) {
                showOrderDetails(additionalJobResponse.getResult().getEventid());
            } else {
                replaceFragment(new JobRosterFragment());
            }
        }

        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        if (fragment == null) return;
        if (isLandscape()) {
            Fragment subFragment = getSupportFragmentManager().findFragmentById(R.id.details_container);
            Toast.makeText(this, "Successfully added.", Toast.LENGTH_LONG).show();
            if (fragment instanceof JobRosterFragment) {
                ((JobRosterFragment) fragment).onAdditionalJobCreation(additionalJobResponse);
            }
            /*if (subFragment instanceof ServiceDetailsFragment) {
                ((ServiceDetailsFragment) subFragment).onAdditionalJobCreation(additionalJobResponse);
            }*/
        } else {
            Toast.makeText(this, "Successfully added.", Toast.LENGTH_LONG).show();
            if (fragment instanceof JobRosterFragment) {
                ((JobRosterFragment) fragment).onAdditionalJobCreation(additionalJobResponse);
            } else if (fragment instanceof ServiceDetailsFragment) {
                ((ServiceDetailsFragment) fragment).onAdditionalJobCreation(additionalJobResponse);
            }
        }
    }

    @Override
    public void onCompleteJob(CompleteJobResponse completeJobResponse) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        if (fragment == null) return;
        if (isLandscape()) {
            Fragment subFragment = getSupportFragmentManager().findFragmentById(R.id.details_container);
            Toast.makeText(this, "Successfully completed.", Toast.LENGTH_LONG).show();
            if (fragment instanceof JobRosterFragment) {
                ((JobRosterFragment) fragment).onJobCompletion(completeJobResponse);
            }
            /*if (subFragment instanceof ServiceDetailsFragment) {
                ((ServiceDetailsFragment) subFragment).onJobCompletion(completeJobResponse);
            }*/
        } else {
            Toast.makeText(this, "Successfully completed.", Toast.LENGTH_LONG).show();
            if (fragment instanceof JobRosterFragment) {
                ((JobRosterFragment) fragment).onJobCompletion(completeJobResponse);
            } else if (fragment instanceof ServiceDetailsFragment) {
                ((ServiceDetailsFragment) fragment).onJobCompletion(completeJobResponse);
                Fragment fragment1 = getSupportFragmentManager().findFragmentByTag(JobRosterFragment.class.getName());
                if (fragment1 instanceof JobRosterFragment) {
                    getViewModel().isReloadNeeded = true;
                }
            }
        }
    }

    @Override
    public void onEnroute(EnrouteResponse enrouteResponse) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        if (fragment == null) return;
        if (isLandscape()) {
            Fragment subFragment = getSupportFragmentManager().findFragmentById(R.id.details_container);
            Toast.makeText(this, "Started journey", Toast.LENGTH_LONG).show();
            /*if (fragment instanceof JobRosterFragment) {
                ((JobRosterFragment) fragment).onEnroute(enrouteResponse);
            }*/
            if (subFragment instanceof ServiceDetailsFragment) {
                ((ServiceDetailsFragment) subFragment).onEnroute(enrouteResponse);
            }
        } else {
            Toast.makeText(this, "Started journey", Toast.LENGTH_LONG).show();
            if (fragment instanceof JobRosterFragment) {
                ((JobRosterFragment) fragment).onEnroute(enrouteResponse);
            } else if (fragment instanceof ServiceDetailsFragment) {
                ((ServiceDetailsFragment) fragment).onEnroute(enrouteResponse);
                Fragment fragment1 = getSupportFragmentManager().findFragmentByTag(JobRosterFragment.class.getName());
                if (fragment1 instanceof JobRosterFragment) {
                    getViewModel().isReloadNeeded = true;
                }
            }
        }
    }

    @Override
    public void onNewRequirementImageUpload() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        if (fragment == null) return;
        if (isLandscape()) {
            Fragment subFragment = getSupportFragmentManager().findFragmentById(R.id.details_container);
            Toast.makeText(this, "Successfully completed.", Toast.LENGTH_LONG).show();
            if (subFragment instanceof ServiceDetailsFragment) {
                ((ServiceDetailsFragment) subFragment).reloadRequirementImages();
            }
        } else {
            Toast.makeText(this, "Successfully completed.", Toast.LENGTH_LONG).show();
            if (fragment instanceof ServiceDetailsFragment) {
                ((ServiceDetailsFragment) fragment).reloadRequirementImages();
            }
        }
    }

    @Override
    public void onConfirmedBreakdown() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        if (fragment == null) return;
        if (isLandscape()) {
            Fragment subFragment = getSupportFragmentManager().findFragmentById(R.id.details_container);
            /*if (fragment instanceof JobRosterFragment) {
                ((JobRosterFragment) fragment).onConfirmedBreakdown();
            }*/
            if (subFragment instanceof ServiceDetailsFragment) {
                ((ServiceDetailsFragment) subFragment).onConfirmedBreakdown();
            }
        } else {
            if (fragment instanceof JobRosterFragment) {
                ((JobRosterFragment) fragment).onConfirmedBreakdown();
            } else if (fragment instanceof ServiceDetailsFragment) {
                ((ServiceDetailsFragment) fragment).onConfirmedBreakdown();
                Fragment fragment1 = getSupportFragmentManager().findFragmentByTag(JobRosterFragment.class.getName());
                if (fragment1 instanceof JobRosterFragment) {
                    getViewModel().isReloadNeeded = true;
                }
            }
        }
    }

    public void loadSplitViewIfLandscape(int eventId) {
        ServiceDetailsFragment serviceDetailsFragment = new ServiceDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("eventId", eventId);
        serviceDetailsFragment.setArguments(bundle);
        replaceFragmentDetails(serviceDetailsFragment, false);
        findViewById(R.id.details_container).setVisibility(View.VISIBLE);
    }
}
