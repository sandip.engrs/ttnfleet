package com.ttnfleetsolutions.ttnfleet.ui.fleet.services.note;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.ttnfleetsolutions.ttnfleet.BR;
import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.databinding.FragmentAddNoteFdBinding;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseActivity;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseFragment;
import com.ttnfleetsolutions.ttnfleet.ui.fleet.DashboardActivity;

import javax.inject.Inject;

/**
 * Created by Sandip Chaulagain on 11/22/2017.
 *
 * @version 1.0
 */

public class AddNoteFragment extends BaseFragment<FragmentAddNoteFdBinding, AddNoteViewModel> implements AddNoteNavigator {

    @Inject
    AddNoteViewModel mViewModel;

    FragmentAddNoteFdBinding mFragmentBinding;

    private int mEventId;

    private String mEventNote;

    @Override
    protected AddNoteViewModel getViewModel() {
        return mViewModel;
    }

    @Override
    protected int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_add_note_fd;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModel.setNavigator(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFragmentBinding = getViewDataBinding();
        setUp();
    }

    private void setUp() {
        if (getArguments() != null) {
            mEventId = getArguments().getInt("eventId");
            mEventNote = getArguments().getString("note");

            if(!TextUtils.isEmpty(mEventNote)) {
                mFragmentBinding.noteEditText.setText(mEventNote);
                mFragmentBinding.noteEditText.setSelection(mEventNote.length());
                if (!((BaseActivity) getActivity()).isLandscape()) {
                    ((DashboardActivity) getActivity()).mToolbar.setTitle(getString(R.string.edit_note));
                } else {
                    ((TextView) getActivity().findViewById(R.id.subTitle)).setText(getString(R.string.edit_note));
                }
            } else {
                if (!((BaseActivity) getActivity()).isLandscape()) {
                    ((DashboardActivity) getActivity()).mToolbar.setTitle(getString(R.string.add_note));
                } else {
                    ((TextView) getActivity().findViewById(R.id.subTitle)).setText(getString(R.string.add_note));
                }
            }
        }
    }

    @Override
    public void handleError(Throwable throwable) {

    }

    public void actionDone() {
        if (!TextUtils.isEmpty(mFragmentBinding.noteEditText.getText().toString()))
            ((DashboardActivity) getActivity()).getViewModel().addNoteAPICall(mEventId, mFragmentBinding.noteEditText.getText().toString());
        else Toast.makeText(getActivity(), getString(R.string.invalid_note), Toast.LENGTH_SHORT).show();
    }
}

