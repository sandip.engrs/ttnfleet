package com.ttnfleetsolutions.ttnfleet.ui.serviceprovider.technician;

import com.ttnfleetsolutions.ttnfleet.data.model.api.event.status.AcceptWorkOrderResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.technician.Technician;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 1/10/2018.
 *
 * @version 1.0
 */

interface AllocateTechnicianNavigator {

    void getTechnicianList(List<Technician> technicianList);

    void handleError(Throwable throwable);

    void handleErrorOnAccept(Throwable throwable);

    void handleErrorOnAcceptAssign(Throwable throwable);

    void onAcceptWorkOrders(AcceptWorkOrderResponse acceptWorkOrderResponse);

}
