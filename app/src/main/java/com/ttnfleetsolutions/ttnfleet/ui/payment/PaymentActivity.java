package com.ttnfleetsolutions.ttnfleet.ui.payment;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.ttnfleetsolutions.ttnfleet.R;

public class PaymentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
    }
}
