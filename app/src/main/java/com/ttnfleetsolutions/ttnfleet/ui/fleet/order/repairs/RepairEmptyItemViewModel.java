package com.ttnfleetsolutions.ttnfleet.ui.fleet.order.repairs;

/**
 * Created by Sandip Chaulagain on 11/10/2017.
 *
 * @version 1.0
 */
class RepairEmptyItemViewModel {

    private RepairEmptyItemViewModelListener mListener;

    public RepairEmptyItemViewModel(RepairEmptyItemViewModelListener listener) {
        this.mListener = listener;
    }

    public void onRetryClick() {
        mListener.onRetryClick();
    }

    public interface RepairEmptyItemViewModelListener {
        void onRetryClick();
    }

}
