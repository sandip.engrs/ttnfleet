package com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.locationtypes;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.LocationType;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */

public class LocationTypeAdapter extends RecyclerView.Adapter<LocationTypeAdapter.MyViewHolder> {

    private List<LocationType> mLocationTypeList;
    private View.OnClickListener mOnClickListener;
    private int mColor;

    public LocationTypeAdapter(List<LocationType> locationTypeList, View.OnClickListener onClickListener, int color) {
        this.mLocationTypeList = locationTypeList;
        this.mOnClickListener = onClickListener;
        this.mColor = color;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_location_type, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.locationType.setText(mLocationTypeList.get(position).getTitle());
        holder.locationType.setTag(position);
        holder.locationType.setOnClickListener(mOnClickListener);

//        holder.locationType.setButtonTintList(ColorStateList.valueOf(mColor));
    }

    @Override
    public int getItemCount() {
        return mLocationTypeList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public RadioButton locationType;

        public MyViewHolder(View view) {
            super(view);
            locationType = view.findViewById(R.id.locationType);
        }
    }
}
