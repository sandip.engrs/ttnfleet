package com.ttnfleetsolutions.ttnfleet.ui.technician.jobroster;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseActivity;
import com.ttnfleetsolutions.ttnfleet.ui.serviceprovider.RejectWithReasonDialogFragment;
import com.ttnfleetsolutions.ttnfleet.ui.technician.TechnicianDashboardActivity;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */

public class JobRosterListAdapter extends RecyclerView.Adapter<JobRosterListAdapter.MyViewHolder> {

    private Context mContext;
    private String[] dates, numbers, issues, locations, models, vin;

    Integer selectedItemPosition = null;

    public JobRosterListAdapter(Context context) {
        this.mContext = context;
        dates = context.getResources().getStringArray(R.array.times);
        numbers = context.getResources().getStringArray(R.array.jobs);
        issues = context.getResources().getStringArray(R.array.services_issue_details);
        locations = mContext.getResources().getStringArray(R.array.locations);
        models = mContext.getResources().getStringArray(R.array.truck_array);
        vin = mContext.getResources().getStringArray(R.array.truck_number);
        if (((TechnicianDashboardActivity) (mContext)).isLandscape()) {
            selectedItemPosition = 0;
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_job_request_new, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        if (selectedItemPosition != null && selectedItemPosition == position) {
            // Here I am just highlighting the background
            holder.rowLayout.setBackgroundColor(mContext.getResources().getColor(R.color.tr_gray));
        } else {
            holder.rowLayout.setBackgroundColor(Color.TRANSPARENT);
        }

        holder.serviceIssue.setText(Html.fromHtml(issues[position]));
        holder.jobCardNo.setText(numbers[position]);

        if (position < 1) {
            holder.coloredLine.setBackgroundColor(ContextCompat.getColor(mContext, android.R.color.holo_orange_dark));
            Drawable img = mContext.getResources().getDrawable(R.drawable.circle2);
            img.setTint(ContextCompat.getColor(mContext, android.R.color.holo_orange_dark));
            holder.status.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
            holder.status.setTextColor(ContextCompat.getColor(mContext, android.R.color.holo_orange_dark));
            holder.status.setText(R.string.new_job);
            holder.jobCardNo.setBackgroundTintList(mContext.getResources().getColorStateList(android.R.color.holo_orange_dark));
            holder.arrivalRepairTimingInfo.setVisibility(View.GONE);
            holder.datetime.setText("Just now");

            holder.rowLayout.setOnClickListener(view -> {
                if (selectedItemPosition != null) {
                    notifyItemChanged(selectedItemPosition);
                }
                selectedItemPosition = position;
                notifyItemChanged(selectedItemPosition);
                ((TechnicianDashboardActivity) mContext).showOrderDetails(2);
            });

            holder.serviceType.setVisibility(View.VISIBLE);
            holder.serviceTypeDetails.setVisibility(View.VISIBLE);

            holder.mAcceptButton.setVisibility(View.VISIBLE);
            holder.mRejectButton.setVisibility(View.VISIBLE);
            holder.mAdditionalJobButton.setVisibility(View.GONE);
            holder.mCompleteJobButton.setVisibility(View.GONE);
            holder.mConfirmBreakdownButton.setVisibility(View.GONE);
            holder.mEnrouteButton.setVisibility(View.GONE);

//            holder.serviceIssue.setText(Html.fromHtml(mContext.getString(R.string.dummy_service)));
        } else {
            holder.coloredLine.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
            Drawable img = mContext.getResources().getDrawable(R.drawable.circle);
            img.setTint(ContextCompat.getColor(mContext, R.color.colorPrimary));
            holder.status.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
            holder.status.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
            holder.jobCardNo.setBackgroundTintList(mContext.getResources().getColorStateList(R.color.colorPrimary));

            holder.serviceType.setVisibility(View.GONE);
            holder.serviceTypeDetails.setVisibility(View.GONE);
            holder.arrivalRepairTimingInfo.setVisibility(View.VISIBLE);

            holder.status.setText(R.string.ongoing);
            holder.datetime.setText("40 mins ago");

            if (position == 3) {
                holder.datetime.setText("2 hours ago");
                holder.mAdditionalJobButton.setVisibility(View.VISIBLE);
                holder.mCompleteJobButton.setVisibility(View.VISIBLE);

                holder.mAcceptButton.setVisibility(View.GONE);
                holder.mRejectButton.setVisibility(View.GONE);
                holder.mConfirmBreakdownButton.setVisibility(View.GONE);
                holder.mEnrouteButton.setVisibility(View.GONE);
                holder.arrivalRepairTimingInfo.setText("ATA: 17:10  |  ETC: 1 hour");
                holder.rowLayout.setOnClickListener(view -> {
                    if (selectedItemPosition != null) {
                        notifyItemChanged(selectedItemPosition);
                    }
                    selectedItemPosition = position;
                    notifyItemChanged(selectedItemPosition);
                    ((TechnicianDashboardActivity) mContext).showOrderDetails(1);
                });

            } else if (position == 2) {
                holder.datetime.setText("45 mins ago");
                holder.mConfirmBreakdownButton.setVisibility(View.VISIBLE);
                holder.mAdditionalJobButton.setVisibility(View.GONE);
                holder.mCompleteJobButton.setVisibility(View.GONE);
                holder.mAcceptButton.setVisibility(View.GONE);
                holder.mRejectButton.setVisibility(View.GONE);
                holder.mEnrouteButton.setVisibility(View.GONE);

                holder.arrivalRepairTimingInfo.setText("ATA: 17:10  |  ETC: 1 hour");

                holder.rowLayout.setOnClickListener(view -> {
                    if (selectedItemPosition != null) {
                        notifyItemChanged(selectedItemPosition);
                    }
                    selectedItemPosition = position;
                    notifyItemChanged(selectedItemPosition);
                    ((TechnicianDashboardActivity) mContext).showOrderDetails(1);
                });

            } else {
                holder.arrivalRepairTimingInfo.setText("ETA: 17:10  |  ETC: 1 hour");
                holder.datetime.setText("20 mins ago");
                holder.mEnrouteButton.setVisibility(View.VISIBLE);
                holder.mConfirmBreakdownButton.setVisibility(View.GONE);
                holder.mAdditionalJobButton.setVisibility(View.GONE);
                holder.mCompleteJobButton.setVisibility(View.GONE);
                holder.mAcceptButton.setVisibility(View.GONE);
                holder.mRejectButton.setVisibility(View.GONE);

                holder.rowLayout.setOnClickListener(view -> {
                    if (selectedItemPosition != null) {
                        notifyItemChanged(selectedItemPosition);
                    }
                    selectedItemPosition = position;
                    notifyItemChanged(selectedItemPosition);
                    ((TechnicianDashboardActivity) mContext).showOrderDetails(1);
                });
            }
        }
        holder.mAdditionalJobButton.setOnClickListener(view -> ((TechnicianDashboardActivity) mContext).additionalJobs(0));

        holder.mCompleteJobButton.setOnClickListener(view -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
            builder.setMessage("You need to verify details on call (+1 248-481-1201) before you complete the job.");
            builder.setNegativeButton("Cancel", (dialogInterface, i) -> dialogInterface.dismiss());
            builder.setPositiveButton("Make a call", (dialogInterface, i) -> dialogInterface.dismiss());
            builder.show();
        });

        holder.mAcceptButton.setOnClickListener(view -> ((TechnicianDashboardActivity) mContext).showJobCheckList(0));

        holder.mRejectButton.setOnClickListener(view -> {
            RejectWithReasonDialogFragment fragment = new RejectWithReasonDialogFragment();
            Bundle bundle = new Bundle();
            bundle.putBoolean("from-sd", false);
            fragment.setArguments(bundle);
            fragment.show(((TechnicianDashboardActivity) mContext).getSupportFragmentManager(), "RejectWithReason");
        });

        holder.mConfirmBreakdownButton.setOnClickListener(view -> ((TechnicianDashboardActivity) mContext).jobInspections(0,"",""));
        if (((BaseActivity) mContext).isLandscape()) {
            holder.mAcceptButton.setVisibility(View.GONE);
            holder.mRejectButton.setVisibility(View.GONE);
            holder.mConfirmBreakdownButton.setVisibility(View.GONE);
            holder.mEnrouteButton.setVisibility(View.GONE);
            holder.mAdditionalJobButton.setVisibility(View.GONE);
            holder.mCompleteJobButton.setVisibility(View.GONE);
        }
        if (((BaseActivity) mContext).isLandscape()) {
            holder.bottomLayout.setVisibility(View.GONE);
        }

        holder.location.setText(locations[position]);
        holder.vehicleName.setText(models[position]);
        holder.vinNo.setText(vin[position]);
    }

    @Override
    public int getItemCount() {
        return 4;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView status, datetime, serviceIssue, location, vehicleName, vinNo, arrivalRepairTimingInfo, jobCardNo, serviceType, serviceTypeDetails;
        public View coloredLine;
        public ConstraintLayout rowLayout;
        public RelativeLayout bottomLayout;
        public Button mAcceptButton, mRejectButton, mAdditionalJobButton, mCompleteJobButton, mEnrouteButton, mConfirmBreakdownButton;

        public MyViewHolder(View view) {
            super(view);
            rowLayout = view.findViewById(R.id.rowLayout);
            bottomLayout = view.findViewById(R.id.bottomLayout);
            coloredLine = view.findViewById(R.id.coloredLine);
            status = view.findViewById(R.id.status);
            datetime = view.findViewById(R.id.datetime);
            serviceIssue = view.findViewById(R.id.serviceIssue);
            location = view.findViewById(R.id.location);
            vehicleName = view.findViewById(R.id.vehicleName);
            vinNo = view.findViewById(R.id.vinNo);
            arrivalRepairTimingInfo = view.findViewById(R.id.arrivalRepairTimingInfo);
            jobCardNo = view.findViewById(R.id.jobCardNo);

            mAcceptButton = view.findViewById(R.id.acceptButton);
            mRejectButton = view.findViewById(R.id.rejectButton);
            mAdditionalJobButton = view.findViewById(R.id.additionalJob);
            mCompleteJobButton = view.findViewById(R.id.completeJob);
            mEnrouteButton = view.findViewById(R.id.enrouteButton);
            mConfirmBreakdownButton = view.findViewById(R.id.confirmBreakdownReportButton);

            serviceType = view.findViewById(R.id.serviceType);
            serviceTypeDetails = view.findViewById(R.id.serviceTypeDetails);

        }
    }
}
