package com.ttnfleetsolutions.ttnfleet.ui.trucker.history;

import com.ttnfleetsolutions.ttnfleet.data.model.api.event.listing.Event;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 1/10/2018.
 *
 * @version 1.0
 */

public interface ServiceHistoryNavigator {

    void getClosedEvents(List<Event> eventList);

    void handleError(Throwable throwable);
}
