package com.ttnfleetsolutions.ttnfleet.ui.fleet.order.status;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.ttnfleetsolutions.ttnfleet.BR;
import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.databinding.FragmentFdOrderStatusDetailsBinding;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseFragment;
import com.ttnfleetsolutions.ttnfleet.ui.fleet.FDTimeLineAdapter;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.model.OrderStatus;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.model.TimeLineModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by Sandip Chaulagain on 11/10/2017.
 *
 * @version 1.0
 */
public class StatusFragment extends BaseFragment<FragmentFdOrderStatusDetailsBinding, StatusViewModel> implements StatusNavigator {

    @Inject
    StatusViewModel mStatusViewModel;

    private FragmentFdOrderStatusDetailsBinding mFragmentStatusBinding;

    @Inject
    LinearLayoutManager mLayoutManager;

    @Inject
    FDTimeLineAdapter mTimeLineAdapter;

    private List<TimeLineModel> mDataList = new ArrayList<>();


    public static StatusFragment newInstance() {
        Bundle args = new Bundle();
        StatusFragment fragment = new StatusFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mStatusViewModel.setNavigator(this);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFragmentStatusBinding = getViewDataBinding();

        setUp();
    }

    /*@Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && mFragmentStatusBinding != null && mFragmentStatusBinding.statusRecyclerView != null && mFragmentStatusBinding.statusRecyclerView.getAdapter() == null) {
            setUp();
        }
    }*/

    @Override
    public StatusViewModel getViewModel() {
        return mStatusViewModel;
    }


    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_fd_order_status_details;
    }

    private void setUp() {
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mFragmentStatusBinding.statusRecyclerView.setLayoutManager(mLayoutManager);
        mFragmentStatusBinding.statusRecyclerView.setAdapter(mTimeLineAdapter);
        setDataListItems();
    }

    private void setDataListItems() {
        for (int i = 0; i < 6; i++) {
            mDataList.add(new TimeLineModel(getResources().getStringArray(R.array.fleet_dispatcher_order_timeline)[i], getResources().getStringArray(R.array.fleet_dispatcher_order_time)[i], OrderStatus.COMPLETED));
        }

       /* mDataList.get(0).setStatus(OrderStatus.COMPLETED);
        mDataList.get(1).setStatus(OrderStatus.COMPLETED);
        mDataList.get(2).setStatus(OrderStatus.COMPLETED);
        mDataList.get(3).setStatus(OrderStatus.COMPLETED);
        mDataList.get(4).setStatus(OrderStatus.COMPLETED);*/
        mDataList.get(5).setStatus(OrderStatus.ACTIVE);
/*        mDataList.get(6).setStatus(OrderStatus.INACTIVE);
        mDataList.get(7).setStatus(OrderStatus.INACTIVE);
        mDataList.get(8).setStatus(OrderStatus.INACTIVE);*/
        /*mDataList.get(9).setStatus(OrderStatus.COMPLETED);
        mDataList.get(10).setStatus(OrderStatus.COMPLETED);
        mDataList.get(11).setStatus(OrderStatus.ACTIVE);*/
        mTimeLineAdapter.addItems(mDataList);
    }
}
