package com.ttnfleetsolutions.ttnfleet.ui.trucker.servicerequest;

import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.servicerequest.jobs.JobsRecyclerViewAdapter;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.servicerequest.requirements.RequirementImageRecyclerViewAdapter;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.servicerequest.workorder.WorkOrderRecyclerViewAdapter;
import com.ttnfleetsolutions.ttnfleet.utils.rx.SchedulerProvider;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Sandip Chaulagain on 2/2/2018.
 *
 * @version 1.0
 */
@Module
public class ServiceDetailsFragmentModule {

    @Provides
    ServiceDetailsViewModel provideServiceDetailsViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        return new ServiceDetailsViewModel(dataManager, schedulerProvider);
    }


    @Provides
    WorkOrderRecyclerViewAdapter provideWorkOrderRecyclerViewAdapter() {
        return new WorkOrderRecyclerViewAdapter(new ArrayList<>());
    }

    @Provides
    JobsRecyclerViewAdapter provideJobsRecyclerViewAdapter() {
        return new JobsRecyclerViewAdapter(new ArrayList<>());
    }

    @Provides
    RequirementImageRecyclerViewAdapter provideRequirementImageRecyclerViewAdapter() {
        return new RequirementImageRecyclerViewAdapter(new ArrayList<>());
    }

}
