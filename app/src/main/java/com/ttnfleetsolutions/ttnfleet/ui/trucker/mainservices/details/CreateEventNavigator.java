package com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.details;

import com.ttnfleetsolutions.ttnfleet.data.model.api.asset.Vehicle;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.CreateEventResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.SystemCode;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.TruckLoadResponse;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 1/10/2018.
 *
 * @version 1.0
 */

public interface CreateEventNavigator {

    void setCodeList(List<SystemCode> codeList);

    void onEventCreation(CreateEventResponse eventResponse);

    void setTruckLoadList(List<TruckLoadResponse.TruckLoad> truckLoadList);

    void setAssetList(List<Vehicle> vehicleList);

    void handleError(Throwable throwable);

    void notifyAdapter();

    void loadDetails(int assetId, String address, String RTA, String RTC, int loadId, String comments);

    void loadLocationDetails(String address);

    void handleErrorAssetList(Throwable throwable);

    void handleErrorTruckLoadTypes(Throwable throwable);
}
