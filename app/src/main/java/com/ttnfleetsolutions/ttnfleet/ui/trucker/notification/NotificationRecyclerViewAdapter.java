package com.ttnfleetsolutions.ttnfleet.ui.trucker.notification;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.ttnfleetsolutions.ttnfleet.data.model.NotificationModel;
import com.ttnfleetsolutions.ttnfleet.databinding.RowNotificationDriverBinding;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseViewHolder;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */

public class NotificationRecyclerViewAdapter  extends RecyclerView.Adapter<BaseViewHolder>  {

    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private List<NotificationModel> mList;

    private NotificationItemViewModel.NotificationItemViewModelListener mListener;

    public void setListener(NotificationItemViewModel.NotificationItemViewModelListener listener) {
        this.mListener = listener;
    }

    public NotificationRecyclerViewAdapter(List<NotificationModel> list) {
        this.mList = list;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RowNotificationDriverBinding rowNotificationDriverBinding = RowNotificationDriverBinding.inflate(LayoutInflater.from(parent.getContext()),
                parent, false);
        return new MyViewHolder(rowNotificationDriverBinding);
    }

    @Override
    public int getItemViewType(int position) {
        if (mList != null && mList.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public int getItemCount() {
        if (mList != null && mList.size() > 0) {
            return mList.size();
        } else {
            return 0;
        }
    }

    public void addItems(List<NotificationModel> list) {
        mList = list;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends BaseViewHolder implements NotificationItemViewModel.NotificationItemViewModelListener {

        private RowNotificationDriverBinding mBinding;

        private NotificationItemViewModel mNotificationItemViewModel;

        public MyViewHolder(RowNotificationDriverBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {

            final NotificationModel notificationModel = mList.get(position);

            mNotificationItemViewModel = new NotificationItemViewModel(notificationModel, this);

            mBinding.setViewModel(mNotificationItemViewModel);

            // Immediate Binding
            // When a variable or observable changes, the binding will be scheduled to change before
            // the next frame. There are times, however, when binding must be executed immediately.
            // To force execution, use the executePendingBindings() method.
            mBinding.executePendingBindings();

        }

        @Override
        public void onItemClick(long id) {
            mListener.onItemClick(id);
        }

        @Override
        public void onItemClickActionPositive(String action) {
            mListener.onItemClickActionPositive(action);
        }

        @Override
        public void onItemClickActionNegative(String action) {
            mListener.onItemClickActionNegative(action);
        }
    }
}

