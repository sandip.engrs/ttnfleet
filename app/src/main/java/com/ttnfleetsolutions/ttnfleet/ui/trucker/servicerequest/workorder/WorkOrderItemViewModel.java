package com.ttnfleetsolutions.ttnfleet.ui.trucker.servicerequest.workorder;

import android.databinding.ObservableField;

import com.ttnfleetsolutions.ttnfleet.data.model.api.event.listing.WorkOrder;

/**
 * Created by Sandip Chaulagain on 1/19/2018.
 *
 * @version 1.0
 */

public class WorkOrderItemViewModel {

    private WorkOrder mWorkOrder;
    public ObservableField<String> systemCode, assemblyCode;
    private ItemViewModelListener mListener;

    WorkOrderItemViewModel(WorkOrder workOrder, ItemViewModelListener listener) {
        this.mWorkOrder = workOrder;
        if (workOrder.getServiceCode() != null)
            systemCode = new ObservableField<>(workOrder.getServiceCode().getDisplaytitle());
        this.mListener = listener;
    }

    public void onItemClick() {
        mListener.onItemClick(mWorkOrder.getWorkorderid());
    }

    public interface ItemViewModelListener {
        void onItemClick(int id);
    }
}
