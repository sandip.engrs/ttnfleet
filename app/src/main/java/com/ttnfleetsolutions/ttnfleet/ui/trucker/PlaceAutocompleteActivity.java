package com.ttnfleetsolutions.ttnfleet.ui.trucker;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.ttnfleetsolutions.ttnfleet.R;

/**
 * Created by Sandip Chaulagain on 11/27/2017.
 *
 * @version 1.0
 */

public class PlaceAutocompleteActivity extends AppCompatActivity {

    private String TAG = PlaceAutocompleteActivity.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        if(getResources().getBoolean(R.bool.portrait_mode)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_autocomplete);

        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                Log.i(TAG, "Place: " + place.getName());
                Intent intent = getIntent();
                intent.putExtra("name", place.getName() + " " + place.getAddress());
                intent.putExtra("latlng", place.getLatLng());
                setResult(RESULT_OK, intent);
                finish();
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i(TAG, "An error occurred: " + status);
                Intent intent = getIntent();
                intent.putExtra("status", status);
                setResult(PlaceAutocomplete.RESULT_ERROR, intent);
                finish();
            }
        });
    }
}
