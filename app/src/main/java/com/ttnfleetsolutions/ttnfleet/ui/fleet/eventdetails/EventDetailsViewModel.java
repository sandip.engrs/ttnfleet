package com.ttnfleetsolutions.ttnfleet.ui.fleet.eventdetails;

import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.databinding.Observable;
import android.databinding.ObservableField;
import android.databinding.PropertyChangeRegistry;
import android.view.View;
import android.widget.TextView;

import com.ttnfleetsolutions.ttnfleet.BR;
import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.listing.Event;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseViewModel;
import com.ttnfleetsolutions.ttnfleet.utils.CommonUtils;
import com.ttnfleetsolutions.ttnfleet.utils.rx.SchedulerProvider;

/**
 * Created by Sandip Chaulagain on 2/2/2018.
 *
 * @version 1.0
 */

public class EventDetailsViewModel extends BaseViewModel<EventDetailsNavigator> implements Observable {

    private PropertyChangeRegistry registry = new PropertyChangeRegistry();

    public ObservableField<String> status = new ObservableField<>();
    public ObservableField<Integer> statusId = new ObservableField<>();
    public ObservableField<String> updatedTime = new ObservableField<>();

    public ObservableField<String> vehicleName = new ObservableField<>();
    public ObservableField<String> vin = new ObservableField<>();
    public ObservableField<String> address = new ObservableField<>();

    public ObservableField<String> eventType = new ObservableField<>();
    public ObservableField<String> breakdownType = new ObservableField<>();
    public ObservableField<String> loadType = new ObservableField<>();
    public ObservableField<String> RTA = new ObservableField<>();
    public ObservableField<String> RTC = new ObservableField<>();

    public EventDetailsViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }


    public Event mEventDetails;

    public Event getEventDetails() {
        return mEventDetails;
    }

    public void setEventDetails(Event mEventDetails) {
        getNavigator().loadEventDetails(mEventDetails);
        setStatus(new ObservableField<>(mEventDetails.getStatus().getTitle()));
        setStatusId(new ObservableField<>(mEventDetails.getStatusid()));
        setUpdatedTime(new ObservableField<>(CommonUtils.getPrettyTime(mEventDetails.getCreateddatetime())));

        setVehicleName(new ObservableField<>(mEventDetails.getVehicle().getVehiclename()));
        setVin(new ObservableField<>(mEventDetails.getVehicle().getVin()));
        setAddress(new ObservableField<>(mEventDetails.getLocation().getAddress()));

        setEventType(new ObservableField<>(mEventDetails.getEventType().getTitle()));
        setBreakdownType(new ObservableField<>(mEventDetails.getBreakdownnote()));
        setLoadType(new ObservableField<>(mEventDetails.getVehicleLoad().getLoadPlans()));
        setRTA(new ObservableField<>(CommonUtils.getFromattedDate(mEventDetails.getRta())));
        setRTC(new ObservableField<>(CommonUtils.getFromattedDate(mEventDetails.getRtc())));

        notifyAll();
        this.mEventDetails = mEventDetails;
    }

    @Bindable
    public ObservableField<String> getStatus() {
        return status;
    }

    public void setStatus(ObservableField<String> status) {
        this.status = status;
        registry.notifyChange(this, BR.status);
    }

    @Bindable
    public ObservableField<Integer> getStatusId() {
        return statusId;
    }

    public void setStatusId(ObservableField<Integer> statusId) {
        this.statusId = statusId;
        registry.notifyChange(this, BR.statusId);
    }

    @Bindable
    public ObservableField<String> getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(ObservableField<String> updatedTime) {
        this.updatedTime = updatedTime;
        registry.notifyChange(this, BR.updatedTime);
    }

    @Bindable
    public ObservableField<String> getEventType() {
        return eventType;
    }

    public void setEventType(ObservableField<String> eventType) {
        this.eventType = eventType;
        registry.notifyChange(this, BR.eventType);
    }

    @Bindable
    public ObservableField<String> getBreakdownType() {
        return breakdownType;
    }

    public void setBreakdownType(ObservableField<String> breakdownType) {
        this.breakdownType = breakdownType;
        registry.notifyChange(this, BR.breakdownType);
    }

    @Bindable
    public ObservableField<String> getLoadType() {
        return loadType;
    }

    public void setLoadType(ObservableField<String> loadType) {
        this.loadType = loadType;
        registry.notifyChange(this, BR.loadType);
    }

    @Bindable
    public ObservableField<String> getVehicleName() {
        return vehicleName;
    }

    public void setVehicleName(ObservableField<String> vehicleName) {
        this.vehicleName = vehicleName;
        registry.notifyChange(this, BR.vehicleName);
    }

    @Bindable
    public ObservableField<String> getVin() {
        return vin;
    }

    public void setVin(ObservableField<String> vin) {
        this.vin = vin;
        registry.notifyChange(this, BR.vin);
    }

    @Bindable
    public ObservableField<String> getAddress() {
        return address;
    }

    public void setAddress(ObservableField<String> address) {
        this.address = address;
        registry.notifyChange(this, BR.address);
    }

    @Bindable
    public ObservableField<String> getRTA() {
        return RTA;
    }

    public void setRTA(ObservableField<String> RTA) {
        this.RTA = RTA;
        registry.notifyChange(this, BR.rTA);
    }

    @Bindable
    public ObservableField<String> getRTC() {
        return RTC;
    }

    public void setRTC(ObservableField<String> RTC) {
        this.RTC = RTC;
        registry.notifyChange(this, BR.rTC);
    }

    @BindingAdapter({"statusBackgroundColor"})
    public static void statusBackgroundColor(View view, int status) {
        TextView textView = (TextView) view;
        switch (status) {
            case 3:
                textView.setBackgroundTintList(view.getContext().getResources().getColorStateList(android.R.color.holo_orange_dark));
                break;
            case 1:
                textView.setBackgroundTintList(view.getContext().getResources().getColorStateList(android.R.color.holo_green_dark));
                break;
            case 2:
                textView.setBackgroundTintList(view.getContext().getResources().getColorStateList(android.R.color.holo_blue_dark));
                break;
            default:
                textView.setBackgroundTintList(view.getContext().getResources().getColorStateList(android.R.color.holo_orange_dark));
                break;
        }
    }

    public void getEventDetailsAPICall(int eventId) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .doServerGetEventDetailsApiCall(eventId)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    setEventDetails(response.getResult());

                    setIsLoading(false);
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                }));
    }


    @Override
    public void addOnPropertyChangedCallback(OnPropertyChangedCallback callback) {
        registry.add(callback);
    }

    @Override
    public void removeOnPropertyChangedCallback(OnPropertyChangedCallback callback) {
        registry.remove(callback);
    }
}
