package com.ttnfleetsolutions.ttnfleet.ui.trucker.servicerequest.timeline;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.github.vipulasri.timelineview.TimelineView;
import com.ttnfleetsolutions.ttnfleet.databinding.RowTimelineBinding;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseViewHolder;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.model.TimeLineModel;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */

public class TimelineAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private List<TimeLineModel> mList;

    private TimelineItemViewModel.TimelineItemViewModelListener mListener;

    public void setListener(TimelineItemViewModel.TimelineItemViewModelListener listener) {
        this.mListener = listener;
    }

    public TimelineAdapter(List<TimeLineModel> list) {
        this.mList = list;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RowTimelineBinding rowTimelineBinding = RowTimelineBinding.inflate(LayoutInflater.from(parent.getContext()),
                parent, false);
        rowTimelineBinding.timeMarker.initLine(viewType);
        return new MyViewHolder(rowTimelineBinding);
    }
    @Override
    public int getItemViewType(int position) {
        return TimelineView.getTimeLineViewType(position, getItemCount());
    }

    /*@Override
    public int getItemViewType(int position) {
        if (mList != null && mList.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }*/

    @Override
    public int getItemCount() {
        if (mList != null && mList.size() > 0) {
            return mList.size();
        } else {
            return 0;
        }
    }

    public void addItems(List<TimeLineModel> list) {
        mList = list;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends BaseViewHolder implements TimelineItemViewModel.TimelineItemViewModelListener {

        private RowTimelineBinding mBinding;

        private TimelineItemViewModel mTimelineItemViewModel;

        public MyViewHolder(RowTimelineBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {

            final TimeLineModel timeLineModel = mList.get(position);

            mTimelineItemViewModel = new TimelineItemViewModel(timeLineModel, this);

            mBinding.setViewModel(mTimelineItemViewModel);

            // Immediate Binding
            // When a variable or observable changes, the binding will be scheduled to change before
            // the next frame. There are times, however, when binding must be executed immediately.
            // To force execution, use the executePendingBindings() method.
            mBinding.executePendingBindings();

        }

        @Override
        public void onItemClick(String action) {
            mListener.onItemClick(action);
        }
    }
}
