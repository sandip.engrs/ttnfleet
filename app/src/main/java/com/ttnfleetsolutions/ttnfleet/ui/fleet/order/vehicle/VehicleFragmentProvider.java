package com.ttnfleetsolutions.ttnfleet.ui.fleet.order.vehicle;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Sandip Chaulagain on 11/10/2017.
 *
 * @version 1.0
 */
@Module
public abstract class VehicleFragmentProvider {

    @ContributesAndroidInjector(modules = VehicleFragmentModule.class)
    abstract VehicleFragment provideVehicleFragmentFactory();
}
