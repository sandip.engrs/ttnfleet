package com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.vmrs.system;

import com.google.gson.Gson;
import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.AssemblyCode;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.DriverListResponse;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.EventRequest;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.ServiceCode;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.SystemCode;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseViewModel;
import com.ttnfleetsolutions.ttnfleet.utils.rx.SchedulerProvider;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sandip Chaulagain on 11/10/2017.
 *
 * @version 1.0
 */

public class SelectServiceViewModel extends BaseViewModel<SelectServiceNavigator> {


    public SelectServiceViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    private List<ServiceCode> mCodeList = new ArrayList<>();

    public List<ServiceCode> getCodeList() {
        return mCodeList;
    }

    public void setCodeList(List<ServiceCode> mCodeList) {
        this.mCodeList = mCodeList;
    }

    private List<DriverListResponse.Driver> mDriverList = new ArrayList<>();

    public List<DriverListResponse.Driver> getDriverList() {
        return mDriverList;
    }

    public void setDriverList(List<DriverListResponse.Driver> mDriverList) {
        this.mDriverList = mDriverList;
    }

    public void getDriverListing(int companyId) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager().doServerGetDriverListingApiCall(companyId)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    setIsLoading(false);
                    setDriverList(response.getResult());
                    getNavigator().setDriverList(response.getResult());
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleErrorDriverListing(throwable);
                }));
    }


    void getSystemList() {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager().doServerGetSystemListApiCall()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    setIsLoading(false);
                    setCodeList(response.getResult());
                    getNavigator().setSystemList(response.getResult());
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                }));
    }

    public void getAssemblyList(String systemCode) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager().doServerGetAssemblyListApiCall(systemCode)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    setIsLoading(false);
                    setCodeList(response.getResult());
                    getNavigator().setSystemList(response.getResult());
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                }));
    }

    public void getComponentList(String systemCode, String assemblyCode) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager().doServerGetComponentListApiCall(systemCode, assemblyCode)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    setIsLoading(false);
                    setCodeList(response.getResult());
                    getNavigator().setSystemList(response.getResult());
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                }));
    }

    void updateSystemCode(String systemCode) {
        Gson gson = new Gson();
        EventRequest eventRequest = gson.fromJson(getDataManager().getEventRequest(), EventRequest.class);
//        eventRequest.setSystemCode(systemCode);
        getDataManager().setEventRequest(gson.toJson(eventRequest, EventRequest.class));
    }

    public void updateAssemblyCode(String assemblyCode) {
        Gson gson = new Gson();
        EventRequest eventRequest = gson.fromJson(getDataManager().getEventRequest(), EventRequest.class);
//        eventRequest.setAssemblyCode(assemblyCode);
        getDataManager().setEventRequest(gson.toJson(eventRequest, EventRequest.class));
    }

    /*public void saveCode(String systemCode, String assemblyCode, String componentCode, String systemCodeName, String assemblyCodeName, String componentCodeName) {
        Gson gson = new Gson();
        EventRequest eventRequest = gson.fromJson(getDataManager().getEventRequest(), EventRequest.class);
        if(eventRequest.getVmrsCode() == null) eventRequest.setVmrsCode(new VMRSCode());
        SystemCode systemCodeObj = new SystemCode();
        systemCodeObj.setCode1(systemCode);
        systemCodeObj.setSystemCodeName(systemCodeName);
        AssemblyCode assemblyCodeObj = new AssemblyCode();
        systemCodeObj.setAssemblyCodeList(assemblyCodeObj);



       *//* VMRSCode code = new VMRSCode(systemCode, assemblyCode, componentCode, systemCodeName, assemblyCodeName, componentCodeName);
        if (eventRequest.getVmrsCodeList() == null) eventRequest.setVmrsCodeList(new ArrayList<>());
        eventRequest.getVmrsCodeList().add(code);*//*
        getDataManager().setEventRequest(gson.toJson(eventRequest, EventRequest.class));
    }*/

    public void onNextButtonClick() {
        getNavigator().onNext();
    }

    /*public void saveCode(String mSystemCode, String mSystemCodeName, List<AssemblyCode> assemblyCodeList) {
        Gson gson = new Gson();
        EventRequest eventRequest = gson.fromJson(getDataManager().getEventRequest(), EventRequest.class);
        SystemCode systemCodeObj = new SystemCode(mSystemCode, mSystemCodeName, assemblyCodeList);
        if(eventRequest.getSystemCodeList() == null) eventRequest.setSystemCodeList(new ArrayList<>());
        eventRequest.getSystemCodeList().add(systemCodeObj);
        getDataManager().setEventRequest(gson.toJson(eventRequest, EventRequest.class));
    }*/

    public void saveCode(String mSystemCode, String mSystemCodeName, String mAssemblyCode, String mAssemblyCodeName, String componentCodeArray, List<String> componentCodeNameArray) {
        Gson gson = new Gson();
        EventRequest eventRequest = gson.fromJson(getDataManager().getEventRequest(), EventRequest.class);

        AssemblyCode assemblyCodeObj;
        SystemCode systemCodeObj;

        //check if system code exist
        int position = eventRequest.checkIfSystemCodeExists(mSystemCode);
        if (position == -1) {
            if (eventRequest.getSystemCodeList() == null)
                eventRequest.setSystemCodeList(new ArrayList<>());
            systemCodeObj = new SystemCode(mSystemCode, mSystemCodeName);

            if (systemCodeObj.getAssemblyCodeList() == null)
                systemCodeObj.setAssemblyCodeList(new ArrayList<>());

            assemblyCodeObj = new AssemblyCode(mAssemblyCode, componentCodeArray, mAssemblyCodeName, componentCodeNameArray);

            systemCodeObj.getAssemblyCodeList().add(assemblyCodeObj);
            eventRequest.getSystemCodeList().add(systemCodeObj);
        } else {
            systemCodeObj = eventRequest.getSystemCodeList().get(position);

            if (systemCodeObj == null) systemCodeObj = new SystemCode(mSystemCode, mSystemCodeName);

            //check if assembly code exist
            int positionAssembly = systemCodeObj.checkIfAssemblyCodeExists(mAssemblyCode);
            if (positionAssembly == -1) {
                assemblyCodeObj = new AssemblyCode(mAssemblyCode, componentCodeArray, mAssemblyCodeName, componentCodeNameArray);
                if (systemCodeObj.getAssemblyCodeList() == null)
                    systemCodeObj.setAssemblyCodeList(new ArrayList<>());
                systemCodeObj.getAssemblyCodeList().add(assemblyCodeObj);
            } else {
                // merge component codes
                assemblyCodeObj = systemCodeObj.getAssemblyCodeList().get(positionAssembly);
                String[] newComponentCodes = componentCodeArray.split(",");
                StringBuilder componentCodeStr = new StringBuilder(assemblyCodeObj.getComponentCode());
                String prefix = ",";

                for (int i = 0; i < newComponentCodes.length; i++) {
                    if (!assemblyCodeObj.getComponentCode().contains(newComponentCodes[i])) {
                        //append code
                        componentCodeStr.append(prefix);
                        componentCodeStr.append(newComponentCodes[i]);
                        //add title
                        assemblyCodeObj.getComponentCodeName().add(componentCodeNameArray.get(i));
                    }
                }
                assemblyCodeObj.setComponentCode(componentCodeStr.toString());
                systemCodeObj.getAssemblyCodeList().set(positionAssembly, assemblyCodeObj);
            }
            eventRequest.getSystemCodeList().set(position, systemCodeObj);
        }
        getDataManager().setEventRequest(gson.toJson(eventRequest, EventRequest.class));
    }

    public void updateDriverId(int driverId) {
        Gson gson = new Gson();
        EventRequest eventRequest = new EventRequest();
        eventRequest.setUserId(driverId);
        eventRequest.setCreatedby(getDataManager().getCurrentUserID());
        getDataManager().setEventRequest(gson.toJson(eventRequest, EventRequest.class));
    }
}
