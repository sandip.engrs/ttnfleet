package com.ttnfleetsolutions.ttnfleet.ui.fleet;

import android.util.Log;

import com.google.gson.Gson;
import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.AddNoteRequest;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.EventRequest;
import com.ttnfleetsolutions.ttnfleet.data.model.api.location.Location;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseViewModel;
import com.ttnfleetsolutions.ttnfleet.utils.rx.SchedulerProvider;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Sandip Chaulagain on 12/22/2017.
 *
 * @version 1.0
 */

public class DashboardViewModel extends BaseViewModel<DashboardNavigator> {

    public DashboardViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    public boolean isReloadNeeded = false;

    void updateLocation(Location location) {
        Gson gson = new Gson();
        EventRequest eventRequest = gson.fromJson(getDataManager().getEventRequest(), EventRequest.class);
        eventRequest.setLocationid(location.getLocationid());
        eventRequest.setLocationTypeId(location.getLocationtypeid());
        eventRequest.setAddress(location.getAddress());
        eventRequest.setLatitude(location.getLatitude());
        eventRequest.setLongitude(location.getLongitude());
        getDataManager().setEventRequest(gson.toJson(eventRequest, EventRequest.class));
    }

    void createNewEventRequest() {
        Gson gson = new Gson();
        getDataManager().setEventRequest(gson.toJson(new EventRequest(), EventRequest.class));
    }

    private JSONObject getAddNoteRequest(int eventId, String note) {
        AddNoteRequest addNoteRequest = new AddNoteRequest();
        addNoteRequest.setEventId(eventId);
        addNoteRequest.setFleetDispacterId(getDataManager().getCurrentUserID());
        addNoteRequest.setFleetDispatcherNote(note);
        try {
            JSONObject jsonObject = new JSONObject(new Gson().toJson(addNoteRequest, AddNoteRequest.class));
            JSONObject dataObject = new JSONObject();
            dataObject.put("data", jsonObject);
            Log.e(AddNoteRequest.class.getSimpleName(), "Json -- " + dataObject.toString());
            return dataObject;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void addNoteAPICall(int eventId, String note) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .doServerAddNoteFDApiCall(getAddNoteRequest(eventId, note))
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    setIsLoading(false);
                    getNavigator().onAddedNote(response);
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                }));
    }
}

