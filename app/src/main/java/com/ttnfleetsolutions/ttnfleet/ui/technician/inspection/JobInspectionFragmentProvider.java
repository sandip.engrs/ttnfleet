package com.ttnfleetsolutions.ttnfleet.ui.technician.inspection;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Sandip Chaulagain on 6/22/2018.
 *
 * @version 1.0
 */
@Module
public abstract class JobInspectionFragmentProvider {

    @ContributesAndroidInjector(modules = JobInspectionFragmentModule.class)
    abstract JobInspectionTechFragment provideJobInspectionTechFragmentFactory();
}
