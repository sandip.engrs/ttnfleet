package com.ttnfleetsolutions.ttnfleet.ui.trucker;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.ui.fleet.DashboardActivity;

/**
 * Created by Sandip Chaulagain on 10/12/2017.
 *
 * @version 1.0
 */

public class BottomSheetDialogFragmentEmergencyRoadSide extends BottomSheetDialogFragment {

    private int mFrom = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_fragment_emergency_roadside, container, false);

        if (getArguments() != null) {
            mFrom = getArguments().getInt("from");
        }

        if(mFrom == 0) {
            rootView.findViewById(R.id.radioButton4).setVisibility(View.VISIBLE);
        } else {
            rootView.findViewById(R.id.radioButton5).setVisibility(View.VISIBLE);
        }
        ((RadioGroup) rootView.findViewById(R.id.radioGroup)).setOnCheckedChangeListener((radioGroup, i) -> {
            dismiss();
            if (getActivity() instanceof MainActivityNew) {
                ((MainActivityNew) getActivity()).showSystemListing();
            } else if (getActivity() instanceof DashboardActivity) {
                ((DashboardActivity) getActivity()).showIssueList(mFrom, ((RadioButton) rootView.findViewById(radioGroup.getCheckedRadioButtonId())).getText().toString());
            }
        });
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().setOnShowListener(dialog -> {
            BottomSheetDialog d = (BottomSheetDialog) dialog;
            FrameLayout bottomSheet = d.findViewById(R.id.design_bottom_sheet);
            CoordinatorLayout coordinatorLayout = (CoordinatorLayout) bottomSheet.getParent();
            BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
            bottomSheetBehavior.setPeekHeight(bottomSheet.getHeight());
            coordinatorLayout.getParent().requestLayout();
        });
    }
}
