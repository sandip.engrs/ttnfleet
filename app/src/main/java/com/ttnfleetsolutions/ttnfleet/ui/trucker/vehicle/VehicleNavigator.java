package com.ttnfleetsolutions.ttnfleet.ui.trucker.vehicle;

import android.arch.lifecycle.LiveData;

import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.Vehicle;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 1/10/2018.
 *
 * @version 1.0
 */

interface VehicleNavigator {

    void updateVehicles(LiveData<List<Vehicle>> listLiveData);

    void handleError(Throwable throwable);
}
