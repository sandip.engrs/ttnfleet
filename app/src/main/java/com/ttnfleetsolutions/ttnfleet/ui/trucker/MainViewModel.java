package com.ttnfleetsolutions.ttnfleet.ui.trucker;

import com.google.gson.Gson;
import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.Location;
import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.ServiceRequest;
import com.ttnfleetsolutions.ttnfleet.data.local.db.entity.Vehicle;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.EventRequest;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseViewModel;
import com.ttnfleetsolutions.ttnfleet.utils.rx.SchedulerProvider;

/**
 * Created by Sandip Chaulagain on 12/22/2017.
 *
 * @version 1.0
 */

public class MainViewModel extends BaseViewModel {

    public MainViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    public void insertVehicle(Vehicle vehicle) {
        getCompositeDisposable().add(getDataManager().insertVehicle(vehicle)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(aBoolean -> {
                }));
    }

    public void insertServiceRequest(ServiceRequest request) {
        getCompositeDisposable().add(getDataManager().insertServiceRequest(request)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(aBoolean -> {
                }));
    }

    void insertLocation(Location location) {
        getCompositeDisposable().add(getDataManager().insertLocation(location)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(aBoolean -> {
                }));
    }

    void updateLocation(com.ttnfleetsolutions.ttnfleet.data.model.api.location.Location location) {
        Gson gson = new Gson();
        EventRequest eventRequest = gson.fromJson(getDataManager().getEventRequest(), EventRequest.class);
        eventRequest.setLocationid(location.getLocationid());
        eventRequest.setLocationTypeId(location.getLocationtypeid());
        eventRequest.setAlwayslive(location.getLocationType().getAlwayslive());
        eventRequest.setAddress(location.getAddress());
        eventRequest.setLatitude(location.getLatitude());
        eventRequest.setLongitude(location.getLongitude());
        getDataManager().setEventRequest(gson.toJson(eventRequest, EventRequest.class));
    }

    void createNewEventRequest() {
        Gson gson = new Gson();
        getDataManager().setEventRequest(gson.toJson(new EventRequest(), EventRequest.class));
    }
}

