package com.ttnfleetsolutions.ttnfleet.ui.trucker.model;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */
public enum Orientation {

    VERTICAL,
    HORIZONTAL

}
