package com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.vmrs.system;

import com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.driver.SelectDriverFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.vmrs.assembly.SelectAssemblyFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.vmrs.component.SelectComponentFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Sandip Chaulagain on 1/22/2018.
 *
 * @version 1.0
 */
@Module
public abstract class SelectServiceFragmentProvider {

    @ContributesAndroidInjector(modules = SelectServiceFragmentModule.class)
    abstract SelectServiceFragment provideSelectServiceFragmentFactory();

    @ContributesAndroidInjector(modules = SelectServiceFragmentModule.class)
    abstract SelectAssemblyFragment provideSelectComponentFragmentFactory();

    @ContributesAndroidInjector(modules = SelectServiceFragmentModule.class)
    abstract SelectComponentFragment provideSelectPositionFragmentFactory();

    @ContributesAndroidInjector(modules = SelectServiceFragmentModule.class)
    abstract SelectDriverFragment provideSelectDriverFragmentFactory();
}
