package com.ttnfleetsolutions.ttnfleet.ui.serviceprovider;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ttnfleetsolutions.ttnfleet.R;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */

public class ServiceEstimationAdapter extends RecyclerView.Adapter<ServiceEstimationAdapter.MyViewHolder> {

    private final String[] times;
    private final String[] desc;

    public ServiceEstimationAdapter(Context context) {
        Context mContext = context;
        String[] tasks = mContext.getResources().getStringArray(R.array.additional_job_task);
        times = mContext.getResources().getStringArray(R.array.additional_job_task_duration);
        desc = mContext.getResources().getStringArray(R.array.additional_job_task_desc);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_service_esitmation, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
//        holder.task.setText(tasks[position]);
//        holder.duration.setText(times[position]);
//        holder.desc.setText(desc[position]);
        switch (position) {
            case 0:
                holder.serviceName.setText("1. A PM - Inspection & Lubrication Dry(142)");
                break;
            case 1:
                holder.serviceName.setText("2. N PM - Sparks Plugs & Wires(186)");
                break;
            case 2:
                holder.serviceName.setText("3. G PM - Gasoline Filter(182)");
                break;
        }
    }

    @Override
    public int getItemCount() {
        return 3;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        //        public final CheckBox task;
        public final TextView serviceName;
//        public final TextView desc;

        public MyViewHolder(View view) {
            super(view);
//            task = view.findViewById(R.id.checkbox2);
//            duration = view.findViewById(R.id.time2);
            serviceName = view.findViewById(R.id.serviceName);
        }
    }
}
