package com.ttnfleetsolutions.ttnfleet.ui.trucker.mainservices.details;

import android.databinding.ObservableField;

import com.ttnfleetsolutions.ttnfleet.data.model.api.event.create.SystemCode;

/**
 * Created by Sandip Chaulagain on 1/19/2018.
 *
 * @version 1.0
 */

public class SelectedCodeItemViewModel {

    private SystemCode mCode;
    public ObservableField<String> name;
    private ItemViewModelListener mListener;

    SelectedCodeItemViewModel(SystemCode code, ItemViewModelListener listener) {
        this.mCode = code;
        this.mListener = listener;

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(mCode.getSystemCodeName());

        if (mCode.getAssemblyCodeList() != null && mCode.getAssemblyCodeList().size() > 0)
            for (int i = 0; i < mCode.getAssemblyCodeList().size(); i++) {
                if (!mCode.getAssemblyCodeList().get(i).getAssemblyCodeName().equalsIgnoreCase("000")) {
                    stringBuilder.append("\n\n" + (i + 1) + ". " + mCode.getAssemblyCodeList().get(i).getAssemblyCodeName());

                    for (int j = 0; j < mCode.getAssemblyCodeList().get(i).getComponentCodeName().size(); j++) {
                        if (!mCode.getAssemblyCodeList().get(i).getComponentCodeName().get(j).equalsIgnoreCase("000"))
                            stringBuilder.append("\n -> " + mCode.getAssemblyCodeList().get(i).getComponentCodeName().get(j));
                    }
                }
            }

        name = new ObservableField<>(stringBuilder.toString());
    }

    public void onRemoveItemClick() {
        mListener.onRemoveItemClick(mCode.getCode1());
//        mListener.onRemoveItemClick(mCode.getPosition(), mCode.getSystemCode(), mCode.getAssemblyCode(), mCode.getComponentCode());
    }

    public interface ItemViewModelListener {
        void onRemoveItemClick(String systemCode);
    }
}
