package com.ttnfleetsolutions.ttnfleet.ui.chat;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ttnfleetsolutions.ttnfleet.R;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */

class ChatAdapter extends BaseAdapter {

    private final Context context;

    private String mReceiver;

    public ChatAdapter(Context context, String receiver) {
        this.context = context;
        this.mReceiver = receiver;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = null;
        ViewHolder1 holder1;
        ViewHolder2 holder2;

        switch (position) {
            case 0:
//                if (convertView == null) {
                    v = LayoutInflater.from(context).inflate(R.layout.chat_user2_item, null, false);

                    holder2 = new ViewHolder2();


                    holder2.messageTextView = v.findViewById(R.id.message_text);
                    holder2.timeTextView = v.findViewById(R.id.time_text);
//                    v.setTag(holder2);

//                } else {
//                    v = convertView;
//                    holder2 = (ViewHolder2) v.getTag();
//                }
                holder2.messageTextView.setText("When can I expect technician here?");
                break;
            case 1:
//                if (convertView == null) {
                    v = LayoutInflater.from(context).inflate(R.layout.chat_user1_item, null, false);
                    holder1 = new ViewHolder1();

                    holder1.messageTextView = v.findViewById(R.id.message_text);
                    holder1.timeTextView = v.findViewById(R.id.time_text);
                holder1.chat_company_reply_author = v.findViewById(R.id.chat_company_reply_author);
//                    v.setTag(holder1);
//                } else {
//                    v = convertView;
//                    holder1 = (ViewHolder1) v.getTag();
//
//                }
                if(!TextUtils.isEmpty(mReceiver)) {
                    holder1.chat_company_reply_author.setText(mReceiver);
                } else {
                    holder1.chat_company_reply_author.setText("Fleet Dispatcher");
                }
//                holder1.chat_company_reply_author.setText(((ChatActivity) context).getSupportActionBar().getTitle());
//                holder1.messageTextView.setText("Technician is on his way. He will be there in 30 mins.");
                holder1.messageTextView.setText("Ok");
                break;
            case 2:
//                if (convertView == null) {
                    v = LayoutInflater.from(context).inflate(R.layout.chat_user1_item, null, false);
                    holder1 = new ViewHolder1();


                    holder1.messageTextView = v.findViewById(R.id.message_text);
                    holder1.timeTextView = v.findViewById(R.id.time_text);
                holder1.chat_company_reply_author = v.findViewById(R.id.chat_company_reply_author);
//                    v.setTag(holder1);
//                } else {
//                    v = convertView;
//                    holder1 = (ViewHolder1) v.getTag();
//
//                }
//                holder1.chat_company_reply_author.setText(((ChatActivity) context).getSupportActionBar().getTitle());
                if(!TextUtils.isEmpty(mReceiver)) {
                    holder1.chat_company_reply_author.setText(mReceiver);
                } else {
                    holder1.chat_company_reply_author.setText("Fleet Dispatcher");
                }
                holder1.messageTextView.setText("There is some additional task to perform. The breaks are not working well.");
                break;
            /*case 3:
//                if (convertView == null) {
                    v = LayoutInflater.from(context).inflate(R.layout.chat_user1_item, null, false);
                    holder1 = new ViewHolder1();


                    holder1.messageTextView = (TextView) v.findViewById(R.id.message_text);
                    holder1.timeTextView = (TextView) v.findViewById(R.id.time_text);
                    holder1.chat_company_reply_author = (TextView) v.findViewById(R.id.chat_company_reply_author);
                    v.setTag(holder1);
//                } else {
//                    v = convertView;
//                    holder1 = (ViewHolder1) v.getTag();
//
//                }
                holder1.messageTextView.setText("Do ahead with it.");
                holder1.chat_company_reply_author.setText("Fleet Dispatcher");
                break;*/
            case 3:
//                if (convertView == null) {
                    v = LayoutInflater.from(context).inflate(R.layout.chat_user2_item, null, false);

                    holder2 = new ViewHolder2();


                    holder2.messageTextView = v.findViewById(R.id.message_text);
                    holder2.timeTextView = v.findViewById(R.id.time_text);
//                    v.setTag(holder2);
//
//                } else {
//                    v = convertView;
//                    holder2 = (ViewHolder2) v.getTag();
//
//                }
                holder2.messageTextView.setText("Ok");
                break;
        }

        /*if (message.getUserType() == UserType.SELF) {
            if (convertView == null) {
                v = LayoutInflater.from(context).inflate(R.layout.chat_user1_item, null, false);
                holder1 = new ViewHolder1();


                holder1.messageTextView = (TextView) v.findViewById(R.id.message_text);
                holder1.timeTextView = (TextView) v.findViewById(R.id.time_text);

                v.setTag(holder1);
            } else {
                v = convertView;
                holder1 = (ViewHolder1) v.getTag();

            }

            holder1.messageTextView.setText(Emoji.replaceEmoji(message.getMessageText(), holder1.messageTextView.getPaint().getFontMetricsInt(), AndroidUtilities.dp(16)));
            holder1.timeTextView.setText(SIMPLE_DATE_FORMAT.format(message.getMessageTime()));

        } else if (message.getUserType() == UserType.OTHER) {

            if (convertView == null) {
                v = LayoutInflater.from(context).inflate(R.layout.chat_user2_item, null, false);

                holder2 = new ViewHolder2();


                holder2.messageTextView = (TextView) v.findViewById(R.id.message_text);
                holder2.timeTextView = (TextView) v.findViewById(R.id.time_text);
                holder2.messageStatus = (ImageView) v.findViewById(R.id.user_reply_status);
                v.setTag(holder2);

            } else {
                v = convertView;
                holder2 = (ViewHolder2) v.getTag();

            }

            holder2.messageTextView.setText(Emoji.replaceEmoji(message.getMessageText(), holder2.messageTextView.getPaint().getFontMetricsInt(), AndroidUtilities.dp(16)));
            //holder2.messageTextView.setText(message.getMessageText());
            holder2.timeTextView.setText(SIMPLE_DATE_FORMAT.format(message.getMessageTime()));

            if (message.getMessageStatus() == Status.DELIVERED) {
                holder2.messageStatus.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_double_tick));
            } else if (message.getMessageStatus() == Status.SENT) {
                holder2.messageStatus.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_single_tick));

            }


        }*/


        return v;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    /*@Override
    public int getItemViewType(int position) {
        return message.getUserType().ordinal();
    }*/

    private class ViewHolder1 {
        public TextView messageTextView;
        public TextView timeTextView;
        public TextView chat_company_reply_author;

    }

    private class ViewHolder2 {
        public TextView messageTextView;
        public TextView timeTextView;

    }
}