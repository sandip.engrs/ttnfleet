package com.ttnfleetsolutions.ttnfleet.ui.technician.requirements;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Sandip Chaulagain on 6/22/2018.
 *
 * @version 1.0
 */
@Module
public abstract class RequirementsFragmentProvider {

    @ContributesAndroidInjector(modules = RequirementsFragmentModule.class)
    abstract RequirementsFragment provideRequirementsFragmentFactory();
}
