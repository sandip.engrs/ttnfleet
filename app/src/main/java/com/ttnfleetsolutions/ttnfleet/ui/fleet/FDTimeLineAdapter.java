package com.ttnfleetsolutions.ttnfleet.ui.fleet;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.vipulasri.timelineview.TimelineView;
import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.form.FeedbackDialogFragment;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.model.DateTimeUtils;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.model.OrderStatus;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.model.Orientation;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.model.TimeLineModel;
import com.ttnfleetsolutions.ttnfleet.ui.trucker.model.VectorDrawableUtils;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */
public class FDTimeLineAdapter extends RecyclerView.Adapter<TimeLineViewHolder> {

    private List<TimeLineModel> mFeedList;
    private Context mContext;
    private Orientation mOrientation;
    private boolean mWithLinePadding;
    private LayoutInflater mLayoutInflater;

    public FDTimeLineAdapter(List<TimeLineModel> feedList, Orientation orientation, boolean withLinePadding) {
        mFeedList = feedList;
        mOrientation = orientation;
        mWithLinePadding = withLinePadding;
    }

    public void setContext(Context context) {
        mContext = context;
    }

    public void addItems(List<TimeLineModel> feedList) {
        mFeedList = feedList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return TimelineView.getTimeLineViewType(position, getItemCount());
    }

    @Override
    public TimeLineViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        mLayoutInflater = LayoutInflater.from(mContext);
        View view;

        view = mLayoutInflater.inflate(R.layout.row_timeline, parent, false);

        return new TimeLineViewHolder(view, viewType);
    }

    @Override
    public void onBindViewHolder(final TimeLineViewHolder holder, int position) {

        TimeLineModel timeLineModel = mFeedList.get(position);
        switch (position) {
            case 0:
                holder.button.setVisibility(View.INVISIBLE);
                holder.button.setText(R.string.dispatch);
                break;
            case 1:
                holder.button.setVisibility(View.INVISIBLE);
                break;
            case 2:
                holder.button.setVisibility(View.INVISIBLE);
                break;
            case 3:
                holder.button.setVisibility(View.INVISIBLE);
                holder.button.setText(R.string.track_location);
                break;
            case 4:
                holder.button.setVisibility(View.INVISIBLE);
                holder.button.setText(R.string.confirm);
                break;
            case 5:
            case 6:
                holder.button.setVisibility(View.INVISIBLE);
                break;
            case 7:
                holder.button.setVisibility(View.INVISIBLE);
                holder.button.setText(R.string.approve);
                break;
            case 11:
                holder.button.setVisibility(View.INVISIBLE);
                holder.button.setText(R.string.complete);
                break;
        }

        if (timeLineModel.getStatus() == OrderStatus.INACTIVE) {
            holder.mDate.setVisibility(View.GONE);
            holder.mTimelineView.setMarker(VectorDrawableUtils.getDrawable(mContext, R.drawable.ic_marker_inactive, android.R.color.darker_gray));
            holder.button.setEnabled(false);
            holder.mMessage.setTextColor(ContextCompat.getColor(mContext, android.R.color.darker_gray));
            holder.button.setBackgroundColor(ContextCompat.getColor(mContext, android.R.color.darker_gray));
//            holder.button.setVisibility(View.VISIBLE);
            holder.button.setVisibility(View.INVISIBLE);
        } else if (timeLineModel.getStatus() == OrderStatus.ACTIVE) {
            holder.mTimelineView.setMarker(VectorDrawableUtils.getDrawable(mContext, R.drawable.ic_marker_active, R.color.colorPrimary));
//            holder.button.setEnabled(true);
            holder.mMessage.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
            holder.button.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
//            holder.button.setVisibility(View.VISIBLE);
        } else {
            holder.mMessage.setTextColor(ContextCompat.getColor(mContext, R.color.gray_darker));
            holder.button.setVisibility(View.INVISIBLE);
            holder.mTimelineView.setMarker(ContextCompat.getDrawable(mContext, R.drawable.ic_marker), ContextCompat.getColor(mContext, R.color.colorPrimary));
//            holder.button.setBackgroundColor(ContextCompat.getColor(mContext, android.R.color.darker_gray));
//            holder.button.setEnabled(false);
        }
        if (!timeLineModel.getDate().isEmpty()) {
//            holder.mDate.setVisibility(View.VISIBLE);
            holder.mDate.setText(DateTimeUtils.parseDateTime(timeLineModel.getDate()));
        } else
            holder.mDate.setVisibility(View.GONE);


        holder.button.setTag(position);

        holder.button.setOnClickListener(view -> {
            switch ((int) view.getTag()) {
                case 3:
//                    mContext.startActivity(new Intent(mContext, TrackLocationActivity.class));
                    ((DashboardActivity) mContext).trackLocation(0);
                    break;
                case 4:
                    holder.mMessage.setText("Work under-progress");
                    holder.button.setVisibility(View.INVISIBLE);
                    /*AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    builder.setMessage("Please confirm technician arrival");
                    builder.setCancelable(false);
                    builder.setPositiveButton("CONFIRM", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            holder.mMessage.setText("Work under-progress");
                            holder.button.setVisibility(View.INVISIBLE);
                            dialogInterface.dismiss();
                        }
                    });
                    builder.setNegativeButton("CANCEL", null);
                    builder.show();*/
                    break;
                case 7:
                    if (holder.button.getText().toString().equalsIgnoreCase("FEEDBACK")) {
                        FeedbackDialogFragment fragment = new FeedbackDialogFragment();
                        fragment.show(((AppCompatActivity) mContext).getSupportFragmentManager(), "feedback");
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                        builder.setMessage("Details verified. Complete Service?");
                        builder.setCancelable(false);
                        builder.setPositiveButton("COMPLETE", (dialogInterface, i) -> {
                            holder.mMessage.setText("Service Completed");
                            holder.button.setText(R.string.feedback);
                            dialogInterface.dismiss();
                        });
                        builder.setNegativeButton("CANCEL", null);
                        builder.show();
                    }
                    break;
            }
        });

        if(mFeedList.size() == 4 && position == 3) {
            holder.button.setVisibility(View.VISIBLE);
            holder.button.setText(R.string.track_location);
            holder.button.setOnClickListener(view -> ((DashboardActivity) mContext).trackLocation(position));
        }

        if(mFeedList.size() == 1) {
            holder.button.setVisibility(View.VISIBLE);
            holder.button.setOnClickListener(view -> ((DashboardActivity) mContext).dispatchService(position, true));
        }

        holder.mMessage.setText(timeLineModel.getMessage());
        if (timeLineModel.getMessage().equalsIgnoreCase("Service Completed")) {
            holder.button.setText(R.string.feedback);
        }
    }

    @Override
    public int getItemCount() {
        return (mFeedList != null ? mFeedList.size() : 0);
    }

}
