package com.ttnfleetsolutions.ttnfleet.ui.trucker.history;

import com.ttnfleetsolutions.ttnfleet.data.DataManager;
import com.ttnfleetsolutions.ttnfleet.data.model.api.event.listing.Event;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseViewModel;
import com.ttnfleetsolutions.ttnfleet.utils.rx.SchedulerProvider;

import java.util.List;

/**
 * Created by Sandip Chaulagain on 11/10/2017.
 *
 * @version 1.0
 */

public class ServiceHistoryViewModel extends BaseViewModel<ServiceHistoryNavigator> {

    public ServiceHistoryViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    private List<Event> mEventList;

    public List<Event> getEventList() {
        return mEventList;
    }

    public void setEventList(List<Event> mEventList) {
        this.mEventList = mEventList;
    }

    void getClosedEventsTrucker() {
//        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .doServerGetClosedEventsApiCall(getDataManager().getCurrentUserID())
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    setEventList(response.getResult());
                    getNavigator().getClosedEvents(response.getResult());
//                    setIsLoading(false);
                }, throwable -> {
//                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                }));
    }

    void getClosedEventsFD() {
//        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .doServerEventListingFleetDispatcherApiCall(getDataManager().getCurrentUserID(), DataManager.FilterDashboardHistory.HISTORY_FD.getType())
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    setEventList(response.getResult());
                    getNavigator().getClosedEvents(response.getResult());
//                    setIsLoading(false);
                }, throwable -> {
//                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                }));
    }

    public void getClosedEventsSP(int statusType) {
//        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .doServerEventListingServiceProviderApiCall(getDataManager().getCurrentUserID(), statusType)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    setEventList(response.getResult());
                    getNavigator().getClosedEvents(response.getResult());
//                    setIsLoading(false);
                }, throwable -> {
//                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                }));
    }

    public void getClosedEventsTech(int statusType) {
//        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .doServerEventListingTechnicianApiCall(getDataManager().getCurrentUserID(), statusType)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    setEventList(response.getResult());
                    getNavigator().getClosedEvents(response.getResult());
//                    setIsLoading(false);
                }, throwable -> {
//                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                }));
    }
}
