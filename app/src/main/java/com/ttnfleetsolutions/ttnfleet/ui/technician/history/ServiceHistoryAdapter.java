package com.ttnfleetsolutions.ttnfleet.ui.technician.history;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ttnfleetsolutions.ttnfleet.R;
import com.ttnfleetsolutions.ttnfleet.ui.base.BaseActivity;
import com.ttnfleetsolutions.ttnfleet.ui.technician.TechnicianDashboardActivity;

/**
 * Created by Sandip Chaulagain on 10/5/2017.
 *
 * @version 1.0
 */

public class ServiceHistoryAdapter extends RecyclerView.Adapter<ServiceHistoryAdapter.MyViewHolder> {

    private Context mContext;
    private String[] times, texts, services_issue, locations, models, numbers;

    Integer selectedItemPosition = null;

    public ServiceHistoryAdapter(Context context) {
        this.mContext = context;
        times = mContext.getResources().getStringArray(R.array.services_date);
        texts = mContext.getResources().getStringArray(R.array.services);
        services_issue = mContext.getResources().getStringArray(R.array.services_issue);
        locations = mContext.getResources().getStringArray(R.array.locations);
        models = mContext.getResources().getStringArray(R.array.truck_array);
        numbers = mContext.getResources().getStringArray(R.array.vin_number);

        if (((BaseActivity) (mContext)).isLandscape()) {
            selectedItemPosition = 0;
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_job_request_new, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        if (selectedItemPosition != null && selectedItemPosition == position) {
            // Here I am just highlighting the background
            holder.rowLayout.setBackgroundColor(mContext.getResources().getColor(R.color.tr_gray));
        } else {
            holder.rowLayout.setBackgroundColor(Color.TRANSPARENT);
        }

        if (position == 0) {
            holder.status.setText(R.string.completed);
            holder.status.setTextColor(ContextCompat.getColor(mContext, android.R.color.holo_blue_dark));
            holder.coloredLine.setBackgroundColor(ContextCompat.getColor(mContext, android.R.color.holo_blue_dark));
            Drawable img = mContext.getResources().getDrawable(R.drawable.ic_done_all);
            img.setTint(ContextCompat.getColor(mContext, android.R.color.holo_blue_dark));
            holder.status.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
//            holder.framelayout.setOnClickListener(view -> mContext.startActivity(new Intent(mContext, JobCardDetailsActivity.class).putExtra("type", 2)));
            holder.jobCardNo.setBackgroundTintList(mContext.getResources().getColorStateList(android.R.color.holo_blue_dark));
        } else if (position == 0) {
            holder.status.setText(R.string.new_job);
            holder.status.setTextColor(ContextCompat.getColor(mContext, android.R.color.holo_orange_dark));
            holder.coloredLine.setBackgroundColor(ContextCompat.getColor(mContext, android.R.color.holo_orange_dark));
            Drawable img = mContext.getResources().getDrawable(R.drawable.circle);
            img.setTint(ContextCompat.getColor(mContext, android.R.color.holo_orange_dark));
            holder.status.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
        } else {
            holder.status.setText(R.string.ongoing);
            holder.status.setTextColor(ContextCompat.getColor(mContext, android.R.color.holo_green_light));
            holder.coloredLine.setBackgroundColor(ContextCompat.getColor(mContext, android.R.color.holo_green_light));
            Drawable img = mContext.getResources().getDrawable(R.drawable.circle);
            img.setTint(ContextCompat.getColor(mContext, android.R.color.holo_green_light));
            holder.status.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);

//            holder.framelayout.setOnClickListener(view -> mContext.startActivity(new Intent(mContext, JobCardDetailsActivity.class).putExtra("type", 2)));
        }
        holder.rowLayout.setOnClickListener(view -> {
            if (selectedItemPosition != null) {
                notifyItemChanged(selectedItemPosition);
            }
            selectedItemPosition = position;
            notifyItemChanged(selectedItemPosition);

            ((TechnicianDashboardActivity) mContext).showOrderDetails(-1);
        });
        holder.bottomLayout.setVisibility(View.GONE);
//        holder.date.setText(times[position]);
//        holder.issue.setText(services_issue[position]);
//        holder.requestNo.setText(texts[position]);
//        holder.locations.setText("Address: " + locations[position]);
//        holder.model.setText(models[position]);
//        holder.number.setText(numbers[position]);
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView status, datetime, serviceIssue, location, vehicleName, vinNo, arrivalRepairTimingInfo, jobCardNo;
        public View coloredLine;
        public ConstraintLayout rowLayout;
        public RelativeLayout bottomLayout;
        public Button mAcceptButton, mRejectButton, mAdditionalJobButton, mCompleteJobButton;

        public MyViewHolder(View view) {
            super(view);
            rowLayout = view.findViewById(R.id.rowLayout);
            bottomLayout = view.findViewById(R.id.bottomLayout);
            coloredLine = view.findViewById(R.id.coloredLine);
            status = view.findViewById(R.id.status);
            datetime = view.findViewById(R.id.datetime);
            serviceIssue = view.findViewById(R.id.serviceIssue);
            location = view.findViewById(R.id.location);
            vehicleName = view.findViewById(R.id.vehicleName);
            vinNo = view.findViewById(R.id.vinNo);
            arrivalRepairTimingInfo = view.findViewById(R.id.arrivalRepairTimingInfo);
            jobCardNo = view.findViewById(R.id.jobCardNo);

            mAcceptButton = view.findViewById(R.id.acceptButton);
            mRejectButton = view.findViewById(R.id.rejectButton);
            mAdditionalJobButton = view.findViewById(R.id.additionalJob);
            mCompleteJobButton = view.findViewById(R.id.completeJob);
        }
    }
}
