package com.ttnfleetsolutions.ttnfleet;

import android.app.Activity;
import android.app.Application;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.interceptors.HttpLoggingInterceptor;
import com.ttnfleetsolutions.ttnfleet.di.component.DaggerAppComponent;

import javax.inject.Inject;

import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;

/**
 * Created by Sandip Chaulagain on 10/3/2017.
 *
 * @version 1.0
 */

public class MyApplication extends Application implements HasActivityInjector {

    @Inject
    DispatchingAndroidInjector<Activity> activityDispatchingAndroidInjector;

    private static final String TAG = MyApplication.class.getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();

        DaggerAppComponent.builder()
                .application(this)
                .build()
                .inject(this);

        if(BuildConfig.DEBUG) {

            AndroidNetworking.initialize(getApplicationContext());
            AndroidNetworking.enableLogging(HttpLoggingInterceptor.Level.BODY);
            AndroidNetworking.setConnectionQualityChangeListener((currentConnectionQuality, currentBandwidth) -> Log.e(TAG, "onChange: currentConnectionQuality : " + currentConnectionQuality + " currentBandwidth : " + currentBandwidth));
        }
    }

    @Override
    public DispatchingAndroidInjector<Activity> activityInjector() {
        return activityDispatchingAndroidInjector;
    }
}