package com.ttnfleetsolutions.ttnfleet.utils;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;
/**
 * Created by Sandip Chaulagain on 5/23/2018.
 *
 * @version 1.0
 */

@GlideModule
public final class MyAppGlideModule extends AppGlideModule {}